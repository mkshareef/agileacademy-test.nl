ALTER TABLE  `wp_ev_event_date` ADD  `threshold` INT( 25 ) NOT NULL ;

CREATE TABLE IF NOT EXISTS `wp_ev_event_know_how` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eventID` int(11) NOT NULL,
  `know_how` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

