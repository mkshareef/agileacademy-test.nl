<?php
/* Template Name: PDF

This template is used to render the invoice as PDF
*/
$plugin_dir = ABSPATH . 'wp-content/plugins';

require_once $plugin_dir.'/pw-event-manager/includes/modelview/modelview-invoice.php';

$invoice = new invoiceModelView();
$invoice->create_pdf();

?>