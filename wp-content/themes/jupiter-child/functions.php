<?php
/* Custom functions code goes here. */

/*
function my_title($title, $separator, $position)
{

    global $pw_config;

    require_once('wp-content/plugins/pw-event-manager/includes/models/model-event.php');
    $event_mdl = new modelEvent();

    $url_slug = get_query_var('eventtitle');
    $lang = strtolower($pw_config['lang_primary']);

    $event = $event_mdl->get_by_slug($url_slug, $lang);

    if (is_page('Event') || is_page('Training') || is_page('Workshop')) { // Checks if page is Event, Training or Workshop page
        remove_action('wp_head', 'jetpack_og_tags'); // JetPack

        if (defined('WPSEO_VERSION')) { // Yoast SEO
            global $wpseo_front;
            remove_action('wp_head', array($wpseo_front, 'head'), 1);
        }

        if (defined('AIOSEOP_VERSION')) { // All-In-One SEO
            global $aiosp;
            remove_action('wp_head', array($aiosp, 'wp_head'));
        }
        remove_action('wp_head', 'rel_canonical');
        remove_action('wp_head', 'index_rel_link');
        remove_action('wp_head', 'start_post_rel_link');
        remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');
        //add_action('wp_head', 'my_head', 0);

        // do whatever you need to $title here
        $event_name = $event['html_title'] == '' ? $event['name'] : $event['html_title'];
        $title = $event_name; // . " - " . get_the_title() . " - " . get_bloginfo('name');
    }

    return $title;
}
*/
//add_filter('wp_title', 'my_title', 20, 3);

?>