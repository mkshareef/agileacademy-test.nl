<?php
/**
 * The main plugin file
 *
 * @package PW Event Manager
 *
 */

/*
Plugin Name: PW Event Manager
Description: This plugin implements a fully functional event manager for Prowareness.
Version: 0.1
Text Domain: pw-event-manager
Domain Path: /languages
*/

require_once( 'includes/pw-event-ctrl.php' );

?>