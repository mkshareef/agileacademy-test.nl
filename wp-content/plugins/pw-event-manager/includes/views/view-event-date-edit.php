<?php

if( !class_exists( 'eventDateEditHtmlView' ) ):


    class eventDateEditHtmlView
    {

        public static function render( $title, $data )
        {
            require_once(plugin_dir_path( plugin_dir_path( __FILE__ )).'/models/model-location.php');
            $mdlLocation = new modelLocation();

            // Locations for the language the event is writen for
            if(isset($data['locale']) && $data['locale'] <> "" ) {
                $locations = $mdlLocation->get_all_by_locale( $data['locale'] );
            }

            $errors = array();
            if( isset($data['errors']) && is_array($data['errors']) ) {
                $errors = $data['errors'];
            }

            // Admin interface language
            global $pw_config;
            $locale = substr(get_locale(), 0, 2 );
            $date_time_format = $pw_config['date_format'][$locale].' H:i:s';
            $date_format = $pw_config['date_format'][$locale];
            $time_format = 'H:i';

            if( strpos($date_format, '-') ) {
                $date_help = str_replace('d','dd', $date_format );
                $date_help = str_replace('m','mm', $date_help );
                $date_help = str_replace('Y','jjjj', $date_help );
                $time_help = 'hh:mm';
            } else if( strpos($date_format, '/') ) {
                $date_help = str_replace('d','dd', $date_format );
                $date_help = str_replace('m','mm', $date_help );
                $date_help = str_replace('Y','yyyy', $date_help );
                $time_help = 'hh:mm';
            } else if( strpos($date_format, '.') ) {
                $date_help = str_replace('d','tt', $date_format );
                $date_help = str_replace('m','mm', $date_help );
                $date_help = str_replace('Y','jjjj', $date_help );
                $time_help = 'ss:mm';
            }

            if( $locale == 'nl' ) {
                $dec_point = ',';
                $thousands_sep = '.';
            } else if( $locale == 'de' ) {
                $dec_point = ',';
                $thousands_sep = '.';
            } else {
                $dec_point = '.';
                $thousands_sep = ',';
            }

            // Prepare data
            $data['updated_by'] = stripslashes( $data['updated_by'] );
            $data['event_name'] = stripslashes( $data['event_name'] );
            $data['start_date'] = stripslashes( $data['start_date'] );
            $data['start_time'] = stripslashes( $data['start_time'] );
            $data['stop_time'] = stripslashes( $data['stop_time'] );
            $data['other_dates'] = stripslashes( $data['other_dates'] );
            $data['alt_price_early_bird'] = stripslashes( $data['alt_price_early_bird'] );
            $data['alt_price_normal'] = stripslashes( $data['alt_price_normal'] );
            $data['alt_price_last_minute'] = stripslashes( $data['alt_price_last_minute'] );

            ?>

            <div class="wrap">
                <h2>
                    <?php echo $title .' '.__('for', 'pw-event-manager').' '.$data['event_name'] ?>
                </h2>
                <p class="description"><?php
                    _e('Last update:', 'pw-event-manager');
                    echo ' '.date( $date_time_format ,strtotime( $data['updated'] )).' ';
                    _e('by', 'pw-event-manager');
                    echo ' '.$data['updated_by']
                    ?></p>
                <?php
                if( ! empty($errors) ) {
                    echo('<p class="error">');
                    _e('The form contains errors', 'pw-event-manager');
                    echo('</p>');
                };
                ?>
                <form method="POST" name="formTrainerEdit" action="<?php $_SERVER['REQUEST_URI'] ?>" enctype="multipart/form-data">
                    <table class="form-table" style="max-width: 950px;">
                        <?php
                            $dt = '';
                            if( $data['start_date'] <> '' ) {
                                $dt = date( $date_format ,strtotime( $data['start_date'] ));
                            }
                        ?>
                        <tr>
                            <th><label for="start_date"><?php  _e('Start date', 'pw-event-manager') ?> * </label></th>
                            <td><input type="text"  class="medium-text" name="start_date" value="<?php echo $dt; ?>">
                                <i><?php  echo $date_help ?></i>
                                <?php

                                if( isset( $errors['start_date']) ) {
                                    echo( '<p class="error">'.implode('<br>', $errors['start_date']).'</p>' );
                                }

                                ?>

                            </td>
                        </tr>
                        <?php
                            $tm = '';
                            if( $data['start_time'] <> '' ) {
                                $tm = date( $time_format ,strtotime( $data['start_time'] ));
                            }
                        ?>
                        <tr>
                            <th><label for="start_time"><?php  _e('Start time', 'pw-event-manager') ?> * </label></th>
                            <td><input type="text"  class="medium-text" name="start_time" value="<?php echo $tm; ?>">
                                <i><?php  echo $time_help ?></i>
                                <?php

                                if( isset( $errors['start_time']) ) {
                                    echo( '<p class="error">'.implode('<br>', $errors['start_time']).'</p>' );
                                }

                                ?>
                            </td>
                        </tr>
                        <?php
                        $tm = '';
                        if( $data['stop_time'] <> '' ) {
                            $tm = date( $time_format ,strtotime( $data['stop_time'] ));
                        }
                        ?>
                        <tr>
                            <th><label for="stop_time"><?php  _e('Stop time', 'pw-event-manager') ?> * </label></th>
                            <td><input type="text"  class="medium-text" name="stop_time" value="<?php echo $tm; ?>">
                                <i><?php  echo $time_help ?></i>
                                <?php

                                if( isset( $errors['stop_time']) ) {
                                    echo( '<p class="error">'.implode('<br>', $errors['stop_time']).'</p>' );
                                }

                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="event_threshold"><?php  _e('No Of Participants', 'pw-event-manager') ?> *</label></th>
                            <td><input type="text"  class="regular-text" name="event_threshold" placeholder="<?php _e('No Of Participants', 'pw-event-manager') ?>"
                                       value="<?php echo $data['event_threshold']; ?>">
                                <?php

                                if( isset( $errors['event_threshold']) ) {
                                    echo( '<p class="error">'.implode('<br>', $errors['event_threshold']).'</p>' );
                                }

                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="locale"><?php  _e('Location', 'pw-event-manager') ?> *</label></th>
                            <td>
                                <select name="date_location" id="date_location">

                                    <option value="0"><?php  _e('- Choose a location -', 'pw-event-manager') ?></option>

                                    <?php
                                    foreach( $locations as $key=>$location ) {

                                        if( $data['locationID'] > 0 ) {
                                            if( $data['locationID'] == $location['id'] ) {
                                                echo '<option value="'.$location['id'].'" selected>'.stripslashes($location['location_name']).'</option>';
                                            } else {
                                                echo '<option value="'.$location['id'].'">'.stripslashes( $location['location_name'] ).'</option>';
                                            }
                                        } else {
                                            echo '<option value="'.$location['id'].'">'.stripslashes( $location['location_name'] ).'</option>';
                                        }
                                    }

                                    ?>

                                </select>
                                <?php
                                if( isset( $errors['date_location']) ) {
                                    echo( '<p class="error">'.implode('<br>', $errors['date_location']).'</p>' );
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="locale"><?php  _e('Subscribe', 'pw-event-manager') ?></label></th>
                            <td>
                                <select name="status" id="date_status">
                                    <?php
                                        $selected = isset($data['status']) && $data['status'] == "open" ? 'selected' : "";
                                        echo '<option value="open" '.$selected.'>'.__('Open for subscription', 'pw-event-manager').'</option>';

                                        $selected = isset($data['status']) && $data['status'] == "full" ? 'selected' : "";
                                        echo '<option value="full" '.$selected.'>'.__('Full, subscription not possible', 'pw-event-manager').'</option>';

                                        $selected = isset($data['status']) && $data['status'] == "closed" ? 'selected' : "";
                                        echo '<option value="closed" '.$selected.'>'.__('Closed for subscription', 'pw-event-manager').'</option>';
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <?php
                            $other_dates = "";
                            if( $data['other_dates'] <> '' ) {
                                $dtArray = explode( ',', $data['other_dates'] );

                                foreach( $dtArray as $key => $value ) {
                                    $dt = date( $date_format ,strtotime( $value ));
                                    $dtArray[$key] = $dt;
                                }
                                $other_dates = implode(', ', $dtArray);
                            }
                        ?>
                        <tr>
                            <th><label for="other_dates"><?php  _e('Other dates', 'pw-event-manager') ?></label></th>
                            <td>
                                <input type="text" class="regular-text" name="other_dates" placeholder="<?php _e('Other dates for this event', 'pw-event-manager') ?>"
                                       value="<?php echo $other_dates; ?>"> <i><?php  echo $date_help ?></i>
                                <p class="description"><?php  _e('Separate multiple dates with a comma.', 'pw-event-manager') ?></p>
                        </tr>
                        <?php
                            $price = '';
                            if( $data['alt_price_early_bird'] <> '' ) {
                                $price = number_format( $data['alt_price_early_bird'], 2, $dec_point, $thousands_sep);
                            }
                        ?>
                        <tr>
                            <th><label for="alt_price_early_bird"><?php  _e('Special early bird price', 'pw-event-manager') ?></label></th>
                            <td>
                                &euro;
                                <input type="text"  class="medium-text" name="alt_price_early_bird" placeholder="0,00"
                                       value="<?php echo $price; ?>">
                                <p class="description"><?php  _e('Valid until 61 days before the training date. Set to 0 when not applicable.', 'pw-event-manager') ?></p>
                            </td>
                        </tr>
                        <?php
                            $price = '';
                            if( $data['alt_price_normal'] <> '' ) {
                                $price = number_format( $data['alt_price_normal'], 2, $dec_point, $thousands_sep);
                            }
                        ?>
                        <tr>
                            <th><label for="alt_price_normal"><?php  _e('Special normal price', 'pw-event-manager') ?></label></th>
                            <td>
                                &euro;
                                <input type="text"  class="medium-text" name="alt_price_normal" placeholder="0,00"
                                       value="<?php echo $price; ?>">
                                <p class="description"><?php  _e('Between 61 and 21 days before training date. Set to 0 when not applicable.', 'pw-event-manager') ?></p>
                            </td>
                        </tr>
                        <?php
                            $price = '';
                            if( $data['alt_price_last_minute'] <> '' ) {
                                $price = number_format( $data['alt_price_last_minute'], 2, $dec_point, $thousands_sep);
                            }
                        ?>
                        <tr>
                            <th><label for="alt_price_last_minute"><?php  _e('Special last minute price', 'pw-event-manager') ?></label></th>
                            <td>
                                &euro;
                                <input type="text"  class="medium-text" name="alt_price_last_minute" placeholder="0,00"
                                       value="<?php echo $price; ?>">
                                <p class="description"><?php  _e('Starts 21 days before the training date. Set to 0 when not applicable.', 'pw-event-manager') ?></p>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" class="button button-primary" name="submitEventDate" value="<?php  _e('Save', 'pw-event-manager') ?>">
                                <input type="submit" class="button button-primary-2" name="submitAndCloseEventDate" value="<?php  _e('Save & close', 'pw-event-manager') ?>">
                                <input type="submit" class="button button-secondary" name="closeEventDate" value="<?php  _e('Cancel', 'pw-event-manager') ?>">
                            </td>
                        </tr>
                    </table>

                </form>
            </div>
        <?php
        }
    }

endif;

?>