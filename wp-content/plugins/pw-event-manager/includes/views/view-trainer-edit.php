<?php

if( !class_exists( 'trainerEditHtmlView' ) ):

    class trainerEditHtmlView
    {
        public static function render( $title, $data )
        {
            global $pw_config;

            $locale = substr(get_locale(), 0, 2 );
            $date_format = $pw_config['date_format'][$locale].' H:i:s';

            $errors = array();
            if( isset($data['errors']) && is_array($data['errors']) ) {
                $errors = $data['errors'];
            }

            // Prepare data
            $data['updated_by'] = stripslashes($data['updated_by']);
            $data['locale'] = stripslashes($data['locale']);
            $data['name'] = stripslashes($data['name']);
            $data['title'] = stripslashes($data['title']);
            $data['company'] = stripslashes($data['company']);
            $data['profile'] = stripslashes($data['profile']);
            ?>

            <div class="wrap">
                <h2>
                    <?php echo( $title ) ?>
                </h2>
                <p class="description"><?php
                    _e('Last update:', 'pw-event-manager');
                    echo ' '.date( $date_format ,strtotime( $data['updated'] )).' ';
                    _e('by', 'pw-event-manager');
                    echo ' '.$data['updated_by']
                    ?></p>
                <?php
                    if( ! empty($errors) ) {
                        echo('<p class="error">');
                        _e('The form contains errors', 'pw-event-manager');
                        echo('</p>');
                    };
                ?>
                <form method="POST" name="formTrainerEdit" action="<?php $_SERVER['REQUEST_URI'] ?>" enctype="multipart/form-data">

                    <table class="form-table">
                        <tr>
                            <th><label for="locale"><?php  _e('Display language', 'pw-event-manager') ?></label></th>
                            <td>
                                <input type="radio" name="locale" value="NL"
                                    <?php echo ($data['locale']=='NL')?'checked':'' ?>
                                    >NL
                                <input type="radio" name="locale" value="EN"
                                    <?php echo ($data['locale']=='EN')?'checked':'' ?>
                                    >EN
                                <input type="radio" name="locale" value="DE"
                                    <?php echo ($data['locale']=='DE')?'checked':'' ?>
                                    >DE
                            </td>
                        </tr>
                        <tr>
                            <th><label for="name"><?php  _e('Name', 'pw-event-manager') ?> * </label></th>
                            <td><input type="text"  class="regular-text" name="name" placeholder="<?php _e('Name of trainer', 'pw-event-manager') ?>"
                                       value="<?php echo $data['name']; ?>">
                                <?php

                                if( isset( $errors['name']) ) {
                                    echo( '<p class="error">'.implode('<br>', $errors['name']).'</p>' );
                                }

                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="title"><?php  _e('Function title', 'pw-event-manager') ?></label></th>
                            <td><input type="text"  class="regular-text" name="title" placeholder="<?php _e('Function title of trainer', 'pw-event-manager') ?>"
                                       value="<?php echo $data['title']; ?>"></td>
                        </tr>
                        <tr>
                            <th><label for="company"><?php  _e('Company', 'pw-event-manager') ?></label></th>
                            <td><input type="text"  class="regular-text" name="company" placeholder="<?php _e('Company of trainer', 'pw-event-manager') ?>"
                                       value="<?php echo $data['company']; ?>"></td>
                        </tr>
                        <tr>
                            <th><label for="picture"><?php  _e('Picture', 'pw-event-manager') ?></label></th>
                            <td><input type="hidden" name="attachment_path" value="<?php echo $data['attachment_path']; ?>">
                                <?php
                                $imgFound = false;

                                if( $data['attachment_path'] <> "" ) {
                                    $img = "";
                                    $img_src = util::get_image_src( $data['attachment_path'] );
                                    if( $img_src ) {
                                        $img = '<br><img src="'.$img_src.'" width="125">';
                                        $imgFound = true;
                                        echo($img);
                                        echo('&nbsp;&nbsp;&nbsp;<input type="submit" class="button button-secondary" name="removePictureTrainer" value="');
                                        _e('Delete', 'pw-event-manager');
                                        echo('">');
                                    }
                                }

                                if( $imgFound == false ) {
                                    echo('<input type="file" name="trainer_image" id="trainer_image" />');
                                    echo('<p class="description">');
                                    _e('The image should be 125px * 125px', 'pw-event-manager');
                                    echo('.</p>');

                                    if( isset( $errors['image']) ) {
                                        echo( '<p class="error">'.implode('<br>', $errors['image']).'</p>' );
                                    }
                                }

                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th><?php  _e('Profile', 'pw-event-manager') ?> *</th>
                            <td><?php

                                $editor_id = "profile";
                                $content = $data['profile'];
                                $settings = array( 'media_buttons' => false, 'textarea_rows' => 7 );

                                if( isset( $errors['profile']) ) {
                                    echo( '<p class="error">'.implode('<br>', $errors['profile']).'</p>' );
                                }

                                wp_editor( $content, $editor_id, $settings );

                            ?></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" class="button button-primary" name="submitTrainer" value="<?php  _e('Save', 'pw-event-manager') ?>">
                                <input type="submit" class="button button-primary-2" name="submitAndCloseTrainer" value="<?php  _e('Save & close', 'pw-event-manager') ?>">
                                <input type="submit" class="button button-secondary" name="closeTrainer" value="<?php  _e('Cancel', 'pw-event-manager') ?>">
                            </td>
                        </tr>
                    </table>

                </form>
            </div>

        <?php
        }
    }

endif;

?>