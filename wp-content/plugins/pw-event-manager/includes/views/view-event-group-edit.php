<?php

if( !class_exists( 'eventGroupEditHtmlView' ) ):

    class eventGroupEditHtmlView
    {

        public static function render( $title, $data )
        {
            global $pw_config;

            $locale = substr(get_locale(), 0, 2 );
            $date_format = $pw_config['date_format'][$locale].' H:i:s';

            $errors = array();
            if( isset($data['errors']) && is_array($data['errors']) ) {
                $errors = $data['errors'];
            }

            //Prepare the data
            $title = stripslashes( $title );
            $data['updated_by'] = stripslashes( $data['updated_by'] );
            $data['group_name'] = stripslashes( $data['group_name'] );
            ?>

            <div class="wrap">
                <h2>
                    <?php echo( $title ) ?>
                </h2>
                <p class="description"><?php
                    echo __('Last update:', 'pw-event-manager').' '.date( $date_format ,strtotime( $data['updated'] )).' '.
                        __('by', 'pw-event-manager').' '.$data['updated_by'];
                    ?></p>
                <?php
                    if( ! empty($errors) ) {
                        echo('<p class="error">');
                        _e('The form contains errors', 'pw-event-manager');
                        echo('</p>');
                    };
                ?>
                <form method="POST" name="formEventGroupEdit" action="<?php $_SERVER['REQUEST_URI'] ?>" enctype="multipart/form-data">
                    <input type="hidden" id="track-changed" value="">
                    <table class="form-table">
                        <tr>
                            <th><label for="locale"><?php  _e('Display language', 'pw-event-manager') ?></label></th>
                            <td>
                                <input type="radio" name="locale" value="NL"
                                    <?php echo ($data['locale']=='NL')?'checked':'' ?>
                                    >NL
                                <input type="radio" name="locale" value="EN"
                                    <?php echo ($data['locale']=='EN')?'checked':'' ?>
                                    >EN
                                <input type="radio" name="locale" value="DE"
                                    <?php echo ($data['locale']=='DE')?'checked':'' ?>
                                    >DE
                            </td>
                        </tr>
                        <tr>
                            <th><label for="group_name"><?php  _e('Group name', 'pw-event-manager') ?> * </label></th>
                            <td><input type="text"  class="regular-text" name="group_name" placeholder="<?php _e('Name of the group', 'pw-event-manager') ?>"
                                       value="<?php echo $data['group_name']; ?>">
                                <?php

                                if( isset( $errors['group_name']) ) {
                                    echo( '<p class="error">'.implode('<br>', $errors['group_name']).'</p>' );
                                }

                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" class="button button-primary" name="submitEventGroup" value="<?php  _e('Save', 'pw-event-manager') ?>">
                                <input type="submit" class="button button-primary-2" name="submitAndCloseEventGroup" value="<?php  _e('Save & close', 'pw-event-manager') ?>">
                                <input type="submit" class="button button-secondary" name="closeEventGroup" value="<?php  _e('Cancel', 'pw-event-manager') ?>">
                            </td>
                        </tr>
                    </table>

                </form>
            </div>

        <?php
        }
    }

endif;

?>