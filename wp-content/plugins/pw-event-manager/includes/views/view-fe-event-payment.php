<?php

/**
 * The page content view
 *
 * @package PA Notebook
 * @subpackage Content view
 * @since 0.1
 */

if( !class_exists( 'feEventPaymentHtmlView' ) ):

    class feEventPaymentHtmlView
    {

        public static function render( $data )
        {
            $html = "";

            $html .= "
                <!DOCTYPE html>
                <html>
                <head>
                <title>".$data['title']."</title>
                <script type='text/javascript' src='/wp-includes/js/jquery/jquery.js'></script>
                <link rel='stylesheet' href='".get_stylesheet_directory_uri()."/style.css' type='text/css' media='all' />
                </head>
                <body>
            ";

            $html .= "<form action=". $data['url'] ." method='post' id='submit_payment_form' content='text/html; charset=UTF-8'>";

            $html .= '<h1>'.$data['title'].'</h1>';

            foreach($data['payment'] as $key => $value)
            {
                $html .= "<input type='hidden' name='". $key . "' value=\"". $value . "\" readonly='readonly'>";
            }

            $html .= "<table border='0' width='900'>";
            $html .= "<tr><td>".$data['msg']."</td></tr><tr><td><input type='submit' value='".$data['label']."' id='submit_payment'/></td></tr>";
            $html .= "</table>";
            $html .= "</form>";
            $html .= "<script>";

            $html .= "jQuery( document ).ready(function() {
                        jQuery('#submit_payment_form').submit();
                      });
                ";

            $html .= "
                </script>
                </body>
                </html>
            ";

            echo $html;
        }
    }

endif;

?>