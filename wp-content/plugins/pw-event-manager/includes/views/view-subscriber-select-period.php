<?php

if( !class_exists( 'subscriberSelectPeriodHtmlView' ) ):

    class subscriberSelectPeriodHtmlView
    {

        public static function render( $title, $data )
        {
            $errors = array();
            if( isset($data['errors']) && is_array($data['errors']) ) {
                $errors = $data['errors'];
            }

            global $pw_config;
            $locale = substr(get_locale(), 0, 2 );
            $date_format = $pw_config['date_format'][$locale];

            if( isset( $data['sel_period_from'] ) ) {
                $from = date( $date_format ,strtotime( $data['sel_period_from'] ));
            } else {
                $from = date( $date_format ,strtotime( 'first day of this month'));
            }

            if( isset( $data['sel_period_to'] ) ) {
                $to = date( $date_format ,strtotime( $data['sel_period_to'] ));
            } else {
                $to = date( $date_format ,strtotime( 'last day of this month'));
            }

            ?>

            <div class="wrap">
                <h2>
                    <?php echo( $title ) ?>
                </h2>
                <p class="description">
                </p>

                <form method="POST" name="formSubscriberSelectPeriod" action="<?php $_SERVER['REQUEST_URI'] ?>" enctype="multipart/form-data">
                    <input type="hidden" name="requested_action" value="<?php echo $data['selected_action']; ?>">
                    <div class="pw-form">
                        <p><?php  _e('Include events between these dates:', 'pw-event-manager') ?></p>
                    <table class="form-table">
                        <tr>
                            <th>
                                <?php
                                    _e('From date', 'pw-event-manager');
                                ?>
                            </th>
                            <td>
                                <input type="text"  class="regular-text" name="sel_period_from" value="<?php echo $from; ?>">
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <?php
                                _e('To date', 'pw-event-manager');
                                ?>
                            </th>
                            <td>
                                <input type="text"  class="regular-text" name="sel_period_to" value="<?php echo $to; ?>">
                            </td>
                        </tr>

                    </table>
                    </div>
                    <p></p>
                    <table class="form-table">
                        <tr>
                            <td>
                                <input type="submit" class="button button-secondary" name="closeSubscriberSelectPeriod" value="<?php  _e('< Back', 'pw-event-manager') ?>">
                                <input type="submit" class="button button-primary" name="submitSubscriberSelectPeriod" value="<?php  _e('Next >', 'pw-event-manager') ?>">
                            </td>
                        </tr>
                    </table>
                    <p></p>

                </form>
            </div>

        <?php

        }
    }

endif;

?>