<?php

if( !class_exists( 'feEventDetailsHtmlView' ) ):

    class feEventDetailsHtmlView
    {

        public static function render( $data )
        {
            $html = "";

            if( isset($data['description'])) {
                $html = '<div class="show-event-description">';
                $html .= nl2br( stripslashes( $data['description'] ));
                $html .= '</div>';
            };

            echo $html;
        }
    }

endif;

?>