<?php

if( !class_exists( 'trainerListHtmlView' ) ):

    class trainerListHtmlView
    {

        public static function render( $title, $table )
        {
            ?>

            <div class="wrap">
                <h2>
                    <?php echo( $title ) ?>
                </h2>

                <?php

                    echo '<form method="POST" name="formTrainerTable" action="' . $_SERVER['REQUEST_URI'] . '" enctype="multipart/form-data">';
                    $table->display();
                    echo('</form>');

                ?>
            </div>

        <?php
        }
    }

endif;

?>