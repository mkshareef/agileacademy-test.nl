<?php

if( !class_exists( 'eventListHtmlView' ) ):

    class eventListHtmlView
    {

        public static function render( $title="", $table="" )
        {
            ?>

            <div class="wrap">
                <h2>
                    <?php echo( $title ) ?>
                </h2>

                <?php

                    echo '<form method="POST" name="formEventTable" action="' . $_SERVER['REQUEST_URI'] . '" enctype="multipart/form-data">';
                    $table->display();
                    echo('</form>');

                ?>
            </div>

        <?php
        }
    }

endif;

?>