<?php

if( !class_exists( 'feEventPlanningHtmlView' ) ):

    class feEventPlanningHtmlView
    {

        public static function render( $data )
        {
            global $pw_config;

            $html = "";

            $details = "";

            $details .= '<div class="show-event-planning">';
            $details .= '<table>';
            $details .= '<tr>';

            $date_format = $pw_config['date_format'][ $data['lang'] ];

            if( $data['lang'] == "nl" ) {
                $details .= '<th>Datum(s)</th><th>Tijd</th><th>Locatie</th><th>Prijs</th><th>Aanmelden</th>';

                $status_request = "Aanvragen";
                $status_open = "Inschrijven";
                $status_full = "Vol";
                $status_closed = "Gesloten";

                $dec_point = ',';
                $thousands_sep = '.';

            } elseif( $data['lang'] == "de" ) {
                $details .= '<th>Datum</th><th>Dauer</th><th>Location</th><th>Preis</th><th>Bewerben</th>';

                $status_request = "Anfrage";
                $status_open = "Anmelden";
                $status_full = "Ausgebucht";
                $status_closed = "Geschlossen";

                $dec_point = ',';
                $thousands_sep = '.';

            } else {
                $details .= '<th>Date(s)</th><th>Duration</th><th>Location</th><th>Price</th><th>Apply</th>';

                $status_request = "Request";
                $status_open = "Register";
                $status_full = "Full";
                $status_closed = "Closed";

                $dec_point = '.';
                $thousands_sep = ',';
            }

            $details .= '</tr>';

            if( isset($data['planning'])) {

                foreach ($data['planning'] as $key => $rec) {

                    $url_subscribe = "";

                    //Prepare data
                    $rec['url_subscribe'] = stripslashes( $rec['url_subscribe'] );
                    $rec['date'] = stripslashes( $rec['date'] );
                    $rec['other_dates'] = stripslashes( $rec['other_dates'] );
                    $rec['price'] = stripslashes( $rec['price'] );
                    $rec['time'] = stripslashes( $rec['time'] );
                    $rec['location'] = stripslashes( $rec['location'] );

                    if( isset($rec['url_subscribe']) && $rec['url_subscribe'] <> "" ) {
                        if( $rec['status'] == 'open') {
                            $url_subscribe = '<a href="'.$rec['url_subscribe'].'">'.$status_open.'</a>';
                        } elseif( $rec['status'] == 'request') {
                            $url_subscribe = '<a href="'.$rec['url_subscribe'].'">'.$status_request.'</a>';
                        } elseif( $rec['status'] == 'full') {
                            $url_subscribe = $status_full;
                        } else {
                            $url_subscribe = $status_closed;
                        }
                    }

                    $dates = $rec['date'];
                    if( $rec['other_dates'] <> "" ) {
                        $dates .= ','.$rec['other_dates'];

                        $date_list = explode(',', $dates);

                        foreach( $date_list as $idx=>$d ) {
                            if( $dates <> 'In company' ) {
                                $date_list[$idx] = date( $date_format ,strtotime( $d ));
                            }
                        }

                        $dates = implode( '<br>',$date_list );
                    } else {
                        if( $dates <> 'In company' )
                        {
                            $dates =  date( $date_format ,strtotime( $dates ));
                        }
                    }

                    if( $dates == 'In company' ) {
                        $price = $rec['price'];
                    } else {
                        $price = '&euro; '.number_format( $rec['price'], 2, $dec_point, $thousands_sep);
                    }

                    $details .= '<tr>
                                 <td>'.$dates.'</td>
                                 <td>'.$rec['time'].'</td>
                                 <td>'.$rec['location'].'</td>
                                 <td>'.$price.'</td>
                                 <td>'.$url_subscribe.'</td>
                                 </tr>';

                }
            }

            $details .= '</table>';
            $details .= '</div>';

            $html = $details;

            echo $html;
        }
    }

endif;

?>