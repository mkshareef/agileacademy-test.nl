<?php

if( !class_exists( 'feEventTrainerHtmlView' ) ):

    class feEventTrainerHtmlView
    {

        public static function render( $data )
        {
            $html = "";

            $title = '';

            if( $data['lang'] == 'nl' ) {
                $title = $data['type'] == 'event' ? 'Spreker(s)' :  'Trainer(s)';
            } elseif( $data['lang'] == 'de' ) {
                $title = $data['type'] == 'event' ? 'Trainer' :  'Trainer';
            } else {
                $title = $data['type'] == 'event' ? 'Host   (s)' :  'Trainer(s)';
            }

            if( is_array( $data['trainers'] ) ) {

                $html .= '<h2 class="show-event-trainer-title">'.$title.'</h2>';

                foreach( $data['trainers'] as $trainer ) {

                    $html .= '<div class="show-event-trainer">';

                    if( $trainer['attachment_path'] <> "" ) {
                        $upload_dir = wp_upload_dir();

                        $html .= '<div class="show-event-trainer-img">';
                        $html .= '<img src="'.$upload_dir['baseurl'].'/'.$trainer['attachment_path'].'">';
                        $html .= '</div>';
                    }

                    $html .= '<div class="show-event-trainer-name">';
                    $html .= '<b>'.stripslashes( $trainer['trainer_name'] ).'</b>';
                    $html .= '</div>';

                    $html .= '</div>';
                }

            };

            echo $html;
        }
    }

endif;

?>