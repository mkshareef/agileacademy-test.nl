<?php

if( !class_exists( 'subscriberEditHtmlView' ) ):

    class subscriberEditHtmlView
    {

        public static function render_confirmation( $title, $data ) {

            $errors = array();
            if( isset($data['errors']) && is_array($data['errors']) ) {
                $errors = $data['errors'];
            }

            ?>
            <div class="wrap">
                <h2>
                    <?php echo( stripslashes( $title ) ) ?>
                </h2>
                <p class="description">

                </p>
                <?php
                if( ! empty($errors) ) {
                    echo('<div class="error">');

                    foreach( $errors as $key => $e ) {
                        echo $e.'<br>';
                    }

                    echo('</div>');
                };
                ?>

                <form method="POST" name="formSubscriptionEdit" action="<?php $_SERVER['REQUEST_URI'] ?>" enctype="multipart/form-data">
                    <table class="form-table">
                        <tr>
                            <td>
                                <input type="submit" class="button button-primary" name="submitConfirm" value="<?php  _e('Sent confirmation', 'pw-event-manager') ?>">
                                <input type="submit" class="button button-secondary" name="closeConfirm" value="<?php  _e('Do not sent confirmation', 'pw-event-manager') ?>">
                            </td>
                        </tr>
                    </table>
                    <p></p>

                    <div class="pw-form">
                        <table class="form-table">
                            <tr>
                                <th><?php  _e('Send confirmation to', 'pw-event-manager') ?></th>
                                <td>
                                    <table class="nested-table">
                                        <?php
                                            echo '<tr>';
                                            echo '<td>';

                                            $data['first_name'] = stripslashes($data['first_name']);
                                            $data['middle_name'] = stripslashes($data['middle_name']);
                                            $data['last_name'] = stripslashes($data['last_name']);
                                            $data['email'] = stripslashes($data['email']);

                                            $name = str_replace('  ', ' ', $data['first_name'].' '.$data['middle_name'].' '.$data['last_name'] );

                                            echo $name. ' &lt;'.$data['email'].'&gt;';
                                            echo '</td>';
                                            echo '</tr>';
                                        ?>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <p></p>
                        <table class="form-table">
                            <tr>
                                <th><?php  _e('Confirmation email', 'pw-event-manager') ?> *</th>
                                <td><input type="text" class="large-text" name="subject_confirm_ext" placeholder="<?php _e('Subject of the mail', 'pw-event-manager') ?>"
                                           value="<?php echo stripslashes( $data['subject_confirm_ext'] ); ?>">
                                </td>
                            </tr>
                            <tr>
                                <th> </th>
                                <td><?php

                                    $editor_id = "mail_confirm_ext";
                                    $content = stripslashes( $data['mail_confirm_ext'] );
                                    $settings = array(
                                        'wpautop' => false,
                                        'media_buttons' => false,
                                        'teeny' => true,
                                        'textarea_rows' => 11 );

                                    wp_editor( $content, $editor_id, $settings );

                                    ?></td>
                            </tr>
                        </table>
                        <p></p>
                    </div>

                </form>
            </div>
        <?php

        }

        public static function render( $title, $data )
        {
            $errors = array();
            if( isset($data['errors']) && is_array($data['errors']) ) {
                $errors = $data['errors'];
            }

            global $pw_config;
            $locale = substr(get_locale(), 0, 2 );
            $date_time_format = $pw_config['date_format'][$locale].' H:i:s';
            $date_format = $pw_config['date_format'][$locale];
            $time_format = 'H:i';

            ?>

            <div class="wrap">
                <h2>
                    <?php echo( stripslashes( $title ) ) ?>
                </h2>
                <p class="description">
                    <?php
                    if( $data['updated'] <> '0000-00-00 00:00:00') {
                        echo __('Last update:', 'pw-event-manager').' '.date( $date_time_format ,strtotime( $data['updated'] )).' '.
                            __('by', 'pw-event-manager').' '. stripslashes( $data['updated_by'] );
                    }
                    ?>
                </p>
                <?php
                if( ! empty($errors) ) {
                    echo('<div class="error">');

                    foreach( $errors as $key => $e ) {
                        echo $e.'<br>';
                    }

                    echo('</div>');
                };
                ?>
                <form method="POST" name="formSubscriberEdit" action="<?php $_SERVER['REQUEST_URI'] ?>" enctype="multipart/form-data">
                    <table class="form-table">
                        <tr>
                            <td>
                                <input type="submit" class="button button-primary" name="submitSubscriber" value="<?php  _e('Save', 'pw-event-manager') ?>">
                                <input type="submit" class="button button-primary-2" name="submitAndCloseSubscriber" value="<?php  _e('Save & close', 'pw-event-manager') ?>">
                                <input type="submit" class="button button-secondary" name="closeSubscriber" value="<?php  _e('Cancel', 'pw-event-manager') ?>">
                                <input type="submit" class="button button-secondary" name="moveSubscriber" value="<?php  _e('Move to other date', 'pw-event-manager') ?>">
                                <input type="submit" class="button button-secondary" name="confirmSubscriber" value="<?php  _e('(Re)sent confirmation', 'pw-event-manager') ?>">
                            </td>
                        </tr>
                    </table>
                    <p></p>
                    <div class="pw-form">
                    <table class="form-table">
                        <tr>
                            <th><?php  _e('Status', 'pw-event-manager') ?></th>
                            <td>

                                <select name="status" id="select-subscription-status">

                                    <?php
                                        $selected = $data['status'] == 'new' ? 'selected' : '';
                                        echo '<option value="new" '.$selected.'>'.__('0. New', 'pw-event-manager').'</option>';

                                        $selected = $data['status'] == 'confirmed' ? 'selected' : '';
                                        echo '<option value="confirmed" '.$selected.'>'.__('1. Confirmed', 'pw-event-manager').'</option>';

                                        $selected = $data['status'] == 'cancelled' ? 'selected' : '';
                                        echo '<option value="cancelled" '.$selected.'>'.__('2. Cancelled', 'pw-event-manager').'</option>';
                                    ?>

                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th><?php  _e('Confirmation sent', 'pw-event-manager') ?></th>
                            <td>
                                <?php
                                    if( $data['confirm_sent'] == '1' ) {
                                        _e('Yes', 'pw-event-manager');
                                    } else {
                                        _e('No', 'pw-event-manager');
                                    }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th><?php  _e('Comments', 'pw-event-manager') ?></th>
                            <td>
                                <textarea id="process-comments" class="medium-text" cols="10" rows="5"
                                          name="process_comments"><?php echo stripslashes( $data['process_comments'] ); ?></textarea>
                            </td>
                        </tr>
                    </table>
                    </div>
                    <p></p>
                    <div class="pw-form">
                    <table class="form-table">
                        <tr>
                            <th><?php  _e('Subscription for', 'pw-event-manager') ?></th>
                            <td>
                                <table class="nested-table" width="100%">
                                    <tr>
                                        <td>
                                            <?php
                                            $dt = date( $date_format ,strtotime( $data['event_details']['date'] ));
                                            $tStart = date( $time_format ,strtotime( $data['event_details']['start_time'] ));
                                            $tStop = date( $time_format ,strtotime( $data['event_details']['stop_time'] ));
                                            echo stripslashes( $data['event_details']['name'] ).'<br>'.
                                                $dt.' '.
                                                $tStart.' - '.$tStop.'<br>'.
                                                stripslashes( $data['event_details']['location_name'] );
                                            ?>
                                        </td>
                                        <td>
                                            <p class="help-message">
                                                <?php
                                                _e('Confirmed participants:', 'pw-event-manager');
                                                echo ' '.$data['nr_confirmed_participants'];
                                                ?>
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <th> </th>
                            <td>
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <th><?php  _e('Company', 'pw-event-manager') ?></th>
                            <td>
                                <?php echo stripslashes( $data['company'] ); ?>
                            </td>
                        </tr>
                        <tr>
                            <th> </th>
                            <td>
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <th><?php  _e('Salutation', 'pw-event-manager') ?></th>
                            <td>

                                <select name="salutation" id="select-subscription-salutation">

                                    <?php
                                    $selected = $data['salutation'] == 'male' ? 'selected' : '';
                                    echo '<option value="male" '.$selected.'>'.__('Sir', 'pw-event-manager').'</option>';

                                    $selected = $data['salutation'] == 'female' ? 'selected' : '';
                                    echo '<option value="female" '.$selected.'>'.__('Madam', 'pw-event-manager').'</option>';
                                    ?>

                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th><?php  _e('First name', 'pw-event-manager') ?></th>
                            <td>
                                <input type="text"  class="regular-text" name="first_name" placeholder="<?php _e('First name', 'pw-event-manager') ?>"
                                       value="<?php echo stripslashes( $data['first_name'] ); ?>">
                            </td>
                        </tr>
                        <tr>
                            <th><?php  _e('Middle name', 'pw-event-manager') ?></th>
                            <td>
                                <input type="text"  class="regular-text" name="middle_name" placeholder="<?php _e('Middle name', 'pw-event-manager') ?>"
                                       value="<?php echo stripslashes( $data['middle_name'] ); ?>">
                            </td>
                        </tr>
                        <tr>
                            <th><?php  _e('Last name', 'pw-event-manager') ?> *</th>
                            <td>
                                <input type="text"  class="regular-text" name="last_name" placeholder="<?php _e('Last name', 'pw-event-manager') ?>"
                                       value="<?php echo stripslashes( $data['last_name'] ); ?>">
                            </td>
                        </tr>
                        <tr>
                            <th> </th>
                            <td>
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <th><?php  _e('Phone', 'pw-event-manager') ?></th>
                            <td>
                                <input type="text"  class="regular-text" name="phone" placeholder="<?php _e('Phone', 'pw-event-manager') ?>"
                                       value="<?php echo stripslashes( $data['phone'] ); ?>">
                            </td>
                        </tr>
                        <tr>
                            <th><?php  _e('Email', 'pw-event-manager') ?> *</th>
                            <td>
                                <input type="text"  class="regular-text" name="email" placeholder="<?php _e('Email', 'pw-event-manager') ?>"
                                       value="<?php echo stripslashes( $data['email'] ); ?>">
                            </td>
                        </tr>
                    </table>
                    <p> </p>
                    </div>

                </form>
            </div>

        <?php

        }
    }

endif;

?>