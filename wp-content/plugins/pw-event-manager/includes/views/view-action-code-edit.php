<?php

if( !class_exists( 'actionCodeEditHtmlView' ) ):

    class actionCodeEditHtmlView
    {
        private static function render_tabs( $current = 'settings', $action, $id )
        {
            $tabs = array(
                'settings' => __('Settings', 'pw-event-manager'),
                'assignment' => __('Scope', 'pw-event-manager')
            );

            $html = '<h2 class="nav-tab-wrapper">';
            foreach ($tabs as $tab => $name) {
                $class = ($tab == $current) ? ' nav-tab-active' : '';

                if( $action == "edit" ) {
                    $html .= "<a class='nav-tab$class pw-ui-tab pw-ui-tab-enabled' id='nav-tab-$tab' href='?page=pw-event-action-codes&action=$action&id=$id&tab=$tab'>$name</a>";
                } elseif( $action == "new" && $tab <> "settings" ) {
                    $html .= "<div class='nav-tab$class pw-ui-tab pw-ui-tab-disabled' id='nav-tab-$tab'>$name</div>";
                } else {
                    $html .= "<a class='nav-tab$class pw-ui-tab pw-ui-tab-enabled' id='nav-tab-$tab' href='?page=pw-event-action-codes&action=$action&tab=$tab'>$name</a>";
                }
            }

            $html .= '</h2>';
            return $html;
        }

        public static function render_settings( $data ) {

            global $pw_config;
            $locale = substr(get_locale(), 0, 2 );
            $date_format = $pw_config['date_format'][$locale];

            if( strpos($date_format, '-') ) {
                $date_help = str_replace('d','dd', $date_format );
                $date_help = str_replace('m','mm', $date_help );
                $date_help = str_replace('Y','jjjj', $date_help );
            } else if( strpos($date_format, '/') ) {
                $date_help = str_replace('d','dd', $date_format );
                $date_help = str_replace('m','mm', $date_help );
                $date_help = str_replace('Y','yyyy', $date_help );
            } else if( strpos($date_format, '.') ) {
                $date_help = str_replace('d','tt', $date_format );
                $date_help = str_replace('m','mm', $date_help );
                $date_help = str_replace('Y','jjjj', $date_help );
            }

            if( $locale == 'nl' ) {
                $dec_point = ',';
                $thousands_sep = '.';
            } else if( $locale == 'de' ) {
                $dec_point = ',';
                $thousands_sep = '.';
            } else {
                $dec_point = '.';
                $thousands_sep = ',';
            }

            //Prepare data
            $data['code'] = stripslashes( $data['code'] );
            $data['check_string'] = stripslashes( $data['check_string'] );
            $data['amount'] = stripslashes( $data['amount'] );
            $data['amount_type'] = stripslashes( $data['amount_type'] );
            $data['start_date'] = stripslashes( $data['start_date'] );
            $data['stop_date'] = stripslashes( $data['stop_date'] );
            $data['description'] = stripslashes( $data['description'] );

        ?>
            <table class="form-table">
            <tr>
                <th><label for="type"><?php  _e('Type', 'pw-event-manager') ?></label></th>
                <td>
                    <input type="radio" name="type" value="training"
                        <?php echo ($data['type']=='training')?'checked':'' ?>
                        ><?php  _e('Training', 'pw-event-manager'); ?>
                    <input type="radio" name="type" value="company"
                        <?php echo ($data['type']=='company')?'checked':'' ?>
                        ><?php  _e('Company', 'pw-event-manager'); ?>
                </td>
            </tr>

            <tr>
                <th><label for="code"><?php  _e('Action code', 'pw-event-manager') ?> *</label></th>
                <td><input type="text"  class="regular-text" name="code" placeholder="<?php _e('Code', 'pw-event-manager') ?>"
                           value="<?php echo $data['code']; ?>">
                </td>
            </tr>

            <tr>
                <th><label for="check_string"><?php  _e('Check string', 'pw-event-manager') ?> (*)</label></th>
                <td><input type="text"  class="regular-text" name="check_string" placeholder="<?php _e('Characters to check for in company name', 'pw-event-manager') ?>"
                           value="<?php echo $data['check_string']; ?>">
                </td>
            </tr>

            <?php
                $amount = number_format( $data['amount'], 2, $dec_point, $thousands_sep);
            ?>
            <tr>
                <th><label for="amount"><?php  _e('Discount', 'pw-event-manager') ?> * </label></th>
                <td><input type="text"  class="medium-text" name="amount" value="<?php echo $amount; ?>">
                    <select name="amount_type" id="select-amount-type">
                        <option value="percent" <?PHP echo isset( $data['amount_type'] ) && $data['amount_type'] == "percent" ? "selected": "" ?>>%</option>
                        <option value="euro" <?PHP echo isset( $data['amount_type'] ) && $data['amount_type'] == "euro" ? "selected": "" ?>>Euro</option>
                    </select>
                </td>
            </tr>

            <tr>
                <th><label for="is_blocked"><?php  _e('Status', 'pw-event-manager') ?></label></th>
                <td><div id="is_blocked_checkbox">
                        <?php
                            $checked="";

                            if( isset($data['is_blocked']) && ! empty( $data['is_blocked'] )) {
                                $checked = $data['is_blocked']=="1"? "checked" : "";
                            }
                        ?>
                        <input type="checkbox" name="is_blocked" <?php echo $checked?>><?php _e('Blocked', 'pw-event-manager') ?>
                    </div>
                </td>
            </tr>
            <tr>
                <th><label for="start_date"><?php  _e('Valid from', 'pw-event-manager') ?> * </label></th>
                <td>
                    <?php
                        $dt = '';
                        if( $data['start_date'] <> '' ) {
                            $dt = date( $date_format ,strtotime( $data['start_date'] ));
                        }
                    ?>
                    <input type="text"  class="medium-text" name="start_date" value="<?php echo $dt; ?>">
                    <span class="description"><?php echo $date_help ?></span>
                </td>
            </tr>
            <tr>
                <th><label for="stop_date"><?php  _e('Valid until', 'pw-event-manager') ?> * </label></th>
                <td>
                    <?php
                        $dt = '';
                        if( $data['stop_date'] <> '' ) {
                            $dt = date( $date_format ,strtotime( $data['stop_date'] ));
                        }
                    ?>
                    <input type="text"  class="medium-text" name="stop_date" value="<?php echo $dt; ?>">
                    <span class="description"><?php echo $date_help ?></span>
                </td>
            </tr>
            <tr>
                <th><label for="description"><?php  _e('Comments', 'pw-event-manager') ?></label></th>
                <td><textarea name="description" class="medium-text" cols="15" rows="5"
                              placeholder="<?php _e('Comments about the code', 'pw-event-manager') ?>
                    "><?php echo $data['description']; ?></textarea>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit" class="button button-primary" name="submitActionCode" value="<?php  _e('Save', 'pw-event-manager') ?>">
                    <input type="submit" class="button button-primary-2" name="submitAndCloseActionCode" value="<?php  _e('Save & close', 'pw-event-manager') ?>">
                    <input type="submit" class="button button-secondary" name="closeActionCode" value="<?php  _e('Cancel', 'pw-event-manager') ?>">
                </td>
            </tr>
        <?php
        }

        public static function render_assignments( $data ) {

            global $pw_config;
            $locale = substr(get_locale(), 0, 2 );
            $date_format = $pw_config['date_format'][$locale];

            if( strpos($date_format, '-') ) {
                $date_help = str_replace('d','dd', $date_format );
                $date_help = str_replace('m','mm', $date_help );
                $date_help = str_replace('Y','jjjj', $date_help );
            } else if( strpos($date_format, '/') ) {
                $date_help = str_replace('d','dd', $date_format );
                $date_help = str_replace('m','mm', $date_help );
                $date_help = str_replace('Y','yyyy', $date_help );
            } else if( strpos($date_format, '.') ) {
                $date_help = str_replace('d','tt', $date_format );
                $date_help = str_replace('m','mm', $date_help );
                $date_help = str_replace('Y','jjjj', $date_help );
            }

            //Prepare data
            $data['code_scope'] = stripslashes( $data['code_scope'] );
            $data['start_book_period'] = stripslashes( $data['start_book_period'] );
            $data['stop_book_period'] = stripslashes( $data['stop_book_period'] );

             ?>
            <table class="form-table" style="max-width: 950px;">
                <tr>
                    <th><label for="start_book_period"><?php  _e('Allow to book', 'pw-event-manager') ?></label></th>
                    <td>

                        <input type="radio" id="code-scope" name="code_scope" value="all" <?php echo isset($data['code_scope']) && $data['code_scope']=='all'? 'checked' : ""; ?>><?php _e('All events', 'pw-event-manager') ?>
                        <input type="radio" id="code-scope" name="code_scope" value="filtered" <?php echo isset($data['code_scope']) && $data['code_scope']<>'all'? 'checked' : ""; ?>><?php _e('Selected events', 'pw-event-manager') ?><br>
                    </td>
                </tr>
                <?php
                    $dt = '';
                    if( $data['start_book_period'] <> '' && $data['start_book_period'] <> '0000-00-00' ) {
                        $dt = date( $date_format ,strtotime( $data['start_book_period'] ));
                    }
                ?>
                <tr>
                    <th><label for="start_book_period"><?php  _e('Allow to book from', 'pw-event-manager') ?></label></th>
                    <td><input type="text"  class="medium-text" name="start_book_period" value="<?php echo $dt; ?>">
                        <span class="description"><?php echo $date_help ?></span>
                    </td>
                </tr>
                <?php
                $dt = '';
                if( $data['stop_book_period'] <> '' && $data['stop_book_period'] <> '0000-00-00' ) {
                    $dt = date( $date_format ,strtotime( $data['stop_book_period'] ));
                }
                ?>
                <tr>
                    <th><label for="stop_book_period"><?php  _e('Allow to book until', 'pw-event-manager') ?></label></th>
                    <td><input type="text"  class="medium-text" name="stop_book_period" value="<?php echo $dt; ?>">
                        <span class="description"><?php echo $date_help ?></span>
                    </td>
                </tr>
                <tr>
                    <th><label for="max_use"><?php  _e('Max nr of uses', 'pw-event-manager') ?></label></th>
                    <td><input type="text"  class="medium-text" name="max_use" value="<?php echo $data['max_use']; ?>">
                        <span class="description"><?php  _e('0 = unlimited', 'pw-event-manager') ?></span>
                    </td>
                </tr>
                <tr>
                    <th><label for="stop_book_period" id="label-action-code-events"><?php  _e('Selected events', 'pw-event-manager') ?></label></th>
                    <td>

                        <?php
                            $nl_events = array();
                            $de_events = array();
                            $en_events = array();

                            if( !empty( $data['all_events'] ) ) {
                                foreach( $data['all_events'] as $key=>$rec ) {
                                    if($rec['locale'] == "NL") {
                                        $nl_events[] = $rec;
                                    } elseif($rec['locale'] == "DE") {
                                        $de_events[] = $rec;
                                    } elseif($rec['locale'] == "EN") {
                                        $en_events[] = $rec;
                                    }
                                }
                            }

                            if( ! empty($nl_events)) {

                                echo "<ol id='checkboxes-action-code-nl'>";

                                echo "<li><input type='checkbox' value='root' /> NL
                                      <ol>";

                                foreach($nl_events as $k => $v ) {

                                    $checked = "";
                                    foreach($data['event_ids'] as $ke => $ve ) {
                                        if( $ve['eventID'] == $v['id'] ){
                                            $checked = "checked";
                                        }
                                    }

                                    echo '<li><input type="checkbox" name="event_ids[]" value="'.$v['id'].' "'.$checked.'/> '.stripslashes( $v['name'] ).'</li>';
                                }

                                echo "</ol></li>";
                                echo "</ol><br>";

                            }

                            if( ! empty($de_events)) {

                                echo "<ol id='checkboxes-action-code-de'>";

                                echo "<li><input type='checkbox' value='root' /> DE
                                          <ol>";

                                foreach($de_events as $k => $v ) {

                                    $checked = "";
                                    foreach($data['event_ids'] as $ke => $ve ) {
                                        if( $ve['eventID'] == $v['id'] ){
                                            $checked = "checked";
                                        }
                                    }

                                    echo '<li><input type="checkbox" name="event_ids[]" value="'.$v['id'].' "'.$checked.'/> '. stripslashes( $v['name'] ).'</li>';
                                }

                                echo "</ol></li>";
                                echo "</ol><br>";

                            }

                            if( ! empty($en_events)) {

                                echo "<ol id='checkboxes-action-code-en'>";

                                echo "<li><input type='checkbox' value='root' /> EN
                                          <ol>";

                                foreach($en_events as $k => $v ) {

                                    $checked = "";
                                    foreach($data['event_ids'] as $ke => $ve ) {
                                        if( $ve['eventID'] == $v['id'] ){
                                            $checked = "checked";
                                        }
                                    }

                                    echo '<li><input type="checkbox" name="event_ids[]" value="'.$v['id'].' "'.$checked.'/> '.stripslashes( $v['name'] ).'</li>';
                                }

                                echo "</ol></li>";
                                echo "</ol><br>";

                            }

                        ?>

                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="submit" class="button button-primary" name="submitActionCode" value="<?php  _e('Save', 'pw-event-manager') ?>">
                        <input type="submit" class="button button-primary-2" name="submitAndCloseActionCode" value="<?php  _e('Save & close', 'pw-event-manager') ?>">
                        <input type="submit" class="button button-secondary" name="closeActionCode" value="<?php  _e('Cancel', 'pw-event-manager') ?>">
                    </td>
                </tr>
            </table>

            <?php

        }


        public static function render( $title, $data, $action, $active_tab, $event_table = null )
        {
            global $pw_config;
            $locale = substr(get_locale(), 0, 2 );
            $date_format = $pw_config['date_format'][$locale].' H:i:s';

            $errors = array();
            if( isset($data['errors']) && ! empty($data['errors']) ) {
                $errors = $data['errors'];
            }

            if( ! empty($errors) ) {
                echo('<div class="error">');

                foreach( $errors as $key => $e ) {

                    echo implode('<br>', $e).'<br>';
                }

                echo('</div>');
            };

            // Prepare data
            $data['updated_by'] = stripslashes(  $data['updated_by'] );

            ?>

            <div class="wrap">
                <h2>
                    <?php echo( $title ) ?>
                </h2>
                <p class="description"><?php
                    echo __('Last update:', 'pw-event-manager').' '.date( $date_format ,strtotime( $data['updated'] )).' ' .
                    __('by', 'pw-event-manager').' '.$data['updated_by']
                    ?>
                </p>
                <form method="POST" name="formTrainerEdit" action="<?php $_SERVER['REQUEST_URI'] ?>" enctype="multipart/form-data">
                <?php

                    echo actionCodeEditHtmlView::render_tabs( $active_tab, $action, $data['id'] );
                    $data['action'] = $action;

                    switch( $active_tab ) {
                        case "settings":
                            echo actionCodeEditHtmlView::render_settings($data);
                            break;
                        case "assignment":
                            echo actionCodeEditHtmlView::render_assignments($data);
                            break;
                    }
                ?>
                </form>

            </div>

        <?php
            //Insert the html for a dialog
            echo isset( $data['dialog_html'] ) ? stripslashes( $data['dialog_html'] ) : "";
        }
    }

endif;

?>