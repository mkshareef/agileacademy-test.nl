<?php

if( !class_exists( 'feEventSubscribeBtnHtmlView' ) ):

    class feEventSubscribeBtnHtmlView
    {

        public static function render( $data )
        {
            $html = "";

            if( isset($data['url_subscribe'])) {
                $html .= '<a href="'.stripslashes( $data['url_subscribe'] ).'" class="event-apply-btn">'.stripslashes( $data['label'] ).'</a>';
            };

            echo $html;
        }
    }

endif;

?>