<?php

if( !class_exists( 'feEventLocationHtmlView' ) ):

    class feEventLocationHtmlView
    {

        public static function render( $data )
        {
            $html = "";

            $title = '';

            if( $data['lang'] == 'nl' ) {
                $title = 'Locatie(s)';
            } elseif( $data['lang'] == 'de' ) {
                $title = 'Location(s)';
            } else {
                $title = 'Location(s)';
            }

            if( is_array( $data['locations'] ) ) {

                $html .= '<h2 class="show-event-location-title">'.$title.'</h2>';

                foreach( $data['locations'] as $location ) {

                    $html .= '<div class="show-event-location">';

                    $html .= '<div class="show-event-location-name">';
                    $html .= '<b>'.stripslashes( $location['location_name'] ).'</b>';
                    $html .= '</div>';
                    $html .= '<div class="show-event-location-img">';
                    $html .= stripslashes( $location['maps_url'] );
                    $html .= '</div>';

                    $html .= '</div>';
                }

            };

            echo $html;
        }
    }

endif;

?>