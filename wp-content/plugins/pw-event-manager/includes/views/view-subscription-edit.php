<?php

if( !class_exists( 'subscriptionEditHtmlView' ) ):

    class subscriptionEditHtmlView
    {

        public static function render_confirmation( $title, $data ) {

            $errors = array();
            if( isset($data['errors']) && is_array($data['errors']) ) {
                $errors = $data['errors'];
            }
            ?>
            <div class="wrap">
            <h2>
                <?php echo( $title ) ?>
            </h2>
            <p class="description">

            </p>
            <?php
            if( ! empty($errors) ) {
                echo('<div class="error">');

                foreach( $errors as $key => $e ) {
                    echo $e.'<br>';
                }

                echo('</div>');
            };
            ?>

                <form method="POST" name="formSubscriptionEdit" action="<?php $_SERVER['REQUEST_URI'] ?>" enctype="multipart/form-data">
                    <table class="form-table">
                        <tr>
                            <td>
                                <input type="submit" class="button button-primary" name="submitConfirm" value="<?php  _e('Sent confirmations', 'pw-event-manager') ?>">
                                <input type="submit" class="button button-secondary" name="closeConfirm" value="<?php  _e('Do not sent confirmations', 'pw-event-manager') ?>">
                            </td>
                        </tr>
                    </table>
                    <p></p>

                    <div class="pw-form">
                        <table class="form-table">
                            <tr>
                                <th><?php  _e('Send confirmation to', 'pw-event-manager') ?></th>
                                <td>
                                    <table class="nested-table">
                                    <?php
                                        foreach( $data['subscribers'] as $rec ) {

                                            $confirmed = $rec['confirm_sent'] == '0' ? false : true;
                                            $moved = (int) $rec['eventID'] <> (int) $data['eventID'] || (int) $rec['eventDateID'] <> (int) $data['eventDateId'] ? true : false;

                                            echo '<tr>';
                                            echo '<td>';

                                            $rec['first_name'] = stripslashes($rec['first_name']);
                                            $rec['middle_name'] = stripslashes($rec['middle_name']);
                                            $rec['last_name'] = stripslashes($rec['last_name']);

                                            $name = str_replace('  ', ' ', $rec['first_name'].' '.$rec['middle_name'].' '.$rec['last_name'] );

                                            $checked = $confirmed === false && $moved === false ? 'checked' : '';
                                            echo '<input type="checkbox" name="send_to[]" value="'.$rec['id'].'" '.$checked.'>'.$name;
                                            echo '</td>';
                                            echo '<td>';
                                            echo '<i>';
                                            if( $confirmed === false ) {
                                                _e('To be confirmed', 'pw-event-manager');
                                            } else {
                                                _e('Has already been confirmed', 'pw-event-manager');
                                            }
                                            echo '</i>';
                                            echo '</td>';
                                            echo '<td>';

                                            if( $moved == true ) {
                                                echo '<i>';
                                                _e('!!! This person is moved to another event !!!', 'pw-event-manager');
                                                echo '</i>';
                                            }

                                            echo '</td>';
                                            echo '</tr>';
                                        }
                                    ?>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <p></p>
                        <table class="form-table">
                            <tr>
                                <th><?php  _e('Confirmation email', 'pw-event-manager') ?> *</th>
                                <td><input type="text" class="large-text" name="subject_confirm_ext" placeholder="<?php _e('Subject of the mail', 'pw-event-manager') ?>"
                                           value="<?php echo stripslashes( $data['subject_confirm_ext'] ); ?>">
                                </td>
                            </tr>
                            <tr>
                                <th> </th>
                                <td><?php

                                    $editor_id = "mail_confirm_ext";
                                    $content = stripslashes( $data['mail_confirm_ext'] );
                                    $settings = array(
                                        'wpautop' => false,
                                        'media_buttons' => false,
                                        'teeny' => true,
                                        'textarea_rows' => 11 );

                                    wp_editor( $content, $editor_id, $settings );

                                    ?></td>
                            </tr>
                        </table>
                        <p></p>
                    </div>

                </form>
            </div>
            <?php

        }

        public static function render( $title, $data )
        {
            $errors = array();
            if( isset($data['errors']) && is_array($data['errors']) ) {
                $errors = $data['errors'];
            }

            global $pw_config;
            $locale = substr(get_locale(), 0, 2 );
            $date_time_format = $pw_config['date_format'][$locale].' H:i:s';
            $date_format = $pw_config['date_format'][$locale];
            $time_format = 'H:i';

            ?>

            <div class="wrap">
                <h2>
                    <?php echo( $title ) ?>
                </h2>
                <p class="description">
                    <?php
                    if( $data['updated'] <> '0000-00-00 00:00:00') {
                        echo __('Last update:', 'pw-event-manager').' '.date( $date_time_format ,strtotime( $data['updated'] )).' '.
                            __('by', 'pw-event-manager').' '. stripslashes( $data['updated_by'] );
                    }
                    ?>
                </p>
                <?php
                    if( ! empty($errors) ) {
                        echo('<div class="error">');

                        foreach( $errors as $key => $e ) {
                            echo $e.'<br>';
                        }

                        echo('</div>');
                    };
                ?>
                <form method="POST" name="formSubscriptionEdit" action="<?php $_SERVER['REQUEST_URI'] ?>" enctype="multipart/form-data">
                    <table class="form-table">
                        <tr>
                            <td>
                                <input type="submit" class="button button-primary" name="submitSubscription" value="<?php  _e('Save', 'pw-event-manager') ?>">
                                <input type="submit" class="button button-primary-2" name="submitAndCloseSubscription" value="<?php  _e('Save & close', 'pw-event-manager') ?>">
                                <input type="submit" class="button button-secondary" name="closeSubscription" value="<?php  _e('Cancel', 'pw-event-manager') ?>">
                            </td>
                        </tr>
                    </table>
                    <p></p>
                    <div class="pw-form">
                    <table class="form-table">
                        <tr>
                            <th><?php  _e('Recieved', 'pw-event-manager') ?></th>
                            <td>
                                <?php echo date( $date_time_format ,strtotime( $data['created'] )); ?>
                            </td>
                        </tr>
                        <tr>
                            <th><?php  _e('Status', 'pw-event-manager') ?></th>
                            <td>

                                <select name="status" id="select-subscription-status">

                                    <?php
                                        $selected = $data['status'] == 'new' ? 'selected' : '';
                                        echo '<option value="new" '.$selected.'>'.__('1. New', 'pw-event-manager').'</option>';

                                        $selected = $data['status'] == 'inprogress' ? 'selected' : '';
                                        echo '<option value="inprogress" '.$selected.'>'.__('2. In progress', 'pw-event-manager').'</option>';

                                        $selected = $data['status'] == 'confirmed' ? 'selected' : '';
                                        echo '<option value="confirmed" '.$selected.'>'.__('3. Confirmed', 'pw-event-manager').'</option>';

                                        $selected = $data['status'] == 'cancelled' ? 'selected' : '';
                                        echo '<option value="cancelled" '.$selected.'>'.__('4. Cancelled', 'pw-event-manager').'</option>';
                                    ?>

                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th><?php  _e('Comments', 'pw-event-manager') ?></th>
                            <td>
                                <textarea id="process-comments" class="medium-text" cols="10" rows="5"
                                          name="process_comments"><?php echo stripslashes( $data['process_comments'] ); ?></textarea>
                            </td>
                        </tr>
                    </table>
                    </div>
                    <p></p>
                    <div class="pw-form">
                    <table class="form-table">
                        <tr>
                            <th><?php  _e('Subscription for', 'pw-event-manager') ?></th>
                            <td>
                                <table class="nested-table" width="100%">
                                <tr>
                                    <td>
                                        <?php

                                        $dt = date( $date_format ,strtotime( $data['event_details']['date'] ));
                                        $tStart = date( $time_format ,strtotime( $data['event_details']['start_time'] ));
                                        $tStop = date( $time_format ,strtotime( $data['event_details']['stop_time'] ));

                                        echo stripslashes( $data['event_details']['name'] ).'<br>'.
                                            $dt.'<br>'.
                                            $tStart.' - '.$tStop.'<br>'.
                                            stripslashes( $data['event_details']['location_name'] );
                                        ?>
                                    </td>
                                    <td>
                                        <p class="help-message">
                                            <?php
                                            _e('Confirmed participants:', 'pw-event-manager');
                                            echo ' '.$data['nr_confirmed_participants'];
                                            ?>
                                        </p>
                                    </td>
                                </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <th><?php  _e('Other dates', 'pw-event-manager') ?></th>
                            <td>
                                <?php
                                    $other_dates = array();
                                    $dArray = explode(',', $data['event_details']['other_dates']);
                                    foreach( $dArray as $key => $value ) {
                                        if( $value <> '' ) {
                                            $other_dates[] = date( $date_format ,strtotime( $value ));;
                                        }
                                    }

                                    echo implode( ', ', $other_dates );
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th><?php  _e('Participant(s)', 'pw-event-manager') ?></th>
                            <td>
                                <table class="nested-table">
                                    <?php
                                        echo '<tr>';
                                        echo '<th>'.__('Name', 'pw-event-manager').'</th>';
                                        echo '<th>'.__('Email', 'pw-event-manager').'</th>';
                                        echo '<th>'.__('Phone', 'pw-event-manager').'</th>';
                                        echo '<th>'.__('Newsletter', 'pw-event-manager').'</th>';
                                        echo '</tr>';

                                        foreach( $data['subscribers'] as $rec ) {
                                            if( $rec['salutation'] == 'male') {
                                                $sex = __('Sir', 'pw-event-manager');
                                            } else {
                                                $sex = __('Madam', 'pw-event-manager');
                                            }

                                            $rec['first_name'] = stripslashes( $rec['first_name'] );
                                            $rec['middle_name'] = stripslashes( $rec['middle_name'] );
                                            $rec['last_name'] = stripslashes( $rec['last_name'] );

                                            $name = str_replace('  ', ' ', $rec['first_name'].' '.$rec['middle_name'].' '.$rec['last_name'] );
                                            $email = stripslashes( $rec['email'] );
                                            $phone = stripslashes( $rec['phone'] );
                                            $newsletter = $rec['newsletter'] == '1' ? __('Yes', 'pw-event-manager') : __('No', 'pw-event-manager');

                                            echo '<tr>';
                                            echo '<td>'.$sex.' '.$name.'</td>';
                                            echo '<td>'.$email.'</td>';
                                            echo '<td>'.$phone.'</td>';
                                            echo '<td>'.$newsletter.'</td>';
                                            echo '</tr>';

                                        }
                                    ?>
                                </table>
                            </td>
                        </tr>

                    </table>

                    <h3><?php _e('Company information', 'pw-event-manager') ?></h3>

                    <table class="form-table">
                        <tr>
                            <th><?php  _e('Company', 'pw-event-manager') ?></th>
                            <td>
                                <?php echo( stripslashes( $data['company'] )); ?>
                            </td>
                        </tr>
                        <tr>
                            <th><?php  _e('Address', 'pw-event-manager') ?></th>
                            <td>
                                <?php
                                echo(
                                    stripslashes( $data['street'] ).' '.stripslashes( $data['street_nr']).'<br>'.
                                    stripslashes( $data['zip']).', '.stripslashes( $data['city']).'<br>'.
                                    stripslashes( $data['country'] )
                                ); ?>
                            </td>
                        </tr>
                    </table>
                    <?php

                    $invoice = $data['invoice'];

                    if( $invoice['invoice_nr'] <> '' ) {
                        echo '<h3>'.__('Invoice details', 'pw-event-manager').'</h3>';
                    ?>

                    <table class="form-table">
                        <tr>
                            <th><?php  _e('Attention to', 'pw-event-manager') ?></th>
                            <td>
                                <?php echo( stripslashes( $data['invoice_attention'] )); ?>
                            </td>
                        </tr>
                        <tr>
                            <th><?php  _e('Attribute', 'pw-event-manager') ?></th>
                            <td>
                                <?php echo( stripslashes( $data['invoice_attribute'] )); ?>
                            </td>
                        </tr>
                        <tr>
                            <th><?php  _e('Email', 'pw-event-manager') ?></th>
                            <td>
                                <?php echo( stripslashes( $data['invoice_email'] )); ?>
                            </td>
                        </tr>
                        <tr>
                            <th><?php  _e('Payment method', 'pw-event-manager') ?></th>
                            <td>
                                <?php
                                    $method =  __('Invoice', 'pw-event-manager');
                                    if( $data['payment_method'] == 'creditcard' ) {
                                        $method =  __('Creditcard', 'pw-event-manager');
                                    }
                                    else if( $data['payment_method'] == 'ideal' ) {
                                        $method =  __('iDeal', 'pw-event-manager');
                                    }
                                    echo($method);
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th><?php  _e('Action code', 'pw-event-manager') ?></th>
                            <td>
                                <?php echo( stripslashes( $data['invoice_action_code'] )); ?>
                            </td>
                        </tr>
                        <tr>
                            <th><?php  _e('Invoice language', 'pw-event-manager') ?></th>
                            <td>
                                <?php echo( strtoupper( $data['invoice_lang'] )); ?>
                            </td>
                        </tr>
                        <tr>
                            <th><?php  _e('Invoice number', 'pw-event-manager') ?></th>
                            <td>
                                <?php echo( stripslashes( $data['invoice']['invoice_nr'] ) ); ?>
                            </td>
                        </tr>
                        <tr>
                            <th><?php  _e('Terms & conditions', 'pw-event-manager') ?></th>
                            <td>
                                <?php
                                    if( isset( $data['agree_terms'] ) && $data['agree_terms'] == "1" ) {
                                        _e('Agreed with Terms & Conditions', 'pw-event-manager');
                                    } else {
                                        _e('Did not agree with Terms & Conditions', 'pw-event-manager');
                                    }
                                ?>
                            </td>
                        </tr>

                    </table>

                    <?php
                    }
                    ?>
                        <h3><?php _e('Other', 'pw-event-manager') ?></h3>
                        <table class="form-table">
                            <tr>
                                <th><?php  _e('Learned about us', 'pw-event-manager') ?></th>
                                <td>
                                    <?php
                                        echo( stripslashes( $data['know_how'] ));
                                        if( $data['know_how_else'] <> '' ) {
                                            echo( '<br>'.stripslashes( $data['know_how_else'] ));
                                        }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <th><?php  _e('Diet', 'pw-event-manager') ?></th>
                                <td>
                                    <?php echo(stripslashes( $data['diet'])); ?>
                                </td>
                            </tr>
                            <tr>
                                <th><?php  _e('Comments', 'pw-event-manager') ?></th>
                                <td>
                                    <?php echo(stripslashes( $data['comments'])); ?>
                                </td>
                            </tr>

                        </table>

                    </div>

                </form>
            </div>

        <?php

        }
    }

endif;

?>