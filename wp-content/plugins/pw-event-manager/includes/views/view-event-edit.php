<?php

if( !class_exists( 'eventEditHtmlView' ) ):

    class eventEditHtmlView
    {
        private static function render_tabs( $current = 'description', $action, $id )
        {
            $tabs = array(
                'description' =>  __('Description', 'pw-event-manager'),
                'pricing' => __('Pricing', 'pw-event-manager'),
                'planning' => __('Planning', 'pw-event-manager'),
                'setup' => __('Setup & notification', 'pw-event-manager'),
                'html' => __('HTML', 'pw-event-manager')
            );

            $html = '<h2 class="nav-tab-wrapper">';
            foreach ($tabs as $tab => $name) {
                $class = ($tab == $current) ? ' nav-tab-active' : '';

                if( $action == "edit" ) {
                    $html .= "<a class='nav-tab$class pw-ui-tab pw-ui-tab-enabled' id='nav-tab-$tab' href='?page=pw-events&action=$action&id=$id&tab=$tab'>$name</a>";
                } elseif( $action == "new" && $tab != "description" ) {
                    $html .= "<div class='nav-tab$class pw-ui-tab pw-ui-tab-disabled' id='nav-tab-$tab'>$name</div>";
                } else {
                    $html .= "<a class='nav-tab$class pw-ui-tab pw-ui-tab-enabled' id='nav-tab-$tab' href='?page=pw-events&action=$action&tab=$tab'>$name</a>";
                }
            }

            $html .= '</h2>';
            return $html;
        }

        public static function render_setup( $data ) {

            // Prepare data
            $data['event_type'] = stripslashes( $data['event_type'] );
            $data['event_status'] = stripslashes( $data['event_status'] );
            $data['attachment_path'] = stripslashes( $data['attachment_path'] );
            $data['notify_from'] = stripslashes( $data['notify_from'] );
            $data['notify_emails'] = stripslashes( $data['notify_emails'] );
            $data['subject_subscribed_ext'] = stripslashes( $data['subject_subscribed_ext'] );
            $data['mail_subscribed_ext'] = stripslashes( $data['mail_subscribed_ext'] );
            $data['subject_confirm_ext'] = stripslashes( $data['subject_confirm_ext'] );
            $data['mail_confirm_ext'] = stripslashes( $data['mail_confirm_ext'] );

        ?>
            <h3><?php  _e('Setup', 'pw-event-manager') ?></h3>
            <table class="form-table">
            <tr>
                <th><label for="event_type"><?php  _e('Type', 'pw-event-manager') ?></label></th>
                <td>
                    <input type="radio" name="event_type" value="training"
                        <?php echo ($data['event_type']=='training')?'checked':'' ?>
                        ><?php  _e('Training', 'pw-event-manager'); ?>
                    <input type="radio" name="event_type" value="workshop"
                        <?php echo ($data['event_type']=='workshop')?'checked':'' ?>
                        ><?php  _e('Workshop', 'pw-event-manager'); ?>
                    <input type="radio" name="event_type" value="event"
                        <?php echo ($data['event_type']=='event')?'checked':'' ?>
                        ><?php  _e('Event', 'pw-event-manager'); ?>
                </td>
            </tr>

            <tr>
                <th><label for="event_status"><?php  _e('Status', 'pw-event-manager') ?></label></th>
                <td>
                    <input type="radio" name="event_status" value="concept"
                        <?php echo ($data['event_status']=='concept')?'checked':'' ?>
                        ><?php  _e('Concept', 'pw-event-manager'); ?>
                    <input type="radio" name="event_status" value="publish"
                        <?php echo ($data['event_status']=='publish')?'checked':'' ?>
                        ><?php  _e('Publish', 'pw-event-manager'); ?>
                    <input type="radio" name="event_status" value="archive"
                        <?php echo ($data['event_status']=='archive')?'checked':'' ?>
                        ><?php  _e('Archive', 'pw-event-manager'); ?>
                </td>
            </tr>

            <tr>
                <th><label for="picture"><?php  _e('Picture', 'pw-event-manager') ?></label></th>
                <td><input type="hidden" name="attachment_path" value="<?php echo $data['attachment_path']; ?>">
                    <?php
                    $imgFound = false;

                    if( $data['attachment_path'] <> "" ) {
                        $img = "";
                        $img_src = util::get_image_src( $data['attachment_path'] );
                        if( $img_src ) {
                            $img = '<br><img src="'.$img_src.'" width="125">';
                            $imgFound = true;
                            echo($img);
                            echo('&nbsp;&nbsp;&nbsp;<input type="submit" class="button button-secondary" name="removePictureTraining" value="');
                            _e('Delete', 'pw-event-manager');
                            echo('">');
                        }
                    }

                    if( $imgFound == false ) {
                        echo('<input type="file" name="training_image" id="training_image" />');
                        echo('<p class="description">');
                        _e('The image should be 500px * 150px', 'pw-event-manager');
                        echo('.</p>');

                        if( isset( $errors['image']) ) {
                            echo( '<p class="error">'.implode('<br>', $errors['image']).'</p>' );
                        }
                    }

                    ?>
                </td>
            </tr>
            </table>

            <h3><?php  _e('Notification', 'pw-event-manager') ?></h3>
            <table class="form-table">
                <tr>
                    <th><?php  _e('From', 'pw-event-manager') ?> *</th>
                    <td><input type="text" class="large-text" name="notify_from" placeholder="Agile Academy <agileacademy@prowareness.nl>"
                               value="<?php echo $data['notify_from']; ?>">
                        <span class="pw-help-text"><?php _e('Example:', 'pw-event-manager') ?> Agile Academy &lt;agileacademy@prowareness.nl&gt;<br><?php _e('Important: make sure there is a space between the last letter of the name and &lt.', 'pw-event-manager') ?></span>
                    <?php
                        if( isset( $errors['notify_from']) ) {
                            echo( '<p class="error">'.implode('<br>', $errors['notify_from']).'</p>' );
                        }
                    ?>
                    </td>
                </tr>
                <tr>
                    <th><?php  _e('Notify internal', 'pw-event-manager') ?> *</th>
                    <td><input type="text" class="large-text" name="notify_emails" placeholder="<?php _e('Internal emails', 'pw-event-manager') ?>"
                               value="<?php echo $data['notify_emails']; ?>">
                    <span class="pw-help-text"><?php _e('Separate multiple emails with a ;', 'pw-event-manager') ?></span>

                    <?php
                    if( isset( $errors['notify_emails']) ) {
                        echo( '<p class="error">'.implode('<br>', $errors['notify_emails']).'</p>' );
                    }
                    ?>
                    </td>
                </tr>
                <tr>
                    <th>Place holders</th>
                    <td>
                        <table class="nested-table">
                            <tr>
                                <td>%FORMALHEAD%</td>
                                <td>-</td>
                                <td><?php _e('Dear Mr./Ms. de Vries', 'pw-event-manager') ?></td>
                            </tr>
                            <tr>
                                <td>%INFORMALHEAD%</td>
                                <td>-</td>
                                <td><?php _e('Dear Jan', 'pw-event-manager') ?></td>
                            </tr>
                            <tr>
                                <td>%EVENTTITLE%</td>
                                <td>-</td>
                                <td><?php _e('Title of the event', 'pw-event-manager') ?></td>
                            </tr>
                            <tr>
                                <td>%EVENTDATE%</td>
                                <td>-</td>
                                <td><?php _e('Start date of the event', 'pw-event-manager') ?></td>
                            </tr>
                            <tr>
                                <td>%EVENTTIME%</td>
                                <td>-</td>
                                <td><?php _e('Time of the event', 'pw-event-manager') ?></td>
                            </tr>
                            <tr>
                                <td>%OTHERDATES%</td>
                                <td>-</td>
                                <td><?php _e('Other dates of the event', 'pw-event-manager') ?></td>
                            </tr>
                            <tr>
                                <td>%LOCATION%</td>
                                <td>-</td>
                                <td><?php _e('Location of the event', 'pw-event-manager') ?></td>
                            </tr>
                            <tr>
                                <td>%SENDERNAME%</td>
                                <td>-</td>
                                <td><?php _e('Name of the sender of the email', 'pw-event-manager') ?></td>
                            </tr>
							<tr>
                                <td>%FOOTER%</td>
                                <td>-</td>
                                <td><?php _e('table with photo and address', 'pw-event-manager') ?></td>
                            </tr>
							<tr>
                                <td colspan=3><?php _e('all &lt;strong&gt; will be made autom. orange', 'pw-event-manager') ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>


            <table class="form-table">
                <tr>
                    <th><?php  _e('Mail from the website', 'pw-event-manager') ?> *</th>
                    <td><input type="text" class="large-text" name="subject_subscribed_ext" placeholder="<?php _e('Subject of the mail', 'pw-event-manager') ?>"
                               value="<?php echo $data['subject_subscribed_ext']; ?>">
                        <?php
                        if( isset( $errors['subject_subscribed_ext']) ) {
                            echo( '<p class="error">'.implode('<br>', $errors['subject_subscribed_ext']).'</p>' );
                        }
                        ?>
                    </td>
                </tr>
            <tr>
                <th> </th>
                <td><?php

                    $editor_id = "mail_subscribed_ext";
                    $content = $data['mail_subscribed_ext'];
                    $settings = array(
                        'wpautop' => false,
                        'media_buttons' => false,
                        'teeny' => true,
                        'textarea_rows' => 11 );

                    if( isset( $errors['mail_subscribed_ext']) ) {
                        echo( '<p class="error">'.implode('<br>', $errors['mail_subscribed_ext']).'</p>' );
                    }

                    wp_editor( $content, $editor_id, $settings );

                    ?></td>
            </tr>
            </table>
            <p></p>
            <table class="form-table">
                <tr>
                    <th><?php  _e('Confirmation mail', 'pw-event-manager') ?> *</th>
                    <td><input type="text" class="large-text" name="subject_confirm_ext" placeholder="<?php _e('Subject of the mail', 'pw-event-manager') ?>"
                               value="<?php echo $data['subject_confirm_ext']; ?>">
                        <?php
                        if( isset( $errors['subject_confirm_ext']) ) {
                            echo( '<p class="error">'.implode('<br>', $errors['subject_confirm_ext']).'</p>' );
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <th> </th>
                    <td><?php

                        $editor_id = "mail_confirm_ext";
                        $content = $data['mail_confirm_ext'];
                        $settings = array(
                            'wpautop' => false,
                            'media_buttons' => false,
                            'teeny' => true,
                            'textarea_rows' => 11
                        );

                        if( isset( $errors['mail_confirm_ext']) ) {
                            echo( '<p class="error">'.implode('<br>', $errors['mail_confirm_ext']).'</p>' );
                        }

                        wp_editor( $content, $editor_id, $settings );

                        ?></td>
                </tr>
            </table>

            <table class="form-table">
            <tr>
                <th></th>
                <td>
                    <input type="submit" class="button button-primary" name="submitSetup" value="<?php  _e('Save', 'pw-event-manager') ?>">
                    <input type="submit" class="button button-primary-2" name="submitAndCloseSetup" value="<?php  _e('Save & close', 'pw-event-manager') ?>">
                    <input type="submit" class="button button-secondary" name="closeEvent" value="<?php  _e('Cancel', 'pw-event-manager') ?>">
                </td>
            </tr>
            </table>

        <?php
        }

        public static function render_description( $data ) {

            $locale = substr(get_locale(), 0, 2 );

            if( $locale == 'nl' ) {
                $dec_point = ',';
                $thousands_sep = '.';
            } else if( $locale == 'de' ) {
                $dec_point = ',';
                $thousands_sep = '.';
            } else {
                $dec_point = '.';
                $thousands_sep = ',';
            }

            // Prepare data
            $data['locale'] = stripslashes( $data['locale'] );
            $data['event_title'] = stripslashes( $data['event_title'] );
            $data['duration'] = stripslashes( $data['duration'] );
            $data['selected_trainer_ids'] = stripslashes( $data['selected_trainer_ids'] );
            $data['description'] = stripslashes( $data['description'] );
            $data['teaser'] = stripslashes( $data['teaser'] );
            $data['selected_group_ids'] = stripslashes( $data['selected_group_ids'] );

        ?>
        <table class="form-table">
            <tr>
                <th><label for="locale"><?php  _e('Display language', 'pw-event-manager') ?></label></th>
                <td>
                    <input type="radio" name="locale" value="NL"
                        <?php echo ($data['locale']=='NL')?'checked':'' ?>
                        >NL
                    <input type="radio" name="locale" value="EN"
                        <?php echo ($data['locale']=='EN')?'checked':'' ?>
                        >EN
                    <input type="radio" name="locale" value="DE"
                        <?php echo ($data['locale']=='DE')?'checked':'' ?>
                        >DE
                </td>
            </tr>
            <tr>
                <th><label for="event_title"><?php  _e('Title', 'pw-event-manager') ?> *</label></th>
                <td><input type="text"  class="regular-text" name="event_title" placeholder="<?php _e('Title of the event', 'pw-event-manager') ?>"
                           value="<?php echo $data['event_title']; ?>"></td>
            </tr>

            <?php
                $duration = number_format( $data['duration'], 2, $dec_point, $thousands_sep);
            ?>
            <tr>
                <th><label for="duration"><?php  _e('Duration (days)', 'pw-event-manager') ?> *</label></th>
                <td><input type="text"  class="small-text" name="duration" value="<?php echo ( $duration ); ?>"></td>
            </tr>
            <tr>
                <th><label for="duration"><?php  _e('Know How', 'pw-event-manager') ?> *</label></th>
                <td>
                    <div style="padding-bottom: 10px;">
                        <input type="text"  class="know-how-text" name="custom-know-how" id="custom-know-how" value="<?php echo $data['custom-know-how']; ?>"/>
                        <input type="submit" class="button button-secondary" id="btn-add-custom-know-how" value="<?php  _e('+', 'pw-event-manager') ?>"/>
                    </div>
                    <select name="know_how" id="select-add-know-how">
                        <option value="">... Loading ...</option>
                    </select>
                    <input type="submit" class="button button-secondary" id="btn-add-know-how" value="<?php  _e('+', 'pw-event-manager') ?>"><br>
                    <input type="hidden" name="selected_know_how_ids" id="selected_know_how_ids" value="<?php echo $data['selected_know_how_ids']; ?>">
                    <div class="link-list" id="selected_know_how_list">
                        ... Loading ...
                    </div>
                </td>
            </tr>

            <tr>
                <th><label for="locale"><?php  _e('Trainer', 'pw-event-manager') ?></label></th>
                <td>
                    <select name="add_trainer" id="select-add-trainer">
                        <option value="">... Loading ...</option>
                    </select>
                    <input type="submit" class="button button-secondary" id="btn-add-trainer" value="<?php  _e('+', 'pw-event-manager') ?>"><br>
                    <input type="hidden" name="selected_trainer_ids" id="selected_trainer_ids" value="<?php echo $data['selected_trainer_ids']; ?>">
                    <div class="link-list" id="selected_trainers_list">
                        ... Loading ...
                    </div>
                </td>
            </tr>

            <tr>
                <th><?php  _e('Description', 'pw-event-manager') ?></th>
                <td><?php

                    $editor_id = "description";
                    $content = $data['description'];
                    $settings = array( 'wpautop' => false, 'media_buttons' => true, 'textarea_rows' => 21 );

                    if( isset( $errors['description']) ) {
                        echo( '<p class="error">'.implode('<br>', $errors['description']).'</p>' );
                    }

                    wp_editor( $content, $editor_id, $settings );

                    ?></td>
            </tr>

            <tr>
                <th><label for="teaser"><?php  _e('Teaser', 'pw-event-manager') ?></label></th>
                <td><input type="text" class="large-text" name="teaser" placeholder="<?php _e('Teaser for the event', 'pw-event-manager') ?>"
                           value="<?php echo $data['teaser']; ?>"></td>
            </tr>

            <tr>
                <th><label for="groups"><?php  _e('Groups', 'pw-event-manager') ?></label></th>
                <td>
                    <div id="event_group_checkbox">
                        ... Loading ...
                    </div>
                    <input type="hidden" name="selected_group_ids" id="selected_group_ids" value="<?php echo $data['selected_group_ids']; ?>">
                </td>
            </tr>

            <tr>
                <td></td>
                <td>
                    <input type="submit" class="button button-primary" name="submitDescription" value="<?php  _e('Save', 'pw-event-manager') ?>">
                    <input type="submit" class="button button-primary-2" name="submitAndCloseDescription" value="<?php  _e('Save & close', 'pw-event-manager') ?>">
                    <input type="submit" class="button button-secondary" name="closeEvent" value="<?php  _e('Cancel', 'pw-event-manager') ?>">
                </td>
            </tr>

        </table>
        <?php
        }

        public static function render_planning( $data, $table ) {
            echo('<br>');
            $table->display();
        }

        public static function render_pricing( $data ) {

            $locale = substr(get_locale(), 0, 2 );

            if( $locale == 'nl' ) {
                $dec_point = ',';
                $thousands_sep = '.';
            } else if( $locale == 'de' ) {
                $dec_point = ',';
                $thousands_sep = '.';
            } else {
                $dec_point = '.';
                $thousands_sep = ',';
            }

            //Prepare data
            $data['price_early_bird'] = stripslashes($data['price_early_bird']);
            $data['price_normal'] = stripslashes($data['price_normal']);
            $data['price_last_minute'] = stripslashes($data['price_last_minute']);

        ?>
            <table class="form-table">
                <?php
                    $price = number_format( $data['price_early_bird'], 2, $dec_point, $thousands_sep);
                ?>
                <tr>
                    <th><label for="locale"><?php  _e('Early bird price', 'pw-event-manager') ?></label></th>
                    <td>
                        &euro;
                        <input type="text"  class="medium-text" name="price_early_bird" placeholder="0,00"
                              value="<?php echo $price; ?>">
                        <p class="description"><?php  _e('Valid until 61 days before the training date. Set to 0 when not applicable.', 'pw-event-manager') ?></p>
                    </td>
                </tr>
                <?php
                    $price = number_format( $data['price_normal'], 2, $dec_point, $thousands_sep);
                ?>
                <tr>
                    <th><label for="locale"><?php  _e('Normal price', 'pw-event-manager') ?></label></th>
                    <td>
                        &euro;
                        <input type="text"  class="medium-text" name="price_normal" placeholder="0,00"
                               value="<?php echo $price; ?>">
                        <p class="description"><?php  _e('Between 61 and 21 days before training date. Set to 0 when not applicable.', 'pw-event-manager') ?></p>
                    </td>
                </tr>
                <?php
                    $price = number_format( $data['price_last_minute'], 2, $dec_point, $thousands_sep);
                ?>
                <tr>
                    <th><label for="locale"><?php  _e('Last minute price', 'pw-event-manager') ?></label></th>
                    <td>
                        &euro;
                        <input type="text"  class="medium-text" name="price_last_minute" placeholder="0,00"
                               value="<?php echo $price; ?>">
                        <p class="description"><?php  _e('Starts 21 days before the training date. Set to 0 when not applicable.', 'pw-event-manager') ?></p>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <br>
                        <input type="submit" class="button button-primary" name="submitPricing" value="<?php  _e('Save', 'pw-event-manager') ?>">
                        <input type="submit" class="button button-primary-2" name="submitAndClosePricing" value="<?php  _e('Save & close', 'pw-event-manager') ?>">
                        <input type="submit" class="button button-secondary" name="closeEvent" value="<?php  _e('Cancel', 'pw-event-manager') ?>">
                    </td>
                </tr>

            </table>
        <?php
        }

        public static function render_html( $data ) {

            // Prepare data
            $data['html_title'] = stripslashes( $data['html_title'] );
            $data['html_description'] = stripslashes( $data['html_description'] );
            $data['html_keywords'] = stripslashes( $data['html_keywords'] );
            $data['html_add_head'] = stripslashes( $data['html_add_head'] );
            $data['html_add_footer'] = stripslashes( $data['html_add_footer'] );
            $data['url_slug'] = stripslashes( $data['url_slug'] );

        ?>
            <table class="form-table">
                <tr>
                    <th><label for="html_title"><?php  _e('HTML Title', 'pw-event-manager') ?></label></th>
                    <td><input type="text"  class="regular-text" name="html_title" placeholder="<?php _e('HTML title for the training', 'pw-event-manager') ?>"
                               value="<?php echo $data['html_title']; ?>"></td>
                </tr>
                <tr>
                    <th><label for="html_description"><?php  _e('HTML Description', 'pw-event-manager') ?></label></th>
                    <td><textarea name="html_description" class="medium-text" cols="15" rows="5"
                                  placeholder="<?php _e('HTML description for the training', 'pw-event-manager') ?>
                    "><?php echo $data['html_description']; ?></textarea>
                </tr>
                <tr>
                    <th><label for="html_keywords"><?php  _e('HTML Keywords', 'pw-event-manager') ?></label></th>
                    <td><input type="text"  class="regular-text" name="html_keywords" placeholder="<?php _e('HTML keywords for the training', 'pw-event-manager') ?>"
                               value="<?php echo $data['html_keywords']; ?>"></td>
                </tr>
                <tr>
                    <th><label for="html_add_head"><?php  _e('Add to head', 'pw-event-manager') ?></label></th>
                    <td><textarea name="html_add_head" class="medium-text" cols="15" rows="10"
                                  placeholder="<?php _e('HTML/JS/CSS to be added to the HTML head', 'pw-event-manager') ?>
                    "><?php echo $data['html_add_head']; ?></textarea>
                </tr>
                <tr>
                    <th><label for="html_add_footer"><?php  _e('Add to footer', 'pw-event-manager') ?></label></th>
                    <td><textarea name="html_add_footer" class="medium-text" cols="15" rows="10"
                                  placeholder="<?php _e('HTML/JS/CSS to be added to the HTML footer', 'pw-event-manager') ?>
                    "><?php echo $data['html_add_footer']; ?></textarea>
                </tr>
                <tr>
                    <th><label for="html_url_slug"><?php  _e('Slug', 'pw-event-manager') ?></label></th>
                    <td><input type="text" name="url_slug" class="regular-text"
                               placeholder="<?php _e('Constant part of the url', 'pw-event-manager') ?>"
                               value="<?php echo $data['url_slug'] ?>">
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="submit" class="button button-primary" name="submitHTML" value="<?php  _e('Save', 'pw-event-manager') ?>">
                        <input type="submit" class="button button-primary-2" name="submitAndCloseHTML" value="<?php  _e('Save & close', 'pw-event-manager') ?>">
                        <input type="submit" class="button button-secondary" name="closeEvent" value="<?php  _e('Cancel', 'pw-event-manager') ?>">
                    </td>
                </tr>
            </table>
        <?php
        }

        public static function render( $title, $data, $action, $active_tab, $event_table = null )
        {
            global $pw_config;
            $locale = substr(get_locale(), 0, 2 );
            $date_format = $pw_config['date_format'][$locale].' H:i:s';

            $errors = array();
            if( isset($data['errors']) && ! empty($data['errors']) ) {
                $errors = $data['errors'];
            }

            if( ! empty($errors) ) {
                echo('<div class="error">');

                foreach( $errors as $key => $e ) {
                    echo $e.'<br>';
                }

                echo('</div>');
            };

            // Prepare data
            $data['updated_by'] = stripslashes( $data['updated_by'] );

            ?>

            <div class="wrap">
                <h2>
                    <?php echo( $title ) ?>
                </h2>
                <p class="description"><?php
                    echo __('Last update:', 'pw-event-manager').' '.date( $date_format ,strtotime( $data['updated'] )).' ' .
                    __('by', 'pw-event-manager').' '.$data['updated_by']
                    ?>
                </p>
                <form method="POST" name="formEventEdit" action="<?php $_SERVER['REQUEST_URI'] ?>" enctype="multipart/form-data">
                <?php

                    echo eventEditHtmlView::render_tabs( $active_tab, $action, $data['id'] );
                    $data['action'] = $action;

                    switch( $active_tab ) {
                        case "setup":
                            echo eventEditHtmlView::render_setup($data);
                            break;
                        case "description":
                            echo eventEditHtmlView::render_description($data);
                            break;
                        case "planning":
                            echo eventEditHtmlView::render_planning($data, $event_table);
                            break;
                        case "pricing":
                            echo eventEditHtmlView::render_pricing($data);
                            break;
                        case "html":
                            echo eventEditHtmlView::render_html($data);
                            break;
                    }
                ?>
                </form>

            </div>

        <?php
            //Insert the html for a dialog
            echo isset( $data['dialog_html'] ) ? $data['dialog_html'] : "";
        }
    }

endif;

?>