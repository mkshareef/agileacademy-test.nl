<?php

if( !class_exists( 'invoiceListHtmlView' ) ):

    class invoiceListHtmlView
    {

        public static function render( $title, $table, $filter = null )
        {
            ?>

            <div class="wrap">
                <h2>
                    <?php echo( $title ) ?>
                </h2>

                <?php
                    echo '<form method="POST" name="formInvoicesTable" action="' . $_SERVER['REQUEST_URI'] . '" enctype="multipart/form-data">';
                    $table->display( $filter );
                    echo('</form>');

                ?>
            </div>

        <?php
        }
    }

endif;

?>