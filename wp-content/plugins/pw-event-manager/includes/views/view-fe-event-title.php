<?php

/**
 * The page content view
 *
 * @package PA Notebook
 * @subpackage Content view
 * @since 0.1
 */

if( !class_exists( 'feEventTitleHtmlView' ) ):

    class feEventTitleHtmlView
    {

        public static function render( $data )
        {
            $html = "";

            if( isset($data['title'])) {
                $html = '<div class="show-event-title">';
                $html .= '<h1>'.stripslashes( $data['title'] ).'</h1>';
                $html .= '</div>';
            };

            echo $html;
        }
    }

endif;

?>