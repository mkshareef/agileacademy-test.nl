<?php

if( !class_exists( 'feKeywordSearchResultsHtmlView' ) ):

    class feKeywordSearchResultsHtmlView
    {

        public static function render( $data )
        {
            $html = "";
            $upload_dir = wp_upload_dir();

            $html .= '<div class="event-search-results">';
            $html .= '<h1>'.stripslashes( $data['title'] ).'</h1>';

            if( isset($data['results']) && $data['nr_results'] > 0 ) {

                if( $data['lang'] == 'nl' ) {
                    $more_info_str = 'Meer info';
                    $show_all_str = 'Toon alles';
                } elseif( $data['lang'] == 'de' ) {
                    $more_info_str = 'Lernen Sie mehr';
                    $show_all_str = 'Alles Anzeigen';
                } else {
                    $more_info_str = 'Learn more';
                    $show_all_str = 'Show all';
                }

                foreach( $data['results'] as $key=>$rec ) {

                    $title = isset($rec['title']) ? stripslashes( $rec['title'] ) : "";
                    $summary = isset($rec['summary']) ? stripslashes( $rec['summary'] ) : "";
                    $image = isset($rec['img_path']) && $rec['img_path'] <> "" ? '<img src="'.$upload_dir['baseurl'].'/'.$rec['img_path'].'">' : "";

                    $html .= '<div class="event-search-result">';
                    $html .= '<div class="event-search-result-title">';
                    $html .= '<h1><a href="'.$rec['url'].'">'.$title.'</a></h1>';
                    $html .= '</div>';
                    $html .= '<div class="event-search-result-body">';
                    $html .= $image;
                    $html .= $summary;
                    $html .= '</div>';
                    //$html .= '<div class="event-search-result-details">';
                    //$html .= $details;
                    //$html .= '</div>';
                    $html .= '<div class="event-search-result-action">';
                    $html .= '<a href="'.$rec['url'].'" class="pw-button">'.$more_info_str.'</a>';
                    $html .= '</div>';
                    $html .= '</div>';

                }
            } else {

                if( $data['lang'] == 'nl' ) {
                    $no_results_msg = 'Geen resultaten gevonden';
                } elseif( $data['lang'] == 'de' ) {
                    $no_results_msg = 'Keine Ergebnisse gefunden';
                } else {
                    $no_results_msg = 'No results found';
                }

                $html .= isset( $data['no_results_msg']) ? '<p>'.$data['no_results_msg'].'</p>' : '<p>'.$no_results_msg.'</p>';
            };

            $html .= '</div>';

            //Paging of the search results
            if( $data['nr_pages'] > 1 ) {
                $html .= '<div class="event-search-browse">';

                for( $n=1; $n <= $data['nr_pages']; $n++ ) {
                    $qs = $data['qrs'];

                    if( $n == (int) $data['show_page'] ) {
                        $active = 'event-search-browse-page-active';
                    } else {
                        $active = '';
                    }

                    if( $qs == '') {
                        $html .= '<a href="?pag='.$n.'" class="event-search-browse-page '.$active.'">'.$n.'</a>';
                    } else {
                        $html .= '<a href="?'.$qs.'&pag='.$n.'" class="event-search-browse-page '.$active.'">'.$n.'</a>';
                    }

                }

                if( $data['nr_pages'] > 1 ) {
                    $html .= '<a href="?'.$qs.'&all=true" class="event-search-browse-page">'.$show_all_str.'</a>';
                }

                $html .= '</div>';
            } else {
                $html .= '<br><br>';
            }

            echo $html;
        }
    }

endif;

?>