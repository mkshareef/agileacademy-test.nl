<?php

if( !class_exists( 'setupListHtmlView' ) ):

    class setupListHtmlView
    {

        private static function render_tabs( $current = 'event_groups' )
        {
            $tabs = array(
                'event_groups' => __('Event groups', 'pw-event-manager'),
                //'other' => __('Other', 'pw-event-manager')
            );

            $html = '<h2 class="nav-tab-wrapper">';
            foreach ($tabs as $tab => $name) {
                $class = ($tab == $current) ? ' nav-tab-active' : '';
                $html .= "<a class='nav-tab$class' href='?page=pw-event-settings&tab=$tab'>$name</a>";

            }

            $html .= '</h2>';
            return $html;
        }

        public static function render( $title="", $active_tab, $table )
        {
            ?>

            <div class="wrap">
                <h2>
                    <?php echo( $title ) ?>
                </h2>


                    <?php
                    echo setupListHtmlView::render_tabs( $active_tab );
                    echo "<br>";
                    switch( $active_tab ) {
                        case "event_groups":
                            echo '<form method="POST" name="formEventGroupTable" action="' . $_SERVER['REQUEST_URI'] . '" enctype="multipart/form-data">';
                            $table->display();
                            echo('</form>');
                            break;
                    }
                    ?>

            </div>

        <?php
        }



    }

endif;

?>