<?php

if( !class_exists( 'subscriberMoveHtmlView' ) ):

    class subscriberMoveHtmlView
    {

        public static function render( $title, $data )
        {
            $errors = array();
            if( isset($data['errors']) && is_array($data['errors']) ) {
                $errors = $data['errors'];
            }

            global $pw_config;
            $locale = substr(get_locale(), 0, 2 );
            $date_format = $pw_config['date_format'][$locale];
            $time_format = 'H:i';

            ?>

            <div class="wrap">
                <h2>
                    <?php echo( $title ) ?>
                </h2>
                <p class="description">
                </p>

                <form method="POST" name="formSubscriberMove" action="<?php $_SERVER['REQUEST_URI'] ?>" enctype="multipart/form-data">
                    <table class="form-table">
                        <tr>
                            <td>
                                <input type="submit" class="button button-primary" name="submitSubscriberMove" value="<?php  _e('Save', 'pw-event-manager') ?>">
                                <input type="submit" class="button button-secondary" name="closeSubscriberMove" value="<?php  _e('Cancel', 'pw-event-manager') ?>">
                            </td>
                        </tr>
                    </table>
                    <p></p>
                    <div class="pw-form">
                    <table class="form-table">

                            <?php

                            $dates = $data['other_event_dates'];

                            foreach( $dates as $key=>$rec ) {

                                $dt = date( $date_format ,strtotime( $rec['start_date'] ));
                                $selected = $rec['id'] == $data['eventDateID'] ? 'checked' : '';
                                echo('<tr>');
                                echo('<td width="50"><input type="radio" name="new_date" value="'.$rec['id'].'" '.$selected.'></td>');
                                echo('<td width="150">'.$dt.'</td>');
                                echo('<td>'.date( $time_format ,strtotime( $rec['start_time'] )).'</td>');
                                echo('</tr>');
                            }
                            ?>
                        </tr>
                    </table>
                    </div>
                    <p></p>

                </form>
            </div>

        <?php

        }
    }

endif;

?>