<?php

if (!class_exists('feEventSubscribeHtmlView')):

    class feEventSubscribeHtmlView
    {

        private static function render_tabs($tabs, $active_tab_id = 1)
        {

            $html = "";

            if (is_array($tabs)) {

                $html .= '<div class="event-subscribe-tabs">';

                foreach ($tabs as $tab_id => $tab) {

                    $active = $tab_id == $active_tab_id ? "active" : "";

                    $html .= '<div class="event-subscribe-tab ' . $active . '">';
                    $html .= '<span>'.$tab.'</span>';
                    $html .= '</div>';
                }

                $html .= '</div>';
            }

            echo $html;
        }

        private function know_how_options() {
            $data = array(
                'en' => array(
                    ''                  => '- Make a choice -',
                    'google'            => 'Via Google',
                    'website'           => 'Via the website',
                    'pro-contributor'   => 'Via a Prowareness employee',
                    'friend'            => 'Via friends',
                    'colleagues'        => "Via colleagues",
                    'news'              => 'Via the newsletter',
                    'twitter'           => 'Via Twitter',
                    'linkedin'          => 'Via Linkedin',
                    'bnr'               => 'Via BNR radio campaign',
                    'sdn-nl'            => 'Via sdn.nl',
                    'improve'           => 'Via Improve',
                    'agile-market'      => 'Via Agile+ Market',
                    'others'            => 'Other...'
                ),
                'nl' => array(
                    ''                  => '- Maak een keuze -',
                    'google'            => 'Via Google',
                    'website'           => 'Via de website',
                    'bnr'               => 'Via BNR',
                    'news'              => 'Via de nieuwsbrief',
                    'twitter'           => 'Via Twitter',
                    'linkedin'          => 'Via Linkedin',
                    'pro-contributor'   => 'Via een Prowareness medewerker',
                    'colleagues'        => "Via collega's",
                    'pro-event'         => 'Via een Prowareness event',
                    'pro-training'      => 'Via een Prowareness training',
                    'conference'        => 'Via een conferentie',
                    'scrum-org'         => 'Via Scrum.org',
                    'others'            => 'Anders...'
                ),
                'de' => array(
                    ''                  => '- Treffen Sie eine Wahl -',
                    'google'            => 'Via Google',
                    'website'           => 'Via Webseite',
                    'pro-contributor'   => 'Via einem Prowareness Mitarbeiter',
                    'friend'            => 'Via Freunde',
                    'colleagues'        => "Via Kollegen",
                    'news'              => 'Via dem Newsletter',
                    'twitter'           => 'Via Twitter',
                    'linkedin'          => 'Via Linkedin',
                    'bnr'               => 'Via BNR Raio Kampagne',
                    'sdn-nl'            => 'Via sdn.nl',
                    'improve'           => 'Via Improve',
                    'agile-market'      => 'Via Agile+ Market',
                    'others'            => 'Andere…'
                )
            );

            return $data;
        }

        public static function render_select_date($incompany, $planning, $selected_date_id, $duration, $lang = "en")
        {
            global $pw_config;

            $html = "";

            $date_format = $pw_config['date_format'][ $lang ];

            $html .= '<form method="post">';
            $html .= '<div class="event-form">';
            $html .= '<table class="subscribe-select-date">';

            $html .= '<tr>';

            if ($lang == "nl") {
                $html .= '<th>Datum</th><th># dagen</th><th>Locatie</th><th>Prijs</th>';
                $submit = 'Volgende &raquo;';

                $dec_point = ',';
                $thousands_sep = '.';
            } elseif ($lang == "de") {
                $html .= '<th>Datum</th><th># tagen</th><th>Location</th><th>Preis</th>';
                $submit = 'Nächsten &raquo;';

                $dec_point = ',';
                $thousands_sep = '.';
            } else {
                $html .= '<th>Date</th><th># days</th><th>Location</th><th>Price</th>';
                $submit = 'Next &raquo;';

                $dec_point = '.';
                $thousands_sep = ',';
            }

            $html .= '</tr>';

            if (is_array($incompany)) {
                $selected = "";

                if( $selected_date_id < 0 ) {
                    $selected_date_id = 0;
                }

                $html .= '<tr><td>';

                if ($selected_date_id == 0) {
                    $selected = 'checked="checked"';
                }
                $html .= '<input type="radio" name="selected_date_id" value="' . $incompany['id'] . '" ' .
                    $selected . '> <label>' . $incompany['start_date'] . '</label><br>';

                $html .= '</td>';
                $html .= '<td>';
                $html .= $incompany['other_dates'];
                $html .= '</td>';
                $html .= '<td>';
                $html .= $incompany['location'];
                $html .= '</td>';
                $html .= '<td>';
                $html .= $incompany['real_price'];

                $html .= '</td></tr>';
            }

            if( $selected_date_id < 0 && isset( $planning[0] ) ) {
                $selected_date_id = $planning[0]['id'];
            }

            if (is_array($planning)) {
                foreach ($planning as $plan) {

                    $selected = "";

                    if ($plan['status'] == 'open') {

                        // Prepare the data
                        $plan['start_date'] = stripslashes($plan['start_date']);
                        $plan['location']['location_name'] = stripslashes($plan['location']['location_name']);
                        $plan['real_price'] = stripslashes($plan['real_price']);

                        $html .= '<tr><td>';

                        if ($selected_date_id == $plan['id']) {
                            $selected = 'checked="checked"';
                        }

                        $html .= '<input type="radio" name="selected_date_id" value="' . $plan['id'] . '" ' . $selected . '> <label>'
                            . date($date_format, strtotime($plan['start_date'])) . '</label><br>';
                        $html .= '</td>';
                        $html .= '<td>';

                        $duration = number_format((float)$duration, 2, $dec_point, $thousands_sep);
                        $html .= str_replace($dec_point . '00', '', $duration);
                        $html .= '</td>';
                        $html .= '<td>';
                        $html .= $plan['location']['location_name'];
                        $html .= '</td>';
                        $html .= '<td>';

                        $price = '&euro; ' . number_format($plan['real_price'], 2, $dec_point, $thousands_sep);

                        $html .= $price;
                        $html .= '</td></tr>';
                    }
                }
            }

            $html .= '</table>';

            //Form actions
            $html .= '<div class="event-form-actions align-right">';
            $html .= '<button type="submit" class="event-form-action-btn" name="event-submit-1">' . $submit . '</button>';
            $html .= '</div>';

            $html .= '</div>';
            $html .= '</form>';


            echo $html;

        }

        public static function render_add_subscribers($data)
        {

            $html = '';

            if ($data['event']['locale'] == 'NL') {

                $labels[0] = 'Aanhef';
                $labels[1] = 'Naam *';
                $labels[2] = 'Telefoonnummer';
                $labels[3] = 'E-mailadres *';

                $sex[0] = 'De heer';
                $sex[1] = 'Mevrouw';

                $newsletter_lbl = 'Ik wil me aanmelden voor de Prowareness nieuwsbrief.';

                $part_add_lbl = 'Extra deelnemer toevoegen';

                $prev_lbl = '&laquo; Terug';
                $next_lbl = 'Volgende &raquo;';

                $has_error_msg = 'FOUT: Onjuiste gegevens aangetroffen voor 1 of meer deelnemers';

                $error['missing_last_name'] = 'Achternaam is verplicht';
                $error['missing_email'] = 'E-mailadres is verplicht';
                $error['invalid_email'] = 'Ongeldig e-mailadres';

            } elseif ($data['event']['locale'] == 'DE') {

                $labels[0] = 'Anrede';
                $labels[1] = 'Name *';
                $labels[2] = 'Telefon';
                $labels[3] = 'Email *';

                $sex[0] = 'Herr';
                $sex[1] = 'Frau';

                $newsletter_lbl = 'Ich möchte mich für den Prowareness Newsletter anmelden';

                $part_add_lbl = 'Füge einen extra Teilnehmer hinzu';

                $prev_lbl = '&laquo; Zurück';
                $next_lbl = 'Nächsten &raquo;';

                $has_error_msg = 'ERROR: Ungültige Daten für einen oder mehrere Teilnehmer gefunden';

                $error['missing_last_name'] = 'Nachname fehlt';
                $error['missing_email'] = 'Email fehlt';
                $error['invalid_email'] = 'Ungültige Emailadresse';

            } else {
                $labels[0] = 'Salutation';
                $labels[1] = 'Name *';
                $labels[2] = 'Phone';
                $labels[3] = 'Email *';

                $sex[0] = 'Sir';
                $sex[1] = 'Madam';

                $newsletter_lbl = 'I want to subscribe to the Prowareness newsletter';

                $part_add_lbl = 'Add extra participant';

                $prev_lbl = '&laquo; Back';
                $next_lbl = 'Next &raquo;';

                $has_error_msg = 'ERROR: Invalid data found for one or more participants';

                $error['missing_last_name'] = 'Last name is missing';
                $error['missing_email'] = 'Email is missing';
                $error['invalid_email'] = 'Invalid email address';

            }

            $html .= '<form method="post">';
            $html .= '<div class="event-form">';

            $n = 0;

            if( isset($data['has_error']) and $data['has_error'] == true ) {
                $html .= '<div class="error">'.$has_error_msg.'</div>';
            }

            if (is_array($data['subscribers'])) {
                foreach ($data['subscribers'] as $subscriber) {

                    // Prepare the data
                    $subscriber['salutation'] = stripslashes($subscriber['salutation']);
                    $subscriber['first_name'] = stripslashes($subscriber['first_name']);
                    $subscriber['middle_name'] = stripslashes($subscriber['middle_name']);
                    $subscriber['last_name'] = stripslashes($subscriber['last_name']);
                    $subscriber['phone'] = stripslashes($subscriber['phone']);
                    $subscriber['email'] = stripslashes($subscriber['email']);

                    $html .= '<div class="section">';
                    $html .= '<button type="submit" class="event-form-remove_btn" name="participant_remove_' . $n . '"></button>';
                    $html .= '<input type="hidden" name="id_' . $n . '" value="' . $subscriber['id'] . '">';

                    $html .= '<table>';

                    //Display errors when they are there
                    if (isset($subscriber['local_errors'])) {
                        $e = "";
                        foreach ($subscriber['local_errors'] as $value) {
                            if( $value <> "" ) {
                                $e .= $error[$value] . '<br>';
                            }

                        }

                        if( $e <> "") {
                            $html .= '';
                            $html .= '<tr>';
                            $html .= '<td>';
                            $html .= '&nbsp;';
                            $html .= '</td>';
                            $html .= '<td colspan="3">';
                            $html .= '<div class="error">' . $e . '</div>';
                            $html .= '</td>';
                            $html .= '</tr>';
                        }
                    }

                    $html .= '<tr>';
                    $html .= '<td>';
                    $html .= '<label>' . $labels[0] . '</label>';
                    $html .= '</td>';
                    $html .= '<td colspan="3">';
                    $html .= '<select name="salutation_' . $n . '">';

                    $selected = $subscriber['salutation'] == 'male' ? 'selected' : '';
                    $html .= '<option value="male" ' . $selected . '>' . $sex[0] . '</option>';

                    $selected = $subscriber['salutation'] == 'female' ? 'selected' : '';
                    $html .= '<option value="female" ' . $selected . '>' . $sex[1] . '</option>';

                    $html .= '</select>';

                    $html .= '</td>';
                    $html .= '</tr>';
                    $html .= '<tr>';
                    $html .= '<td>';
                    $html .= '<label>' . $labels[1] . '</label>';
                    $html .= '</td>';
                    $html .= '<td>';
                    $html .= '<input type="text" class="size-25" name="first_name_' . $n . '" value="' . $subscriber['first_name'] . '">';
                    $html .= '</td>';
                    $html .= '<td>';
                    $html .= '<input type="text" class="size-25" name="middle_name_' . $n . '" value="' . $subscriber['middle_name'] . '">';
                    $html .= '</td>';
                    $html .= '<td>';
                    $html .= '<input type="text" class="size-25"name="last_name_' . $n . '" value="' . $subscriber['last_name'] . '">';
                    $html .= '</td>';
                    $html .= '</tr>';
                    $html .= '<tr>';
                    $html .= '<td>';
                    $html .= '<label>' . $labels[2] . '</label>';
                    $html .= '</td>';
                    $html .= '<td colspan="3">';
                    $html .= '<input type="text" name="phone_' . $n . '" value="' . $subscriber['phone'] . '">';
                    $html .= '</td>';
                    $html .= '</tr>';
                    $html .= '<tr>';
                    $html .= '<td>';
                    $html .= '<label>' . $labels[3] . '</label>';
                    $html .= '</td>';
                    $html .= '<td colspan="3">';
                    $html .= '<input type="text" name="email_' . $n . '" value="' . $subscriber['email'] . '">';
                    $html .= '</td>';
                    $html .= '</tr>';
                    $html .= '<tr>';
                    $html .= '<td>&nbsp;';
                    $html .= '</td>';
                    $html .= '<td colspan="3">';
                    $checked = isset($subscriber['newsletter']) && $subscriber['newsletter'] == '1' ? 'checked' : '';
                    $html .= '<input type="checkbox" name="newsletter_' . $n . '" value="yes" ' . $checked . '> ' . $newsletter_lbl;
                    $html .= '</td>';
                    $html .= '</tr>';
                    $html .= '</table>';

                    $html .= '</div>';

                    $n++;

                }
            }

            //Add participant
            $html .= '<div class="event-form-actions">';
            $html .= '<span>+ </span><button type="submit" class="event-form-link-btn" name="event-participant-add">' . $part_add_lbl . '</button>';
            $html .= '</div>';

            //Form actions
            $html .= '<div class="event-form-actions align-right">';
            $html .= '<button type="submit" class="event-form-action-btn" name="event-back-2">' . $prev_lbl . '</button>';
            $html .= '<button type="submit" class="event-form-action-btn" name="event-submit-2">' . $next_lbl . '</button>';
            $html .= '</div>';

            $html .= '</div>';
            $html .= '</form>';

            echo $html;
        }

        public static function render_company_details($data)
        {
            $html = '';
            global $pw_config;

            $pay_online_discount = $pw_config['payment_online_discount'];
            $pay_teaser = '';

            if ($data['event']['locale'] == 'NL') {
                $labels[0] = 'Betaalmethode';
                $labels[1] = 'Taal factuur';
                $labels[2] = 'Bedrijf';
                $labels[3] = 'Ter attentie van *';
                $labels[4] = 'Factuurkenmerk';
                $labels[5] = 'Straat *';
                $labels[6] = 'Nummer *';
                $labels[7] = 'Postcode *';
                $labels[8] = 'Plaats *';
                $labels[9] = 'Land *';
                $labels[10] = 'E-mailadres factuur *';
                $labels[11] = 'Actiecode';

                $payment[0] = 'Creditcard';
                $payment[1] = 'iDeal';
                $payment[2] = 'Factuur';

                if( $pay_online_discount > 0 ) {
                    $pay_teaser = 'TIP: Bij betaling via Creditcard of iDeal ontvangt u &euro; '.number_format( $pay_online_discount, 2, ',', '.').' korting.';
                }

                $language[0] = 'Nederlands';
                $language[1] = 'Engels';

                $prev_lbl = '&laquo; Terug';
                $next_lbl = 'Volgende &raquo;';

                $validate_lbl = 'Code controleren';
                $code_invalid = 'Deze code is ongeldig';
                $code_discount = 'Korting:';
                $code_nr_available = 'Aantal beschikbaar:';

                $error['missing_invoice_attention'] = 'Ter attentie van ontbreekt';
                $error['missing_street'] = 'Straat ontbreekt';
                $error['missing_street_nr'] = 'Nummer ontbreekt';
                $error['missing_zip'] = 'Postcode ontbreekt';
                $error['missing_city'] = 'Plaats ontbreekt';
                $error['missing_country'] = 'Land ontbreekt';
                $error['missing_invoice_email'] = 'E-mailadres factuur ontbreekt';
                $error['invalid_invoice_email'] = 'E-mailadres factuur is ongeldig';

            } elseif ($data['event']['locale'] == 'DE') {
                $labels[0] = 'Zahlungsmethode';
                $labels[1] = 'Rechnungssprache';
                $labels[2] = 'Unternehmen';
                $labels[3] = 'Adressiert an *';
                $labels[4] = 'Deine Rechnungskennziffer';
                $labels[5] = 'Straße *';
                $labels[6] = 'Nummer *';
                $labels[7] = 'PLZ *';
                $labels[8] = 'Stadt *';
                $labels[9] = 'Land *';
                $labels[10] = 'Rechnungsemail *';
                $labels[11] = 'Aktion Code';

                $payment[0] = 'Creditcard';
                $payment[1] = 'iDeal';
                $payment[2] = 'Rechnung';

                if( $pay_online_discount > 0 ) {
                    $pay_teaser = 'Tipp: Wenn Sie per Kreditkarte oder iDeal zahlen, erhalten Sie einen Rabatt in Höhe von &euro; '.number_format( $pay_online_discount, 2, ',', '.').'.';
                }

                $language[0] = 'Niederländisch';
                $language[1] = 'Englisch';

                $prev_lbl = '&laquo; Zurück';
                $next_lbl = 'Nächsten &raquo;';

                $validate_lbl = 'Bestätige Code';
                $code_invalid = 'Dieser Code ist ungültig';
                $code_discount = 'Rabatt:';
                $code_nr_available = 'Nr verfügbar:';

                $error['missing_invoice_attention'] = 'Adressiert an ist notwendig';
                $error['missing_street'] = 'Straße ist notwendig';
                $error['missing_street_nr'] = 'Nummer ist notwendig';
                $error['missing_zip'] = 'PLZ ist notwendig';
                $error['missing_city'] = 'Stadt ist notwendig';
                $error['missing_country'] = 'Land ist notwendig';
                $error['missing_invoice_email'] = 'Rechnungsemail ist notwendig';
                $error['invalid_invoice_email'] = 'Rechnungsemail ist ungültige ';

            } else {
                $labels[0] = 'Payment method';
                $labels[1] = 'Language invoice';
                $labels[2] = 'Company';
                $labels[3] = 'Address to *';
                $labels[4] = 'Your invoice mark';
                $labels[5] = 'Street *';
                $labels[6] = 'Number *';
                $labels[7] = 'Zip *';
                $labels[8] = 'City *';
                $labels[9] = 'Country *';
                $labels[10] = 'Email invoice *';
                $labels[11] = 'Action code';

                $payment[0] = 'Creditcard';
                $payment[1] = 'iDeal';
                $payment[2] = 'Invoice';

                if( $pay_online_discount > 0 ) {
                    $pay_teaser = 'TIP: When you pay via Creditcard or iDeal you will recieve a discount of &euro; '.number_format( $pay_online_discount, 2, ',', '.').'.';
                }

                $language[0] = 'Dutch';
                $language[1] = 'English';

                $prev_lbl = '&laquo; Back';
                $next_lbl = 'Next &raquo;';

                $validate_lbl = 'Validate code';
                $code_invalid = 'This code is invalid';
                $code_discount = 'Discount:';
                $code_nr_available = 'Nr available:';

                $error['missing_invoice_attention'] = 'Address to is missing';
                $error['missing_street'] = 'Street is missing';
                $error['missing_street_nr'] = 'Number is missing';
                $error['missing_zip'] = 'Zip is missing';
                $error['missing_city'] = 'City is missing';
                $error['missing_country'] = 'Country is missing';
                $error['missing_invoice_email'] = 'Email invoice is missing';
                $error['invalid_invoice_email'] = 'Email invoice is not valid';

            }

            $html .= '<form method="post">';
            $html .= '<div class="event-form">';

            $subscription = $data['subscription'];

            // Prepare data
            $subscription['payment_method'] = stripslashes($subscription['payment_method']);
            $subscription['invoice_lang'] = stripslashes($subscription['invoice_lang']);
            $data['event']['locale'] = stripslashes( $data['event']['locale']);
            $subscription['company'] = stripslashes($subscription['company']);
            $subscription['invoice_attention'] = stripslashes($subscription['invoice_attention']);
            $subscription['invoice_attribute'] = stripslashes($subscription['invoice_attribute']);
            $subscription['street'] = stripslashes($subscription['street']);
             $subscription['street_nr'] = stripslashes($subscription['street_nr']);
            $subscription['zip'] = stripslashes($subscription['zip']);
            $subscription['city'] = stripslashes($subscription['city']);
            $subscription['country'] = stripslashes($subscription['country']);
            $subscription['invoice_email'] = stripslashes($subscription['invoice_email']);
            $subscription['invoice_action_code'] = stripslashes($subscription['invoice_action_code']);

            $action_code_result = "";
            if( isset( $subscription['action_code_result'] )) {

                $action_code = $subscription['action_code_result'];

                if( $action_code['is_valid'] == 0 ) {
                    $action_code_result = $code_invalid.'<br>';
                } else {

                    if( $action_code['discount_percent'] > 0 ) {
                        $action_code_result = $code_discount.' '.$action_code['discount_percent'].' %<br>';
                    } else {
                        $action_code_result = $code_discount.' &euro; '.$action_code['discount_euro'].'<br>';
                    }

                    if( $action_code['limit_nr_use'] == 1 ) {
                        if( isset($action_code['nr_use_left']) ) {
                            $action_code_result .= $code_nr_available.' '.$action_code['nr_use_left'].'<br>';
                        } else {
                            $action_code_result .= $code_nr_available.' 0<br>';
                        }

                    }

                }

            }

            $html .= '<table class="subscribe-company-details">';

            //Display errors when they are there
            if (isset($data['subscription_error']) && is_array( $data['subscription_error']) ) {
                $e = "";
                foreach ($data['subscription_error'] as $value) {
                    if( $value <> "" ) {
                        $e .= $error[$value] . '<br>';
                    }

                }

                if( $e <> "") {
                    $html .= '';
                    $html .= '<tr>';
                    $html .= '<td width="200">';
                    $html .= '&nbsp;';
                    $html .= '</td>';
                    $html .= '<td>';
                    $html .= '<div class="error">' . $e . '</div>';
                    $html .= '</td>';
                    $html .= '</tr>';
                }
            }

            if ($data['fieldset'] == 'payed') {
                $html .= '<tr>';
                $html .= '<td width="200">';
                $html .= '<label>' . $labels[0] . '</label>';
                $html .= '</td>';
                $html .= '<td>';

                $checked[0] = $subscription['payment_method'] == 'creditcard' || $subscription['payment_method'] == '' ? 'checked' : "";
                $checked[1] = $subscription['payment_method'] == 'ideal' ? 'checked' : "";
                $checked[2] = $subscription['payment_method'] == 'invoice' ? 'checked' : "";

                $html .= '<input type="radio" name="payment_method" value="creditcard" '.$checked[0].'> ' . $payment[0] . '
                      <input type="radio" name="payment_method" value="ideal" '.$checked[1].'> ' . $payment[1] . '
                      <input type="radio" name="payment_method" value="invoice" '.$checked[2].'> ' . $payment[2];

                if( $pay_teaser <> '' ) {
                    $html .= '<br><br><i>'.$pay_teaser.'</i>';
                }

                $html .= '</td>';
                $html .= '</tr>';

                $html .= '<tr>';
                $html .= '<td width="200">';
                $html .= '<label>' . $labels[1] . '</label>';
                $html .= '</td>';
                $html .= '<td>';

                if( strtolower( $pw_config['lang_primary']) == 'nl' ) {
                    if( $subscription['invoice_lang'] == 'nl' || $subscription['invoice_lang'] == 'en' ) {
                        $checked[0] = $subscription['invoice_lang'] == 'nl' ? 'checked' : "";
                        $checked[1] = $subscription['invoice_lang'] == 'en' ? 'checked' : "";
                    } else {
                        $checked[0] = $data['event']['locale'] == 'NL' ? 'checked' : "";
                        $checked[1] = $data['event']['locale'] == 'EN' ? 'checked' : "";
                    }

                    $html .= '<input type="radio" name="invoice_lang" value="nl" '.$checked[0].'> ' . $language[0] . '
                      <input type="radio" name="invoice_lang" value="en" '.$checked[1].'> ' . $language[1];

                } else if( strtolower( $pw_config['lang_primary']) == 'de' ) {
                    if ($subscription['invoice_lang'] == 'de' || $subscription['invoice_lang'] == 'en') {
                        $checked[0] = $subscription['invoice_lang'] == 'de' ? 'checked' : "";
                        $checked[1] = $subscription['invoice_lang'] == 'en' ? 'checked' : "";
                    } else {
                        $checked[0] = $data['event']['locale'] == 'DE' ? 'checked' : "";
                        $checked[1] = $data['event']['locale'] == 'EN' ? 'checked' : "";
                    }

                    $html .= '<input type="radio" name="invoice_lang" value="de" ' . $checked[0] . '> ' . $language[0] . '
                      <input type="radio" name="invoice_lang" value="en" ' . $checked[1] . '> ' . $language[1];
                }

                $html .= '</td>';
                $html .= '</tr>';
            }

            $html .= '<tr>';
            $html .= '<td width="200">';
            $html .= '<label>' . $labels[2] . '</label>';
            $html .= '</td>';
            $html .= '<td>';
            $html .= '<input type="text" name="company" class="large" value="' . $subscription['company'] . '">';
            $html .= '</td>';
            $html .= '</tr>';

            if ($data['fieldset'] == 'payed') {
                $html .= '<tr>';
                $html .= '<td>';
                $html .= '<label>' . $labels[3] . '</label>';
                $html .= '</td>';
                $html .= '<td>';
                $html .= '<input type="text" name="invoice_attention" class="large" value="' . $subscription['invoice_attention'] . '">';
                $html .= '</td>';
                $html .= '</tr>';


                $html .= '<tr>';
                $html .= '<td>';
                $html .= '<label>' . $labels[4] . '</label>';
                $html .= '</td>';
                $html .= '<td>';
                $html .= '<input type="text" name="invoice_attribute" class="medium" value="' . $subscription['invoice_attribute'] . '">';
                $html .= '</td>';
                $html .= '</tr>';
            }

            $html .= '<tr>';
            $html .= '<td>';
            $html .= '<label>' . $labels[5] . '</label>';
            $html .= '</td>';
            $html .= '<td>';
            $html .= '<input type="text" name="street" class="large" value="' . $subscription['street'] . '">';
            $html .= '</td>';
            $html .= '</tr>';

            $html .= '<tr>';
            $html .= '<td>';
            $html .= '<label>' . $labels[6] . '</label>';
            $html .= '</td>';
            $html .= '<td>';
            $html .= '<input type="text" name="street_nr" class="small" value="' . $subscription['street_nr'] . '">';
            $html .= '</td>';
            $html .= '</tr>';

            $html .= '<tr>';
            $html .= '<td>';
            $html .= '<label>' . $labels[7] . '</label>';
            $html .= '</td>';
            $html .= '<td>';
            $html .= '<input type="text" name="zip" class="small" value="' . $subscription['zip'] . '">';
            $html .= '</td>';
            $html .= '</tr>';

            $html .= '<tr>';
            $html .= '<td>';
            $html .= '<label>' . $labels[8] . '</label>';
            $html .= '</td>';
            $html .= '<td>';
            $html .= '<input type="text" name="city" class="medium" value="' . $subscription['city'] . '">';
            $html .= '</td>';
            $html .= '</tr>';

            $html .= '<tr>';
            $html .= '<td>';
            $html .= '<label>' . $labels[9] . '</label>';
            $html .= '</td>';
            $html .= '<td>';
            $html .= '<input type="text" name="country" class="medium" value="' . $subscription['country'] . '">';
            $html .= '</td>';
            $html .= '</tr>';

            if( $data['fieldset'] == 'payed') {
                $html .= '<tr>';
                $html .= '<td>';
                $html .= '<label>' . $labels[10] . '</label>';
                $html .= '</td>';
                $html .= '<td>';
                $html .= '<input type="text" name="invoice_email" class="medium" value="' . $subscription['invoice_email'] . '">';
                $html .= '</td>';
                $html .= '</tr>';

                $html .= '<tr>';
                $html .= '<td>';
                $html .= '<label>' . $labels[11] . '</label><a name="actioncode"></a>';
                $html .= '</td>';
                $html .= '<td>';
                $html .= '<input type="text" name="invoice_action_code" class="medium" value="' . $subscription['invoice_action_code'] . '"><br>';
                $html .= '<div id="action-code-result">'.$action_code_result.'</div><button type="submit" name="action-code-validate">'.$validate_lbl.'</button>';
                $html .= '</td>';
                $html .= '</tr>';
            }

            $html .= '</table>';

            //Form actions
            $html .= '<div class="event-form-actions align-right">';
            $html .= '<button type="submit" class="event-form-action-btn" name="event-back-3">' . $prev_lbl . '</button>';
            $html .= '<button type="submit" class="event-form-action-btn" name="event-submit-3">' . $next_lbl . '</button>';
            $html .= '</div>';

            $html .= '</div>';
            $html .= '</form>';

            echo $html;
        }

        public static function render_other($data)
        {
            $html = '';
            global $pw_config;

            if ($data['event']['locale'] == 'NL') {
                $labels[0] = 'Hoe heb je ons leren kennen *';
                $labels[1] = 'Indien anders';
                $labels[2] = 'Dieetwensen';
                $labels[3] = 'Opmerkingen';
                $labels[4] = 'Algemene voorwaarden *';

                //TODO make these options configurable in the backend
                $know_options[0] = '- Maak een keuze -';
                $know_options[1] = 'Via Google';
                $know_options[2] = 'Via de website';
                $know_options[3] = 'Via BNR';
                $know_options[4] = 'Via de nieuwsbrief';
                $know_options[5] = 'Via Twitter';
                $know_options[6] = 'Via Linkedin';
                $know_options[7] = 'Via een Prowareness medewerker';
                $know_options[8] = "Via collega's";
                $know_options[9] = 'Via een Prowareness event';
                $know_options[10] = 'Via een Prowareness training';
                $know_options[11] = 'Via een conferentie';
                $know_options[12] = 'Via Scrum.org';
                $know_options[13] = 'Anders...';

                if( $pw_config['url_term_cond']['nl'] == '' ) {
                    $agree_terms = 'Ja, ik ga akkoord met de algemene voorwaarden';
                } else {
                    $agree_terms = 'Ja, ik ga akkoord met de <a href="'.$pw_config['url_term_cond']['nl'].'" target="_new">algemene voorwaarden</a>';
                }

                $prev_lbl = '&laquo; Terug';
                $next_lbl = 'Volgende &raquo;';

                $error['missing_know_how'] = 'Hoe heb je ons leren kennen ontbreekt';
                $error['missing_know_how_else'] = 'Indien anders ontbreekt';
                $error['missing_agree_terms'] = 'Akkoord met algemene voorwaarden ontbreekt';

            } elseif ($data['event']['locale'] == 'DE') {

                $labels[0] = 'Wie haben Sie uns gefunden *';
                $labels[1] = 'Oder anders';
                $labels[2] = 'Ernährungsweise';
                $labels[3] = 'Bemerkungen';
                $labels[4] = 'Geschäftsbedingungen *';

                //TODO make these options configurable in the backend
                $know_options[0] = '- Treffen Sie eine Wahl -';
                $know_options[1] = 'Via Google';
                $know_options[2] = 'Via Webseite';
                $know_options[3] = 'Via einem Prowareness Mitarbeiter';
                $know_options[4] = 'Via Freunde';
                $know_options[5] = "Via Kollegen";
                $know_options[6] = 'Via dem Newsletter';
                $know_options[7] = 'Via Twitter';
                $know_options[8] = 'Via Linkedin';
                $know_options[9] = 'Via BNR Raio Kampagne';
                $know_options[10] = 'Via sdn.nl';
                $know_options[11] = 'Via Improve';
                $know_options[12] = 'Via Agile+ Market';
                $know_options[13] = 'Andere…';

                if( $pw_config['url_term_cond']['de'] == '' ) {
                    $agree_terms = 'Ja, ich stimme den Geschäftsbedingungen zu';
                } else {
                    $agree_terms = 'Ja, ich stimme den <a href="'.$pw_config['url_term_cond']['de'].'" target="_new">Geschäftsbedingungen</a> zu';
                }

                $prev_lbl = '&laquo; Zurück';
                $next_lbl = 'Nächsten &raquo;';

                $error['missing_know_how'] = 'Wie haben Sie uns gefunden fehlt';
                $error['missing_know_how_else'] = 'Wenn anders fehlt';
                $error['missing_agree_terms'] = 'Zustimmmung zu Geschäftsbedingungen fehlt';


            } else {
                $labels[0] = 'How did you learn about us *';
                $labels[1] = 'When other';
                $labels[2] = 'Wished for diet';
                $labels[3] = 'Remarks';
                $labels[4] = 'Terms & Conditions *';

                //TODO make these options configurable in the backend
                $know_options[0] = '- Make a choice -';
                $know_options[1] = 'Via Google';
                $know_options[2] = 'Via the website';
                $know_options[3] = 'Via a Prowareness employee';
                $know_options[4] = 'Via friends';
                $know_options[5] = "Via colleagues";
                $know_options[6] = 'Via the newsletter';
                $know_options[7] = 'Via Twitter';
                $know_options[8] = 'Via Linkedin';
                $know_options[9] = 'Via BNR radio campaign';
                $know_options[10] = 'Via sdn.nl';
                $know_options[11] = 'Via Improve';
                $know_options[12] = 'Via Agile+ Market';
                $know_options[13] = 'Other...';

                if( $pw_config['url_term_cond']['en'] == '' ) {
                    $agree_terms = 'Yes, I agree to the terms & Conditions';
                } else {
                    $agree_terms = 'Yes, I agree to the <a href="'.$pw_config['url_term_cond']['en'].'" target="_new">terms & Conditions</a>';
                }

                $prev_lbl = '&laquo; Back';
                $next_lbl = 'Next &raquo;';

                $error['missing_know_how'] = 'How did you learn about us is missing';
                $error['missing_know_how_else'] = 'When other is missing';
                $error['missing_agree_terms'] = 'Agreement with terms & conditions is missing';

            }

            $know_options = array();
            $know_options = $data['eventKnowHow'];
            if(!$know_options) {
                $know_options = self::know_how_options();
                $know_options = $know_options['nl'];
            }
            $html .= '<form method="post">';
            $html .= '<div class="event-form">';

            $subscription = $data['subscription'];

            // Prepare data
            $subscription['know_how'] = stripslashes($subscription['know_how']);
            $subscription['know_how_else'] = stripslashes($subscription['know_how_else']);
            $subscription['diet'] = stripslashes($subscription['diet']);
            $subscription['comments'] = stripslashes($subscription['comments']);

            $html .= '<table>';

            //Display errors when they are there
            if (isset($data['subscription_error']) && is_array( $data['subscription_error']) ) {
                $e = "";
                foreach ($data['subscription_error'] as $value) {
                    if( $value <> "" ) {
                        $e .= $error[$value] . '<br>';
                    }

                }

                if( $e <> "") {
                    $html .= '';
                    $html .= '<tr>';
                    $html .= '<td width="200">';
                    $html .= '&nbsp;';
                    $html .= '</td>';
                    $html .= '<td>';
                    $html .= '<div class="error">' . $e . '</div>';
                    $html .= '</td>';
                    $html .= '</tr>';
                }
            }

            $html .= '<tr>';
            $html .= '<td width="200">';
            $html .= '<label>' . $labels[0] . '</label>';
            $html .= '</td>';
            $html .= '<td>';
            $html .= '<select name="know_how">';

            foreach( $know_options as $key => $value ) {
                if( $subscription['know_how'] == $value ) {
                    $html .= '<option value="'.$value.'" selected>'.$value.'</option>';
                } else {
                    $html .= '<option value="'.$value.'">'.$value.'</option>';
                }
            }

            $html .= '</select>';
            $html .= '</td>';
            $html .= '</tr>';

            $html .= '<tr>';
            $html .= '<td>';
            $html .= '<label>' . $labels[1] . '</label>';
            $html .= '</td>';
            $html .= '<td>';
            $html .= '<input type="text" name="know_how_else" class="large" value="' . $subscription['know_how_else'] . '">';
            $html .= '</td>';
            $html .= '</tr>';

            $html .= '<tr>';
            $html .= '<td>';
            $html .= '<label>' . $labels[2] . '</label>';
            $html .= '</td>';
            $html .= '<td>';
            $html .= '<textarea name="diet">'.$subscription['diet'].'</textarea>';
            $html .= '</td>';
            $html .= '</tr>';

            $html .= '<tr>';
            $html .= '<td>';
            $html .= '<label>' . $labels[3] . '</label>';
            $html .= '</td>';
            $html .= '<td>';
            $html .= '<textarea name="comments">'.$subscription['comments'].'</textarea>';
            $html .= '</td>';
            $html .= '</tr>';

            $html .= '<tr>';
            $html .= '<td>';
            $html .= '<label>' . $labels[4] . '</label>';
            $html .= '</td>';
            $html .= '<td>';

            $checked = $subscription['agree_terms'] == '1' ? 'checked' : '';
            $html .= '<input type="checkbox" name="agree_terms" value="1" '.$checked.'> '.$agree_terms;
            $html .= '</td>';
            $html .= '</tr>';

            $html .= '</table>';

            //Form actions
            $html .= '<div class="event-form-actions align-right">';
            $html .= '<button type="submit" class="event-form-action-btn" name="event-back-4">' . $prev_lbl . '</button>';
            $html .= '<button type="submit" class="event-form-action-btn" name="event-submit-4">' . $next_lbl . '</button>';
            $html .= '</div>';

            $html .= '</div>';
            $html .= '</form>';

            echo $html;
        }

        public static function render_summary($data)
        {
            global $pw_config;
            $html = "";

            $lang = strtolower( $data['event']['locale'] ) ;
            $date_format = $pw_config['date_format'][ $lang ];

            if ($data['event']['locale'] == 'NL') {

                $introduction_free = 'Controleer je gegevens en klik op de knop <b>Inschrijving afronden</b> om je inschrijving in te sturen.';
                $introduction_payed = 'Controleer je gegevens en klik op de knop <b>Inschrijving afronden</b> om je inschrijving in te sturen. '.
                    'Je wordt vervolgens doorgeleid naar de betaling/facuur.';

                $labels[0] = 'Inschrijving voor';
                $labels[1] = 'Deelnemer(s)';
                $labels[2] = 'Bedrijfsgegevens';
                $labels[3] = 'Factuurgegevens';
                $labels[4] = 'Dieet';
                $labels[5] = 'Opmerkingen';
                $labels[6] = 'Algemene voorwaarden';
                $labels[7] = 'Kosten';
                $labels[8] = 'Betaalmethode';

                $payment[0] = 'Creditcard';
                $payment[1] = 'iDeal';
                $payment[2] = 'Factuur';

                $invoice_lbl[0] = 'Ter attentie van: ';
                $invoice_lbl[1] = 'Factuurkenmerk: ';
                $invoice_lbl[2] = 'Stuur factuur aan: ';

                //TODO Add link to T&C
                $terms_lbl = 'Ja, ik ga akkoord met de algemene voorwaarden';

                $sub_total_lbl = 'Subtotaal';
                $discount_lbl = 'Korting';
                $vat_lbl = 'BTW';
                $total_lbl = 'Totaal';

                $prev_lbl = '&laquo; Terug';
                $next_lbl = 'Inschrijving afronden';

                $dec_point = ',';
                $thousands_sep = '.';

            } elseif ($data['event']['locale'] == 'DE') {

                $introduction_free = 'Bitte überprüfen Sie die Details unten und Klicke <b>Bestätige Anmeldung </b> um Ihre Anmeldung zu speichern.';
                $introduction_payed = 'Bitte überprüfen Sie die Details unten und Klicke <b>Bestätige Anmeldung </b> um Ihre Anmeldung zu speichern. '.
                    'Sie werden zum Zahlungsprozesse/Rechnung weitergeleitet.';

                $labels[0] = 'Anmeldung für';
                $labels[1] = 'Teilnehmer';
                $labels[2] = 'Unternehmensdetails';
                $labels[3] = 'Rechnungsdetails';
                $labels[4] = 'Ernährungsweise';
                $labels[5] = 'Bemerkungen';
                $labels[6] = 'Geschäftsbedingungen';
                $labels[7] = 'Kosten';
                $labels[8] = 'Zahlungsmethode';

                $payment[0] = 'Creditcard';
                $payment[1] = 'iDeal';
                $payment[2] = 'Rechnung';

                $invoice_lbl[0] = 'Adressiert an: ';
                $invoice_lbl[1] = 'Deine Rechnungskennziffer: ';
                $invoice_lbl[2] = 'Rechnungsemail an: ';

                //TODO Add link to T&C
                $terms_lbl = 'Ja, ich stimme den Geschäftsbedingungen zu';

                $sub_total_lbl = 'Zwischensumme Total';
                $discount_lbl = 'Rabatt';
                $vat_lbl = 'VAT';
                $total_lbl = 'Total';

                $prev_lbl = '&laquo; Zurück';
                $next_lbl = 'Bestätige Anmeldung';

                $dec_point = ',';
                $thousands_sep = '.';

            } else {

                $introduction_free = 'Please check the details below and click <b>Confirm subscription</b> to save your subscription.';
                $introduction_payed = 'Please check the details below and click <b>Confirm subscription</b> to save your subscription. '.
                    'You will be redirected to the payment process/invoice.';

                $labels[0] = 'Subscription for';
                $labels[1] = 'Participant(s)';
                $labels[2] = 'Company details';
                $labels[3] = 'Invoice details';
                $labels[4] = 'Diet';
                $labels[5] = 'Remarks';
                $labels[6] = 'Terms & Conditions';
                $labels[7] = 'Costs';
                $labels[8] = 'Payment method';

                $invoice_lbl[0] = 'Address to: ';
                $invoice_lbl[1] = 'Your invoice mark: ';
                $invoice_lbl[2] = 'Email invoice to: ';

                $payment[0] = 'Creditcard';
                $payment[1] = 'iDeal';
                $payment[2] = 'Invoice';

                //TODO Add link to T&C
                $terms_lbl = 'Yes, I agree with the terms & conditions';

                $sub_total_lbl = 'Sub total';
                $discount_lbl = 'Discount';
                $vat_lbl = 'VAT';
                $total_lbl = 'Total';

                $prev_lbl = '&laquo; Back';
                $next_lbl = 'Confirm subscription';

                $dec_point = '.';
                $thousands_sep = ',';

            }

            $html .= '<form method="post">';
            $html .= '<div class="event-form">';

            $subscription = $data['subscription'];

            // Prepare some data
            $subscription['company'] = stripslashes($subscription['company']);
            $subscription['invoice_attention'] = stripslashes($subscription['invoice_attention']);
            $subscription['street'] = stripslashes($subscription['street']);
            $subscription['street_nr'] = stripslashes($subscription['street_nr']);
            $subscription['zip'] = stripslashes($subscription['zip']);
            $subscription['city'] = stripslashes($subscription['city']);
            $subscription['country']  = stripslashes($subscription['country']);

            $date_details = $subscription['selected_date_details'];

            $price = $date_details['real_price'];

            $subscribers = array();
            foreach( $data['subscribers'] as $key => $subscriber) {
                $name = $subscriber['first_name'].' '.$subscriber['middle_name'].' '.$subscriber['last_name'];
                $subscribers[] = preg_replace('/\s+/', ' ', $name);
            }

            $invoice_details = $subscription['company'] <> '' ? $subscription['company'].'<br>' : '';

            if( $data['fieldset'] == 'payed') {
                $invoice_details .= $invoice_lbl[0].' '.$subscription['invoice_attention'].'<br>';
            }

            $invoice_details .= $subscription['street'].' '.$subscription['street_nr'].'<br>';
            $invoice_details .= $subscription['zip'].', '.$subscription['city'].'<br>';
            $invoice_details .= $subscription['country'].'<br>';
            $invoice = '';

            if( $data['fieldset'] == 'payed') {

                // Prepare data
                $subscription['invoice_attribute'] = stripslashes($subscription['invoice_attribute']);
                $subscription['invoice_email'] = stripslashes( $subscription['invoice_email'] );

                $invoice_details .= $invoice_lbl[1].' '.$subscription['invoice_attribute'].'<br>';
                $invoice_details .= $invoice_lbl[2].' '.$subscription['invoice_email'].'<br>';

                $invoice = $subscription['invoice'];
            }

            if( $data['fieldset'] == 'payed') {
                $html .= '<p class="subscribe-summary-intro">'.$introduction_payed.'</p>';
            } else {
                $html .= '<p class="subscribe-summary-intro">'.$introduction_free.'</p>';
            }

            $html .= '<table>';

            $data['event']['name'] = stripslashes($data['event']['name']);
            $date_details['location']['location_name'] = stripslashes($date_details['location']['location_name']);

            $html .= '<tr>';
            $html .= '<td width="200">';
            $html .= '<label>'.$labels[0].'</label>';
            $html .= '</td>';
            $html .= '<td>';
            $html .=  $data['event']['name'].'<br>';

            $html .=  date( $date_format, strtotime($date_details['start_date'] )).' '.
                date( 'H:i', strtotime($date_details['start_time'] )).' - '.
                date( 'H:i', strtotime($date_details['stop_time'] )).'<br>';

            $html .= $date_details['location']['location_name'];
            $html .= '</td>';
            $html .= '</tr>';

            $html .= '<tr>';
            $html .= '<td>';
            $html .= '<label>'.$labels[1].'</label>';
            $html .= '</td>';
            $html .= '<td>';
            $html .= stripslashes( implode( '<br>', $subscribers) );
            $html .= '</td>';
            $html .= '</tr>';

            $html .= '<tr>';
            $html .= '<td>';

            if( $data['fieldset'] == 'payed') {
                $html .= '<label>'.$labels[3].'</label>';
            } else {
                $html .= '<label>'.$labels[2].'</label>';
            }
            $html .= '</td>';
            $html .= '<td>';
            $html .= $invoice_details;
            $html .= '</td>';
            $html .= '</tr>';

            $html .= '<tr>';
            $html .= '<td>';
            $html .= '<label>'.$labels[4].'</label>';
            $html .= '</td>';
            $html .= '<td>';
            $html .= $subscription['diet'] == '' ? '-<br>' : stripslashes( $subscription['diet'] ).'<br>';
            $html .= '</td>';
            $html .= '</tr>';

            $html .= '<tr>';
            $html .= '<td>';
            $html .= '<label>'.$labels[5].'</label>';
            $html .= '</td>';
            $html .= '<td>';
            $html .= $subscription['comments'] == '' ? '-<br>' : stripslashes( $subscription['comments'] ).'<br>';
            $html .= '</td>';
            $html .= '</tr>';

            $html .= '<tr>';
            $html .= '<td>';
            $html .= '<label>'.$labels[6].'</label>';
            $html .= '</td>';
            $html .= '<td>';
            $html .= $terms_lbl;
            $html .= '</td>';
            $html .= '</tr>';

            if( $data['fieldset'] == 'payed') {
                $html .= '<tr>';
                $html .= '<td>';
                $html .= '<label>'.$labels[7].'</label>';
                $html .= '</td>';
                $html .= '<td>';

                $html .= '<label class="costs">'.$sub_total_lbl.'</label>'.$invoice['nr_subscribers'].' * &euro; '.number_format( $invoice['real_price'], 2, $dec_point, $thousands_sep).'<br>';

                if( $invoice['discount_total'] > 0 ) {
                    $html .= '<label class="costs">'.$discount_lbl.'</label>'.$invoice['nr_discount'].' * '. $invoice['discount_amount_msg'] .'<br>';
                }

                if( $invoice['pay_online_discount'] > 0 ) {
                    $html .= '<label class="costs">'.$discount_lbl.'</label>&euro; '.number_format( $invoice['pay_online_discount'], 2, $dec_point, $thousands_sep) .'<br>';
                }

                $html .= '<label class="costs">'.$vat_lbl.'</label>'.$invoice['vat'].' %<br>';
                $html .= '<label class="costs">'.$total_lbl.'</label>&euro; '.number_format( $invoice['total'], 2, $dec_point, $thousands_sep).'<br>';
                $html .= '</td>';
                $html .= '</tr>';

                $html .= '<tr>';
                $html .= '<td>';
                $html .= '<label>'.$labels[8].'</label>';
                $html .= '</td>';
                $html .= '<td>';

                if( $subscription['payment_method'] == 'creditcard' ) {
                    $html .= $payment[0];
                } else if( $subscription['payment_method'] == 'ideal' ) {
                    $html .= $payment[1];
                } else {
                    $html .= $payment[2];
                }

                $html .= '</td>';
                $html .= '</tr>';

            }

            $html .= '</table>';

            //Form actions
            $html .= '<div class="event-form-actions align-right">';
            $html .= '<button type="submit" class="event-form-action-btn" name="event-back-5">' . $prev_lbl . '</button>';
            $html .= '<button type="submit" class="event-form-action-btn" name="event-submit-5">' . $next_lbl . '</button>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</form>';

            echo $html;
        }

        public static function render_done( $data ) {
            $html = "";

            if ($data['event']['locale'] == 'NL') {
                $done_msg = 'Bedankt voor uw inschrijving.';

            } elseif ($data['event']['locale'] == 'DE') {
                $done_msg = 'Danke für Ihre Anmeldung.';
            } else {
                $done_msg = 'Thank you for your subscription.';
            }

            $html .= '<form method="post">';
            $html .= '<div class="event-form">';
            $html .= '<p>'.$done_msg.'</p>';
            $html .= '</div>';
            $html .= '</form>';

            echo $html;
        }

        public static function render($data, $session)
        {
            $html = "";

            if (is_array($data)) {

                $lang = strtolower($data['event']['locale']);

                echo '<h3 class="subscribe-title">' . stripslashes( $data['event']['name'] ) . '</h3>';

                //Render tabs
                feEventSubscribeHtmlView::render_tabs($data['tabs'], $data['active_tab_id']);

                if( isset($session['payment_done']) && $session['payment_done'] == '1' ) {

                    feEventSubscribeHtmlView::render_done($data);

                } else {
                    switch ($data['active_tab_id']) {
                        case 1:
                            $incompany = $data['event']['incompany'];
                            $planning = $data['event']['planning'];
                            $duration = $data['event']['duration'];

                            feEventSubscribeHtmlView::render_select_date($incompany, $planning, $data['selected_date_id'], $duration, $lang);
                            break;
                        case 2:
                            feEventSubscribeHtmlView::render_add_subscribers($data);
                            break;
                        case 3:
                            feEventSubscribeHtmlView::render_company_details($data);
                            break;
                        case 4:
                            feEventSubscribeHtmlView::render_other($data);
                            break;
                        case 5:
                            feEventSubscribeHtmlView::render_summary($data);
                            break;
                        default:
                            echo "<p>An error occurred</p>";
                    }
                }


            };

            echo $html;

        }
    }

endif;

?>