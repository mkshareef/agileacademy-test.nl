<?php

if( !class_exists( 'subscriptionsDownloadHtmlView' ) ):

    class subscriptionsDownloadHtmlView
    {

        public static function render( $title, $data )
        {

            ?>

            <div class="wrap">
                <h2>
                    <?php echo( stripslashes( $title ) ) ?>
                </h2>
                <p class="description">
                </p>

                <form method="POST" name="formSubscriptionEdit" action="<?php $_SERVER['REQUEST_URI'] ?>" enctype="multipart/form-data">
                    <p></p>
                    <div class="pw-form">
                    <table class="form-table">
                          <tr>
                            <th><?php  _e('.CSV file', 'pw-event-manager') ?></th>
                            <td>
                                <a href="<?php echo $data['file_url']; ?>" class="action-btn" target="_blank"><?php  _e('Click to download', 'pw-event-manager') ?></a>
                            </td>
                        </tr>
                    </table>
                    </div>
                    <p></p>
                    <table class="form-table">
                        <tr>
                            <td>
                                <a href="?page=pw-events-subscriptions" class="button button-primary">&laquo; <?php _e('Back', 'pw-event-manager') ?></a>
                            </td>
                        </tr>
                    </table>
                </form>

            </div>

        <?php

        }
    }

endif;

?>