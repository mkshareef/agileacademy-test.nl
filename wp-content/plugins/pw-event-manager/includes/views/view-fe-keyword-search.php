<?php

if( !class_exists( 'feKeywordSearchHtmlView' ) ):

    class feKeywordSearchHtmlView
    {

        public static function render( $data  )
        {

            ?>

            <div class="container-event-search">

                <form method="GET" action="<?php echo $data['result_page'] ?>">
                    <input type="hidden" id="search-page-id" value="<?php the_ID(); ?>">
                    <h2><?php echo stripslashes( $data['title'] ) ?></h2>
                    <input type="text" value="<?php echo isset( $data['search_event'] ) ? stripslashes( $data['search_event'] ) : "" ?>"
                           placeholder="<?php echo stripslashes( $data['search-place-holder'] ) ?>" name="search_event" id="search_keyword-input">
                    <div id="event-cat-autocomplete"></div>
                    <div id="event-search-cat-select">
                        <select name="category">
                            <?php
                                if( isset($data['categories'] )  ) {
                                    foreach( $data['categories'] as $key=>$rec ) {
                                        $selected = isset( $data['category_selected'] ) &&
                                            (int) $data['category_selected'] == (int) $rec['id'] ? 'selected' : "";
                                        echo '<option value="'.$rec['id'].'" '.$selected .'>'.stripslashes( $rec['group_name'] ).'</option>';
                                    }
                                } else {
                                    echo '<option value="0">-</option>';
                                }
                            ?>
                        </select>
                    </div>

                    <button type="submit"><?php echo $data['search-button'] ?> &raquo;</button>
                </form>

            </div>

        <?php

        }
    }

endif;

?>