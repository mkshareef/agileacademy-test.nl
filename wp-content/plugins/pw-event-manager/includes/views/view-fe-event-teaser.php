<?php

if( !class_exists( 'feEventTeaserHtmlView' ) ):

    class feEventTeaserHtmlView
    {

        public static function render( $data )
        {
            $html = "";

            if( isset($data['teaser']) && $data['teaser'] <> "" ) {
                $html = '<div class="show-event-teaser">';
                $html .= nl2br( stripslashes( $data['teaser'] ));
                $html .= '</div>';
            };

            echo $html;
        }
    }

endif;

?>