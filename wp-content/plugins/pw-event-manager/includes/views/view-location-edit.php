<?php

if( !class_exists( 'locationEditHtmlView' ) ):

    class locationEditHtmlView
    {

        public static function render( $title, $data )
        {
            global $pw_config;

            $locale = substr(get_locale(), 0, 2 );
            $date_format = $pw_config['date_format'][$locale].' H:i:s';

            $errors = array();
            if( isset($data['errors']) && is_array($data['errors']) ) {
                $errors = $data['errors'];
            }

            //Prepare data
            $data['updated_by'] = stripslashes( $data['updated_by'] );
            $data['locale'] = stripslashes($data['locale']);
            $data['location_name'] = stripslashes($data['location_name']);
            $data['maps_url'] = stripslashes($data['maps_url']);
            $data['profile'] = stripslashes( $data['profile'] );
            ?>

            <div class="wrap">
                <h2>
                    <?php echo( $title ) ?>
                </h2>
                <p class="description"><?php
                    echo __('Last update:', 'pw-event-manager').' '.date( $date_format ,strtotime( $data['updated'] )).' '.
                        __('by', 'pw-event-manager').' '.$data['updated_by'];
                    ?></p>
                <?php
                    if( ! empty($errors) ) {
                        echo('<p class="error">');
                        _e('The form contains errors', 'pw-event-manager');
                        echo('</p>');
                    };
                ?>
                <form method="POST" name="formLocationEdit" action="<?php $_SERVER['REQUEST_URI'] ?>" enctype="multipart/form-data">

                    <table class="form-table">
                        <tr>
                            <th><label for="locale"><?php  _e('Display language', 'pw-event-manager') ?></label></th>
                            <td>
                                <input type="radio" name="locale" value="NL"
                                    <?php echo ($data['locale']=='NL')?'checked':'' ?>
                                    >NL
                                <input type="radio" name="locale" value="EN"
                                    <?php echo ($data['locale']=='EN')?'checked':'' ?>
                                    >EN
                                <input type="radio" name="locale" value="DE"
                                    <?php echo ($data['locale']=='DE')?'checked':'' ?>
                                    >DE
                            </td>
                        </tr>
                        <tr>
                            <th><label for="location_name"><?php  _e('Location', 'pw-event-manager') ?> * </label></th>
                            <td><input type="text"  class="regular-text" name="location_name" placeholder="<?php _e('Name of location', 'pw-event-manager') ?>"
                                       value="<?php echo $data['location_name']; ?>">
                                <?php

                                if( isset( $errors['location_name']) ) {
                                    echo( '<p class="error">'.implode('<br>', $errors['location_name']).'</p>' );
                                }

                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="maps_url"><?php  _e('Google Maps embed code', 'pw-event-manager') ?></label></th>
                            <td>
                               <textarea name="maps_url" class="medium-text" cols="15" rows="7"
                                          placeholder="<?php _e('Embedding code to show Google Maps', 'pw-event-manager') ?>
                                "><?php echo $data['maps_url']; ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="route"><?php  _e('Route description', 'pw-event-manager') ?></label></th>
                            <td><input type="hidden" name="attachment_path" value="<?php echo $data['attachment_path']; ?>">
                                <?php
                                $fileFound = false;

                                if( $data['attachment_path'] <> "" ) {
                                    $href = "";
                                    $file_ref = util::get_file_path( $data['attachment_path'] );
                                    if( $file_ref ) {
                                        $href = '<a href="'.$file_ref.'" target="_new">'.$file_ref.'</a>';
                                        $fileFound = true;
                                        echo($href);
                                        echo('&nbsp;&nbsp;&nbsp;<input type="submit" class="button button-secondary" name="removeAttachmentLocation" value="');
                                        _e('Delete', 'pw-event-manager');
                                        echo('">');
                                    }
                                }

                                if( $fileFound == false ) {
                                    echo('<input type="file" name="location_file" id="location_file" />');
                                    echo('<p class="description">');
                                    _e('Add a PDF with the route', 'pw-event-manager');
                                    echo('.</p>');

                                    if( isset( $errors['location_file']) ) {
                                        echo( '<p class="error">'.implode('<br>', $errors['location_file']).'</p>' );
                                    }
                                }

                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th><?php  _e('Location details', 'pw-event-manager') ?> *</th>
                            <td><?php

                                $editor_id = "profile";
                                $content = $data['profile'];
                                $settings = array( 'media_buttons' => false, 'textarea_rows' => 14 );

                                if( isset( $errors['profile']) ) {
                                    echo( '<p class="error">'.implode('<br>', $errors['profile']).'</p>' );
                                }

                                wp_editor( $content, $editor_id, $settings );

                            ?></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" class="button button-primary" name="submitLocation" value="<?php  _e('Save', 'pw-event-manager') ?>">
                                <input type="submit" class="button button-primary-2" name="submitAndCloseLocation" value="<?php  _e('Save & close', 'pw-event-manager') ?>">
                                <input type="submit" class="button button-secondary" name="closeLocation" value="<?php  _e('Cancel', 'pw-event-manager') ?>">
                            </td>
                        </tr>
                    </table>

                </form>
            </div>

        <?php
        }
    }

endif;

?>