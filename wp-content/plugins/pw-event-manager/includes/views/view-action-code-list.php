<?php

if( !class_exists( 'acionCodeListHtmlView' ) ):


    class acionCodeListHtmlView
    {

        public static function render( $title="", $table="" )
        {
            ?>

            <div class="wrap">
                <h2>
                    <?php echo( $title ) ?>
                </h2>

                <?php

                    echo '<form method="POST" name="formActionCodeTable" id="form-action-code-table" action="' . $_SERVER['REQUEST_URI'] . '" enctype="multipart/form-data">';
                    empty($table) ? "" : $table->display();
                    echo('</form>');

                ?>
            </div>

        <?php
        }
    }

endif;

?>