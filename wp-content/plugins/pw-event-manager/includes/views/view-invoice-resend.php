<?php

if( !class_exists( 'invoiceResendHtmlView' ) ):

    class invoiceResendHtmlView
    {

        public static function render( $title, $data )
        {
            global $pw;

            $errors = array();
            if( isset($data['errors']) && is_array($data['errors']) ) {
                $errors = $data['errors'];
            }

            ?>

            <div class="wrap">
                <h2>
                    <?php echo( $title ) ?>
                </h2>
                <form method="POST" name="formInvoiceResend" action="<?php $_SERVER['REQUEST_URI'] ?>" enctype="multipart/form-data">
                    <table class="form-table">
                        <tr>
                            <th><label for="invoice_email"><?php  _e('Email', 'pw-event-manager') ?> * </label></th>
                            <td><input type="text"  class="regular-text" name="invoice_email" placeholder="<?php _e('example@domain.nl', 'pw-event-manager') ?>"
                                       value="<?php echo stripslashes( $data['invoice_email'] ); ?>">
                                <?php

                                if( isset( $errors['invoice_email']) ) {
                                    echo( '<p class="error">'.$errors['invoice_email'].'</p>' );
                                }

                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" class="button button-primary" name="submitInvoiceResend" value="<?php  _e('Send', 'pw-event-manager') ?>">
                                <input type="submit" class="button button-secondary" name="closeInvoiceResend" value="<?php  _e('Cancel', 'pw-event-manager') ?>">
                            </td>
                        </tr>
                    </table>

                </form>
            </div>

        <?php
        }
    }

endif;

?>