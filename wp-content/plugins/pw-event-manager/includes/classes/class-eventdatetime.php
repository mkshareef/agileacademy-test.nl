<?php
/**
 * Direct access to the script is not allowed
 */
defined('ABSPATH') or die("No script kiddies please!");

if( !class_exists( 'eventDateTime' ) ):

    class eventDateTime
    {

        public function __construct()
        {

        }

        static function is_validate_date($date, $format = 'Y-m-d H:i:s')
        {
            $d = DateTime::createFromFormat($format, $date);
            return $d && $d->format($format) == $date;
        }

        /**
         * @return string Display name of the current user
         */
        static function current_time_mysql() {
            $mysqltime = current_time("Y-m-d H:i:s");
            return $mysqltime;
        }

        static function mysql_to_display( $mysqldatetime ) {
            $phpdate = strtotime( $mysqldatetime );
            $dspdate = date( 'd M Y H:i:s', $phpdate );

            return $dspdate;
        }

        static function to_mysql_time( $source_format, $date ) {

            if( $date <> '' ) {
                if( strpos($source_format, '-') ) {
                    $d = explode('-', $date );
                    $d[0] = str_pad($d[0], 2, "0", STR_PAD_LEFT);
                    $d[1] = str_pad($d[1], 2, "0", STR_PAD_LEFT);
                    $date = implode('-', $d );
                } else if( strpos($source_format, '/') ) {
                    $d = explode('/', $date );
                    $d[0] = str_pad($d[0], 2, "0", STR_PAD_LEFT);
                    $d[1] = str_pad($d[1], 2, "0", STR_PAD_LEFT);
                    $date = implode('/', $d );
                } else if( strpos($source_format, '.') ) {
                    $d = explode('.', $date );
                    $d[0] = str_pad($d[0], 2, "0", STR_PAD_LEFT);
                    $d[1] = str_pad($d[1], 2, "0", STR_PAD_LEFT);
                    $date = implode('.', $d );
                }
            }

            //Correct for leading zero's
            if( eventDateTime::is_validate_date( $date, $source_format ) ) {
                $target_format = 'Y-m-d H:i:s';
                $date = DateTime::createFromFormat( $source_format, $date );
                return $date->format( $target_format );
            } else {
                return "";
            }

        }

        static function to_mysql_date( $source_format, $date ) {

            if( strpos($source_format, '-') ) {
                $d = explode('-', $date );
                $d[0] = str_pad($d[0], 2, "0", STR_PAD_LEFT);
                $d[1] = str_pad($d[1], 2, "0", STR_PAD_LEFT);
                $date = implode('-', $d );
            } else if( strpos($source_format, '/') ) {
                $d = explode('/', $date );
                $d[0] = str_pad($d[0], 2, "0", STR_PAD_LEFT);
                $d[1] = str_pad($d[1], 2, "0", STR_PAD_LEFT);
                $date = implode('/', $d );
            } else if( strpos($source_format, '.') ) {
                $d = explode('.', $date );
                $d[0] = str_pad($d[0], 2, "0", STR_PAD_LEFT);
                $d[1] = str_pad($d[1], 2, "0", STR_PAD_LEFT);
                $date = implode('.', $d );
            }

            //Correct for leading zero's
            if( eventDateTime::is_validate_date( $date, $source_format ) ) {
                $target_format = 'Y-m-d';
                $date = DateTime::createFromFormat( $source_format, $date );
                return $date->format( $target_format );
            } else {
                return "";
            }

        }

    }

endif;