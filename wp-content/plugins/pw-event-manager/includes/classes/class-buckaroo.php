<?php

defined('ABSPATH') or die("No script kiddies please!");

/*

    Numbers to use for testing can be found at:

    http://support.buckaroo.nl/index.php/Testgegevens

 */

if( !class_exists( 'Buckaroo' ) ):

    class Buckaroo
    {

        protected $test_mode = false;
        protected $website_key = "";      //Unique Buckaroo key of the website

        private $secret_key = "";

        private $live_url = "https://checkout.buckaroo.nl/html/";
        private $test_url = "https://testcheckout.buckaroo.nl/html/";

        private $amount = 0;            //Total amount of the invoice. Formaat: 1000.00 (Dot as decimal seperator)
        private $currency = "EUR";      //Currency code
        private $invoice_number = "";   //Prowareness invoice nr

        private $description = "";      //Description to aid the customer
        private $language = "EN";
        private $culture = "en-US";     //de-DE, nl-NL or en-US

        private $return_url = "";       //The url to return to when the payment is finished
        private $cancel_url = "";       //The url to return to when the payment is cancelled
        private $error_url = "";        //The url to return to when the payment results in an error
        private $reject_url = "";       //The url to return to when the payment is rejected by the processor

        private $push_url = "";             //Asynchronous callback url
        private $push_failure_url = "";     //Asynchronous callback url for failed transactions

        private $requested_services = "";   //List the payment options the user can choose in a comma separated list

        private $session_id = "";       //The session ID will be used to get the invoice
        private $signature = "";        //The digital signature of the payment request

        // Default constructor when calling new Buckaroo()
        public function __construct($TestMode = false)
        {
            if($TestMode)
            {
                $this->test_mode = true;
            }
        }

        public function set_website_key( $key ) {
            $this->website_key = $key;
        }

        public function set_secret_key( $key ) {
            $this->secret_key = $key;
        }

        public function set_amount( $amount ) {

            if( $amount == "" || $amount < 0 ) {
                $this->amount = 0;
            } else {
                $this->amount = number_format ( $amount , 2 , ".", "" );
            }

        }

        public function set_currency( $currency ) {

            if( $currency == "" ) {
                $this->currency = "EUR";
            } else {
                $this->currency = $currency;
            }

        }

        public function set_invoice_number( $nr ) {
            $this->invoice_number = $nr;
        }

        public function set_description( $description ) {
            $this->description = $description;
        }

        public function set_language( $language ) {

            $this->language = strtoupper( $language );

            if( $this->language=="NL" ) {
                $this->culture = "nl-NL";
            } else if ( $this->language=="DE" ) {
                $this->culture = "de-DE";
            } else {
                $this->culture = "en-US";
            }

        }

        public function set_return_url( $url ) {
            $this->return_url = $url;
        }

        public function set_cancel_url( $url ) {
            $this->cancel_url = $url;
        }

        public function set_error_url( $url ) {
            $this->error_url = $url;
        }

        public function set_reject_url( $url ) {
            $this->reject_url = $url;
        }

        public function set_push_url( $url ) {
            $this->push_url = $url;
        }

        public function set_push_failure_url( $url ) {
            $this->push_failure_url = $url;
        }

        public function set_requested_services( $services ) {
            $this->requested_services = $services;
        }

        public function set_session_id( $session_id ) {
            $this->session_id = $session_id;
        }

        public function handle_payment() {

            $path = plugin_dir_path( plugin_dir_path(__file__) );
            require_once( $path.'/views/view-fe-event-payment.php');

            $payment_vw = new feEventPaymentHtmlView();

            //Validate the required values
            if( $this->website_key == "" || $this->amount == 0 || $this->currency == "" ||
                $this->invoice_number == "" || $this->return_url == "" ||
                $this->push_url == "" || $this->push_failure_url == "" || $this->secret_key == "" ) {

                return false;

            } else {

                //Gather all request variables

                //Required
                $payment['brq_websitekey'] = $this->website_key;
                $payment['brq_amount'] = $this->amount;
                $payment['brq_currency'] = $this->currency;
                $payment['brq_invoicenumber'] = $this->invoice_number;
                $payment['brq_return'] = $this->return_url;
                $payment['brq_push'] = $this->push_url;
                $payment['brq_pushfailure'] = $this->push_failure_url;
                $payment['add_session_id'] = $this->session_id;

                //Optional
                if( $this->description <> "" ) {
                    $payment['brq_description'] = $this->description;
                }

                if( $this->culture <> "" ) {
                    $payment['brq_culture'] = $this->culture;
                }

                //Fall back is brq_return
                if( $this->cancel_url <> "" ) {
                    $payment['brq_returncancel'] = $this->cancel_url;
                }

                if( $this->error_url <> "" ) {
                    $payment['brq_returnerror'] = $this->error_url;
                }

                if( $this->reject_url <> "" ) {
                    $payment['brq_returnreject'] = $this->reject_url;
                }

                //When empty all available payment options will be shown
                if( $this->requested_services <> "" ) {
                    $payment['brq_requestedservices'] = $this->requested_services;
                }

                if( $this->test_mode == 1 ) {
                    $payment['brq_test'] = 'true';
                }

                //Sort the request alphabetically by fieldname, case insensitive
                ksort($payment, SORT_NATURAL | SORT_FLAG_CASE);

                //Generate the signature
                $signature = $this->generate_signature($payment);

                //Add the signature
                $payment['brq_signature'] = $signature;

                //Sent the request

                if( $this->test_mode == 1 ) {
                    $url = $this->test_url;
                } else {
                    $url = $this->live_url;
                }

                if( $this->language == "NL") {
                    $title = "Redirecting...";
                    $msg = "U wordt doorgeleid naar de betaalpagina.<br><br>Indien u niet automatisch doorgestuurd wordt, kunt u op de onderstaande knop klikken.";
                    $label = "Afrekenen";
                } else if( $this->language == "DE") {
                    $title = "Redirecting...";
                    $msg = "Sie werden auf die Zahlungsseite weitergeleitet.<br><br>Bitte klicken Sie auf den Button, falls Sie nicht automatisch weitergeleitet werden.";
                    $label = "Zahlung";
                } else {
                    $title = "Redirecting...";
                    $msg = "You will be redirected to the payment page.<br><br>Please click the button below when the automatic redirection does not happen.";
                    $label = "Payment";
                }

                $data['title'] = $title;
                $data['payment'] = $payment;
                $data['msg'] = $msg;
                $data['label'] = $label;
                $data['url'] = $url;

                $payment_vw->render($data);

            }

        }

        private function generate_signature( $payment ) {

            //Concatenate into one string without separator/whitespace
            $signatureString = '';

            foreach($payment as $key => $value) {
                $signatureString .= $key . '=' . $value;
            }

            //Add the secret key to the end
            $signatureString .= $this->secret_key;

            //Generate the signature
            $signature = hash( "sha512", $signatureString );

            return $signature;
        }

        public function validate_response( $response ) {

            //Get the signature from the response
            $signature = $response['BRQ_SIGNATURE'];

            //Get the payment response without the signature
            unset( $response['BRQ_SIGNATURE']);

            //Sort the request alphabetically by fieldname, case insensitive
            ksort($response, SORT_NATURAL | SORT_FLAG_CASE);

            $check_signature = $this->generate_signature($response);

            global $pw;

            $result = false;

            if( $signature == $check_signature ) {
                $result = true;
            }

            return $result;

        }

    }

endif;