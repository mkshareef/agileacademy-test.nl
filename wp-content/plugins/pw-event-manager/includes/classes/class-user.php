<?php
/**
 * Direct access to the script is not allowed
 */
defined('ABSPATH') or die("No script kiddies please!");

//require_once ( plugin_dir_path( plugin_dir_path(__file__) ).'models/model-trainer.php');
require_once ( plugin_dir_path( plugin_dir_path(__file__) ).'classes/class-wp-list-table.php');

if( !class_exists( 'user' ) ):

    class user
    {

        public function __construct()
        {

        }

        /**
         * @return string Display name of the current user
         */
        static function current_user_name() {

            $current_user = wp_get_current_user();
            return $current_user->display_name;

        }

    }

endif;