<?php
/**
 * Direct access to the script is not allowed
 */
defined('ABSPATH') or die("No script kiddies please!");

require_once ( plugin_dir_path( plugin_dir_path(__file__) ).'classes/class-wp-list-table.php');

if( !class_exists( 'actionCodeTable' ) ):

    class actionCodeTable extends PWP_List_Table
    {
        private $table_data = array();
        private $paged_data = array();

        public function __construct()
        {
            parent::__construct();
        }

        function set_table_data($data)
        {
            $list = array();

            for ($i = 0; $i < count($data); ++$i) {
                $list[$i]['id'] = (int) $data[$i]['id'];
                $list[$i]['code'] = stripslashes( $data[$i]['code'] );
                $list[$i]['check_string'] = stripslashes( $data[$i]['check_string'] );
                $list[$i]['type'] = stripslashes( $data[$i]['type'] );
                $list[$i]['description'] = stripslashes( $data[$i]['description'] );
                $list[$i]['start_date'] = $data[$i]['start_date'];
                $list[$i]['stop_date'] = $data[$i]['stop_date'];
                $list[$i]['amount'] = $data[$i]['amount'];
                $list[$i]['amount_type'] = $data[$i]['amount_type'];
                $list[$i]['is_blocked'] = $data[$i]['is_blocked'];
                $list[$i]['code_scope'] = stripslashes( $data[$i]['code_scope'] );
                $list[$i]['start_book_period'] = $data[$i]['start_book_period'];
                $list[$i]['stop_book_period'] = $data[$i]['stop_book_period'];
                $list[$i]['max_use'] = $data[$i]['max_use'];
                $list[$i]['updated'] = stripslashes( $data[$i]['updated'] );
                $list[$i]['updated_by'] = stripslashes( $data[$i]['updated_by'] );
            }

            $this->table_data = $list;
        }

        function get_columns()
        {
            $columns = array(
                'cb' => '<input type="checkbox" />',
                'code' => __('Code', 'pw-event-manager'),
                'checkstring' => __('Check string', 'pw-event-manager'),
                'period_open' => __('Period', 'pw-event-manager'),
                'scope' => __('Scope', 'pw-event-manager'),
                'description' => __('Description', 'pw-event-manager')
            );

            return $columns;
        }

        function prepare_items()
        {
            //For paging
            $per_page = 12;
            $current_page = $this->get_pagenum();
            $total_items = count($this->table_data);

            $columns = $this->get_columns();
            $hidden = array();
            $sortable = $this->get_sortable_columns();
            $this->_column_headers = array(
                $columns,
                $hidden,
                $sortable);
            usort($this->table_data, array(&$this, 'usort_reorder'));

            //Get the date for the current page
            $this->paged_data = array_slice($this->table_data,(($current_page-1)*$per_page),$per_page);

            $this->set_pagination_args( array(
                'total_items' => $total_items,
                'per_page'    => $per_page
            ) );

            $this->items = $this->paged_data;
        }

        function get_sortable_columns() {
            $sortable_columns = array(
                'code'  => array('code',true),
                'checkstring' => array('checkstring',false)
            );
            return $sortable_columns;
        }

        function usort_reorder( $a, $b ) {
            // If no sort, default to code
            $orderby = ( ! empty( $_GET['orderby'] ) ) ? $_GET['orderby'] : 'code';

            // If no order, default to asc
            $order = ( ! empty($_GET['order'] ) ) ? $_GET['order'] : 'asc';

            // Determine sort order
            $result = strcmp( strtolower( $a[$orderby] ), strtolower( $b[$orderby] ) );

            // Send final sort direction to usort
            return ( $order === 'asc' ) ? $result : -$result;
        }

        function column_default($item, $column_name)
        {
            require_once('class-util.php');

            global $pw_config;
            $locale = substr(get_locale(), 0, 2 );
            $date_format = $pw_config['date_format'][$locale];

            if( $locale == 'nl' ) {
                $dec_point = ',';
                $thousands_sep = '.';
            } else if( $locale == 'de' ) {
                $dec_point = ',';
                $thousands_sep = '.';
            } else {
                $dec_point = '.';
                $thousands_sep = ',';
            }

            switch ($column_name) {
                case 'code':
                    $discount = "";
                    if( $item['amount'] <> "" && $item['amount'] <> '0' ) {
                        $amount = number_format( $item['amount'], 2, $dec_point, $thousands_sep);

                        if( $item['amount_type'] == 'percent' ) {
                            $discount = '<br>'.$amount. ' %';
                        } else {
                            $discount = '<br>&euro; '.$amount;
                        }
                    }

                    $status = $item['is_blocked'] == "" ? "" : '<br><i>'.__('Blocked', 'pw-event-manager').'</i>';
                    $edit_url = '<a href="?page=pw-event-action-codes&action=edit&id='.$item['id'].'">'.$item['code'].'</a>';
                    return $item['code'] == "" ? "-" : $edit_url.$discount.$status;
                case 'checkstring':
                    $type = $item['type'];
                    $checkstring = $item['check_string'] == "" ? "-" : $item['check_string'];

                    $r = $checkstring;
                    if( $type == 'training' ) {
                        $r = "";
                    }
                    return $r;
                case 'period_open':
                    $dtStart = date( $date_format ,strtotime( $item['start_date'] ));
                    $dtStop = date( $date_format ,strtotime( $item['stop_date'] ));
                    return $dtStart .'<br>'. $dtStop;
                case 'scope':

                    $scope = $item['code_scope'] == 'filtered' ? __('Selected events', 'pw-event-manager') : __('All events', 'pw-event-manager');
                    $periodStart =  $item['start_book_period'] == "0000-00-00" ? "" : date( $date_format ,strtotime( $item['start_book_period'] ));
                    $periodStop =  $item['stop_book_period'] == "0000-00-00" ? "" : date( $date_format ,strtotime( $item['stop_book_period'] ));

                    $max = (int) $item['max_use'];
                    if( $max > 0 ) {
                        $max_use = '<br>'.__('Available:', 'pw-event-manager').' '.$max;
                    } else {
                        $max_use = '';
                    }

                    if( $periodStart <> "" && $periodStop <> "" ) {
                        $period = '<br>'.__('Between', 'pw-event-manager').' '.$periodStart.' / '. $periodStop;
                    } else {
                        $period = "";
                    }

                    return $scope.$period.$max_use;
                case 'is_blocked':
                    return $item['is_blocked'] == "" ? __('No', 'pw-event-manager') : __('Yes', 'pw-event-manager');

                case 'updated':

                    $date_format = $pw_config['date_format'][$locale].' H:i';
                    $dt = date( $date_format ,strtotime( $item['updated'] ));

                    $updated_by = $item['updated_by'] == "" ? "" : "<br>".$item['updated_by'];

                    return $dt == "" ? "-" : $dt.$updated_by;
                case 'description':
                    return $item['description'];
                default:
                    return "-";
            }
        }

        /*
        function get_bulk_actions()
        {
            $actions = array('list_delete' => __('Delete', 'pw-event-manager') );
            return $actions;
        }
        */

        function column_cb($item)
        {
            return sprintf('<input type="checkbox" name="list[]" value="%s" />', $item['id']);
        }

        function extra_tablenav($which)
        {

            if ($which == "top") {
                //The code that goes before the table is here
                echo '<div class="alignleft actions"><a href="?page=pw-event-action-codes&action=new" class="button-primary action pw-action-btn">'.__('New', 'pw-event-manager').'</a></div>';
            }
            if ($which == "bottom") {
                //The code that goes after the table is there
                echo '<div class="alignleft actions"><a href="?page=pw-event-action-codes&action=new" class="button-primary action pw-action-btn">'.__('New', 'pw-event-manager').'</a></div>';
            }

        }

    }

endif;



