<?php
/**
 * Direct access to the script is not allowed
 */
defined('ABSPATH') or die("No script kiddies please!");

require_once ( plugin_dir_path( plugin_dir_path(__file__) ).'classes/class-wp-list-table.php');

if( !class_exists( 'eventTable' ) ):

    class eventTable extends PWP_List_Table
    {
        private $table_data = array();
        private $paged_data = array();

        public function __construct()
        {
            parent::__construct();
        }

        function set_table_data($data)
        {
            $list = array();

            for ($i = 0; $i < count($data); ++$i) {
                $list[$i]['id'] = (int) $data[$i]['id'];
                $list[$i]['name'] = stripslashes( $data[$i]['name'] );
                $list[$i]['locale'] = stripslashes( $data[$i]['locale'] );
                $list[$i]['duration'] = stripslashes( $data[$i]['duration'] );
                $list[$i]['trainers'] = $data[$i]['trainers'];
				//$list[$i]['parts'] = $data[$i]['parts'];
                $list[$i]['type'] = $data[$i]['type'];
                $list[$i]['publication_status'] = $data[$i]['publication_status'];
                $list[$i]['price_normal'] = $data[$i]['price_normal'];
                $list[$i]['price_last_minute'] = $data[$i]['price_last_minute'];
                $list[$i]['price_early_bird'] = $data[$i]['price_early_bird'];
                $list[$i]['updated'] = stripslashes( $data[$i]['updated'] );
                $list[$i]['updated_by'] = stripslashes( $data[$i]['updated_by'] );
            }

            $this->table_data = $list;
        }

        function get_columns()
        {
            $columns = array(
                'cb' => '<input type="checkbox" />',
                'name' => __('Name', 'pw-event-manager'),
                'locale' => __('Details', 'pw-event-manager'),
                'trainers' => __('Trainer(s)', 'pw-event-manager'),
				//'parts' => __('Parts', 'pw-event-manager'),
                'publication_status' => __('Status', 'pw-event-manager'),
                'price' => __('Pricing', 'pw-event-manager'),
                'updated' => __('Updated', 'pw-event-manager'),
            );

            return $columns;
        }

        function prepare_items()
        {
            //For paging
            $per_page = 100;
            $current_page = $this->get_pagenum();
            $total_items = count($this->table_data);

            $columns = $this->get_columns();
            $hidden = array();
            $sortable = $this->get_sortable_columns();
            $this->_column_headers = array(
                $columns,
                $hidden,
                $sortable);
            usort($this->table_data, array(&$this, 'usort_reorder'));

            //Get the data for the current page
            $this->paged_data = array_slice($this->table_data,(($current_page-1)*$per_page),$per_page);

            $this->set_pagination_args( array(
                'total_items' => $total_items,
                'per_page'    => $per_page
            ) );

            $this->items = $this->paged_data;
        }

        function get_sortable_columns() {
            $sortable_columns = array(
                'name'  => array('name',true),
                'publication_status' => array('publication_status',false)
            );
            return $sortable_columns;
        }

        function usort_reorder( $a, $b ) {
            // If no sort, default to code
            $orderby = ( ! empty( $_GET['orderby'] ) ) ? $_GET['orderby'] : 'name';

            // If no order, default to asc
            $order = ( ! empty($_GET['order'] ) ) ? $_GET['order'] : 'asc';

            // Determine sort order
            $result = strcmp( strtolower( $a[$orderby] ), strtolower( $b[$orderby] ) );

            // Send final sort direction to usort
            return ( $order === 'asc' ) ? $result : -$result;
        }

        function column_default($item, $column_name)
        {
            require_once('class-util.php');

            global $pw_config;
            $locale = substr(get_locale(), 0, 2 );
            $date_time_format = $pw_config['date_format'][$locale].' H:i:s';
            $date_format = $pw_config['date_format'][$locale];
            $time_format = 'H:i';

            if( $locale == 'nl' ) {
                $dec_point = ',';
                $thousands_sep = '.';
            } else if( $locale == 'de' ) {
                $dec_point = ',';
                $thousands_sep = '.';
            } else {
                $dec_point = '.';
                $thousands_sep = ',';
            }

            switch ($column_name) {
                case 'name':
                    $edit_url = '<a href="?page=pw-events&action=edit&id='.$item['id'].'">'.$item['name'].'</a>';
                    return $item['name'] == "" ? "-" : $edit_url;
                case 'locale':
                    $lang = $item['locale'] == "" ? "-" : $item['locale'];
                    $duration = $item['duration'] == "" ? "-" : number_format( $item['duration'], 2, $dec_point, $thousands_sep).' '.__('day(s)', 'pw-event-manager');
                    $type = $item['type'] == "" ? "-" : ucfirst( $item['type'] );

                    return $lang.'<br>'.$duration.'<br>'.$type;
                case 'trainers':
                    return isset( $item['trainers'] ) === false ? "-" : stripslashes( implode( $item['trainers'] , '<br>') );
				//case 'parts':
				//	return isset( $item['parts'] ) === false ? "-" : stripslashes( implode( $item['parts'] , '<br>') );
                case 'publication_status':

                    $status = '';
                    if( $item['publication_status'] == 'concept' ) {
                        $status = __('Concept', 'pw-event-manager');
                    }
                    else if( $item['publication_status'] == 'publish' ) {
                        $status = __('Published', 'pw-event-manager');
                    }
                    else if( $item['publication_status'] == 'archive' ) {
                        $status = __('Archived', 'pw-event-manager');
                    }

                    return $status == "" ? "-" : $status;
                case 'price':
                    $is_free = true;

                    $price_early_bird = '';
                    $price_normal = '';
                    $price_last_minute = '';

                    if( $item['price_early_bird'] > 0 ) {
                        $price = number_format( $item['price_early_bird'], 2, $dec_point, $thousands_sep);
                        $price_early_bird =  __('Early', 'pw-event-manager').': &euro; '.$price.'<br>';
                        $is_free = false;
                    }
                    if( $item['price_normal'] > 0 ) {
                        $price = number_format( $item['price_normal'], 2, $dec_point, $thousands_sep);
                        $price_normal =  __('Normal', 'pw-event-manager').': &euro; '.$price.'<br>';
                        $is_free = false;
                    }
                    if( $item['price_last_minute'] > 0 ) {
                        $price = number_format( $item['price_last_minute'], 2, $dec_point, $thousands_sep);
                        $price_last_minute =  __('Last', 'pw-event-manager').': &euro; '.$price;
                        $is_free = false;
                    }

                    return $is_free === true ? __('Free', 'pw-event-manager') : $price_early_bird.$price_normal.$price_last_minute;
                case 'updated':
                    $dt = date( $date_time_format ,strtotime( $item['updated'] ));

                    $updated_by = $item['updated_by'] == "" ? "" : "<br>".$item['updated_by'];

                    return $dt == "" ? "-" : $dt.$updated_by;
                default:
                    return "-";
            }
        }

        /*
        function get_bulk_actions()
        {
            $actions = array('list_delete' => __('Delete', 'pw-event-manager') );
            return $actions;
        }
        */

        function column_cb($item)
        {
            return sprintf('<input type="checkbox" name="list[]" value="%s" />', $item['id']);
        }

        function extra_tablenav($which)
        {

            if ($which == "top") {
                //The code that goes before the table is here
                echo '<div class="alignleft actions"><a href="?page=pw-events&action=new" class="button-primary action pw-action-btn">'.__('New', 'pw-event-manager').'</a></div>';
            }
            if ($which == "bottom") {
                //The code that goes after the table is there
                echo '<div class="alignleft actions"><a href="?page=pw-events&action=new" class="button-primary action pw-action-btn">'.__('New', 'pw-event-manager').'</a></div>';
            }

        }

    }

endif;



