<?php
/**
 * Direct access to the script is not allowed
 */
defined('ABSPATH') or die("No script kiddies please!");

//require_once ( plugin_dir_path( plugin_dir_path(__file__) ).'models/model-trainer.php');
//require_once ( plugin_dir_path( plugin_dir_path(__file__) ).'classes/class-wp-list-table.php');

if( !class_exists( 'dialog' ) ):

    class dialog
    {

        public function __construct()
        {

        }

        static function yes_no_dialog_html( $html_id, $title, $message, $yes_str, $no_str ) {

           $html = '
           <div id="'.$html_id.'" class="dialog-frame">
                <div class="dialog-container">
                    <div class="dialog-title">
                        '.$title.'
                    </div>
                    <div class="dialog-message">
                        '.$message.'
                    </div>
                     <div class="dialog-buttons">
                        <button id="ok-btn">'.$yes_str.'</button>
                        <button id="cancel-btn">'.$no_str.'</button>
                    </div>
                </div>
            </div>
           ';

            return $html;
        }

    }

endif;