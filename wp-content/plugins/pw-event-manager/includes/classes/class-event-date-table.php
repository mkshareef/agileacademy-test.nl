<?php
/**
 * Direct access to the script is not allowed
 */
defined('ABSPATH') or die("No script kiddies please!");

require_once ( plugin_dir_path( plugin_dir_path(__file__) ).'classes/class-wp-list-table.php');

if( !class_exists( 'eventDateTable' ) ):

    class eventDateTable extends PWP_List_Table
    {
        private $table_data = array();
        private $paged_data = array();

        public function __construct()
        {
            parent::__construct();
        }

        function set_table_data($data)
        {
            $list = array();

            for ($i = 0; $i < count($data); ++$i) {
                $list[$i]['id'] = (int) $data[$i]['id'];
                $list[$i]['start_date'] = $data[$i]['start_date'];
                $list[$i]['start_time'] = $data[$i]['start_time'];
                $list[$i]['stop_time'] = $data[$i]['stop_time'];
                $list[$i]['other_dates'] = $data[$i]['other_dates'];
                $list[$i]['location_name'] = stripslashes( $data[$i]['location_name'] );
                $list[$i]['alt_price_normal'] = $data[$i]['alt_price_normal'];
                $list[$i]['alt_price_last_minute'] = $data[$i]['alt_price_last_minute'];
                $list[$i]['alt_price_early_bird'] = $data[$i]['alt_price_early_bird'];
                $list[$i]['status'] = $data[$i]['status'];
                $list[$i]['updated'] = stripslashes( $data[$i]['updated'] );
                $list[$i]['updated_by'] = stripslashes( $data[$i]['updated_by'] );
            }

            $this->table_data = $list;
        }

        function get_columns()
        {
            $columns = array(
                'cb' => '<input type="checkbox" />',
                'start_date' => __('Date', 'pw-event-manager'),
                'other_dates' => __('Other dates', 'pw-event-manager'),
                'location' => __('Location', 'pw-event-manager'),
                'status' => __('Status', 'pw-event-manager'),
                'special_pricing' => __('Special price', 'pw-event-manager'),
                'updated' => __('Updated', 'pw-event-manager')
            );

            return $columns;
        }

        function prepare_items()
        {
            //For paging
            $per_page = 20;
            $current_page = $this->get_pagenum();
            $total_items = count($this->table_data);

            $columns = $this->get_columns();
            $hidden = array();
            $sortable = $this->get_sortable_columns();
            $this->_column_headers = array(
                $columns,
                $hidden,
                $sortable);
            usort($this->table_data, array(&$this, 'usort_reorder'));

            //Get the data for the current page
            $this->paged_data = array_slice($this->table_data,(($current_page-1)*$per_page),$per_page);

            $this->set_pagination_args( array(
                'total_items' => $total_items,
                'per_page'    => $per_page
            ) );

            $this->items = $this->paged_data;
        }

        function get_sortable_columns() {
            $sortable_columns = array(
                'start_date'  => array('start_date',true),
                'location' => array('location',false)
            );
            return $sortable_columns;
        }

        function usort_reorder( $a, $b ) {
            // If no sort, default to code
            $orderby = ( ! empty( $_GET['orderby'] ) ) ? $_GET['orderby'] : 'start_date';

            if( $orderby == 'location') {
                $orderby = 'location_name';
            }

            // If no order, default to asc
            $order = ( ! empty($_GET['order'] ) ) ? $_GET['order'] : 'asc';

            // Determine sort order
            $result = strcmp( strtolower( $a[$orderby] ), strtolower( $b[$orderby] ) );

            // Date column default decending
            if( $orderby == 'start_date') {
                $result = -$result;
            }

            // Send final sort direction to usort
            return ( $order === 'asc' ) ? $result : -$result;
        }

        function column_default($item, $column_name)
        {
            require_once('class-util.php');

            global $pw_config;
            $locale = substr(get_locale(), 0, 2 );
            $date_time_format = $pw_config['date_format'][$locale].' H:i:s';
            $date_format = $pw_config['date_format'][$locale];
            $time_format = 'H:i';

            if( $locale == 'nl' ) {
                $dec_point = ',';
                $thousands_sep = '.';
            } else if( $locale == 'de' ) {
                $dec_point = ',';
                $thousands_sep = '.';
            } else {
                $dec_point = '.';
                $thousands_sep = ',';
            }

            switch ($column_name) {
                case 'start_date':
                    $date_start = date( $date_format ,strtotime( $item['start_date'] ));
                    $time_start = date( $time_format ,strtotime( $item['start_time'] ));
                    $time_stop =  date( $time_format ,strtotime( $item['stop_time'] ));

                    $edit_url = '<a href="?page=pw-event-date&action=edit&id='.$item['id'].'">'.$date_start.'</a>';
                    return $item['start_date'] == "" ? "-" : $edit_url.'<br>'.$time_start.' - '.$time_stop;
                case 'other_dates':
                    if( $item['other_dates'] <> '' ) {
                        $dtArray = explode(',', $item['other_dates']);

                        foreach( $dtArray as $key=>$value) {
                            $dtArray[$key] = date( $date_format ,strtotime( $value ));
                        }

                        $other_dates = implode('<br>', $dtArray);
                    } else {
                        $other_dates = '-';
                    }

                    return $other_dates;
                case 'location':
                    return $item['location_name'] == "" ? "-" : $item['location_name'];
                case 'status':
                    if( $item['status'] == 'open') {
                        $status = __('Open', 'pw-event-manager');
                    }
                    else if( $item['status'] == 'full') {
                        $status = __('Full', 'pw-event-manager');
                    }
                    else if( $item['status'] == 'closed') {
                        $status = __('Closed', 'pw-event-manager');
                    }

                    return $status;
                case 'special_pricing':

                    $alt_price_early_bird = '';
                    $alt_price_normal = '';
                    $alt_price_last_minute = '';

                    if( $item['alt_price_early_bird'] > 0 ) {
                        $price = number_format( $item['alt_price_early_bird'], 2, $dec_point, $thousands_sep);
                        $alt_price_early_bird =  __('Early', 'pw-event-manager').': &euro; '.$price.'<br>';
                    }
                    if( $item['alt_price_normal'] > 0 ) {
                        $price = number_format( $item['alt_price_normal'], 2, $dec_point, $thousands_sep);
                        $alt_price_normal =  __('Normal', 'pw-event-manager').': &euro; '.$price.'<br>';
                    }
                    if( $item['alt_price_last_minute'] > 0 ) {
                        $price = number_format( $item['alt_price_last_minute'], 2, $dec_point, $thousands_sep);
                        $alt_price_last_minute =  __('Last', 'pw-event-manager').': &euro; '.$price;
                    }

                    return $alt_price_early_bird.$alt_price_normal.$alt_price_last_minute;
                case 'updated':

                    $dt = date( $date_time_format ,strtotime( $item['updated'] ));

                    $updated_by = $item['updated_by'] == "" ? "" : "<br>".$item['updated_by'];

                    return $dt == "" ? "-" : $dt.$updated_by;
                default:
                    return "-";
            }
        }

        /*
        function get_bulk_actions()
        {
            $actions = array('list_delete_date' => __('Delete', 'pw-event-manager') );
            return $actions;
        }
        */

        function column_cb($item)
        {
            return sprintf('<input type="checkbox" name="list_dates[]" value="%s" />', $item['id']);
        }

        function extra_tablenav($which)
        {
            $id = 0;
            if( isset($_GET['id'])) {
                $id = (int) $_GET['id'];
            }
            if ($which == "top") {
                //The code that goes before the table is here
                echo '<div class="alignleft actions"><a href="?page=pw-event-date&action=new&eventId='.$id.'" class="button-primary action pw-action-btn">'.__('New', 'pw-event-manager').'</a></div>';
            }
            if ($which == "bottom") {
                //The code that goes after the table is there
                echo '<div class="alignleft actions"><a href="?page=pw-event-date&action=new&eventId='.$id.'" class="button-primary action pw-action-btn">'.__('New', 'pw-event-manager').'</a></div>';
            }

        }

    }

endif;



