<?php
/**
 * Direct access to the script is not allowed
 */
defined('ABSPATH') or die("No script kiddies please!");

if( !class_exists( 'export' ) ):

    class export
    {

        public function __construct()
        {

        }

        static public function export_subscribers( $subscribers ) {

            global $pw_config;

            $lang = $pw_config['lang_primary'];
            $date_format = $pw_config['date_format'][$lang];

            $path = plugin_dir_path( plugin_dir_path(__file__) );
            require_once( $path.'/models/model-participant.php');
            require_once( $path.'/libs/phpexportdata/php-export-data.class.php');

            // Render the PDF to disk
            $dirName = WP_CONTENT_DIR . '/uploads/export';

            //Clean up old files
            foreach (glob($dirName."*.csv") as $file) {

                // If file is 1 hour (3600 seconds) old then delete it
                if (filemtime($file) < ( time() - 3600 )) {
                    unlink($file);
                }
            }

            $fileName = time().'.csv';

            if (!file_exists( $dirName )) {
                mkdir( $dirName, 0755, true);
            }

            $exporter = new ExportDataCSV('file', $dirName.'/'.$fileName);
            $exporter->initialize();

            $columns = array(
                "Status",
                "Confirmation",
                "Remarks",
                "Event",
                "Date",
                "Start",
                "Stop",
                "Location",
                "Company",
                "Salutation",
                "Firstname",
                "Middlename",
                "Lastname",
                "Phone",
                "Email",
                "Newsletter"
            );

            $exporter->addRow( $columns );

            foreach( $subscribers as $item ) {
                $row = array();

                $row[] = stripslashes( $item['status'] );
                $row[] = $item['confirm_sent'];
                $row[] = stripslashes( preg_replace( "/\r|\n/", "", $item['process_comments'] ));
                $row[] = stripslashes( $item['event_details']['name'] );

                $evt_date =  date( $date_format ,strtotime( $item['event_details']['date'] ));
                $row[] = $evt_date;

                $start = date( 'H:i' ,strtotime( $item['event_details']['start_time'] ));
                $row[] = $start;

                $stop = date( 'H:i' ,strtotime( $item['event_details']['stop_time'] ));
                $row[] = $stop;
                $row[] = stripslashes( $item['event_details']['location_name'] );
                $row[] = stripslashes( $item['company'] );
                $row[] = $item['salutation'];
                $row[] = stripslashes( $item['first_name'] );
                $row[] = stripslashes( $item['middle_name'] );
                $row[] = stripslashes( $item['last_name'] );
                $row[] = stripslashes( $item['phone'] );
                $row[] = stripslashes( $item['email'] );
                $row[] = $item['newsletter'];

                $exporter->addRow( $row );
            }

            $exporter->finalize();

            $result['dir'] = $dirName;
            $result['file'] = $fileName;

            return $result;
        }

        static public function export_subscriptions( $subscriptions ) {

            $path = plugin_dir_path( plugin_dir_path(__file__) );
            require_once( $path.'/models/model-subscription.php');
            require_once( $path.'/libs/phpexportdata/php-export-data.class.php');

            global $pw_config;

            $lang = $pw_config['lang_primary'];
            $date_format = $pw_config['date_format'][$lang];

            // Render the PDF to disk
            $dirName = WP_CONTENT_DIR . '/uploads/export';

            //Clean up old files
            foreach (glob($dirName."*.csv") as $file) {

                // If file is 1 hour (3600 seconds) old then delete it
                if (filemtime($file) < ( time() - 3600 )) {
                    unlink($file);
                }
            }

            $fileName = time().'.csv';

            if (!file_exists( $dirName )) {
                mkdir( $dirName, 0755, true);
            }

            $exporter = new ExportDataCSV('file', $dirName.'/'.$fileName);
            $exporter->initialize();

            $columns = array(
                "Received",
                "Status",
                "Event",
                "Event date",
                "Start",
                "Stop",
                "Location",
                "Company",
                "Nr subscribers",
                "Learned about us",
                "Learned else"
            );

            $exporter->addRow( $columns );

            //global $pw;

            foreach( $subscriptions as $item ) {

                //$pw->print_var($item);

                $row = array();

                $created = date( $date_format ,strtotime( $item['created'] ));
                $row[] = $created;
                $row[] = stripslashes( $item['status'] );
                $row[] = stripslashes( $item['event_details']['name'] );

                $evt_date =  date( $date_format ,strtotime( $item['event_details']['date'] ));
                $row[] = $evt_date;

                $start = date( 'H:i' ,strtotime( $item['event_details']['start_time'] ));
                $row[] = $start;

                $stop = date( 'H:i' ,strtotime( $item['event_details']['stop_time'] ));
                $row[] = $stop;

                $row[] = stripslashes( $item['event_details']['location_name'] );
                $row[] = stripslashes( $item['company'] );
                $row[] = count( $item['subscribers'] );
                $row[] = stripslashes( $item['know_how'] );
                $row[] = stripslashes( $item['know_how_else'] );

                $exporter->addRow( $row );
            }

            $exporter->finalize();

            $result['dir'] = $dirName;
            $result['file'] = $fileName;

            return $result;
        }

        static public function export_report_subpermonth( $subscribers ) {

            global $pw_config;

            //global $pw;
            //$pw->print_var($subscribers);

            $lang = $pw_config['lang_primary'];
            $date_format = $pw_config['date_format'][$lang];

            if( $lang == 'nl' ) {
                $dec_point = ',';
                $thousands_sep = '.';
            } else if( $lang == 'de' ) {
                $dec_point = ',';
                $thousands_sep = '.';
            } else {
                $dec_point = '.';
                $thousands_sep = ',';
            }

            $path = plugin_dir_path( plugin_dir_path(__file__) );
            require_once( $path.'/libs/phpexportdata/php-export-data.class.php');

            // Render the PDF to disk
            $dirName = WP_CONTENT_DIR . '/uploads/export';

            //Clean up old files
            foreach (glob($dirName."*.csv") as $file) {

                // If file is 1 hour (3600 seconds) old then delete it
                if (filemtime($file) < ( time() - 3600 )) {
                    unlink($file);
                }
            }

            $fileName = time().'.csv';

            if (!file_exists( $dirName )) {
                mkdir( $dirName, 0755, true);
            }

            $exporter = new ExportDataCSV('file', $dirName.'/'.$fileName);
            $exporter->initialize();

            $columns = array(
                "Month",
                "Event",
                "Event date",
                "Start",
                "Stop",
                "Location",
                "Company",
                "Salutation",
                "First name",
                "Middle name",
                "Last name",
                "Email",
                "Phone"
            );

            $exporter->addRow( $columns );

            foreach( $subscribers as $item ) {
                $row = array();

                $month = date( "Y-m" ,strtotime( $item['start_date'] ));
                $row[] = $month;

                $row[] = stripslashes($item['name']);

                $evt_date =  date( $date_format ,strtotime( $item['start_date'] ));
                $row[] = $evt_date;

                $time = date( 'H:i' ,strtotime( stripslashes( $item['start_time'] ) ));
                $row[] = $time;

                $time = date( 'H:i' ,strtotime( stripslashes( $item['stop_time'] ) ));
                $row[] = $time;

                $row[] = stripslashes( $item['location_name'] );
                $row[] = stripslashes( $item['company'] );
                $row[] = $item['salutation'];
                $row[] = stripslashes( $item['first_name'] );
                $row[] = stripslashes( $item['middle_name'] );
                $row[] = stripslashes( $item['last_name'] );
                $row[] = stripslashes( $item['email'] );
                $row[] = stripslashes( $item['phone'] );

                $exporter->addRow( $row );
            }

            $exporter->finalize();

            $result['dir'] = $dirName;
            $result['file'] = $fileName;

            return $result;
        }

        static public function export_report_turnover( $report_lines, $hide_turnover = false ) {

            global $pw_config;

            $lang = $pw_config['lang_primary'];
            $date_format = $pw_config['date_format'][$lang];

            if( $lang == 'nl' ) {
                $dec_point = ',';
                $thousands_sep = '.';
            } else if( $lang == 'de' ) {
                $dec_point = ',';
                $thousands_sep = '.';
            } else {
                $dec_point = '.';
                $thousands_sep = ',';
            }

            $path = plugin_dir_path( plugin_dir_path(__file__) );
            require_once( $path.'/libs/phpexportdata/php-export-data.class.php');

            // Render the PDF to disk
            $dirName = WP_CONTENT_DIR . '/uploads/export';

            //Clean up old files
            foreach (glob($dirName."*.csv") as $file) {

                // If file is 1 hour (3600 seconds) old then delete it
                if (filemtime($file) < ( time() - 3600 )) {
                    unlink($file);
                }
            }

            $fileName = time().'.csv';

            if (!file_exists( $dirName )) {
                mkdir( $dirName, 0755, true);
            }

            $exporter = new ExportDataCSV('file', $dirName.'/'.$fileName);
            $exporter->initialize();

            if( $hide_turnover === false ) {

                $columns = array(
                    "Event",
                    "Date",
                    "Start",
                    "Stop",
                    "Location",
                    "Nr participants",
                    "Gross revenue",
                    "Discount",
                    "Net revenue"
                );

            } else {

                $columns = array(
                    "Event",
                    "Date",
                    "Start",
                    "Stop",
                    "Location",
                    "Nr participants"
                );

            }

            $exporter->addRow( $columns );

            foreach( $report_lines as $item ) {
                $row = array();

                $row[] = stripslashes( $item['event_name'] );

                $evt_date =  date( $date_format ,strtotime( $item['event_start_date'] ));
                $row[] = stripslashes( $evt_date );

                $time = date( 'H:i' ,strtotime( stripslashes( $item['event_start_time'] ) ));
                $row[] = stripslashes( $time );

                $time = date( 'H:i' ,strtotime( stripslashes( $item['event_stop_time'] ) ));
                $row[] = stripslashes( $time );

                $row[] = stripslashes( $item['event_location'] );
                $row[] = stripslashes( $item['nr_participants'] );

                if( $hide_turnover === false ) {
                    $row[] = $item['gross_revenue'];
                    $row[] = $item['discount'];
                    $row[] = $item['net_revenue'];
                }

                $exporter->addRow( $row );
            }

            $exporter->finalize();

            $result['dir'] = $dirName;
            $result['file'] = $fileName;

            return $result;
        }


    }

endif;