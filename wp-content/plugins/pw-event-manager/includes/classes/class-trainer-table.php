<?php
/**
 * Direct access to the script is not allowed
 */
defined('ABSPATH') or die("No script kiddies please!");

//require_once ( plugin_dir_path( plugin_dir_path(__file__) ).'models/model-trainer.php');
require_once ( plugin_dir_path( plugin_dir_path(__file__) ).'classes/class-wp-list-table.php');

if( !class_exists( 'trainerTable' ) ):

    class trainerTable extends PWP_List_Table
    {
        private $table_data = array();
        private $paged_data = array();

        public function __construct()
        {
            parent::__construct();
        }

        function set_table_data($data)
        {
            $list = array();

            for ($i = 0; $i < count($data); ++$i) {
                $list[$i]['id'] = (int) $data[$i]['id'];
                $list[$i]['trainer_name'] = stripslashes( $data[$i]['trainer_name'] );
                $list[$i]['function_title'] = stripslashes( $data[$i]['function_title'] );
                $list[$i]['locale'] = stripslashes( $data[$i]['locale'] );
                $list[$i]['attachment_path'] = $data[$i]['attachment_path'];
                $list[$i]['updated'] = stripslashes( $data[$i]['updated'] );
                $list[$i]['updated_by'] = stripslashes( $data[$i]['updated_by'] );
                $list[$i]['ip'] = stripslashes( $data[$i]['ip'] );
            }

            $this->table_data = $list;
        }

        function get_columns()
        {
            $columns = array(
                'cb' => '<input type="checkbox" />',
                'trainer_name' => __('Name', 'pw-event-manager'),
                'img' => __('Picture', 'pw-event-manager'),
                'locale' => __('Language', 'pw-event-manager'),
                'updated' => __('Updated', 'pw-event-manager'),
                'updated_by' => __('Updated by', 'pw-event-manager')
            );

            return $columns;
        }

        function prepare_items()
        {
            //For paging
            $per_page = 12;
            $current_page = $this->get_pagenum();
            $total_items = count($this->table_data);

            $columns = $this->get_columns();
            $hidden = array();
            $sortable = $this->get_sortable_columns();
            $this->_column_headers = array(
                $columns,
                $hidden,
                $sortable);
            usort($this->table_data, array(&$this, 'usort_reorder'));

            //Get the date for the current page
            $this->paged_data = array_slice($this->table_data,(($current_page-1)*$per_page),$per_page);

            $this->set_pagination_args( array(
                'total_items' => $total_items,
                'per_page'    => $per_page
            ) );

            $this->items = $this->paged_data;
        }

        function get_sortable_columns() {
            $sortable_columns = array(
                'trainer_name'  => array('trainer_name',true),
                'locale' => array('locale',false)
            );
            return $sortable_columns;
        }

        function usort_reorder( $a, $b ) {
            // If no sort, default to title
            $orderby = ( ! empty( $_GET['orderby'] ) ) ? $_GET['orderby'] : 'trainer_name';

            // If no order, default to asc
            $order = ( ! empty($_GET['order'] ) ) ? $_GET['order'] : 'asc';

            // Determine sort order
            $result = strcmp( strtolower( $a[$orderby] ), strtolower( $b[$orderby] ) );

            // Send final sort direction to usort
            return ( $order === 'asc' ) ? $result : -$result;
        }

        function column_default($item, $column_name)
        {
            require_once('class-util.php');

            switch ($column_name) {
                case 'img':
                    $img = "-";
                    if( $item['attachment_path'] <> "" ) {
                        $img_src = util::get_image_src( $item['attachment_path'] );
                        if( $img_src ) {
                            $img = '<img src="'.$img_src.'" width="48">';
                        }
                    }

                    return $img;
                case 'trainer_name':
                    $edit_url = '<a href="?page=pw-events-trainers&action=edit&id='.$item['id'].'">'.$item['trainer_name'].'</a>';
                    $function_title = $item['function_title'] == "" ? "" : "<br>". $item['function_title'];
                    return $item['trainer_name'] == "" ? "-" : $edit_url.$function_title;
                case 'locale':
                    return $item['locale'] == "" ? "-" : $item['locale'];
                case 'updated':
                    global $pw_config;
                    $locale = substr(get_locale(), 0, 2 );
                    $date_format = $pw_config['date_format'][$locale].' H:i:s';
                    $dt = date( $date_format ,strtotime( $item['updated'] ));

                    return $dt == "" ? "-" : $dt;
                case 'updated_by':
                    return $item['updated_by'] == "" ? "-" : $item['updated_by'];
                default:
                    return "-";
            }
        }

        function get_bulk_actions()
        {
            $actions = array('list_delete' => __('Delete', 'pw-event-manager') );
            return $actions;
        }

        function column_cb($item)
        {
            return sprintf('<input type="checkbox" name="list[]" value="%s" />', $item['id']);
        }

        function extra_tablenav($which)
        {

            if ($which == "top") {
                //The code that goes before the table is here
                echo '<div class="alignleft actions"><a href="?page=pw-events-trainers&action=new" class="button-primary action pw-action-btn">'.__('New', 'pw-event-manager').'</a></div>';
            }
            if ($which == "bottom") {
                //The code that goes after the table is there
                echo '<div class="alignleft actions"><a href="?page=pw-events-trainers&action=new" class="button-primary action pw-action-btn">'.__('New', 'pw-event-manager').'</a></div>';
            }

        }

    }

endif;



