<?php
/**
 * Direct access to the script is not allowed
 */
defined('ABSPATH') or die("No script kiddies please!");

require_once ( plugin_dir_path( plugin_dir_path(__file__) ).'classes/class-wp-list-table.php');
require_once ( plugin_dir_path( plugin_dir_path(__file__) ).'models/model-event.php');
require_once ( plugin_dir_path( plugin_dir_path(__file__) ).'models/model-location.php');
require_once ( plugin_dir_path( plugin_dir_path(__file__) ).'models/model-event-date.php');

if( !class_exists( 'eventSubscribersTable' ) ):

	// builds a table of all the events and the number of participants
    class eventSubscribersTable extends PWP_List_Table
    {
        private $table_data = array();
        private $paged_data = array();

        public function __construct()
        {
            parent::__construct();
        }

        function set_table_data($data)
        {
            $list = array();
			/*  'count',
				'event_id',
				'event_name',
				'start_date' 
				'location_name'	*/
            for ($i = 0; $i < count($data); ++$i) {
                $list[$i]['id'] = (int) $data[$i]['event_id'];
                $list[$i]['start_date'] = stripslashes( $data[$i]['start_date'] );
				$list[$i]['event_name'] = stripslashes( $data[$i]['event_name'] );
                $list[$i]['count'] = (int) $data[$i]['count'];                
				$list[$i]['location_name'] = stripslashes( $data[$i]['location_name'] );
            }

            $this->table_data = $list;
        }

        function get_columns()
        {
            $columns = array(
                //'cb' => '<input type="checkbox" />',
                'event_name' => __('Event', 'pw-event-manager'),
				'start_date' => __('Event date', 'pw-event-manager'),
				'location_name' => __('Location', 'pw-event-manager'),
				'count' => __('Aantal', 'pw-event-manager'),
            );

            return $columns;
        }

        function prepare_items( $show_all = false )
        {
            //For paging
            $per_page = 500;

            // This preserves the count of the nr of results
            if( $show_all == true ) {
                $per_page = count( $this->table_data );
            }

            $current_page = $this->get_pagenum();
            $total_items = count($this->table_data);

            $columns = $this->get_columns();
            $hidden = array();
            $sortable = $this->get_sortable_columns();
            $this->_column_headers = array(
                $columns,
                $hidden,
                $sortable);
            usort($this->table_data, array(&$this, 'usort_reorder'));

            //Get the data for the current page
            $this->paged_data = array_slice($this->table_data,(($current_page-1)*$per_page),$per_page);

            $this->set_pagination_args( array(
                'total_items' => $total_items,
                'per_page'    => $per_page
            ) );

            $this->items = $this->paged_data;
        }

        function get_sortable_columns() {
            $sortable_columns = array(
                'status'  => array('status',true),
				'date' => array('date', true),
				'event' => array('event', true)
            );
            return $sortable_columns;
        }

        function usort_reorder( $a, $b ) {

            //global $pw;
            //$pw->print_var($a);

            // If no sort, default to code
            $orderby = ( ! empty( $_GET['orderby'] ) ) ? $_GET['orderby'] : 'status';

            // If no order, default to asc
            $order = ( ! empty($_GET['order'] ) ) ? $_GET['order'] : 'asc';

            // Determine sort order
            $result = strcmp( strtolower( $a[$orderby] ), strtolower( $b[$orderby] ) );

            // Date column default decending
            if( $orderby == 'status' ) {
                $a_nr = 0;
                $b_nr = 0;

                $s = $a['status'];
                if( $s == 'new' ) { $a_nr = 1; }
                else if( $s == 'inprogress' ) { $a_nr = 2; }
                else if( $s == 'confirmed' ) { $a_nr = 3; }
                else if( $s == 'cancelled' ) { $a_nr = 4; }

                $s = $b['status'];
                if( $s == 'new' ) { $b_nr = 1; }
                else if( $s == 'inprogress' ) { $b_nr = 2; }
                else if( $s == 'confirmed' ) { $b_nr = 3; }
                else if( $s == 'cancelled' ) { $b_nr = 4; }

                if( $a_nr < $b_nr ) {
                    $result = -1;
                }
                else if( $a_nr > $b_nr ) {
                    $result = 1;
                } else {
                    // Take date / time as 2nd sort criteria when equal
                    $a_dt = $a['date'].' '.$a['start_time'].' '.$a['stop_time'];
                    $b_dt = $b['date'].' '.$b['start_time'].' '.$b['stop_time'];

                    $result = strcmp( strtolower( $a_dt ), strtolower( $b_dt ) );

                    $result = -$result; //Most recent on top

                    if( $result == 0 ) {

                        // Take title / location as 3rd sort criteria when equal
                        $a_event = $a['event'].' '.$a['location'];
                        $b_event = $b['event'].' '.$b['location'];

                        $result = strcmp( strtolower( $a_event ), strtolower( $b_event ) );

                        if( $result == 0 ) {
                            // Take lastname as 4th sort criteria when equal
                            $a_ln = $a['last_name'];
                            $b_ln = $b['last_name'];

                            $result = strcmp( strtolower( $a_ln ), strtolower( $b_ln ) );

                            if( $result == 0 ) {
                                // Take firstname as 5th sort criteria when equal
                                $a_fn = $a['first_name'];
                                $b_fn = $b['first_name'];

                                $result = strcmp( strtolower( $a_fn ), strtolower( $b_fn ) );
                            }
                        }
                    }
                }
            }

            // Send final sort direction to usort
            return ( $order === 'asc' ) ? $result : -$result;
        }

        function column_default($item, $column_name)
        {
            require_once('class-util.php');

            global $pw_config;
            $locale = substr(get_locale(), 0, 2 );
            $date_time_format = $pw_config['date_format'][$locale].' H:i';
            $date_format = $pw_config['date_format'][$locale];
            $time_format = 'H:i';
			/*  'count',
				'event_id',
				'event_name',
				'start_date' */
            switch ($column_name) {
                case 'start_date':
                    $dt = date( $date_format ,strtotime( $item['start_date'] ));
                    return $dt;
                case 'event':
                    $event = $item['event'] == "" ? "-" : $item['event'];
                    $location = $item['location'] == "" ? "-" : $item['location'];
                    return '<b>'.$event.'</b><br>'.$location;

                case 'name':
                    $name = trim( $item['last_name'].', '.$item['first_name'].' '.$item['middle_name'] );
                    $company = $item['company'] == "" ? "-" : $item['company'];
                    return '<b>'.$name.'</b><br>'.$company;

                case 'contact':
                    $tmp = array_filter( array( $item['phone'], $item['email']) ); //Remove empty values
                    return implode( '<br>', $tmp );
                case 'status':
                    $to_be_sent = $item['confirm_sent'] == '1' ? '' : '<br><i>'.__('Confirmation to be sent', 'pw-event-manager').'</i>';

                    $status = __('0. New', 'pw-event-manager');
                    if( $item['status'] == 'confirmed' ) {
                        $status = __('1. Confirmed', 'pw-event-manager') ;
                    }
                    if( $item['status'] == 'cancelled' ) {
                        $status = __('2. Cancelled', 'pw-event-manager') ;
                    }

                    $edit_url = '<a href="?page=pw-events-subscribers&action=edit&id='.$item['id'].'">'.$status.'</a>';
                    return $edit_url.$to_be_sent;
                default:
                    return $item[$column_name];
            }
        }

        function display_filtered( $which, $filter = null ) {

            // Prepare the filter data
			/*
            global $pw_config;
            $locale = substr(get_locale(), 0, 2 );
            $date_format = $pw_config['date_format'][$locale];
            $time_format = 'H:i';
            $nr_confirmed = 0;

            $event_date_mdl = new modelEventDate();

            $filtered_search = "";
            $filtered_event_id = 0;
            $filtered_event_date_id = 0;
            $filtered_location_id = 0;

            global $pw;

            // Get the number of confirmed subscribers in the active result set
            foreach( $this->table_data as $k => $v ) {
                if( $v['status'] == 'confirmed' ) {
                    $nr_confirmed += 1;
                }
            }

            if( ! is_null( $filter ) ) {
                $filtered_search = isset( $filter['s'] ) ? $filter['s'] :  "";
                $filtered_event_id = isset( $filter['fe'] ) ? (int) $filter['fe'] :  0;

                // Only show dates when an event is selected
                if( $filtered_event_id > 0 ) {
                    $filtered_event_date_id = isset($filter['fed']) ? (int)$filter['fed'] : 0;
                }

                $filtered_location_id =  isset( $filter['fl'] ) ? (int) $filter['fl'] :  0;
            }

            $filtered =  $event_date_mdl->get_all_filtered( $filtered_event_id, $filtered_event_date_id, $filtered_location_id);

            $events = array();
            $dates = array();
            $locations = array();

            foreach( $filtered as $key => $val ) {
                $events[ $val['eventID'] ] = $val['name'] . ' (' . $val['locale'] . ')';

                $dt = date( $date_format ,strtotime( $val['start_date'] ));
                $tstart = date( $time_format ,strtotime( $val['start_time'] ));
                $tstop = date( $time_format ,strtotime( $val['stop_time'] ));
                $dates[ $val['eventDateID'] ] = $dt . ' ' . $tstart . ' / ' . $tstop;
                $locations[ $val['locationID'] ] = $val['location_name'];
            }

            // Clear dates array when no event is selected to prevent ambigious date selection. (Multiple event can have the same date )
            if( $filtered_event_id <= 0 ) {
                $dates = array();
            }

            if ( $which == "top" ){
                //The code that goes before the table is here

                echo '<div class="pw-table-filters">';
                echo '<input name="search_val" type="text" id="search_val" class="medium-text" value="'.$filtered_search.'" placeholder="'.__('Search', 'pw-event-manager').'"><br>';
                echo '<select class="table_filter" name="filter_event" id="filter_event">';
                echo '<option value="0">'.__('- All events -', 'pw-event-manager') .'</option>';

                if( ! empty( $events ) ) {
                    foreach( $events as $key => $val ) {
                        $selected = $filtered_event_id == $key ? 'selected' : '';
                        echo '<option value="'.$key.'" '.$selected.'>'.stripslashes( $val ).'</option>';
                    }
                }

                echo '</select>';

                if( ! empty( $dates ) ) {
                    echo '<select class="table_filter" name="filter_date" id="filter_date">';
                    echo '<option value="0">'.__('- All dates -', 'pw-event-manager') .'</option>';

                    foreach( $dates as $key => $val ) {
                        $selected = $filtered_event_date_id == $key ? 'selected' : '';
                        echo '<option value="'.$key.'" '.$selected.'>'.stripslashes( $val ).'</option>';
                    }

                    echo '</select>';
                } else {
                    echo '<select class="table_filter" name="filter_date" id="filter_date">';
                    echo '<option value="0">'.__('- Select an event first -', 'pw-event-manager') .'</option>';
                    echo '</select>';
                }


                echo '<select class="table_filter" name="filter_location" id="filter_location">';
                echo '<option value="0">'.__('- All locations -', 'pw-event-manager') .'</option>';
                if( ! empty( $locations ) ) {
                    foreach( $locations as $key => $val ) {
                        $selected = $filtered_location_id == $key ? 'selected' : '';
                        echo '<option value="'.$key.'" '.$selected.'>'.stripslashes( $val ).'</option>';
                    }
                }

                echo '</select>';

                echo '<span>&nbsp;&nbsp;<a href="?page=pw-events-subscribers">'.__('Show all', 'pw-event-manager') .'</a></span>';
                echo '<div class="pw-table-nr-confirmed">'.__('Nr participants:', 'pw-event-manager').' '.$nr_confirmed.'</div>';
                echo '</div>';
            }
			*/
        }

        function get_bulk_actions()
        {
            $actions = array('list_export' => __('Export all to Excel', 'pw-event-manager') );
            return $actions;
        }

        function column_cb($item)
        {
            return sprintf('<input type="checkbox" name="list[]" value="%s" />', $item['id']);
        }

    }
endif;



