<?php

if( !class_exists( 'eventDateEditHtmlView' ) ):


    class eventDateEditHtmlView
    {

        public static function render( $title, $data )
        {
           require_once(plugin_dir_path( plugin_dir_path( __FILE__ )).'/models/model-location.php');
            $mdlLocation = new modelLocation();

            if(isset($data['locale']) && $data['locale'] <> "" ) {
                $locations = $mdlLocation->get_all_by_locale( $data['locale'] );
            }

            $errors = array();
            if( isset($data['errors']) && is_array($data['errors']) ) {
                $errors = $data['errors'];
            }

            ?>
            <div class="wrap">
                <h2>
                    <?php echo( $title ).' '._e('for', 'pw-event-manager').' '.$data['event_name'] ?>
                </h2>
                <p class="description"><?php
                    _e('Last update:', 'pw-event-manager');
                    echo ' '.$data['updated'].' ';
                    _e('by', 'pw-event-manager');
                    echo ' '.$data['updated_by']
                    ?></p>
                <?php
                if( ! empty($errors) ) {
                    echo('<p class="error">');
                    _e('The form contains errors', 'pw-event-manager');
                    echo('</p>');
                };
                ?>
                <form method="POST" name="formTrainerEdit" action="<?php $_SERVER['REQUEST_URI'] ?>" enctype="multipart/form-data">
                    <table class="form-table" style="max-width: 950px;">
                        <tr>
                            <th><label for="start_date"><?php  _e('Start date', 'pw-event-manager') ?> * </label></th>
                            <td><input type="text"  class="medium-text" name="start_date" value="<?php echo $data['start_date']; ?>">
                                <?php

                                if( isset( $errors['start_date']) ) {
                                    echo( '<p class="error">'.implode('<br>', $errors['start_date']).'</p>' );
                                }

                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="start_time"><?php  _e('Start time', 'pw-event-manager') ?> * </label></th>
                            <td><input type="text"  class="medium-text" name="start_time" value="<?php echo $data['start_time']; ?>">
                                <?php

                                if( isset( $errors['start_time']) ) {
                                    echo( '<p class="error">'.implode('<br>', $errors['start_time']).'</p>' );
                                }

                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="stop_time"><?php  _e('Stop time', 'pw-event-manager') ?> * </label></th>
                            <td><input type="text"  class="medium-text" name="stop_time" value="<?php echo $data['stop_time']; ?>">
                                <?php

                                if( isset( $errors['stop_time']) ) {
                                    echo( '<p class="error">'.implode('<br>', $errors['stop_time']).'</p>' );
                                }

                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="locale"><?php  _e('Location', 'pw-event-manager') ?> *</label></th>
                            <td>
                                <select name="date_location" id="date_location">

                                    <option value="0"><?php  _e('- Choose a location -', 'pw-event-manager') ?></option>

                                    <?php
                                    foreach( $locations as $key=>$location ) {

                                        if( $data['locationID'] > 0 ) {
                                            if( $data['locationID'] == $location['id'] ) {
                                                echo '<option value="'.$location['id'].'" selected>'.$location['location_name'].'</option>';
                                            } else {
                                                echo '<option value="'.$location['id'].'">'.$location['location_name'].'</option>';
                                            }
                                        } else {
                                            echo '<option value="'.$location['id'].'">'.$location['location_name'].'</option>';
                                        }
                                    }

                                    ?>

                                </select>
                                <?php
                                if( isset( $errors['date_location']) ) {
                                    echo( '<p class="error">'.implode('<br>', $errors['date_location']).'</p>' );
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="locale"><?php  _e('Subscribe', 'pw-event-manager') ?> *</label></th>
                            <td>
                                <select name="status" id="date_status">
                                    <?php
                                        $selected = isset($data['status']) && $data['status'] == "open" ? 'selected' : "";
                                        echo '<option value="open" '.$selected.'>'.__('Open for subscription', 'pw-event-manager').'</option>';

                                        $selected = isset($data['status']) && $data['status'] == "full" ? 'selected' : "";
                                        echo '<option value="full" '.$selected.'>'.__('Full, subscription not possible', 'pw-event-manager').'</option>';

                                        $selected = isset($data['status']) && $data['status'] == "closed" ? 'selected' : "";
                                        echo '<option value="closed" '.$selected.'>'.__('Closed for subscription', 'pw-event-manager').'</option>';
                                    ?>
                                </select>
                                <?php
                                if( isset( $errors['date_location']) ) {
                                    echo( '<p class="error">'.implode('<br>', $errors['date_location']).'</p>' );
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="other_dates"><?php  _e('Other dates', 'pw-event-manager') ?></label></th>
                            <td>
                                <input type="text" class="regular-text" name="other_dates" placeholder="<?php _e('Other dates for this event', 'pw-event-manager') ?>"
                                       value="<?php echo $data['other_dates']; ?>">
                        </tr>
                        <tr>
                            <th><label for="alt_price_early_bird"><?php  _e('Special early bird price', 'pw-event-manager') ?></label></th>
                            <td>
                                &euro;
                                <input type="text"  class="medium-text" name="alt_price_early_bird" placeholder="0,00"
                                       value="<?php echo $data['alt_price_early_bird']; ?>">
                            </td>
                        </tr>
                        <tr>
                            <th><label for="alt_price_normal"><?php  _e('Special normal price', 'pw-event-manager') ?></label></th>
                            <td>
                                &euro;
                                <input type="text"  class="medium-text" name="alt_price_normal" placeholder="0,00"
                                       value="<?php echo $data['alt_price_normal']; ?>">
                            </td>
                        </tr>
                        <tr>
                            <th><label for="alt_price_last_minute"><?php  _e('Special last minute price', 'pw-event-manager') ?></label></th>
                            <td>
                                &euro;
                                <input type="text"  class="medium-text" name="alt_price_last_minute" placeholder="0,00"
                                       value="<?php echo $data['alt_price_last_minute']; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" class="button button-primary" name="submitEventDate" value="<?php  _e('Save', 'pw-event-manager') ?>">
                                <input type="submit" class="button button-primary-2" name="submitAndCloseEventDate" value="<?php  _e('Save & close', 'pw-event-manager') ?>">
                                <input type="submit" class="button button-secondary" name="closeEventDate" value="<?php  _e('Cancel', 'pw-event-manager') ?>">
                            </td>
                        </tr>
                    </table>

                </form>
            </div>
        <?php
        }
    }

endif;

?>