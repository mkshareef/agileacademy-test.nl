<?php
/**
 * Direct access to the script is not allowed
 */
defined('ABSPATH') or die("No script kiddies please!");

require_once ( plugin_dir_path( plugin_dir_path(__file__) ).'classes/class-wp-list-table.php');

if( !class_exists( 'invoiceTable' ) ):

    class invoiceTable extends PWP_List_Table
    {

        private $table_data = array();
        private $paged_data = array();

        public function __construct()
        {
            parent::__construct();
        }

        function set_table_data($data)
        {
            $list = array();

            for ($i = 0; $i < count($data); ++$i) {
                $list[$i]['id'] = (int) $data[$i]['id'];
                $list[$i]['invoice_nr'] = stripslashes( $data[$i]['invoice_nr'] );
                $list[$i]['invoice_date'] = stripslashes( $data[$i]['invoice_date'] );
                $list[$i]['invoice_company'] = stripslashes( $data[$i]['invoice_company'] );
                $list[$i]['payment_method'] = stripslashes( $data[$i]['payment_method'] );
                $list[$i]['status'] = stripslashes( $data[$i]['status'] );
                $list[$i]['description'] = stripslashes( $data[$i]['description'] );
                $list[$i]['subscribers'] = stripslashes( $data[$i]['subscribers'] );
                $list[$i]['invoice_lang'] = stripslashes( $data[$i]['invoice_lang'] );
            }

            $this->table_data = $list;
        }

        function get_columns()
        {
            $columns = array(
                'cb' => '<input type="checkbox" />',
                'invoice_nr' => __('Nr', 'pw-event-manager'),
                'invoice_date' => __('Invoice date', 'pw-event-manager'),
                'invoice_lang' => __('Language', 'pw-event-manager'),
                'description' => __('Description', 'pw-event-manager'),
                'payment_method' => __('Payment', 'pw-event-manager'),
                'invoice_company' => __('Participants', 'pw-event-manager')
            );

            return $columns;
        }

        function prepare_items()
        {
            //For paging
            $per_page = 20;
            $current_page = $this->get_pagenum();
            $total_items = count($this->table_data);

            $columns = $this->get_columns();
            $hidden = array();
            $sortable = $this->get_sortable_columns();
            $this->_column_headers = array(
                $columns,
                $hidden,
                $sortable);
            usort($this->table_data, array(&$this, 'usort_reorder'));

            //Get the data for the current page
            $this->paged_data = array_slice($this->table_data,(($current_page-1)*$per_page),$per_page);

            $this->set_pagination_args( array(
                'total_items' => $total_items,
                'per_page'    => $per_page
            ) );

            $this->items = $this->paged_data;
        }

        function get_sortable_columns() {
            $sortable_columns = array(
                'invoice_nr'  => array('invoice_nr',true)
            );
            return $sortable_columns;
        }

        function usort_reorder( $a, $b ) {

            //global $pw;
            //$pw->print_var($a);

            // If no sort, default to code
            $orderby = ( ! empty( $_GET['orderby'] ) ) ? $_GET['orderby'] : 'invoice_nr';

            // If no order, default to asc
            $order = ( ! empty($_GET['order'] ) ) ? $_GET['order'] : 'asc';

            // Determine sort order
            $result = strcmp( strtolower( $a[$orderby] ), strtolower( $b[$orderby] ) );

            if( $orderby = 'invoice_nr' ) {
                $result = - $result;
            }

            // Send final sort direction to usort
            return ( $order === 'asc' ) ? $result : -$result;
        }

        function column_default($item, $column_name)
        {
            require_once('class-util.php');

            global $pw_config;
            $locale = substr(get_locale(), 0, 2 );
            $date_format = $pw_config['date_format'][$locale];

            switch ($column_name) {
                case 'invoice_nr':
                    $edit_url = '<a href="?page=pw-events-invoices&id='.$item['id'].'">'.$item['invoice_nr'].'</a>';
                    return $item['invoice_nr'] == "" ? "-" : $edit_url;
                case 'invoice_date':
                    $dt = date( $date_format ,strtotime( $item['invoice_date'] ));
                    return $dt;
                case 'invoice_company':
                    $company = $item['invoice_company'] == "" ? "-" : $item['invoice_company'];
                    $subscribers = $item['subscribers'] == "" ? "-" : $item['subscribers'];
                    return '<b>'.$company.'</b><br>'.$subscribers;
                case 'payment_method':
                    $m = $item['payment_method'] == "" ? "-" : $item['payment_method'];
                    $s = $item['status'] == "" ? "-" : $item['status'];

                    $method = __('Invoice', 'pw-event-manager');
                    if( $m == 'creditcard') {
                        $method = __('Creditcard', 'pw-event-manager');
                    } else if( $m == 'ideal') {
                        $method = __('iDeal', 'pw-event-manager');
                    }

                    $status = '';
                    if( $m <> 'invoice') {
                        if( $s == 'payed') {
                            $status = __('Payed', 'pw-event-manager');
                        } else {
                            $status = __('Open', 'pw-event-manager');
                        }
                    }

                    return '<b>'.$method.'</b><br>'.$status;
                case 'invoice_lang':
                    return strtoupper( $item['invoice_lang'] );
                case 'description':

                    $dArray = explode( ', ', $item['description'] );

                    $l = strtolower( $item['invoice_lang'] );
                    $df = $pw_config['date_format'][$l];

                    $dArray[1] =  date( $df ,strtotime( $dArray[1] ));

                    $description = implode( ', ', $dArray );
                    return $description;
                default:
                    return "-";
            }
        }


        function display_filtered( $which, $filter = null ) {

            // Prepare the filter data

            global $pw_config;
            $locale = substr(get_locale(), 0, 2 );
            $date_format = $pw_config['date_format'][$locale];

            $filtered_search = "";
            $filtered_date = "";

            global $pw;

            if( ! is_null( $filter ) ) {
                $filtered_search = isset( $filter['s'] ) ? $filter['s'] : "";
                $filtered_date = isset( $filter['fd'] ) ? $filter['fd'] : "";
            }

            $dates = array();

            foreach( $this->table_data as $key => $val ) {
                $dt = date( 'Y-m-d' ,strtotime( $val['invoice_date'] ));
                $dates[] = $dt;
            }

            $dates = array_unique( $dates );
            asort($dates);
            $dates = array_reverse($dates);

            if ( $which == "top" ){
                //The code that goes before the table is here

                echo '<div class="pw-table-filters">';
                echo '<input name="search_inv_val" type="text" id="search_inv_val" class="medium-text" value="'.$filtered_search.'" placeholder="'.__('Search', 'pw-event-manager').'"> ';
                echo '<select class="table_filter" name="filter_inv_date" id="filter_inv_date">';
                echo '<option value="">'.__('- All dates -', 'pw-event-manager') .'</option>';

                if( ! empty( $dates ) ) {
                    foreach( $dates as $key => $val ) {
                        $selected = ( $filtered_date == $val and $filtered_date <> "" ) ? 'selected' : '';

                        $dt_dsp = date( $date_format ,strtotime( $val ));
                        echo '<option value="'.$val.'" '.$selected.'>'.$dt_dsp.'</option>';
                    }
                }

                echo '</select>';

                echo '<span>&nbsp;&nbsp;<a href="?page=pw-events-invoices">'.__('Show all', 'pw-event-manager') .'</a></span>';
                echo '</div>';
            }

        }


        function get_bulk_actions()
        {
            $actions = array(
                'print' => __('Print selected', 'pw-event-manager'),
                'mailtocfo' => __('Mail to CFO', 'pw-event-manager'),
                'invoicesummary' => __('Revenue per event', 'pw-event-manager'),
                'subpermonth' => __('Subscribers per event', 'pw-event-manager')
            );
            return $actions;
        }

        function column_cb($item)
        {
            return sprintf('<input type="checkbox" name="list[]" value="%s" />', $item['id']);
        }

    }

endif;



