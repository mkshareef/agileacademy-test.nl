<?php
/**
 * Direct access to the script is not allowed
 */
defined('ABSPATH') or die("No script kiddies please!");

require_once ( plugin_dir_path( plugin_dir_path(__file__) ).'classes/class-wp-list-table.php');

if( !class_exists( 'subscriptionTable' ) ):

    class subscriptionTable extends PWP_List_Table
    {

        private $table_data = array();
        private $count = 0;
        private $paged_data = array();

        public function __construct()
        {
            parent::__construct();
        }

        function set_table_data($data, $count)
        {

            $list = array();

            for ($i = 0; $i < count($data); ++$i) {
                $list[$i]['id'] = (int) $data[$i]['id'];
                $list[$i]['created'] =  $data[$i]['created'];
                $list[$i]['status'] = stripslashes( $data[$i]['status'] );
                $list[$i]['name'] = stripslashes( $data[$i]['event_details']['name'] );
                $list[$i]['date'] = stripslashes( $data[$i]['event_details']['date'] );
                $list[$i]['start_time'] = stripslashes( $data[$i]['event_details']['start_time'] );
                $list[$i]['stop_time'] = stripslashes( $data[$i]['event_details']['stop_time'] );
                $list[$i]['other_dates'] = stripslashes( $data[$i]['event_details']['other_dates'] );
                $list[$i]['location_name'] = stripslashes( $data[$i]['event_details']['location_name'] );

                $list[$i]['invoice_nr'] = stripslashes( $data[$i]['invoice']['invoice_nr'] );
                $list[$i]['payment_method'] = stripslashes( $data[$i]['invoice']['payment_method'] );
                $list[$i]['invoice_status'] = stripslashes( $data[$i]['invoice']['status'] );
                $list[$i]['transaction_status'] = stripslashes( $data[$i]['invoice']['transaction_status'] );

                $list[$i]['company'] = stripslashes( $data[$i]['company'] );

                $subscriber_array = array();
                if( is_array( $data[$i]['subscribers'] ) ) {
                    foreach( $data[$i]['subscribers'] as $subscriber ) {
                        $fname = stripslashes( $subscriber['first_name'] );
                        $mname = stripslashes( $subscriber['middle_name'] );
                        $lname = stripslashes( $subscriber['last_name'] );
                        $subscriber_array[] = str_replace( "  ", " ", trim( $fname.' '.$mname.' '.$lname ) );
                    }
                }

                $list[$i]['subscribers'] = implode( '<br>', $subscriber_array );
            }

            $this->table_data = $list;
            $this->count = $count;
        }

        function get_columns()
        {
            $columns = array(
                'cb' => '<input type="checkbox" />',
                'status' => __('Status', 'pw-event-manager'),
                'created' => __('Received', 'pw-event-manager'),
                'event' => __('Event', 'pw-event-manager'),
                'subscribers' => __('Subscribers', 'pw-event-manager'),
                'payment' => __('Invoice', 'pw-event-manager'),
            );

            return $columns;
        }

        function prepare_items( $show_all = false )
        {
            //For paging
            $per_page = 20;
            $current_page = $this->get_pagenum();
            $total_items = $this->count;

            // This preserves the count of the nr of results
            if( $show_all == true ) {
                $per_page = count( $this->table_data );
            }


            //print_r($total_items);exit;

            $columns = $this->get_columns();
            $hidden = array();
            $sortable = $this->get_sortable_columns();
            $this->_column_headers = array(
                $columns,
                $hidden,
                $sortable);
            usort($this->table_data, array(&$this, 'usort_reorder'));

            //Get the data for the current page
            $this->paged_data = $this->table_data;

            $this->set_pagination_args( array(
                'total_items' => $total_items,
                'per_page'    => $per_page
            ) );

            $this->items = $this->paged_data;
        }

        function get_sortable_columns() {
            $sortable_columns = array(
                'status'  => array('status',true),
                'created' => array('created',true)
            );
            return $sortable_columns;
        }

        function usort_reorder( $a, $b ) {
            // If no sort, default to code
            global $pw;
            //$pw->print_var($a);
            $orderby = ( ! empty( $_GET['orderby'] ) ) ? $_GET['orderby'] : 'status';

            // If no order, default to asc
            $order = ( ! empty($_GET['order'] ) ) ? $_GET['order'] : 'asc';

            // Determine sort order
            $result = strcmp( strtolower( $a[$orderby] ), strtolower( $b[$orderby] ) );

            // Date column default decending
            if( $orderby == 'created') {
                $result = -$result;
            } elseif( $orderby == 'status' ) {
                $a_nr = 0;
                $b_nr = 0;

                $s = $a['status'];
                if( $s == 'new' ) { $a_nr = 1; }
                else if( $s == 'inprogress' ) { $a_nr = 2; }
                else if( $s == 'confirmed' ) { $a_nr = 3; }
                else if( $s == 'cancelled' ) { $a_nr = 4; }

                $s = $b['status'];
                if( $s == 'new' ) { $b_nr = 1; }
                else if( $s == 'inprogress' ) { $b_nr = 2; }
                else if( $s == 'confirmed' ) { $b_nr = 3; }
                else if( $s == 'cancelled' ) { $b_nr = 4; }

                if( $a_nr < $b_nr ) {
                    $result = -1;
                }
                else if( $a_nr > $b_nr ) {
                    $result = 1;
                } else {
                    // Take date as sort criteria when equal
                    $result = strcmp( strtolower( $a['created'] ), strtolower( $b['created'] ) );
                    $result = -$result;
                }
            }

            // Send final sort direction to usort
            return ( $order === 'asc' ) ? $result : -$result;
        }

        function column_default($item, $column_name)
        {
            require_once('class-util.php');

            global $pw_config;
            $locale = substr(get_locale(), 0, 2 );
            $date_time_format = $pw_config['date_format'][$locale].' H:i';
            $date_format = $pw_config['date_format'][$locale];
            $time_format = 'H:i';

            switch ($column_name) {
                case 'status':
                    $s = $item['status'] == "" ? "-" : $item['status'];

                    if( $s == 'new' ) {
                        $status = __('1. New', 'pw-event-manager');
                    }
                    if( $s == 'inprogress' ) {
                        $status = __('2. In progress', 'pw-event-manager');
                    }
                    if( $s == 'confirmed' ) {
                        $status = __('3. Confirmed', 'pw-event-manager');
                    }
                    if( $s == 'cancelled' ) {
                        $status = __('4. Cancelled', 'pw-event-manager');
                    }

                    $edit_url = '<a href="?page=pw-events-subscriptions&id='.$item['id'].'">'.$status.'</a>';
                    return $edit_url;
                case 'created':
                    $dt = date( $date_time_format ,strtotime( $item['created'] ));
                    return $dt;
                case 'event':
                    $name = $item['name'] == "" ? "-" : $item['name'];
                    $dt = date( $date_format ,strtotime( $item['date'] ));
                    $start = date( $time_format ,strtotime( $item['start_time'] ));
                    $stop = date( $time_format ,strtotime( $item['stop_time'] ));
                    $location = $item['location_name'] == "" ? "-" : $item['location_name'];
                    $details = '<b>'.$name.'</b><br>'.$dt.'<br>'.$start.' - '.$stop.'<br>'.$location;
                    return $details;
                case 'payment':
                    $invoice_nr = $item['invoice_nr'] == "" ? "" : '<b>'.$item['invoice_nr'].'</b>';

                    $payment_method = "";
                    $invoice_status = "";
                    if( $item['payment_method'] == 'creditcard' ) {
                        $payment_method = __('Creditcard', 'pw-event-manager');
                        $invoice_status = $item['invoice_status'] == "payed" ? __('Payed', 'pw-event-manager') : __('Open', 'pw-event-manager');
                    }
                    else if( $item['payment_method'] == 'ideal' ) {
                        $payment_method = __('iDeal', 'pw-event-manager');
                        $invoice_status = $item['invoice_status'] == "payed" ? __('Payed', 'pw-event-manager') : __('Open', 'pw-event-manager');
                    }
                    else if( $item['payment_method'] == 'invoice' ) {
                        $payment_method = __('Invoice', 'pw-event-manager');
                        $invoice_status = $item['invoice_status'] == "payed" ? __('Payed', 'pw-event-manager') : __('Open', 'pw-event-manager');
                    }


                    $details = array( $invoice_nr, $payment_method, $invoice_status);
                    $details = array_filter($details); //Strip empty elements

                    $result = implode('<br>', $details);
                    return $result == '' ? __('Free', 'pw-event-manager') : $result;

                case 'subscribers':
                    $company = $item['company'] == "" ? "" : $item['company'];
                    $subscribers = $item['subscribers'] == "" ? "" : $item['subscribers'];

                    return '<b>'.$company.'</b><br>'.$subscribers;
                default:
                    return "-";
            }
        }

        function display_filtered( $which, $filter = null ) {

            // Prepare the filter data

            global $pw_config;
            $locale = substr(get_locale(), 0, 2 );
            $date_format = $pw_config['date_format'][$locale];
            $time_format = 'H:i';

            $event_date_mdl = new modelEventDate();

            $filtered_search = "";
            $filtered_event_id = 0;
            $filtered_event_date_id = 0;
            $filtered_location_id = 0;

            global $pw;

            if( ! is_null( $filter ) ) {
                $filtered_search = isset( $filter['s'] ) ? $filter['s'] :  "";
                $filtered_event_id = isset( $filter['fe'] ) ? (int) $filter['fe'] :  0;

                // Only show dates when an event is selected
                if( $filtered_event_id > 0 ) {
                    $filtered_event_date_id = isset($filter['fed']) ? (int)$filter['fed'] : 0;
                }

                $filtered_location_id =  isset( $filter['fl'] ) ? (int) $filter['fl'] :  0;
            }

            $filtered =  $event_date_mdl->get_all_filtered( $filtered_event_id, $filtered_event_date_id, $filtered_location_id);

            $events = array();
            $dates = array();
            $locations = array();

            foreach( $filtered as $key => $val ) {
                $events[ $val['eventID'] ] = $val['name'] . ' (' . $val['locale'] . ')';

                $dt = date( $date_format ,strtotime( $val['start_date'] ));
                $tstart = date( $time_format ,strtotime( $val['start_time'] ));
                $tstop = date( $time_format ,strtotime( $val['stop_time'] ));
                $dates[ $val['eventDateID'] ] = $dt . ' ' . $tstart . ' / ' . $tstop;
                $locations[ $val['locationID'] ] = $val['location_name'];
            }

            // Clear dates array when no event is selected to prevent ambigious date selection. (Multiple event can have the same date )
            if( $filtered_event_id <= 0 ) {
                $dates = array();
            }

            if ( $which == "top" ){
                //The code that goes before the table is here

                echo '<div class="pw-table-filters">';
                echo '<input name="search_val" type="text" id="search_sub_val" class="medium-text" value="'.$filtered_search.'" placeholder="'.__('Search', 'pw-event-manager').'"><br>';
                echo '<select class="table_filter" name="filter_event" id="filter_sub_event">';
                echo '<option value="0">'.__('- All events -', 'pw-event-manager') .'</option>';

                if( ! empty( $events ) ) {
                    foreach( $events as $key => $val ) {
                        $selected = $filtered_event_id == $key ? 'selected' : '';
                        echo '<option value="'.$key.'" '.$selected.'>'.stripslashes( $val ).'</option>';
                    }
                }

                echo '</select>';

                if( ! empty( $dates ) ) {
                    echo '<select class="table_filter" name="filter_date" id="filter_sub_date">';
                    echo '<option value="0">'.__('- All dates -', 'pw-event-manager') .'</option>';

                    foreach( $dates as $key => $val ) {
                        $selected = $filtered_event_date_id == $key ? 'selected' : '';
                        echo '<option value="'.$key.'" '.$selected.'>'.stripslashes( $val ).'</option>';
                    }

                    echo '</select>';
                } else {
                    echo '<select class="table_filter" name="filter_date" id="filter_sub_date">';
                    echo '<option value="0">'.__('- Select an event first -', 'pw-event-manager') .'</option>';
                    echo '</select>';
                }


                echo '<select class="table_filter" name="filter_location" id="filter_sub_location">';
                echo '<option value="0">'.__('- All locations -', 'pw-event-manager') .'</option>';
                if( ! empty( $locations ) ) {
                    foreach( $locations as $key => $val ) {
                        $selected = $filtered_location_id == $key ? 'selected' : '';
                        echo '<option value="'.$key.'" '.$selected.'>'.stripslashes( $val ).'</option>';
                    }
                }

                echo '</select>';

                echo '<span>&nbsp;&nbsp;<a href="?page=pw-events-subscriptions">'.__('Show all', 'pw-event-manager') .'</a></span>';
                echo '</div>';
            }
        }

        function get_bulk_actions()
        {
            $actions = array('list_export' => __('Export to Excel', 'pw-event-manager') );
            return $actions;
        }

        function column_cb($item)
        {
            return sprintf('<input type="checkbox" name="list[]" value="%s" />', $item['id']);
        }

    }

endif;



