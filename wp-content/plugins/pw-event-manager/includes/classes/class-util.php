<?php
/**
 * Direct access to the script is not allowed
 */
defined('ABSPATH') or die("No script kiddies please!");

//require_once ( plugin_dir_path( plugin_dir_path(__file__) ).'models/model-trainer.php');
require_once ( plugin_dir_path( plugin_dir_path(__file__) ).'classes/class-wp-list-table.php');

if( !class_exists( 'util' ) ):

    class util
    {

        public function __construct()
        {

        }

        /**
         * @return string browser IP address of the current user
         */
        static function get_user_ip() {

            $client  = @$_SERVER['HTTP_CLIENT_IP'];
            $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
            $remote  = $_SERVER['REMOTE_ADDR'];

            if(filter_var($client, FILTER_VALIDATE_IP))
            {
                $ip = $client;
            }
            elseif(filter_var($forward, FILTER_VALIDATE_IP))
            {
                $ip = $forward;
            }
            else
            {
                $ip = $remote;
            }

            return $ip;

        }

        static function get_image_src( $attachment_path ) {

            $upload_dir = wp_upload_dir();

            return( $upload_dir['baseurl'].'/'.$attachment_path ); //url

        }

        /**
         * @param mixed $file_obj
         * @param string $target_dir Relative to the WP upload directory
         * @return string Directory relative to wp upload directory
         */
        static function upload_img( $file_obj, $target_dir ) {

            $result = "";
            $f ="";
            $upload_dir = wp_upload_dir();

            // Upload succeeded?
            if ($file_obj['error'] !== UPLOAD_ERR_OK) {
                return "";
            }

            // Able to determine image type of uploaded file?
            $info = getimagesize($file_obj['tmp_name']);
            if ($info === FALSE) {
                return "";
            }

            // Is an image?
            if (($info[2] !== IMAGETYPE_GIF) && ($info[2] !== IMAGETYPE_JPEG) && ($info[2] !== IMAGETYPE_PNG)) {
                return "";
            }

            //Create target_dir in the Uploads dir
            wp_mkdir_p( $upload_dir['basedir'].'/'.$target_dir );

            $upload_file = $upload_dir['basedir'] .'/'. $target_dir .'/'. $file_obj['name'];

            $n = 0;

            $f = pathinfo( $upload_file );

            $new_file = $upload_file;

            while( file_exists( $new_file ) ) {

                $n++;
                $new_file = "";

                if( $f['dirname'] <> "" ) {
                    $new_file .= $f['dirname'].'/';
                }

                if( $f['filename'] <> "" ) {
                    $new_file .= $f['filename'].'('.$n.')';
                }

                if( $f['extension'] <> "" ) {
                    $new_file .= '.'.$f['extension'];
                }

            }

            if( $new_file <> "" ) {
                $upload_file = $new_file;
            }

            if (! file_exists($upload_file) ) {

                if ( move_uploaded_file($file_obj['tmp_name'], $upload_file) ) {
                    chmod( $upload_file, 0664 );
                }

            }

            $f = pathinfo( $upload_file );
            $result = $target_dir .'/'. $f['basename'];

            return $result;

        }

        static function delete_img( $img_path ) {
            $upload_dir = wp_upload_dir();
            $file = $upload_dir['basedir'].'/'.$img_path;

            if ( file_exists($file) ) {
                unlink($file);
            }
        }

        static function upload_file( $file_obj, $target_dir ) {

            $result = "";
            $f ="";
            $upload_dir = wp_upload_dir();

            // Upload succeeded?
            if ($file_obj['error'] !== UPLOAD_ERR_OK) {
                return "";
            }

            //Create target_dir in the Uploads dir
            wp_mkdir_p( $upload_dir['basedir'].'/'.$target_dir );

            $upload_file = $upload_dir['basedir'] .'/'. $target_dir .'/'. $file_obj['name'];

            $n = 0;

            $f = pathinfo( $upload_file );

            $new_file = $upload_file;

            while( file_exists( $new_file ) ) {

                $n++;
                $new_file = "";

                if( $f['dirname'] <> "" ) {
                    $new_file .= $f['dirname'].'/';
                }

                if( $f['filename'] <> "" ) {
                    $new_file .= $f['filename'].'('.$n.')';
                }

                if( $f['extension'] <> "" ) {
                    $new_file .= '.'.$f['extension'];
                }

            }

            if( $new_file <> "" ) {
                $upload_file = $new_file;
            }

            if (! file_exists($upload_file) ) {

                if ( move_uploaded_file($file_obj['tmp_name'], $upload_file) ) {
                    chmod( $upload_file, 0664 );
                }

            }

            $f = pathinfo( $upload_file );
            $result = $target_dir .'/'. $f['basename'];

            return $result;

        }

        static function delete_file( $file_path ) {
            $upload_dir = wp_upload_dir();
            $file = $upload_dir['basedir'].'/'.$file_path;

            if ( file_exists($file) ) {
                unlink($file);
            }
        }

        static function get_file_path( $attachment_path ) {

            $upload_dir = wp_upload_dir();

            return( $upload_dir['baseurl'].'/'.$attachment_path ); //url

        }

        static function get_pricing_type( $event_date ) {

            //Early bird	€ 1.195,- 	Valid until 61 days before the training date
            //Normal rate	€ 1.495,-	Between 61 and 21 days before training date.
            //Last minute	€ 995,-	    Starts 21 days before the training date.

            $date_early = strtotime($event_date. ' - 61 days');
            $date_last = strtotime($event_date. ' - 21 days');

            $date_now = strtotime( current_time("Y-m-d") );

            $result = 'normal';

            if( $date_now >= $date_last ) {
                $result = 'lastminute';
            } elseif( $date_now <= $date_early ) {
                $result = 'early';
            }

            return $result;
        }

        static function generate_slug($string){
            $slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
            return strtolower( $slug );
        }

    }

endif;