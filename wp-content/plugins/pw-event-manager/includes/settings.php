<?php

global $pw_config;

$pw_config['dev_mode'] = false; //Set to false for production sites, true for development

// Set the primary language for the pluging. In case the language cannot be determined,
// the plugin will fallback to english.
// nl = Dutch, de=German, en=English
$pw_config['lang_primary'] = 'nl';

$pw_config['date_format']['nl'] = 'd-m-Y';
$pw_config['date_format']['de'] = 'd.m.Y';
$pw_config['date_format']['en'] = 'm/d/Y';

$pw_config['invoice_nr_prefix'] = 'PROACEVT';  // Fixed part of the invoice nr
$pw_config['invoice_nr_length'] = 6;           // The number of digits for the number part

// Format Agile Academy <agileacademy@prowareness.nl>
// The space before < is important!
$pw_config['default_notify_from'] = "Agile Academy <agileacademy@prowareness.nl>";
$pw_config['cfo_email'] = 'cfooffice@prowareness.nl';

// Separate multiple email addresses with a ;
$pw_config['default_internal_notify'] = 'EventNotificationNL@prowareness.nl';

// Absolute url to Terms & Conditions (Algemene voorwaarden)
$pw_config['url_term_cond']['nl'] = 'http://www.agileacademy.nl/algemene-voorwaarden/';
$pw_config['url_term_cond']['de'] = 'http://www.agileacademy.nl/en/algemenevoorwaarden';
$pw_config['url_term_cond']['en'] = 'http://www.agileacademy.nl/de/algemenevoorwaarden';

//TODO implement VAT calculation for the subscribtion because of Germany
$pw_config['invoice_vat_percent'] = 0;         // The VAT percentage added to the invoice

/**
 * The META information for old invoices is not allowed to change. Therefore each invoice stores its version and
 * uses the array below for the lookup of the information for the specific version.
 * When you need to change the information for the invoice, increase the invoice_version and add additional
 * array entries for the new version
 */
$pw_config['invoice_version'] = 2;

//Lang=NL invoice version details
$pw_config['invoice_meta_nl'][1] = array(
    'company_name' => 'Prowareness Academy BV',
    'company_address' => 'Brassersplein 1',
    'company_zip' => '2612 CT',
    'company_city' => 'Delft',
    'company_country' => 'Nederland',
    'company_phone' => '015 - 241 18 00',
    'company_fax' => '015 - 241 18 21',
    'company_email' => 'agileacademy@prowareness.nl',
    'company_website' => 'http://www.prowareness.nl',
    'company_bank' => '180.748.343',
    'company_iban' => 'NL84 RABO 0180 748 343',
    'company_swift_bic' => 'RABONL2U',
    'company_kvk' => '60393106 te Delft',
    'company_vat' => '8538.90.778.B.01',
    'company_tax' => '8538.90.778.L.01',
    'company_note' => 'Vrijgesteld van omzetbelasting op grond van art 11-1-o wet omzetbelasting 1968.',
    'company_footer' => 'Het bedrag van deze factuur dient <b>binnen 30 dagen na factuurdatum</b> bijgeschreven te worden op onze Rabobankrekening  NL84 RABO 0180 748 343 t.n.v. Prowareness Academy B.V. onder vermelding van bovenvermeld factuurnummer.'

);

$pw_config['invoice_meta_nl'][2] = array(
    'company_name' => 'Prowareness Academy BV',
    'company_address' => 'Brassersplein 1',
    'company_zip' => '2612 CT',
    'company_city' => 'Delft',
    'company_country' => 'Nederland',
    'company_phone' => '015 - 241 18 00',
    'company_fax' => '015 - 241 18 21',
    'company_email' => 'agileacademy@prowareness.nl',
    'company_website' => 'http://www.prowareness.nl',
    'company_bank' => '180.748.343',
    'company_iban' => 'NL84 RABO 0180 748 343',
    'company_swift_bic' => 'RABONL2U',
    'company_kvk' => '60393106 te Delft',
    'company_vat' => '8538.90.778.B.01',
    'company_tax' => '8538.90.778.L.01',
    'company_note' => 'Vrijgesteld van omzetbelasting op grond van art 11-1-o wet omzetbelasting 1968.',
    'company_footer' => 'Het bedrag van deze factuur dient <b>binnen 7 dagen na factuurdatum</b> bijgeschreven te worden op onze Rabobankrekening  NL84 RABO 0180 748 343 t.n.v. Prowareness Academy B.V. onder vermelding van bovenvermeld factuurnummer.'
);

//Lang=DE invoice version details
$pw_config['invoice_meta_de'][1] = array(
    'company_name' => 'Prowareness Academy BV',
    'company_address' => 'Brassersplein 1',
    'company_zip' => '2612 CT',
    'company_city' => 'Delft',
    'company_country' => 'Niederlande',
    'company_phone' => '015 - 241 18 00',
    'company_fax' => '015 - 241 18 21',
    'company_email' => 'agileacademy@prowareness.nl',
    'company_website' => 'http://www.prowareness.nl',
    'company_bank' => '180.748.343',
    'company_iban' => 'NL84 RABO 0180 748 343',
    'company_swift_bic' => 'RABONL2U',
    'company_kvk' => '60393106 te Delft',
    'company_vat' => '8538.90.778.B.01',
    'company_tax' => '8538.90.778.L.01',
    'company_note' => 'Vrijgesteld van omzetbelasting op grond van art 11-1-o wet omzetbelasting 1968.',
    'company_footer' => 'Het bedrag van deze factuur dient <b>binnen 30 dagen na factuurdatum</b> bijgeschreven te worden op onze Rabobankrekening  NL84 RABO 0180 748 343 t.n.v. Prowareness Academy B.V. onder vermelding van bovenvermeld factuurnummer.'

);

$pw_config['invoice_meta_de'][2] = array(
    'company_name' => 'Prowareness Academy BV',
    'company_address' => 'Brassersplein 1',
    'company_zip' => '2612 CT',
    'company_city' => 'Delft',
    'company_country' => 'Niederlande',
    'company_phone' => '015 - 241 18 00',
    'company_fax' => '015 - 241 18 21',
    'company_email' => 'agileacademy@prowareness.nl',
    'company_website' => 'http://www.prowareness.nl',
    'company_bank' => '180.748.343',
    'company_iban' => 'NL84 RABO 0180 748 343',
    'company_swift_bic' => 'RABONL2U',
    'company_kvk' => '60393106 te Delft',
    'company_vat' => '8538.90.778.B.01',
    'company_tax' => '8538.90.778.L.01',
    'company_note' => 'Vrijgesteld van omzetbelasting op grond van art 11-1-o wet omzetbelasting 1968.',
    'company_footer' => 'Het bedrag van deze factuur dient <b>binnen 7 dagen na factuurdatum</b> bijgeschreven te worden op onze Rabobankrekening  NL84 RABO 0180 748 343 t.n.v. Prowareness Academy B.V. onder vermelding van bovenvermeld factuurnummer.'

);

//Lang=EN invoice version details
$pw_config['invoice_meta_en'][1] = array(
    'company_name' => 'Prowareness Academy BV',
    'company_address' => 'Brassersplein 1',
    'company_zip' => '2612 CT',
    'company_city' => 'Delft',
    'company_country' => 'Netherlands',
    'company_phone' => '015 - 241 18 00',
    'company_fax' => '015 - 241 18 21',
    'company_email' => 'agileacademy@prowareness.nl',
    'company_website' => 'http://www.prowareness.nl',
    'company_bank' => '180.748.343',
    'company_iban' => 'NL84 RABO 0180 748 343',
    'company_swift_bic' => 'RABONL2U',
    'company_kvk' => '60393106 te Delft',
    'company_vat' => '8538.90.778.B.01',
    'company_tax' => '8538.90.778.L.01',
    'company_note' => 'Exempt from tax under Section 11-1-o VAT Law in 1968.',
    'company_footer' => '<b>Within 30 days of the invoice date </b> the amount of this invoice must  be credited to our Rabobank account NL84 RABO 0180 748 343 to the attention of Prowareness Academy B.V. citing above invoice number.'

);

$pw_config['invoice_meta_en'][2] = array(
    'company_name' => 'Prowareness Academy BV',
    'company_address' => 'Brassersplein 1',
    'company_zip' => '2612 CT',
    'company_city' => 'Delft',
    'company_country' => 'Netherlands',
    'company_phone' => '015 - 241 18 00',
    'company_fax' => '015 - 241 18 21',
    'company_email' => 'agileacademy@prowareness.nl',
    'company_website' => 'http://www.prowareness.nl',
    'company_bank' => '180.748.343',
    'company_iban' => 'NL84 RABO 0180 748 343',
    'company_swift_bic' => 'RABONL2U',
    'company_kvk' => '60393106 te Delft',
    'company_vat' => '8538.90.778.B.01',
    'company_tax' => '8538.90.778.L.01',
    'company_note' => 'Exempt from tax under Section 11-1-o VAT Law in 1968.',
    'company_footer' => '<b>Within 7 days of the invoice date </b> the amount of this invoice must  be credited to our Rabobank account NL84 RABO 0180 748 343 to the attention of Prowareness Academy B.V. citing above invoice number.'

);

/**
 * When this value is greater then 0, a discount will be given when paying online (eg. credit card, iDeal).
 * When the user chooses invoice no discount will be given.
 * Use a dot as the decimal separator (eg. 10.00 and not 10,00)
 */
$pw_config['payment_online_discount'] = 10.00;

/**
 * RABOBANK OMNIKASSA (OBSOLETE. Retained for backup purposes. When Buckaroo is ok Omnikassa can be removed.
 */
$pw_config['omni']['dev']['merchant_id'] = '002020000000001';
$pw_config['omni']['dev']['security_key'] = '002020000000001_KEY1';
$pw_config['omni']['dev']['security_key_version'] = '1';

$pw_config['omni']['prd']['merchant_id'] = '220347952470001';
$pw_config['omni']['prd']['security_key'] = 'vpPyl50H1u5pBS3N1NU5eyGccHhNexSpKreNEji8mIs';
$pw_config['omni']['prd']['security_key_version'] = '2';

/**
 * BUCKAROO
 */
$pw_config['buckaroo']['test_mode'] = 0;    //1 = yes, 0 = no. Use 0 for live site

//Live account buckaroo for Agile Academy
$pw_config['buckaroo']['website_key'] = "4f2PB4ZF7i";
$pw_config['buckaroo']['secret_key'] = "dsfkjhfdjsdfjkh432323jhj324324";  //To be obtained from the Buckaroo management console

//Test account Buckaroo
//$pw_config['buckaroo']['website_key'] = "VXQDqiwH7V";
//$pw_config['buckaroo']['secret_key'] = "2344jkljfdf9sd388fd43343kj";  //To be obtained from the Buckaroo management console




?>