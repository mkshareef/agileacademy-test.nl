<?php
/**
 * Direct access to the script is not allowed
 */
defined('ABSPATH') or die("No script kiddies please!");

$path = plugin_dir_path( plugin_dir_path(__file__) );

if( !class_exists( 'paymentModelView' ) ):

    class paymentModelView
    {

        private $session_md = null;
        private $invoice_mdl = null;
        private $invoice_mv = null;

        public function __construct()
        {
            $path = plugin_dir_path( plugin_dir_path(__file__) );

            require_once( $path.'/models/model-session.php');
            require_once( $path.'/models/model-invoice.php');

            require_once( $path.'/classes/class-util.php');
            require_once( $path.'/classes/class-buckaroo.php');

            require_once( $path.'/modelview/modelview-invoice.php');

            require_once( $path.'/settings.php');

            $this->session_md = new modelSession();
            $this->invoice_mdl = new modelInvoice();
            $this->invoice_mv = new invoiceModelView();

        }

        public function do_payment() {

            global $pw_config;

            $token = $_GET['sess'];
            $lang = "nl";

            if( isset( $_GET['lang'] ) ) {
                $lang =  strtolower( sanitize_text_field( $_GET['lang'] ));
            }

            if( $lang <> 'nl' && $lang <> 'de' ) {
                $lang = 'en';
            }

            if( $lang == 'nl') {
                $no_session = 'De sessie is (niet meer) geldig';
            } else if ($lang == 'de') {
                $no_session = 'Diese Sitzung ist nicht mehr verfügbar';
            } else {
                $no_session = 'This session is not valid (anymore)';
            }

            $session = "";
            if( strlen($token) == 32 ) {
                $session = $this->session_md->get_by_token($token);
            }

            if( $session == '' ) {
                echo $no_session;
                exit();
            }

            $invoice = "";
            if( $session['payment_done'] == '0' ) {

                $invoice = $this->invoice_mdl->get_by_id((int) $session['invoiceID']);
                $invoice = stripslashes_deep( $invoice );

            } else {

                //Redirect to invoice + PDF generation
                $url = "invoice?sess=".$token;

                if( $lang <> strtolower( $pw_config['lang_primary'] ) ) {
                    $url = strtolower( $lang ).'/'.$url;
                }

                echo('<meta http-equiv="refresh" content="0; url=' . $url . '">');
                exit();

            }

            if( $invoice == '' ) {
                echo $no_session;
                exit();
            }

            //Do the Buckaroo.
            $mode = $pw_config['buckaroo']['test_mode'];

            $pay = new Buckaroo( $mode );

            $website_key = $pw_config['buckaroo']['website_key'];
            $secret_key = $pw_config['buckaroo']['secret_key'];

            $pay->set_website_key($website_key);
            $pay->set_secret_key($secret_key);
            $pay->set_session_id($token);

            //Update the invoice with the transaction_reference for lookup
            $this->invoice_mdl->update( $invoice['id'], array( 'transaction_reference' => $token ));

            //Calculate the total of the invoice
            $total_event = $invoice['invoice_nr_subscribed'] * $invoice['price_single_evt'];
            $total_discount = $invoice['discount_nr'] * $invoice['discount_amount'];

            if( isset($invoice['pay_online_discount']) ) {
                $total_discount += $invoice['pay_online_discount'];
            }

            $total = $total_event - $total_discount;

            $pay->set_amount( $total );

            $pay->set_currency('EUR');

            $invoice_nr = $invoice['invoice_nr'];
            $pay->set_invoice_number( $invoice_nr );

            $pay->set_description( $invoice['description']);

            $pay->set_language($lang);
            $pay->set_return_url( get_site_url().'/payment_live.php' );
            $pay->set_push_url( get_site_url().'/payment_auto.php' );
            $pay->set_push_failure_url( get_site_url().'/payment_auto.php' );

            if( $invoice['payment_method'] == 'ideal' ) {
                $payment_methods = 'ideal';
            } else {
                $payment_methods = 'mastercard,visa,amex';
            }

            $pay->set_requested_services($payment_methods);

            $pay->handle_payment();




        }

        // $mode = live or auto
        public function validate( $mode ) {

            global $pw_config;

            if(empty($_POST['BRQ_PAYMENT']) || empty($_POST['BRQ_SIGNATURE']))
            {
                echo '<p>Invalid response.</p>';
                exit();
            }

            $test_mode = $pw_config['buckaroo']['test_mode'];
            $secret_key = $pw_config['buckaroo']['secret_key'];

            $pay = new Buckaroo( $test_mode );

            $pay->set_secret_key($secret_key);

            $is_valid = $pay->validate_response( $_POST );

            if( $is_valid == false )
            {
                echo '<p>Invalid signature in response.</p>';
                exit();
            }

            $status = 'new';

            $trans_status = $_POST['BRQ_STATUSCODE'];

            if( $trans_status == '190') {
                $status = 'payed';
            }

            //Get the invoice
            $invoice = $this->invoice_mdl->get_by_transaction_reference( $_POST['ADD_SESSION_ID']);

            $old_status = $invoice['status'];
            $trans_status_old = $invoice['transaction_status'];

            //Add the result to the invoice
            $transaction = array(
                'status' => $status,
                'transaction_reference' => $_POST['ADD_SESSION_ID'],
                'transaction_status' => $_POST['BRQ_STATUSCODE'],
                'transaction_id' => $_POST['BRQ_PAYMENT'],
                'transaction_date' => $_POST['BRQ_TIMESTAMP'],
            );

            $this->invoice_mdl->update($invoice['id'], $transaction );
            $invoice['status'] = $status;

            //Get the session for the current invoice_id
            $session = $this->session_md->get_by_invoice_id($invoice['id']);

            // When payment status is changed, sent the invoice.
            // When payment fails, an 'Open' invoice should be sent once.
            // After success an updated 'Payed' invoice is sent.
            if( ( $status <> $old_status ) || ( ( $trans_status <> $trans_status_old  ) && $trans_status_old == ''  ) ) {

                // Email a PDF with the invoice in the requested invoice language
                $send_to = $invoice['invoice_email'];

                $this->invoice_mv->email_invoice_pdf( $invoice, $send_to, $invoice['invoice_lang'] );

                if( $status == 'payed' ) {
                    //Sent copy to CFO
                    $this->invoice_mv->email_invoice_pdf( $invoice, $pw_config['cfo_email'], $invoice['invoice_lang'] );
                }

                // Set the sent flag
                $this->invoice_mdl->update($invoice['id'], array( 'invoice_sent' => '1'));

            }

            if( $status == 'payed' ) {
                //No need for further processing, so session can be closed
                if( $session <> '' ) {
                    $this->session_md->update($session['id'], array('payment_done' => '1') );
                }
            }

            //If live -> redirect to the PDF invoice with the right language.
            if( $mode == 'live' ) {

                if( $status == 'payed' ) {
                    //Redirect to the PDF invoice with the right language.

                    $url = "invoice?sess=".$session['nonce'];

                    if( strtolower( $invoice['invoice_lang'] ) <> strtolower( $pw_config['lang_primary'] ) ) {
                        $url = strtolower( $invoice['invoice_lang'] ).'/'.$url;
                    }

                    echo('<meta http-equiv="refresh" content="0; url=' . $url . '">');
                    exit();

                } else {
                    // Show message: Something went wrong during payment.
                    // Redirect back to subscription, step 3 with instructions to change the payment method.

                    echo '<html>';
                    echo '<head>';
                    echo '<meta charset="UTF-8" />';

                    echo '<style>';
                    echo '#payment_error{'.
                        'width: 100%; max-width: 750px;'.
                        'padding: 14px; margin: 14px auto;'.
                        'font-family: Source Sans Pro, sans-serif; }';
                    echo '#payment_error h1 { color: #ff6e00; }';
                    echo '#payment_error a { color: blue; }';
                    echo '</style>';

                    echo '</head>';
                    echo '<body>';

                    echo '<div id="payment_error">';

                    $url = "/subscribe/?sess=".$session['nonce'].'&tab=3';

                    if( strtolower( $session['ui_lang'] ) <> strtolower( $pw_config['lang_primary'] ) ) {
                        $url = strtolower( $session['ui_lang'] ).$url;
                    }

                    if( $session['ui_lang'] == 'nl') {
                        $url = "/subscribe/?sess=".$session['nonce'].'&tab=3';
                        echo '<h1>De betaling is mislukt</h1>';
                        echo '<p>Klik op de url hieronder om terug te keren naar uw inschrijving. Hier kunt u een andere betaalmethode kiezen.</p>';
                        echo '<a href="'.$url.'">Terug naar de inschrijving</a>';
                    } elseif( $session['ui_lang'] == 'de') {
                        echo '<h1>Zahlung fehlgeschlagen</h1>';
                        echo '<p>Klicke auf den Linken unten, um zur Anmeldung zurückzukehren. Dort können Sie eine andere Zahlungsmethode auswählen.</p>';
                        echo '<a href="'.$url.'">Zurück zum anmeldung</a>';
                    } else {
                        echo '<h1>Payment failed</h1>';
                        echo '<p>Click on the link below to return to your subscription. Here you can select a different payment method.</p>';
                        echo '<a href="'.$url.'">Back to subscription</a>';
                    }

                    echo '</body>';
                    echo '</html>';

                }

            } else {

                echo 'OK';
            }

        }

    }

endif;