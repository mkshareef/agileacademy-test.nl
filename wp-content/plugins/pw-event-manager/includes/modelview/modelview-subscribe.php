<?php
/**
 * Direct access to the script is not allowed
 */
defined('ABSPATH') or die("No script kiddies please!");

/*
 * This is an helper class to handle event subscriptions
 */

if( !class_exists( 'subscribeModelView' ) ):

    class subscribeModelView
    {

        private $event_mv = null;

        private $action_code_md = null;
        private $subscription_md = null;
        private $participant_md = null;
        private $session_md = null;
        private $eventdate_md = null;
        private $eventknowhow_md = null;
        private $location_md = null;
        private $invoice_md = null;

        private $expire_minutes = 30;

        public function __construct()
        {
            $path = plugin_dir_path( plugin_dir_path(__file__) );

            require_once( $path.'/models/model-action-code.php');
            require_once( $path.'/models/model-session.php');
            require_once( $path.'/models/model-subscription.php');
            require_once( $path.'/models/model-participant.php');
            require_once( $path.'/models/model-event-date.php');
            require_once( $path.'/models/model-event-know-how.php');
            require_once( $path.'/models/model-location.php');
            require_once( $path.'/models/model-invoice.php');

            require_once( $path.'/classes/class-util.php');
            require_once( $path.'/classes/class-mail.php');
            require_once( $path.'/modelview/modelview-event.php');

            $this->event_mv = new eventModelView();
            $this->action_code_md = new modelActionCode();
            $this->session_md = new modelSession();
            $this->subscription_md = new modelSubscription();
            $this->participant_md = new modelParticipant();
            $this->eventdate_md = new modelEventDate();
            $this->eventknowhow_md = new modelEventKnowHow();
            $this->location_md = new modelLocation();
            $this->invoice_md = new modelInvoice();
        }

        private function know_how_options() {
            $data = array(
                'en' => array(
                    ''                  => '- Make a choice -',
                    'google'            => 'Via Google',
                    'website'           => 'Via the website',
                    'pro-contributor'   => 'Via a Prowareness employee',
                    'friend'            => 'Via friends',
                    'colleagues'        => "Via colleagues",
                    'news'              => 'Via the newsletter',
                    'twitter'           => 'Via Twitter',
                    'linkedin'          => 'Via Linkedin',
                    'bnr'               => 'Via BNR radio campaign',
                    'sdn-nl'            => 'Via sdn.nl',
                    'improve'           => 'Via Improve',
                    'agile-market'      => 'Via Agile+ Market',
                    'others'            => 'Other...'
                ),
                'nl' => array(
                    ''                  => '- Maak een keuze -',
                    'google'            => 'Via Google',
                    'website'           => 'Via de website',
                    'bnr'               => 'Via BNR',
                    'news'              => 'Via de nieuwsbrief',
                    'twitter'           => 'Via Twitter',
                    'linkedin'          => 'Via Linkedin',
                    'pro-contributor'   => 'Via een Prowareness medewerker',
                    'colleagues'        => "Via collega's",
                    'pro-event'         => 'Via een Prowareness event',
                    'pro-training'      => 'Via een Prowareness training',
                    'conference'        => 'Via een conferentie',
                    'scrum-org'         => 'Via Scrum.org',
                    'others'            => 'Anders...'
                ),
                'de' => array(
                    ''                  => '- Treffen Sie eine Wahl -',
                    'google'            => 'Via Google',
                    'website'           => 'Via Webseite',
                    'pro-contributor'   => 'Via einem Prowareness Mitarbeiter',
                    'friend'            => 'Via Freunde',
                    'colleagues'        => "Via Kollegen",
                    'news'              => 'Via dem Newsletter',
                    'twitter'           => 'Via Twitter',
                    'linkedin'          => 'Via Linkedin',
                    'bnr'               => 'Via BNR Raio Kampagne',
                    'sdn-nl'            => 'Via sdn.nl',
                    'improve'           => 'Via Improve',
                    'agile-market'      => 'Via Agile+ Market',
                    'others'            => 'Andere…'
                )
            );

            return $data;
        }

        //Retrieve an existing session or start a new one.
        //Return empy session when expired.
        private function get_session( $token ) {
            $session = "";

            //Expire sessions to avoid unauthorized access
            $this->session_md->expire_sessions();

            if( $token <> '' ) {
                //Get the session
                $session = $this->session_md->get_by_token($token);
            } else {
                //Start session
                $token = $this->session_md->get_new_token();

                $expire = date("Y-m-d H:i:s", strtotime( current_time("Y-m-d H:i:s")." +$this->expire_minutes minutes"));

                //Start a new session with the token
                $session['nonce'] = $token;
                $session['subscriptionID'] = 0;
                $session['invoiceID'] = 0;
                $session['expire_time'] =  $expire;
                $session['ui_lang'] = '';
                $session['ip'] = util::get_user_ip();

                $id = $this->session_md->start_session( $session );

                $session['id'] = $id;
            }

            return $session;
        }

        private function sync_subscribers_with_db( $subscribers_from_db, $subscription_id, $token ) {

            $subscribers_post = array();    //Store the posted subscribers
            $subscribers_to_show = array();    //Contains the subscribers to be displayed

            $n=0;
            $found = true;

            while( $found == true ) {
                if( isset( $_POST['last_name_'.$n])) {

                    $subscribers_post[] = array(
                        'id' => (int) $_POST['id_'.$n],
                        'salutation' => $this->make_safe_text( $_POST['salutation_'.$n], 16 ),
                        'first_name' => $this->make_safe_text( $_POST['first_name_'.$n], 128 ),
                        'middle_name' => $this->make_safe_text( $_POST['middle_name_'.$n], 32 ),
                        'last_name' => $this->make_safe_text( $_POST['last_name_'.$n], 128 ),
                        'phone' => $this->make_safe_text( $_POST['phone_'.$n], 64 ),
                        'email' => $this->make_safe_text( $_POST['email_'.$n], 255 ),
                        'newsletter' => isset( $_POST['newsletter_'.$n] ) ? '1' : ''
                    );

                } else {
                    $found = false;
                }

                $n++;
                if( $n > 1000 ) { break; }
            }

            //Check for subscribers to be deleted from this subscription
            $delete_requested = false;
            $delete_request = '';
            foreach($_POST as $key => $value) {
                $pos = strpos($key , "participant_remove_");
                if ($pos === 0){
                    $delete_requested = true;
                    $delete_request = $key;
                }
            }

            //Perform the soft delete
            if( $delete_requested == true ) {
                $nr = substr(strrchr($delete_request, "_"), 1);
                if( $subscribers_post[$nr]['id'] > 0 ) {
                    $this->participant_md->remove_from_subscription( $subscribers_post[$nr]['id'], $subscription_id, $token );
                }
                unset( $subscribers_post[$nr] );
            }

            //Use posted data when relevant action is used (add, save, delete subscriber)
            if( isset($_POST['event-participant-add']) or isset($_POST['event-submit-2']) or isset( $_POST['event-back-2'] ) or $delete_requested == true ) {
                $subscribers_to_show = $subscribers_post;
            } else {
                $subscribers_to_show = $subscribers_from_db;
            }

            return $subscribers_to_show;
        }

        private function generate_invoice_nr() {

            global $pw_config;

            $prefix = $pw_config['invoice_nr_prefix'];
            $nr_length = (int) $pw_config['invoice_nr_length'];

            //Get the last invoice and retrieve the last used invoice nr with the prefix
            $last_used = $this->invoice_md->get_last_used_invoice_nr( $prefix );

            //Strip the prefix from the nr
            $last_user_nr = 0;
            if( $last_used <> '' ) {
                $last_user_nr = (int) str_replace( $prefix, '', $last_used );
            }

            //Format the new invoice number
            $next_nr = str_pad( $last_user_nr + 1, $nr_length, "0", STR_PAD_LEFT);
            $invoice_nr = $prefix.$next_nr;

            return $invoice_nr;
        }

        public function process() {
            global $pw;
            global $pw_config;

            $path = plugin_dir_path( plugin_dir_path(__file__) );

            require_once($path.'/classes/class-eventdatetime.php');
            require_once($path.'/classes/class-util.php');

            require_once( $path.'/views/view-fe-event-subscribe.php');

            $event_id = 0;              //ID of the selected event
            $selected_date_id = 0;      //ID of the selected date for the selected event
            $subscription_id = 0;       //ID of the current subscription
            $invoice_id = 0;            //ID of the invoice for the current subscription

            $event = "";
            $subscription = "";
            $invoice = "";
            $active_tab = 1;
            $token = "";
            $session = "";
            $is_incompany = false;
            $has_error = false;
            $error_msg = array();

            //Get the active tab.
            if( isset($_GET['tab'])) {
                $active_tab = (int) $_GET['tab'];
            }

            if( $active_tab <= 0 or $active_tab > 6 ) {
                $active_tab = 1;
            }

            //Get the session token
            if( isset($_GET['sess'])) {
                $token = substr( $_GET['sess'], 0, 32);
            }

            // Lookup the session. Exit when an invalid session is found
            $session = $this->get_session( $token );

            if ($session == "") {
                echo('Invalid sesson or session expired');
                exit();
            } else {
                $token = $session['nonce'];
            }

            // At this point we have an active session.
            $subscription_id = (int) $session['subscriptionID'];
            $invoice_id = (int) $session['invoiceID'];

            // Get the stored subscription data if available
            $subscription = "";
            if( $subscription_id > 0 ) {
                $subscription = $this->subscription_md->get_by_id($subscription_id);
            }

            // For payed event: When payment is finished -> Redirect to the invoice
            if( isset($session['payment_done']) && $session['payment_done'] == '1' && $invoice_id > 0 ) {

                $url = "invoice?sess=".$token;

                if( strtolower( $subscription['invoice_lang'] ) <> strtolower( $pw_config['lang_primary'] ) ) {
                    $url = strtolower( $subscription['invoice_lang'] ).'/'.$url;
                }

                echo('<meta http-equiv="refresh" content="0; url=' . $url . '">');
                exit();
            }

            //Get the stored invoice data if available
            $invoice = "";
            if( $invoice_id > 0 )  {
                $invoice = $this->invoice_md->get_by_id($invoice_id);
            }

            //Get the event and selected date from $_GET or from the subscription
            $event_id = 0;
            $selected_date_id = 0;

            if( isset($_GET['evt']) && isset($_GET['dt']) ) {
                $event_id = (int) $_GET['evt'];

                if( $_GET['dt'] <> '' ) {
                    $selected_date_id = (int)$_GET['dt'];
                } else {
                    $selected_date_id = -1;
                }

                $is_incompany = $selected_date_id == 0;
            } else {
                if( $subscription <> "") {
                    $event_id = (int)$subscription['eventID'];
                    $selected_date_id = (int)$subscription['eventDateId'];
                }
            }

            //Exit when an invalid event is given
            if ($event_id == 0 ) {
                echo('Invalid event data found');
                exit();
            }

            //Retrieve subscribers from the database
            $subscribers_db = array();

            if( $subscription <> '' ) {
                $subscribers_db = $this->participant_md->get_all_by_subscription_id( $subscription_id );
            }

            //Lookup the event
            if( $event_id > 0 ) {
                $event = $this->event_mv->get_event_by_id($event_id);
                $eventKnowHow = $this->eventknowhow_md->get_all_by_id($event_id);

                //Unset some values for more easy debugging
                unset( $event['description']);
                unset( $event['trainers']);
            }

            //Exit when an invalid event is given
            if( $event == "" ) {
                echo "Invalid event data found";
                exit();
            }

            //Get details for the selected date
            $selected_date_details = '';
            if( $selected_date_id > 0 ) {
                foreach( $event['planning'] as $key => $dt ) {
                    if( $dt['id'] == $selected_date_id ) {
                        $selected_date_details = $dt;
                    }
                }
            }

            //Once everything is initialized we might have to update the data with $_POST-data

            //$session                  = active session
            //$event                    = the selected event
            //$selected_date_details    = details about the selected date
            //$subscription             = active subscription
            //$subscribers_db           = subscribers in the db for this subscription
            //$invoice                  = the invoice

            // Process subscriber related actions
            // The return value is the list to be displayed
            $subscribers_html = $this->sync_subscribers_with_db( $subscribers_db, $subscription_id, $token );

            // HANDLE DATE SELECTION
            $step_1_done = false;

            //In company has been pre selected. Redirect to the right form.
            if( $is_incompany == true ) {

                if( $event['locale'] == "NL" ) {
                    $url = "/incompany/?incompany=In Company Training: ".$event['name'];
                } elseif(  $event['locale'] == "DE" ) {
                    $url = "/incompany/?incompany=Training im Unternehmen: ".$event['name'];
                } else {
                    $url = "/incompany/?incompany=In Company Training: ".$event['name'];
                }

                if( $event['locale'] <> strtoupper( $pw_config['lang_primary'] ) ) {
                    $url = '/'.strtolower( $event['locale'] ).'/'.$url;
                }

                echo('<meta http-equiv="refresh" content="0; url=' . $url . '">');
                exit();
            }

            if( isset( $_POST['event-submit-1'] ) ) {

                $selected_date_id = isset( $_POST['selected_date_id'] ) ? (int)$_POST['selected_date_id'] : 0;

                //Redirect to in company request if id = 0
                if( $selected_date_id == 0 ) {

                    if( $event['locale'] == "NL" ) {
                        $url = "/incompany/?incompany=In Company Training: ".$event['name'];
                    } elseif(  $event['locale'] == "DE" ) {
                        $url = "/incompany/?incompany=Training im Unternehmen: ".$event['name'];
                    } else {
                        $url = "/incompany/?incompany=In Company Training: ".$event['name'];
                    }

                    if( $event['locale'] <> strtoupper( $pw_config['lang_primary'] ) ) {
                        $url = '/'.strtolower( $event['locale'] ).'/'.$url;
                    }

                    echo('<meta http-equiv="refresh" content="0; url=' . $url . '">');
                    exit();

                }

                //Make sure the selected_date_id exists for the selected training
                $is_valid_date_id = false;

                if( is_array($event['planning']) ) {
                    foreach( $event['planning'] as $plan ) {
                        if( $plan['id'] == $selected_date_id ) {
                            $is_valid_date_id = true;
                        };
                    }
                }

                if(  $is_valid_date_id == false ) {
                    echo "Invalid date";
                    exit();
                }

                //We have a valid date. Set or update the subscription and subscribers
                if( $subscription == "" ) {
                    //Insert a new subscribtion

                    $item = array();
                    $item['eventID'] = $event_id;
                    $item['eventDateId'] = $selected_date_id;
                    $item['status'] = "concept";
                    $item['company'] = "";
                    $item['invoice_attention'] = "";
                    $item['invoice_attribute'] = "";
                    $item['street'] = "";
                    $item['street_nr'] = "";
                    $item['zip'] = "";
                    $item['city'] = "";
                    $item['country'] = "";
                    $item['invoice_email'] = "";
                    $item['invoice_action_code'] = "";
                    $item['payment_method'] = "";
                    $item['know_how'] = "";
                    $item['know_how_else'] = "";
                    $item['diet'] = "";
                    $item['comments'] = "";
                    $item['agree_terms'] = "";
                    $item['removed'] = "0";
                    $item['created'] = eventDateTime::current_time_mysql();
                    $item['ip'] = util::get_user_ip();

                    $subscription_id = $this->subscription_md->insert($item);

                    //Make sure the subscription is linked to the session
                    $this->session_md->update($session['id'], array( 'subscriptionID'=>$subscription_id ));

                    //Retrieve the inserte subscription from the db
                    $subscription = $this->subscription_md->get_by_id($subscription_id);
                } else {
                    //Update existing subscription
                    $this->subscription_md->update( $subscription_id, array( 'eventDateId' => $selected_date_id ));
                }

                if( $subscription_id == 0 ) {
                    echo('Failed to save the subscription');
                    exit();
                }

                //Make sure all subscribers are linked to the selected event date
                foreach( $subscribers_html as $key=>$rec ) {
                    $this->participant_md->update($rec['id'], array( 'eventDateId' => $selected_date_id ) );
                }

                //Goto the next step
                $step_1_done = true;

            }

            // Make sure the session nows the active ui language.
            // This is required by the payment process to return to the right subscription on payment error
            $this->session_md->update($session['id'], array( 'ui_lang' => strtolower( $event['locale'] ) ));

            //Update the in memory subscription
            $subscription['selected_date_details'] = $selected_date_details;
            $subscription['eventDateId'] = $selected_date_id;

            // ADD PARTICIPANTS
            $step_2_done = false;
            $step_2_error = false;

            if( isset( $_POST['event-submit-2'] ) || isset( $_POST['event-back-2'] ) ) {
                $has_error = false;

                //Check input for errors
                if( is_array( $subscribers_html )) {
                    for( $n=0; $n < count($subscribers_html); $n++ ) {
                        $local_err = array();

                        if( $subscribers_html[$n]['last_name'] == "" ) {
                            $has_error = true;

                            $local_err[] = "missing_last_name";
                        }

                        if( $subscribers_html[$n]['email'] == "" ) {
                            $has_error = true;

                            $local_err[] = "missing_email";
                        } else if( is_email( $subscribers_html[$n]['email'] ) == false ) {
                            $has_error = true;

                            $local_err[] = "invalid_email";
                        }

                        $subscribers_html[$n]['local_errors'] = $local_err;

                    }
                }

                if( $has_error == true ) {
                    $data['has_error'] = true;
                    $step_2_error = true;
                } else {

                    //If not error: store participants and move to next step

                    if( isset( $subscribers_html ) and is_array( $subscribers_html ) ) {
                        foreach( $subscribers_html as $subscriber ) {

                            $item['eventID'] = $event_id;
                            $item['eventDateID'] = $selected_date_id;
                            $item['subscriptionID'] = $subscription_id;
                            $item['nonce'] = $token;
                            $item['salutation'] = $subscriber['salutation'];
                            $item['first_name'] = $subscriber['first_name'];
                            $item['middle_name'] = $subscriber['middle_name'];
                            $item['last_name'] = $subscriber['last_name'];
                            $item['email'] = $subscriber['email'];
                            $item['phone'] = $subscriber['phone'];
                            $item['newsletter'] = $subscriber['newsletter'];
                            $item['removed'] = '0';
                            $item['ip'] = util::get_user_ip();

                            if( $subscriber['id'] == 0 ) {
                                $this->participant_md->insert($item);
                            } else {
                                $this->participant_md->update_with_nonce( $subscriber['id'], $token, $item );
                            }

                        }
                    }

                    $step_2_done = true;
                }

            }

            // INVOICE DETAILS (PAYED EVENT) or COMPANY DETAILS (FREE EVENT)
            // PRESERVE DATA IN CASE OF ACTION CODE VALIDATION
            $step_3_done = false;
            $step_3_error = false;

            if( isset( $_POST['event-submit-3'] ) || isset( $_POST['event-back-3'] ) || isset( $_POST['action-code-validate']) ) {
                $has_error = false;
                $local_err = array();

                //Check for errors in input
                if( isset( $_POST['event-submit-3'] ) || isset( $_POST['event-back-3'] ) ) {

                    if (isset($_POST['invoice_attention']) && $_POST['invoice_attention'] == "") {
                        $has_error = true;

                        $local_err[] = "missing_invoice_attention";
                    }

                    if ($_POST['street'] == "") {
                        $has_error = true;

                        $local_err[] = "missing_street";
                    }

                    if ($_POST['street_nr'] == "") {
                        $has_error = true;

                        $local_err[] = "missing_street_nr";
                    }

                    if ($_POST['zip'] == "") {
                        $has_error = true;

                        $local_err[] = "missing_zip";
                    }

                    if ($_POST['city'] == "") {
                        $has_error = true;

                        $local_err[] = "missing_city";
                    }

                    if ($_POST['country'] == "") {
                        $has_error = true;

                        $local_err[] = "missing_country";
                    }

                    if (isset($_POST['invoice_email']) && $_POST['invoice_email'] == "") {
                        $has_error = true;

                        $local_err[] = "missing_invoice_email";
                    } else if (isset($_POST['invoice_email']) && is_email($_POST['invoice_email']) == false) {
                        $has_error = true;

                        $local_err[] = "invalid_invoice_email";
                    }

                    if ($has_error == true) {
                        $data['subscription_error'] = $local_err;
                        $step_3_error = true;

                    } else {
                        $data['subscription_error'] = "";
                    }
                }

                //TODO Add validation of the action code to UI (AJAX implementation)
                $subscription['payment_method'] = isset($_POST['payment_method']) ? $this->make_safe_text( $_POST['payment_method'], 32 ): "";
                $subscription['company'] = isset($_POST['company']) ? $this->make_safe_text( $_POST['company'], 128 ) : "";
                $subscription['invoice_attention'] = isset($_POST['invoice_attention']) ? $this->make_safe_text( $_POST['invoice_attention'], 128 ) : "";
                $subscription['invoice_lang'] = isset($_POST['invoice_lang']) ? $this->make_safe_text( $_POST['invoice_lang'], 3 ) : "";
                $subscription['invoice_attribute'] = isset($_POST['invoice_attribute']) ? $this->make_safe_text( $_POST['invoice_attribute'], 255 ) : "";
                $subscription['street'] = isset($_POST['street']) ? $this->make_safe_text( $_POST['street'], 128 ) : "";
                $subscription['street_nr'] = isset($_POST['street_nr']) ? $this->make_safe_text( $_POST['street_nr'], 32 ) : "";
                $subscription['zip'] = isset($_POST['zip']) ? $this->make_safe_text( $_POST['zip'], 16 ) : "";
                $subscription['city'] = isset($_POST['city']) ? $this->make_safe_text( $_POST['city'], 32 ) : "";
                $subscription['country'] = isset($_POST['country']) ? $this->make_safe_text( $_POST['country'], 32 ) : "";
                $subscription['invoice_email'] = isset($_POST['invoice_email']) ? $this->make_safe_text( $_POST['invoice_email'], 255 ) : "";
                $subscription['invoice_action_code'] = isset($_POST['invoice_action_code']) ? $this->make_safe_text( $_POST['invoice_action_code'], 32 ) : "";

                if( $has_error == false ) {

                    $item['payment_method'] = $subscription['payment_method'];
                    $item['company'] = $subscription['company'];
                    $item['invoice_attention'] = $subscription['invoice_attention'];
                    $item['invoice_lang'] = $subscription['invoice_lang'];
                    $item['invoice_attribute'] = $subscription['invoice_attribute'];
                    $item['street'] = $subscription['street'];
                    $item['street_nr'] = $subscription['street_nr'];
                    $item['zip'] = $subscription['zip'];
                    $item['city'] = $subscription['city'];
                    $item['country'] = $subscription['country'];
                    $item['invoice_email'] = $subscription['invoice_email'];
                    $item['invoice_action_code'] = $subscription['invoice_action_code'];

                    $this->subscription_md->update( $subscription_id, $item );

                    $step_3_done = true;
                }

            }

            //Always (re)validate the action code when it is available
            if ( isset( $subscription['invoice_action_code'] ) && $subscription['invoice_action_code'] <> '') {

                $selected_date = $selected_date_details['start_date'];

                $action_code_result = $this->event_mv->get_discount($subscription['invoice_action_code'],
                    $subscription['company'], $event_id, $selected_date, $invoice_id);

                $subscription['action_code_result'] = $action_code_result;
            }

            // Other questions and agreements
            $step_4_done = false;
            $step_4_error = false;

            if( isset( $_POST['event-submit-4'] ) || isset( $_POST['event-back-4'] ) ) {
                $has_error = false;

                $local_err = array();

                //Check input for errors
                if( strpos( $_POST['know_how'], '-') !== false ) {
                    $has_error = true;

                    $local_err[] = "missing_know_how";
                }

                if( strpos( $_POST['know_how'], '-') === false ) {
                    if( strpos( $_POST['know_how'], '...') !== false ) {
                        if($_POST['know_how_else'] == '') {
                            $has_error = true;

                            $local_err[] = "missing_know_how_else";
                        }
                    }
                }

                //agree_terms
                if( isset($_POST['agree_terms']) == false || $_POST['agree_terms'] == '') {
                    $has_error = true;

                    $local_err[] = "missing_agree_terms";
                }

                if( $has_error == true ) {
                    $data['subscription_error'] = $local_err;
                    $step_4_error = true;
                } else {
                    $data['subscription_error'] = "";
                }

                $subscription['know_how'] = isset($_POST['know_how']) ? $this->make_safe_text( $_POST['know_how'] ) : "";
                $subscription['know_how_else'] = isset($_POST['know_how_else']) ? $this->make_safe_text( $_POST['know_how_else'] ) : "";
                $subscription['diet'] = isset($_POST['diet']) ? $this->make_safe_text( $_POST['diet'] ) : "";
                $subscription['comments'] = isset($_POST['comments']) ? $this->make_safe_text( $_POST['comments'] ) : "";
                $subscription['agree_terms'] = isset($_POST['agree_terms']) ? $this->make_safe_text( $_POST['agree_terms'] ) : "";

                if( $has_error == false ) {

                    $item['know_how'] = $subscription['know_how'];
                    $item['know_how_else'] = $subscription['know_how_else'];
                    $item['diet'] = $subscription['diet'];
                    $item['comments'] = $subscription['comments'];
                    $item['agree_terms'] = $subscription['agree_terms'];
                    $item['created'] = eventDateTime::current_time_mysql();

                    $this->subscription_md->update( $subscription_id, $item );

                    $step_4_done = true;

                }

            }


            // For step 5 we need to do some pre processing to show the correct data.

            // Get the pricing for the current event. 0 = free event
            $real_price = 0;

            $planning = $event['planning'];
            if( is_array($planning) ) {
                foreach( $planning as $plan ) {
                    if( $plan['id'] == $selected_date_id ) {
                        $real_price = $plan['real_price'];
                    }
                }
            }

            if( $real_price == 0 ) {
                $data['fieldset'] = 'free';
            } else {
                $data['fieldset'] = 'payed';

                //Calculate the invoice details
                $nr_sub = count($subscribers_html);
                $nr_discount = $nr_sub;
                $msg = '';
                $discount_total = 0;
                $discount_pay_online = 0;

                if( isset( $subscription['action_code_result']) && (int) $subscription['action_code_result']['is_valid'] == 1) {

                    if( (int) $subscription['action_code_result']['limit_nr_use'] == 1 ) {

                        if( isset($subscription['action_code_result']['nr_use_left']) ) {
                            $nr_left = (int) $subscription['action_code_result']['nr_use_left'];
                        } else {
                            $nr_left = 0;
                        }

                        if( $nr_left <= 0 ) {   //No discount left
                            $nr_discount = 0;
                        } elseif( $nr_sub <= $nr_left ) { //Enough discount left
                            $nr_discount = $nr_sub;
                        } else {
                            $nr_discount = $nr_sub - $nr_left; //Partial discount left
                        }
                    } else {
                        $nr_discount = $nr_sub;
                    }

                    if( $subscription['action_code_result']['discount_percent'] > 0 ) {
                        $percentage = $subscription['action_code_result']['discount_percent'];
                        $msg = $percentage.' %';

                        $discount_total = ($nr_discount * $real_price) * ( $percentage / 100 );
                        $discount_total = number_format((float)$discount_total, 2, '.', '');

                    } else {
                        $amount = $subscription['action_code_result']['discount_euro'];
                        $msg = '&euro; '.$amount;

                        $discount_total = $nr_discount * $amount;
                        $discount_total = number_format((float)$discount_total, 2, '.', '');
                    }
                }

                $total = ($nr_sub * $real_price) - $discount_total;

                //Calculate the discount for online payment
                //Discount is per invoice.

                if( $pw_config['payment_online_discount'] > 0 &&
                    $subscription['payment_method'] <> "invoice" ) {
                    $discount_pay_online = $pw_config['payment_online_discount'];
                } else {
                    $discount_pay_online = 0.00;
                }

                // Online payment requires total to be higher then 0.
                // For that reason payment type switches to invoice when 0.
                // To prevent a catch22 (switch back to online payment, apply discount, 0, switch to invoice, no discount, etc)
                // only apply the online payment discount when total is higher then the discount
                //
                if( $total > $discount_pay_online ) {
                    $total = $total - $discount_pay_online;
                } else {
                    $discount_pay_online = 0.00;
                }

                if( $total < 0 ) {
                    $total = 0;
                }

                $subscription['invoice']['real_price'] = $real_price;
                $subscription['invoice']['nr_subscribers'] = $nr_sub;
                $subscription['invoice']['nr_discount'] = $nr_discount;
                $subscription['invoice']['discount_amount_msg'] = $msg;
                $subscription['invoice']['discount_total'] = $discount_total;
                $subscription['invoice']['pay_online_discount'] = $discount_pay_online;
                $subscription['invoice']['vat'] = 0;
                $subscription['invoice']['total'] = $total;

                //Reset the payment method to invoice when total amount = 0 euro.
                if( $subscription['invoice']['total'] == 0.00 ) {

                    $subscription['payment_method'] = "invoice";

                }

            }

            //Prepare the common invoice data
            global $pw_config;

            $version = $pw_config['invoice_version'];
            $vat = $pw_config['invoice_vat_percent'];

            $list = array();

            if( is_array( $subscribers_html ) ) {
                foreach( $subscribers_html as $key => $subscriber) {
                    $name = $subscriber['first_name'].' '.$subscriber['middle_name'].' '.$subscriber['last_name'];
                    $list[] = preg_replace('/\s+/', ' ', $name);
                }
            }

            $subscriber_description = implode(", ", $list);

            $item = array();

            if( $subscription <> "" && isset( $subscription['company'] ) ) {
                $item['subscriptionID'] = $subscription_id;
                $item['invoice_company'] = $subscription['company'];
                $item['invoice_attention'] = $subscription['invoice_attention'];
                $item['invoice_attribute'] = $subscription['invoice_attribute'];
                $item['invoice_street'] = $subscription['street'];
                $item['invoice_street_nr'] = $subscription['street_nr'];
                $item['invoice_zip'] = $subscription['zip'];
                $item['invoice_city'] = $subscription['city'];
                $item['invoice_country'] = $subscription['country'];
                $item['invoice_email'] = $subscription['invoice_email'];
                $item['invoice_action_code'] = $subscription['invoice_action_code'];

                $item['payment_method'] = $subscription['payment_method'];
                $item['description'] = $event['name'].', '.$selected_date_details['start_date'];
                $item['subscribers'] = $subscriber_description;
                $item['location'] = $selected_date_details['location']['location_name'];
                $item['duration'] = $event['duration'];
                $item['other_dates'] = $selected_date_details['other_dates'];

                $item['invoice_date'] = eventDateTime::current_time_mysql();
                $item['invoice_version'] = $version; //The version determines what meta data will be shown.
                $item['invoice_lang'] = strtolower( $subscription['invoice_lang'] );

                if( isset($subscription['invoice'])) {
                    $item['invoice_nr_subscribed'] = $subscription['invoice']['nr_subscribers'];
                    $item['price_single_evt'] = $subscription['invoice']['real_price'];
                    $item['discount_nr'] = $subscription['invoice']['nr_discount'];
                } else {
                    $item['invoice_nr_subscribed'] = 0;
                    $item['price_single_evt'] = 0;
                    $item['discount_nr'] = 0;
                }

                if( $item['discount_nr'] > 0 ) {
                    $item['discount_amount'] = $subscription['invoice']['discount_total'] /  $item['discount_nr'];
                } else {
                    $item['discount_amount'] = 0;
                }

                $item['pay_online_discount'] = $subscription['invoice']['pay_online_discount'];

                $item['vat_percent'] = $vat;

                $item['invoice_sent'] = '0';
                $item['status'] = 'new';
                $item['removed'] = '0';
                $item['ip'] = util::get_user_ip();
            }

            $step_5_done = false;
            $step_5_error = false;

            //Confirm all details and (optionally) handle payment

            //On submit 5 we need te create a new invoice if one does not exist
            //In step 5 no data is changed, so we do not need to do this when stepping back.
            if( isset( $_POST['event-submit-5'] )) {

                //Create the invoice if it is not allready created
                if( $session['invoiceID'] == 0 ) {

                    // If Payed event...
                    if( $real_price > 0 ) {

                        //Generate invoice number like PROACEVT000001
                        $invoice_nr = $this->generate_invoice_nr();

                        //Insert invoice
                        $item['invoice_nr'] = $invoice_nr;

                        $invoice_id = $this->invoice_md->insert($item);

                        //Get the inserted invoice
                        $invoice = $this->invoice_md->get_by_id( $invoice_id );

                        //Set the invoice ID in the session
                        $this->session_md->update($session['id'], array('invoiceID' => $invoice_id ));
                    }

                }
                $step_5_done = true;
            }

            //On submit of every step we need to recalculate an existing invoice to make sure all data is correct.
            if( $invoice_id > 0 ) {
                $item['invoice_nr'] = $invoice['invoice_nr'];
                $this->invoice_md->update( $invoice_id, $item );

                $invoice = $this->invoice_md->get_by_id( $invoice_id );
            }

            //Navigate to the next step when required
            $url = "";

            if( isset( $_POST['event-submit-1'] ) && $step_1_done == true ) {
                $url = "?sess=".$token.'&tab=2';
            }

            if( isset( $_POST['event-submit-2'] ) && $step_2_error == false && $step_2_done == true ) {
                $url = "?sess=".$token.'&tab=3';
            }

            if( isset( $_POST['event-back-2'] ) && $step_2_error == false && $step_2_done == true ) {
                $url = "?sess=".$token.'&tab=1';   // BACK TO STEP 1
            }

            if( isset( $_POST['event-submit-3'] ) && $step_3_error == false && $step_3_done == true ) {
                $url = "?sess=".$token.'&tab=4';
            }

            if( isset( $_POST['event-back-3'] ) && $step_3_error == false && $step_3_done == true ) {
                $url = "?sess=".$token.'&tab=2';   // BACK TO STEP 2
            }

            if( isset( $_POST['event-submit-4'] ) && $step_4_error == false && $step_4_done == true ) {
                $url = "?sess=".$token.'&tab=5';
            }

            if( isset( $_POST['event-back-4'] ) && $step_4_error == false && $step_4_done == true ) {
                $url = "?sess=".$token.'&tab=3';   // BACK TO STEP 3
            }

            if( isset( $_POST['event-back-5'] ) ) {
                $url = "?sess=".$token.'&tab=4';   // BACK TO STEP 4
            }

            if( isset( $_POST['event-submit-5'] ) && $step_5_done == true ) {
                /*
                     We have all information.
                     For a payed event we need to go to the payment process and make an invoice.
                          - Generate a unique invoice nr
                          - Save the invoice in the db
                          - Set the invoiceId in the session

                     If we have an existing invoice, we need to update the data

                          - Generate a PDF
                          - Sent notifications
                          - Redirect to payment process or to the invoice directly

                     For a free event:
                          - Sent notifications
                          - Change the status of the subscription to 'new'
                          - Redirect to thankyou message
                          - Destroy the session
                */

                //Modify the status of the subscription from concept to new
                $this->subscription_md->update( $subscription['id'], array('status' => 'new' ) );

                // Send notification(s) to subscribers
                $m_from = $event['notify_from'];
                $m_to_internal = $event['notify_emails'];

                //$m_attachments[] = array( WP_CONTENT_DIR . '/uploads/test/test.pdf' );
                $m_attachments = null;

                if( strtolower( $event['locale'] ) == 'nl') {
                    $date_format = $pw_config['date_format']['nl'];
                } elseif( strtolower( $event['locale'] ) == 'de') {
                    $date_format = $pw_config['date_format']['de'];
                } else {
                    $date_format = $pw_config['date_format']['en'];
                }

                $m_event_title = stripslashes( $event['name']);
                $m_event_date = date( $date_format ,strtotime( $selected_date_details['start_date'] ));
                $m_event_time = date( 'H:i', strtotime( $selected_date_details['start_time'] )).' - '. date( 'H:i', strtotime( $selected_date_details['stop_time'] ));

                $m_other_dates = '';
                if(  $selected_date_details['other_dates'] <> '' ) {
                    $tmp = explode(',', $selected_date_details['other_dates']);

                    foreach( $tmp as $k => $t ) {
                        $tmp[$k] = date( $date_format ,strtotime( $t ));
                    }

                    $m_other_dates = implode( ',', $tmp );

                }

                $m_location = stripslashes( $selected_date_details['location']['location_name'] );

                global $current_user;
                get_currentuserinfo();

                $m_sendname = $current_user->user_firstname.' '.$current_user->user_lastname;

                $to_notify_internal = false;
                $name_list = '';

                foreach( $subscribers_html as $key=>$sub ) {
                    $m_to = "";

                    $m_email = $sub['email'];

                    $m_salute = $sub['salutation'];

                    if( strtolower($event['locale']) == 'nl') {

                        $start_formal = "Geachte ";
                        $start_informal = "Beste ";

                        if( $m_salute == 'male' ) {
                            $m_salute = 'heer';
                        } elseif( $m_salute == 'female' ) {
                            $m_salute = 'mevrouw';
                        } else {
                            $m_salute = 'heer/mevrouw';
                        }
                    } elseif( strtolower($event['locale']) == 'de') {

                        $start_formal = "Sehr geerhte ";
                        $start_informal = "Beste ";

                        if( $m_salute == 'male' ) {
                            $m_salute = 'Herr';
                        } elseif( $m_salute == 'female' ) {
                            $m_salute = 'Frau';
                        } else {
                            $m_salute = 'Frau/Herr';
                        }
                    } else {

                        $start_formal = "Dear ";
                        $start_informal = "Dear ";

                        if( $m_salute == 'male' ) {
                            $m_salute = 'Mr.';
                        } elseif( $m_salute == 'female' ) {
                            $m_salute = 'Ms.';
                        } else {
                            $m_salute = 'Mr./Ms.';
                        }
                    }

                    $m_formal = $start_formal.str_replace( '  ', ' ', $m_salute.' '.$sub['middle_name'].' '.$sub['last_name'] );
                    $m_informal = $start_informal.str_replace( '  ', ' ', $sub['first_name'] );
                    $m_full_name = str_replace( '  ', ' ', $m_salute.' '.$sub['first_name'].' '.$sub['middle_name'].' '.$sub['last_name'] );

                    $name_list .= $name_list == '' ? ucfirst( $m_full_name ) : ', '. ucfirst( $m_full_name );

                    if( is_email( $m_email ) && $sub['notification_sent'] == '0' ) {
                        $m_to[] = $m_email;

                        $m_subject = $event['subject_subscribed_ext'];

                        $m_subject = str_replace( '%FORMALHEAD%', $m_formal, $m_subject);
                        $m_subject = str_replace( '%INFORMALHEAD%', $m_informal, $m_subject);
                        $m_subject = str_replace( '%EVENTTITLE%', $m_event_title, $m_subject);
                        $m_subject = str_replace( '%EVENTDATE%', $m_event_date, $m_subject);
                        $m_subject = str_replace( '%EVENTTIME%', $m_event_time, $m_subject);
                        $m_subject = str_replace( '%OTHERDATES%', $m_other_dates, $m_subject);
                        $m_subject = str_replace( '%LOCATION%', $m_location, $m_subject);
                        $m_subject = str_replace( '%SENDERNAME%', $m_sendname, $m_subject);

                        $m_msg = $event['mail_subscribed_ext'];

                        $m_msg = str_replace( '%FORMALHEAD%', $m_formal, $m_msg);
                        $m_msg = str_replace( '%INFORMALHEAD%', $m_informal, $m_msg);
                        $m_msg = str_replace( '%EVENTTITLE%', $m_event_title, $m_msg);
                        $m_msg = str_replace( '%EVENTDATE%', $m_event_date, $m_msg);
                        $m_msg = str_replace( '%EVENTTIME%', $m_event_time, $m_msg);
                        $m_msg = str_replace( '%OTHERDATES%', $m_other_dates, $m_msg);
                        $m_msg = str_replace( '%LOCATION%', $m_location, $m_msg);
                        $m_msg = str_replace( '%SENDERNAME%', $m_sendname, $m_msg);

                        mail::send_mail($m_to, $m_from, $m_subject, $m_msg, $m_attachments);

                        //Prevent the sending of multiple copies on reload/back etc
                        $this->participant_md->update( $sub['id'], array('notification_sent'=>'1') );

                        $to_notify_internal = true;
                    }

                }

                if( $to_notify_internal === true )
                {
                    //Let Prowareness know there is a new subscription.

                    $m_subject_int = 'New subscription Agile Academy';

                    $m_msg_int = "Details of the subscription:<br><br>";
                    $m_msg_int .= $m_event_title."<br>";
                    $m_msg_int .= $m_event_date." ".$m_event_time."<br>";
                    $m_msg_int .= $m_location."<br><br>";
                    $m_msg_int .= $subscription['company']."<br>";
                    $m_msg_int .= $name_list."<br><br>";

                    //$m_msg_int = addslashes($m_msg_int); //To preserve the newlines. The send_mail strips slashes.

                    if( $m_to_internal <> '' ) {
                        $to_int = explode( ';', $m_to_internal );
                        mail::send_mail( $to_int, $m_from, $m_subject_int, $m_msg_int, null);
                    }

                }

                if( $subscription['payment_method'] == 'ideal') {

                   //Leave the session open. It will be closed by the return URL from the payment process.

                   //Redirect to Buckaroo payment
                   if( strtolower( $invoice['invoice_lang'] ) == 'nl' ) {
                       $url = "/payment.php?sess=".$token.'&lang=nl';
                   } else if( strtolower( $invoice['invoice_lang'] ) == 'de' ) {
                       $url = "/payment.php?sess=".$token.'&lang=de';
                   } else {
                       $url = "/payment.php?sess=".$token.'&lang=en';
                   }

                } else if( $subscription['payment_method'] == 'creditcard') {

                   //Leave the session open. It will be closed by the return URL from the payment process.

                   //Redirect to Buckaroo payment
                   if( strtolower( $invoice['invoice_lang'] ) == 'nl' ) {
                       $url = "/payment.php?sess=".$token.'&lang=nl';
                   } else if( strtolower( $invoice['invoice_lang'] ) == 'de' ) {
                       $url = "/payment.php?sess=".$token.'&lang=de';
                   } else {
                       $url = "/payment.php?sess=".$token.'&lang=en';
                   }
                } else {

                    //No need for further processing, so session can be closed.
                    $session['payment_done'] = '1';
                    $this->session_md->update($session['id'], array( 'payment_done'=>'1' ));

                    // Payed events should be redirected to the invoice
                    if( $real_price > 0 ) {

                        //Redirect to invoice + PDF generation
                        if( strtolower( $invoice['invoice_lang'] ) == 'nl' ) {
                            $url = "invoice?sess=".$token;
                        } else if( strtolower( $invoice['invoice_lang'] ) == 'de' ) {
                            $url = "de/invoice?sess=".$token;
                        } else {
                            $url = "en/invoice?sess=".$token;
                        }

                    }


                }

            }

            //If a return url is set, we should redirect to it
            if( $url <> "" ) {
                echo('<meta http-equiv="refresh" content="0; url=' . $url . '">');
                exit();
            }

            //Prepare the data for the rendering

            $resultsView = new feEventSubscribeHtmlView();

            if( $event['locale'] == "NL" ) {
                if( $real_price == 0 ) {
                    $tabs = array(
                        '1' => 'Datum & Locatie',
                        '2' => 'Deelnemers',
                        '3' => 'Bedrijfsgegevens',
                        '4' => 'Overig',
                        '5' => 'Bevestigen'
                    );
                } else {
                    $tabs = array(
                        '1' => 'Datum & Locatie',
                        '2' => 'Deelnemers',
                        '3' => 'Factuurgegevens',
                        '4' => 'Overig',
                        '5' => 'Betalen',
                        '6' => 'Factuur'
                    );
                }

            } elseif( $event['locale'] == "DE" ) {
                if( $real_price == 0 ) {
                    $tabs = array(
                        '1' => 'Datum & Location',
                        '2' => 'Teilnehmer',
                        '3' => 'Unternemensinformation',
                        '4' => 'Andere',
                        '5' => 'Bestätige'
                    );
                } else {
                    $tabs = array(
                        '1' => 'Datum & Location',
                        '2' => 'Teilnehmer',
                        '3' => 'Rechnungsinformation',
                        '4' => 'Andere',
                        '5' => 'Zahlung',
                        '6' => 'Rechnung'
                    );
                }

            } else {
                if( $real_price == 0 ) {
                    $tabs = array(
                        '1' => 'Date & Location',
                        '2' => 'Participants',
                        '3' => 'Company information',
                        '4' => 'Other',
                        '5' => 'Confirm'
                    );
                } else {
                    $tabs = array(
                        '1' => 'Date & Location',
                        '2' => 'Participants',
                        '3' => 'Invoice information',
                        '4' => 'Other',
                        '5' => 'Payment',
                        '6' => 'Invoice'
                    );
                }

            }

            $data['event'] = $event;
            $locale = isset($event['locale']) ? strtolower($event['locale']) : 'en';

            //print_r($eventKnowHow);
            $allKnowHows = $this->know_how_options();
            $allKnowHows = $allKnowHows[$locale];
            foreach ($eventKnowHow as $k => $val) {
                $key = $val['know_how'];
                $arr[$key] = isset($allKnowHows[$key]) ? $allKnowHows[$key] : str_replace('-', ' ', $key);

            }


            $data['eventKnowHow'] = $arr;
            $data['tabs'] = $tabs;
            $data['active_tab_id'] = $active_tab;
            $data['selected_date_id'] = $selected_date_id;
            $data['subscribers'] = $subscribers_html;

           if( isset($_POST['event-participant-add']) ||
                isset($data['subscribers']) == false ||
                is_array( $data['subscribers'] ) == false ||
                isset($data['subscribers'][0]) == false ) {

                $data['subscribers'][] = array(
                    'id' => 0,
                    'salutation' => 'male',
                    'first_name' => '',
                    'middle_name' => '',
                    'last_name' => '',
                    'phone' => '',
                    'email' => '',
                    'newsletter' => '' );
            }

            $data['subscription'] = $subscription;

            $resultsView->render( $data, $session );

        }

        //----------------------------------------------------------------
        //                  SECURITY
        //----------------------------------------------------------------

        public function make_safe_text( $var, $max_length = 0 ) {

            $v = filter_var( $var, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES );

            if( $max_length > 0 ) {
                $v = substr( $v, 0, $max_length );
            }

            return $v;
        }
    }

endif;