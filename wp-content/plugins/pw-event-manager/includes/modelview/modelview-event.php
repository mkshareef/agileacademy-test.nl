<?php
/**
 * Direct access to the script is not allowed
 */
defined('ABSPATH') or die("No script kiddies please!");

/*
 * This is an helper class to aid in getting events and related details
 */

if( !class_exists( 'eventModelView' ) ):

    class eventModelView
    {

        private $event_mdl = null;
        private $location_mdl = null;
        private $trainer_mdl = null;
        private $group_mdl = null;
        private $action_code_mdl = null;
        private $action_code_assignment_mdl = null;
        private $invoice_mdl = null;

        private $event_date_mdl = null;
        private $event_trainer_mdl = null;
        private $event_group_mdl = null;

        public function __construct()
        {
            $path = plugin_dir_path( plugin_dir_path(__file__) );

            require_once( $path.'/models/model-event.php');
            require_once( $path.'/models/model-location.php');
            require_once( $path.'/models/model-trainer.php');
            require_once( $path.'/models/model-group.php');
            require_once( $path.'/models/model-action-code.php');
            require_once( $path.'/models/model-action-code-assignment.php');
            require_once( $path.'/models/model-invoice.php');
            require_once( $path.'/models/model-event-date.php');
            require_once( $path.'/models/model-event-trainer.php');
            require_once( $path.'/models/model-event-group.php');

            require_once( $path.'/classes/class-util.php');

            $this->event_mdl = new modelEvent();
            $this->location_mdl = new modelLocation();
            $this->trainer_mdl = new modelTrainer();
            $this->group_mdl = new modelGroup();
            $this->action_code_mdl = new modelActionCode();
            $this->event_date_mdl = new modelEventDate();
            $this->event_trainer_mdl = new modelEventTrainer();
            $this->event_group_mdl = new modelEventGroup();
            $this->action_code_assignment_mdl = new modelActionCodeAssignment();
            $this->invoice_mdl = new modelInvoice();
        }

        public function get_pwml_page_lang() {

            global $pw_config;
            $lang = strtolower( $pw_config['lang_primary'] );

            if (has_filter('wpml_post_language_details')) {
                $lang_details = apply_filters('wpml_post_language_details', NULL);
                $lang = explode('_', $lang_details['locale'])[0];
            }

            return $lang;
        }

        private function get_incompany_row( $event_id, $event_type, $lang ) {

            $result = "";

            if( $event_type=="training" ) {
                if($lang == "nl") {
                    $result = array(
                        'id' => '0',
                        'start_date' => 'In company',
                        'time' => 'In company',
                        'other_dates' => 'In overleg',
                        'duration' => 'In overleg',
                        'location' => 'In overleg',
                        'status' => 'request',
                        'url_subscribe' =>  '/subscribe?evt='.$event_id.'&dt=0',
                        'real_price' => 'In overleg'
                    );
                } elseif($lang == "de") {
                    $result = array(
                        'id' => '0',
                        'start_date' => 'Im Unternehmen',
                        'time' => 'zu besprechen',
                        'other_dates' => 'zu besprechen',
                        'duration' => 'zu besprechen',
                        'location' => 'zu besprechen',
                        'status' => 'request',
                        'url_subscribe' =>  '/de/subscribe?evt='.$event_id.'&dt=0',
                        'real_price' => 'zu besprechen'
                    );
                } else {
                    $result = array(
                        'id' => '0',
                        'start_date' => 'In company',
                        'time' => 'To be discussed',
                        'other_dates' => 'To be discussed',
                        'duration' => 'To be discussed',
                        'location' => 'To be discussed',
                        'status' => 'request',
                        'url_subscribe' =>  '/en/subscribe?evt='.$event_id.'&dt=0',
                        'real_price' => 'To be discussed'
                    );
                }
            }

            return $result;

        }

        private function get_price( $pricing_type, $event, $planned ) {

            $price = 0;

            //Get the global event prices
            //Override if there is a value set on date level
            if( $pricing_type == 'lastminute') {

                if( $planned['alt_price_last_minute'] > 0 ) {
                    $price = $planned['alt_price_last_minute'];
                } elseif( $event['price_last_minute'] > 0 ) {
                    $price = $event['price_last_minute'];
                } elseif( $planned['alt_price_normal'] > 0 ) {
                    $price = $planned['alt_price_normal'];
                } else {
                    $price = $event['price_normal'];
                }

            } elseif( $pricing_type == 'early') {

                if( $planned['alt_price_early_bird'] > 0 ) {
                    $price = $planned['alt_price_early_bird'];
                } elseif( $event['price_early_bird'] > 0 ) {
                    $price = $event['price_early_bird'];
                } elseif( $planned['alt_price_normal'] > 0 ) {
                    $price = $planned['alt_price_normal'];
                } else {
                    $price = $event['price_normal'];
                }

            } else {
                if( $planned['alt_price_normal'] > 0 ) {
                    $price = $planned['alt_price_normal'];
                } else {
                    $price = $event['price_normal'];
                }
            }

            return $price;
        }

        public function get_event_by_slug( $slug, $lang ) {

            if( $slug <> "" && $lang <> "" ) {

                //Get the event
                $event = $this->event_mdl->get_by_slug( $slug, $lang );

                if( $event <> "" ) {

                    $event_id = $event['id'];

                    //Get in company record for trainings and the planning for all events
                    $incompany = $this->get_incompany_row( $event_id, $event['type'], $lang);

                    $event['incompany'] = $incompany;

                    $planning = $this->event_date_mdl->get_planning_by_id( $event['id'] );

                    if( $planning <> "" ) {
                        for( $p = 0; $p < count($planning); $p++ ) {

                            //Get the real pricing based on early bird, normal and last minute prices
                            $pricing_type = util::get_pricing_type($planning[$p]['start_date']);

                            $planning[$p]['real_price'] = $this->get_price( $pricing_type, $event, $planning[$p] );

                            //Get the location details based on locationID
                            $planning[$p]['location'] = $this->location_mdl->get_by_id( $planning[$p]['locationID'] );
                        }
                    }

                    $event['planning'] = $planning;

                    //Get the trainer details
                    $event['trainers'] = "";

                    $trainers = $this->event_trainer_mdl->get_all_by_id( $event_id );

                    foreach( $trainers as $key => $trainer ) {

                        $rec = $this->trainer_mdl->get_by_id( $trainer['trainerID'] );

                        if( $rec <> "" ) {
                            $event['trainers'][] = $rec;
                        }
                    }

                }

                $record = $event;
            } else {
                $record = "";
            }

            return $record;
        }

        public function get_event_by_id( $eventId ) {

            $event_id = (int) $eventId;

            if( $event_id > 0  ) {

                //Get the event
                $event = $this->event_mdl->get_by_id($event_id);

                if( $event <> "" ) {
                    $lang = strtolower( $event['locale']);

                    //Get in company record for trainings and the planning for all events
                    $incompany = $this->get_incompany_row( $event_id, $event['type'], $lang);

                    $event['incompany'] = $incompany;

                    $planning = $this->event_date_mdl->get_planning_by_id( $event['id'] );

                    if( $planning <> "" ) {
                        for( $p = 0; $p < count($planning); $p++ ) {

                            //Get the real pricing based on early bird, normal and last minute prices
                            $pricing_type = util::get_pricing_type($planning[$p]['start_date']);

                            $planning[$p]['real_price'] = $this->get_price( $pricing_type, $event, $planning[$p] );

                            //Get the location details based on locationID
                            $planning[$p]['location'] = $this->location_mdl->get_by_id( $planning[$p]['locationID'] );
                        }
                    }

                    $event['planning'] = $planning;

                    //Get the trainer details
                    $event['trainers'] = "";

                    $trainers = $this->event_trainer_mdl->get_all_by_id( $event_id );

                    foreach( $trainers as $key => $trainer ) {

                        $rec = $this->trainer_mdl->get_by_id( $trainer['trainerID'] );

                        if( $rec <> "" ) {
                            $event['trainers'][] = $rec;
                        }
                    }

                }

                $record = $event;
            } else {
                $record = "";
            }

            return $record;
        }

        public function get_discount( $code, $company_name, $event_id, $event_date, $current_invoice_id ) {

            $action_code = "";

            $result['is_valid'] = 1;
            $result['discount_percent'] = 0;
            $result['discount_euro'] = 0;
            $result['limit_nr_use'] = 0;

            //No valid event_id provided
            if( (int) $event_id == 0 ) {
                $result['is_valid'] = 0;
            }

            //No date provided
            if( $event_date == "" || $event_date == "0000-00-00" ) {
                $result['is_valid'] = 0;
            }

            //No code provided
            if( $code == '') {
                $result['is_valid'] = 0;
            } else {
                //Get the details of the provided code
                $action_code = $this->action_code_mdl->get_by_code($code);
            }

            //Not an existing code
            if( $action_code == "") {
                $result['is_valid'] = 0;
            }

            //Code is invalid so we might as well exit here
            if( $result['is_valid'] == 0 ) {
                return $result;
            }

            //For type=company we need to search the company name for a matching check string
            //Type= training is not limited to a single company
            if( $action_code['type'] == 'company' ) {
                $company_name = strtolower( $company_name );
                $needle = strtolower( $action_code['check_string'] );

                if( strpos( $company_name, $needle ) === false ) {
                    $result['is_valid'] = 0;
                }
            }

            //Is the action code blocked?
            if( $action_code['is_blocked'] == '1' ) {
                $result['is_valid'] = 0;
            }

            //Is today in the allowed period of use?
            $now = strtotime( current_time('mysql') );

            $valid_from = strtotime($action_code['start_date']);
            $valid_until = strtotime($action_code['stop_date']);

            if( $now < $valid_from or $now > $valid_until ) {
                $result['is_valid'] = 0;
            }

            //Code is invalid so we might as well exit here
            if( $result['is_valid'] == 0 ) {
                return $result;
            }

            //Can we use the code for all events or just a specific subset?

            $is_valid_event = false;
            if( $action_code['code_scope'] == 'all' ) {
                //All events are allowed
                $is_valid_event = 1;
            } else {
                //Just a subset of selected events

                //Get the event id's that are associated with this action code
                $allowed_events = $this->action_code_assignment_mdl->get_all_by_id( $action_code['id'] );

                //Loop the allowed events and verify this event is a valid event
                if( is_array($allowed_events) ) {
                    foreach( $allowed_events as $key => $evt ) {
                        if( $evt['eventID'] == $event_id ) {
                            $is_valid_event = 1;
                        }
                    }
                }
            }

            //Is the code limited to a certain range for the start date?

            $start_is_in_range = 0;

            $start_range = $action_code['start_book_period'];
            $stop_range = $action_code['stop_book_period'];
            $selected_time = strtotime( $event_date );

            if( $start_range == '0000-00-00' && $stop_range == '0000-00-00' ) {
                //No range set
                $start_is_in_range = 1;
            } elseif ( $start_range != '0000-00-00' && $stop_range == '0000-00-00' ) {
                //Only start of range is set, no limit on the end date
                $range_from = strtotime( $start_range );

                if( $selected_time >= $range_from ) {
                    $start_is_in_range = 1;
                }

            } elseif ( $start_range == '0000-00-00' && $stop_range != '0000-00-00' ) {
                //One end of range is set, no limit on the start date
                $range_to = strtotime( $stop_range );

                if( $selected_time <= $range_to ) {
                    $start_is_in_range = 1;
                }
            } else {
                //We have a start and and of the range
                $range_from = strtotime( $start_range );
                $range_to = strtotime( $stop_range );

                if( $selected_time >= $range_from && $selected_time <= $range_to ) {
                    $start_is_in_range = 1;
                }
            }

            //If there is a limited on the nr of uses, we need to check the invoices for the usage count.
            $nr_allowed_to_use = (int) $action_code['max_use'];

            $is_maxed_out = 0;
            $nr_left = 0;

            if( $nr_allowed_to_use > 0 ) {

                $result['limit_nr_use'] = 1;

                //Count the number of uses, excluding the current invoice
                $nr_used = $this->invoice_mdl->count_action_code_used($code, $current_invoice_id);

                if( $nr_used >= $nr_allowed_to_use ) {
                    $is_maxed_out = 1;
                    $nr_left = 0;
                } else {
                    $nr_left = $nr_allowed_to_use - $nr_used;
                }
            }

            if( $is_valid_event == 1 && $start_is_in_range == 1 && $is_maxed_out == 0 ) {
                //OK!
                $result['is_valid'] = 1;

                if( $action_code['amount_type'] == 'percent' ) {
                    $result['discount_percent'] = $action_code['amount'];
                    $result['discount_euro'] = 0;
                } else {
                    $result['discount_percent'] = 0;
                    $result['discount_euro'] = $action_code['amount'];
                }

                $result['nr_use_left'] = $nr_left;
            }

            return $result;

        }

    }

endif;