<?php
/**
 * Direct access to the script is not allowed
 */
defined('ABSPATH') or die("No script kiddies please!");

$path = plugin_dir_path( plugin_dir_path(__file__) );
require_once( $path.'libs/fpdf17/fpdf.php');

class PDF extends FPDF
{
    var $B;
    var $I;
    var $U;
    var $HREF;

    function PDF($orientation='P', $unit='mm', $size='A4')
    {
        // Call parent constructor
        $this->FPDF($orientation,$unit,$size);
        // Initialization
        $this->B = 0;
        $this->I = 0;
        $this->U = 0;
        $this->HREF = '';
    }

    function WriteHTML($html)
    {
        // HTML parser
        $html = str_replace("\n",' ',$html);
        $a = preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
        foreach($a as $i=>$e)
        {
            if($i%2==0)
            {
                // Text
                if($this->HREF)
                    $this->PutLink($this->HREF,$e);
                else
                    $this->Write(5,$e);
            }
            else
            {
                // Tag
                if($e[0]=='/')
                    $this->CloseTag(strtoupper(substr($e,1)));
                else
                {
                    // Extract attributes
                    $a2 = explode(' ',$e);
                    $tag = strtoupper(array_shift($a2));
                    $attr = array();
                    foreach($a2 as $v)
                    {
                        if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                            $attr[strtoupper($a3[1])] = $a3[2];
                    }
                    $this->OpenTag($tag,$attr);
                }
            }
        }
    }

    function OpenTag($tag, $attr)
    {
        // Opening tag
        if($tag=='B' || $tag=='I' || $tag=='U')
            $this->SetStyle($tag,true);
        if($tag=='A')
            $this->HREF = $attr['HREF'];
        if($tag=='BR')
            $this->Ln(5);
    }

    function CloseTag($tag)
    {
        // Closing tag
        if($tag=='B' || $tag=='I' || $tag=='U')
            $this->SetStyle($tag,false);
        if($tag=='A')
            $this->HREF = '';
    }

    function SetStyle($tag, $enable)
    {
        // Modify style and select corresponding font
        $this->$tag += ($enable ? 1 : -1);
        $style = '';
        foreach(array('B', 'I', 'U') as $s)
        {
            if($this->$s>0)
                $style .= $s;
        }
        $this->SetFont('',$style);
    }

    function PutLink($URL, $txt)
    {
        // Put a hyperlink
        $this->SetTextColor(0,0,255);
        $this->SetStyle('U',true);
        $this->Write(5,$txt,$URL);
        $this->SetStyle('U',false);
        $this->SetTextColor(0);
    }

    function Header()
    {
        $path = plugin_dir_path(plugin_dir_path( plugin_dir_path(__file__) ));
        $img = $path.'assets/img/prowareness_paper.jpg';
        $this->Image($img,0,0,-300);
       $this->Ln(24);
    }

}

if( !class_exists( 'invoiceModelView' ) ):

    class invoiceModelView
    {

        private $session_md = null;
        private $invoice_mdl = null;
        private $participant_md = null;

        public function __construct()
        {
            $path = plugin_dir_path( plugin_dir_path(__file__) );

            require_once( $path.'/models/model-session.php');
            require_once( $path.'/models/model-invoice.php');
            require_once( $path.'/models/model-participant.php');
            require_once( $path.'/classes/class-util.php');
            require_once( $path.'/settings.php');
            require_once( $path.'/classes/class-mail.php');

            $this->session_md = new modelSession();
            $this->invoice_mdl = new modelInvoice();
            $this->participant_md = new modelParticipant();
        }

        public function get_pwml_page_lang() {

            global $pw_config;
            $lang = strtolower( $pw_config['lang_primary'] );

            if (has_filter('wpml_post_language_details')) {
                $lang_details = apply_filters('wpml_post_language_details', NULL);
                $lang = explode('_', $lang_details['locale'])[0];
            }

            return $lang;
        }

        //  The PDF class deal wrong with characters like ë. This function makes sure the render as intended.
        private function replace_special_chars($word) {

            $word = utf8_decode($word);
            return $word;

        }

        public function create_credit_copy( $invoice ) {

            $invoice = stripslashes_deep( $invoice );

            $path = plugin_dir_path( plugin_dir_path(__file__) );
            require_once($path.'/classes/class-eventdatetime.php');

            $credit_copy = array(
                'subscriptionID' => $invoice['subscriptionID'],
                'invoice_company' => $invoice['invoice_company'],
                'invoice_attention' => $invoice['invoice_attention'],
                'invoice_attribute' => $invoice['invoice_attribute'],
                'invoice_street' => $invoice['invoice_street'],
                'invoice_street_nr' => $invoice['invoice_street_nr'],
                'invoice_zip' => $invoice['invoice_zip'],
                'invoice_city' => $invoice['invoice_city'],
                'invoice_country' => $invoice['invoice_country'],
                'invoice_email' => $invoice['invoice_email'],
                'invoice_action_code' => $invoice['invoice_action_code'],
                'payment_method' => $invoice['payment_method'],
                'description' => $invoice['description'],
                'removed' => $invoice['removed'],
                'invoice_nr_subscribed' => $invoice['invoice_nr_subscribed'],
                'invoice_nr' => $invoice['invoice_nr'].'C',
                'invoice_date' => eventDateTime::current_time_mysql(),
                'price_single_evt' => $invoice['price_single_evt'],
                'discount_nr' => $invoice['discount_nr'],
                'discount_amount' => $invoice['discount_amount'],
                'discount_type' => $invoice['discount_type'],
                'vat_percent' => $invoice['vat_percent'],
                'status' => $invoice['status'],
                'invoice_sent' => '0',
                'invoice_version' => $invoice['invoice_version'],
                'duration' => $invoice['duration'],
                'subscribers' => $invoice['subscribers'],
                'location' => $invoice['location'],
                'other_dates' => $invoice['other_dates'],
                'transaction_reference' => '',
                'transaction_status' => '',
                'transaction_id' => '',
                'transaction_date' => '',
                'invoice_lang' => $invoice['invoice_lang'],
                'invoice_type' => 'credit'

            );

            return $credit_copy;
        }

        public function email_invoice_pdf( $invoice, $send_to = '', $lang = 'en' ) {

            global $pw_config;

            $invoice = stripslashes_deep( $invoice );
            $subscriptionID = $invoice['subscriptionID'];
            $participant = $this->participant_md->get_all_by_subscription_id($subscriptionID);

            $date_format = $pw_config['date_format'][$lang];

            // Render the PDF to disk
            $fileName = WP_CONTENT_DIR . '/uploads/invoices/'.$invoice['invoice_nr'].'.pdf';
            $this->render_invoice_pdf($invoice, $lang, $fileName, false );

            if( $send_to == '' ) {
                $send_to = $invoice['invoice_email'];
            }

            $from = 'CFO Office <cfooffice@prowareness.nl>';

            $dArray = explode( ', ', $invoice['description'] );
            $dArray[1] =  date( $date_format ,strtotime( $dArray[1] ));
            $description = implode( ', ', $dArray );

            $m_salute = $participant[0]['salutation'];

            if( strtolower($lang) == 'nl') {

                $start_formal = "Geachte ";

                if( $m_salute == 'male' ) {
                    $m_salute = 'heer';
                } elseif( $m_salute == 'female' ) {
                    $m_salute = 'mevrouw';
                } else {
                    $m_salute = 'heer/mevrouw';
                }
            } elseif( strtolower($lang) == 'de') {

                $start_formal = "Sehr geehrter ";

                if( $m_salute == 'male' ) {
                    $m_salute = 'Herr';
                } elseif( $m_salute == 'female' ) {
                    $m_salute = 'Frau';
                } else {
                    $m_salute = 'Frau/Herr';
                }
            } else {

                $start_formal = "Dear ";

                if ($m_salute == 'male') {
                    $m_salute = 'Mr.';
                } elseif ($m_salute == 'female') {
                    $m_salute = 'Ms.';
                } else {
                    $m_salute = 'Mr./Ms.';
                }
            }

            if( strtolower($lang) == 'nl' ) {

                if( $invoice['invoice_type'] == 'debet') {
                    $subject = 'Factuur '.$invoice['invoice_nr'].' ('.$description.')';
                    $msg = "$start_formal"."$m_salute ".$participant[0]['last_name'].",<br><br>";
                    $msg .= "In de bijlage vindt u de factuur voor de volgende aanmelding:<br><br>";

                    $msg .= $description."<br>";
                    $msg .= $invoice['location']."<br><br>";

                    $msg .= "Deelnemer(s): ";
                    $msg .= $invoice['subscribers']."<br><br>";
                    $msg .= "Bij vragen kunt u contact met ons opnemen via +31 (0) 15 2411 800.<br><br>";
                    $msg .= "Vriendelijke groeten,<br>";
                    $msg .= "Agile Academy<br>";
                } else {
                    $subject = 'Creditfactuur '.$invoice['invoice_nr'].' ('.$description.')';
                    $msg = "$start_formal"."$m_salute ".$participant[0]['last_name'].",<br><br>";
                    $msg .= "In de bijlage vindt u de creditfactuur voor de volgende aanmelding:<br><br>";

                    $msg .= $description."<br>";
                    $msg .= $invoice['location']."<br><br>";

                    $msg .= "Deelnemer(s): ";
                    $msg .= $invoice['subscribers']."<br><br>";
                    $msg .= "Bij vragen kunt u contact met ons opnemen via +31 (0) 15 2411 800.<br><br>";
                    $msg .= "Vriendelijke groeten,<br>";
                    $msg .= "Agile Academy<br>";
                }


            } elseif( strtolower($lang) == 'de' ) {

                if( $invoice['invoice_type'] == 'debet') {
                    $subject = 'Rechnung '.$invoice['invoice_nr'].' ('.$description.')';
                    $msg = "$start_formal"."$m_salute ".$participant[0]['last_name'].",<br><br>";
                    $msg .= "Anbei befindet sich die rechnung für folgende Anmeldung:<br><br>";

                    $msg .= $description."<br>";
                    $msg .= $invoice['location']."<br><br>";

                    $msg .= "Teilnehmer: ";
                    $msg .= $invoice['subscribers']."<br><br>";
                    $msg .= "Falls Fragen auftauchen, kontaktieren Sie uns via +31 (0) 15 2411 800.<br><br>";
                    $msg .= "Freundliche Grüße,<br>";
                    $msg .= "Agile Academy<br>";
                } else {
                    $subject = 'Creditfactuur DE '.$invoice['invoice_nr'].' ('.$description.')';
                    $msg = "$start_formal"."$m_salute ".$participant[0]['last_name'].",<br><br>";
                    $msg .= "Hinzugefügt finden Sie die Gutschrift für folgende Anmeldung:<br><br>";

                    $msg .= $description."<br>";
                    $msg .= $invoice['location']."<br><br>";

                    $msg .= "Teilnehmer(s): ";
                    $msg .= $invoice['subscribers']."<br><br>";
                    $msg .= "Falls Fragen auftauchen, kontaktieren Sie uns via +31 (0) 15 2411 800.<br><br>";
                    $msg .= "Freundliche Grüße,<br>";
                    $msg .= "Agile Academy<br>";
                }

            } else {

                if( $invoice['invoice_type'] == 'debet') {
                    $subject = 'Invoice '.$invoice['invoice_nr'].' ('.$description.')';
                    $msg = "$start_formal"."$m_salute ".$participant[0]['last_name'].",<br><br>";
                    $msg .= "Attached you will find the invoice for the following subscription:<br><br>";
                    $msg .= $description."<br>";
                    $msg .= $invoice['location']."<br><br>";

                    $msg .= "Participant(s): ";
                    $msg .= $invoice['subscribers']."<br><br>";
                    $msg .= "In case of questions you can contact us via +31 (0) 15 2411 800.<br><br>";
                    $msg .= "Kind regards,<br>";
                    $msg .= "Agile Academy<br>";
                } else {
                    $subject = 'Credit invoice '.$invoice['invoice_nr'].' ('.$description.')';
                    $msg = "$start_formal"."$m_salute ".$participant[0]['last_name'].",<br><br>";
                    $msg .= "Attached you will find the credit invoice for the following subscription:<br><br>";

                    $msg .= $description."<br>";
                    $msg .= $invoice['location']."<br><br>";

                    $msg .= "Participant(s): ";
                    $msg .= $invoice['subscribers']."<br><br>";
                    $msg .= "In case of questions you can contact us via +31 (0) 15 2411 800.<br><br>";
                    $msg .= "Kind regards,<br>";
                    $msg .= "Agile Academy<br>";
                }

            }

            $attachments = array( $fileName );

            mail::send_mail($send_to, $from, $subject, $msg, $attachments);

            // Remove the PDF from disk
            unlink( $fileName );
        }

        public function render_invoice_html( $invoice, $lang, $token, $is_admin = null, $mode = null ) {

            global $pw_config;

            $invoice = stripslashes_deep( $invoice );

            if( $mode === null ) {
                $mode = 'read';
            }

            if( $is_admin === null ) {
                $is_admin = false;
            }

            $version = (int) $invoice['invoice_version'];

            if( $version < 1 ) {
                $version = 1;
            }

            $date_format = $pw_config['date_format'][$lang];

            if( $lang == 'nl' ) {

                $meta = $pw_config['invoice_meta_nl'][$version];

                $dec_point = ',';
                $thousands_sep = '.';

                $intro = 'Een kopie van deze factuur is gestuurd aan ';
                $download_lbl = 'Download als PDF';

                $invoice_lbl = array(
                    'invoice_heading' => 'Factuur',
                    'credit_heading' => 'Creditfactuur',
                    'invoice_nr' => 'Factuurnummer',
                    'order_date' => 'Besteldatum',
                    'payment_method' => 'Betaalmethode',
                    'payment_status' => 'Betaalstatus',
                    'invoice_date' => 'Factuurdatum',
                    'description' => 'Omschrijving',
                    'amount' => 'Aantal',
                    'price' => 'Prijs',
                    'total' => 'Totaal',
                    'discount' => 'Korting',
                    'subscribers' => 'Deelnemer(s)',
                    'duration' => 'Aantal dagen',
                    'start_date' => 'Aanvangsdatum',
                    'other_dates' => 'Overige datums',
                    'invoice_to' => 'Factureren aan',
                    'your_ref' => 'Factuurkenmerk',
                    'phone' => 'Tel',
                    'fax' => 'Fax',
                    'email' => 'E-mail',
                    'bank_account' => 'Bankrekening',
                    'iban' => 'IBAN',
                    'swift_bic' => 'Swiftadres/BIC',
                    'kvk' => 'KvK-nr',
                    'vat' => 'BTW-nr',
                    'tax' => 'Loonheffingsnr',
                );

                $payment_options = array(
                    'ideal' => 'iDeal',
                    'creditcard' => 'Creditcard',
                    'invoice' => 'Factuur',
                );

                $payment_status = array(
                    'new' => 'Open',
                    'payed' => 'Betaald'
                );

            } else if( $lang == 'de' ) {

                $meta = $pw_config['invoice_meta_de'][$version];

                $dec_point = ',';
                $thousands_sep = '.';

                $intro = 'Eine Kopie der Rechnung wurde gesendet an';
                $download_lbl = 'Download als PDF';

                $invoice_lbl = array(
                    'invoice_heading' => 'Rechnung',
                    'credit_heading' => 'Gutschrift',
                    'invoice_nr' => 'Rechnungsnummer',
                    'order_date' => 'Auftragsdatum',
                    'payment_method' => 'Zahlungsmethode',
                    'payment_status' => 'Zahlungsstatus',
                    'invoice_date' => 'Rechnungsdatum',
                    'description' => 'Beschreibung',
                    'amount' => 'Betrag',
                    'price' => 'Preis',
                    'total' => 'Gesamt',
                    'discount' => 'Rabatt',
                    'subscribers' => 'Teilnehmer',
                    'duration' => 'Anzahl der Tage',
                    'start_date' => 'Startdatum',
                    'other_dates' => 'Anderes Datum',
                    'invoice_to' => 'Rechnung zu',
                    'your_ref' => 'Deine Referenzen',
                    'phone' => 'Tel',
                    'fax' => 'Fax',
                    'email' => 'Email',
                    'bank_account' => 'Bank Konto',
                    'iban' => 'IBAN',
                    'swift_bic' => 'Swift/BIC',
                    'kvk' => 'CoC-NR',
                    'vat' => 'VAT-Nr',
                    'tax' => 'Tax-Nr',
                );

                $payment_options = array(
                    'ideal' => 'iDeal DE',
                    'creditcard' => 'Creditcard DE',
                    'invoice' => 'Factuur DE',
                );

                $payment_status = array(
                    'new' => 'Offen',
                    'payed' => 'Bezahlt'
                );

            } else {

                $meta = $pw_config['invoice_meta_en'][$version];

                $dec_point = '.';
                $thousands_sep = ',';

                $intro = 'A copy of this invoice has been mailed to ';
                $download_lbl = 'Download as PDF';

                $invoice_lbl = array(
                    'invoice_heading' => 'Invoice',
                    'credit_heading' => 'Credit invoice',
                    'invoice_nr' => 'Invoice number',
                    'order_date' => 'Order date',
                    'payment_method' => 'Payment',
                    'payment_status' => 'Payment status',
                    'invoice_date' => 'Invoice date',
                    'description' => 'Description',
                    'amount' => 'Quantity',
                    'price' => 'Price',
                    'total' => 'Total',
                    'discount' => 'Discount',
                    'subscribers' => 'Participant(s)',
                    'duration' => 'Number of days',
                    'start_date' => 'Start date',
                    'other_dates' => 'Other dates',
                    'invoice_to' => 'Bill to',
                    'your_ref' => 'Your reference',
                    'phone' => 'Tel',
                    'fax' => 'Fax',
                    'email' => 'Email',
                    'bank_account' => 'Bank account',
                    'iban' => 'IBAN',
                    'swift_bic' => 'Swift/BIC',
                    'kvk' => 'CoC-nr',
                    'vat' => 'VAT-nr',
                    'tax' => 'Tax-nr',
                );

                $payment_options = array(
                    'ideal' => 'iDeal',
                    'creditcard' => 'Creditcard',
                    'invoice' => 'Invoice',
                );

                $payment_status = array(
                    'new' => 'Open',
                    'payed' => 'Payed'
                );

            }

            $html = '';

            $html .= '<table class="pw_invoice_intro">';
            $html.= '<tr>';
            $html.= '<td>';
            $html.= $intro.' '.$invoice['invoice_email'].'.';
            $html.= '</td>';
            $html.= '<td class="align-right">';

            if( $is_admin == false ) {
                $html.= '<a href="pdf/?sess='.$token.'" class="action-btn" target="_blank">' . $download_lbl . '</a>';
            } else {
                $html.= '&nbsp;';
            }

            $html.= '</td>';
            $html.= '</tr>';
            $html.= '</table>';

            $html.= '<table class="pw_invoice_table">';

            $html.= '<tr>';
            $html.= '<td width="200">';

            if( $invoice['invoice_type'] == 'debet') {
                $html.= '<h3>'.$invoice_lbl['invoice_heading'].'</h3>';
            } else {
                $html.= '<h3>'.$invoice_lbl['credit_heading'].'</h3>';
            }

            $html.= '</td>';
            $html.= '<td>';
            $html.= ' ';
            $html.= '</td>';
            $html.= '</tr>';

            $html.= '<tr>';
            $html.= '<td>';
            $html.= '<label>'.$invoice_lbl['invoice_nr'].'</label>';
            $html.= '</td>';
            $html.= '<td>';
            $html.= $invoice['invoice_nr'];
            $html.= '</td>';
            $html.= '</tr>';

            $html.= '<tr>';
            $html.= '<td>';
            $html.= '<label>'.$invoice_lbl['order_date'].'</label>';
            $html.= '</td>';
            $html.= '<td>';
            $html.= date( $date_format ,strtotime( $invoice['invoice_date'] ));
            $html.= '</td>';
            $html.= '</tr>';

            $html.= '<tr>';
            $html.= '<td>';
            $html.= '<label>'.$invoice_lbl['payment_method'].'</label>';
            $html.= '</td>';
            $html.= '<td>';

            if($invoice['payment_method'] == 'creditcard' ) {
                $html.= $payment_options['creditcard'];
            } else if($invoice['payment_method'] == 'ideal' ) {
                $html.= $payment_options['ideal'];
            } else {
                $html.= $payment_options['invoice'];
            }

            $html.= '</td>';
            $html.= '</tr>';

            $html.= '<tr>';
            $html.= '<td>';
            $html.= '<label>'.$invoice_lbl['payment_status'].'</label>';
            $html.= '</td>';
            $html.= '<td>';

            if( $invoice['status'] == 'payed' ) {
                $html.= $payment_status['payed'];
            } else {
                $html.= $payment_status['new'];
            }

            $html.= '</td>';
            $html.= '</tr>';

            $html.= '<tr>';
            $html.= '<td>';
            $html.= '<label>'.$invoice_lbl['invoice_date'].'</label>';
            $html.= '</td>';
            $html.= '<td>';
            $html.= date( $date_format ,strtotime( $invoice['invoice_date'] ));
            $html.= '</td>';
            $html.= '</tr>';

            $html .= '</table>';

            $html .= '<table class="pw_invoice_table invoice_lines">';

            $html.= '<td width="300">';

            $html.= '<label>'.$invoice_lbl['description'].'</label><br><br>';
            $dArray = explode( ', ', $invoice['description'] );

            $l = strtolower( $invoice['invoice_lang'] );
            $df = $pw_config['date_format'][$l];
            $dArray[1] =  date( $df ,strtotime( $dArray[1] )); //Event date
            $event_date = $dArray[1];

            $description = implode( ', ', $dArray );

            $html.= $description;
            $html.= '</td>';
            $html.= '<td>';
            $html.= '<label>'.$invoice_lbl['amount'].'</label><br><br>';
            $html.= $invoice['invoice_nr_subscribed'];
            $html.= '</td>';
            $html.= '<td>';
            $html.= '<label>'.$invoice_lbl['price'].'</label><br><br>';
            $html.= '&euro; '.number_format( $invoice['price_single_evt'], 2, $dec_point, $thousands_sep);
            $html.= '</td>';
            $html.= '<td>';
            $html.= '<label>'.$invoice_lbl['total'].'</label><br><br>';

            $total_event = $invoice['invoice_nr_subscribed'] * $invoice['price_single_evt'];

            if( $invoice['invoice_type'] == 'debet') {
                $html.= '&euro; '.number_format( $total_event, 2, $dec_point, $thousands_sep);
            } else {
                $html.= '- &euro; '.number_format( $total_event, 2, $dec_point, $thousands_sep);
            }

            $html.= '</td>';
            $html.= '</tr>';

            $total_discount = $invoice['discount_nr'] * $invoice['discount_amount'];

            if( isset($invoice['pay_online_discount']) ) {
                $total_discount += $invoice['pay_online_discount'];
            }

            if( $total_discount > 0 ) {
                $html.= '<tr>';
                $html.= '<td>';
                $html.= ' ';
                $html.= '</td>';
                $html.= '<td>';
                $html.= ' ';
                $html.= '</td>';
                $html.= '<td>';
                $html.= $invoice_lbl['discount'];
                $html.= '</td>';
                $html.= '<td>';

                if( $invoice['invoice_type'] == 'debet') {
                    $html .= '- &euro; ' . number_format($total_discount, 2, $dec_point, $thousands_sep);
                } else {
                    $html .= '&euro; ' . number_format($total_discount, 2, $dec_point, $thousands_sep);
                }
                $html.= '</td>';
                $html.= '</tr>';
            }

            $html.= '<tr>';
            $html.= '<td>';
            $html.= ' ';
            $html.= '</td>';
            $html.= '<td>';
            $html.= ' ';
            $html.= '</td>';
            $html.= '<td>';

            $total = $total_event - $total_discount;
            $html.= '<label>'.$invoice_lbl['total'].'</label>';
            $html.= '</td>';
            $html.= '<td>';

            if( $invoice['invoice_type'] == 'debet') {
                $html .= '&euro; ' . number_format($total, 2, $dec_point, $thousands_sep);
            } else {
                $html .= '- &euro; ' . number_format($total, 2, $dec_point, $thousands_sep);
            }

            $html.= '</td>';
            $html.= '</tr>';

            $html .= '</table>';

            $html .= '<table class="pw_invoice_table">';

            $html.= '<tr>';
            $html.= '<td width="200">';
            $html.= '<label>'.$invoice_lbl['subscribers' ].'</label>';
            $html.= '</td>';
            $html.= '<td>';
            $html.= $invoice['subscribers'];
            $html.= '</td>';
            $html.= '</tr>';

            $html.= '<tr>';
            $html.= '<td width="200">';
            $html.= '<label>'.$invoice_lbl['duration' ].'</label>';
            $html.= '</td>';
            $html.= '<td>';
            $html.= $invoice['duration'];
            $html.= '</td>';
            $html.= '</tr>';

            $html.= '<tr>';
            $html.= '<td width="200">';
            $html.= '<label>'.$invoice_lbl['start_date' ].'</label>';
            $html.= '</td>';
            $html.= '<td>';
            $html.= $event_date;
            $html.= '</td>';
            $html.= '</tr>';

            if( $invoice['other_dates'] <> '' ) {

                $l = strtolower( $invoice['invoice_lang'] );
                $df = $pw_config['date_format'][$l];
                $dArray = explode(', ', $invoice['other_dates'] );

                foreach( $dArray as $k=>$v ) {
                    $dArray[$k] =  date( $df ,strtotime( $v ));
                }

                $other_dates = implode( ', ', $dArray );

                $html.= '<tr>';
                $html.= '<td width="200">';
                $html.= '<label>'.$invoice_lbl['other_dates' ].'</label>';
                $html.= '</td>';
                $html.= '<td>';
                $html.= $other_dates;
                $html.= '</td>';
                $html.= '</tr>';
            }

            $html .= '</table>';

            $html .= '<table class="pw_invoice_table">';

            $html.= '<tr>';
            $html.= '<td>';
            $html.= '<label>'.$meta['company_name' ].'</label><br><br>';
            $html.= $meta['company_address'].'<br>'.$meta['company_zip'].', '.$meta['company_city'].'<br>'.$meta['company_country'].'<br><br>';
            $html.= $invoice_lbl['phone'].': '.$meta['company_phone'].'<br>';
            $html.= $invoice_lbl['fax'].': '.$meta['company_fax'].'<br>';
            $html.= $invoice_lbl['email'].': '.$meta['company_email'].'<br>';
            $html.= $meta['company_website'].'<br><br>';
            $html.= $invoice_lbl['bank_account'].': '.$meta['company_bank'].'<br>';
            $html.= $invoice_lbl['iban'].': '.$meta['company_iban'].'<br>';
            $html.= $invoice_lbl['swift_bic'].': '.$meta['company_swift_bic'].'<br>';
            $html.= $invoice_lbl['kvk'].': '.$meta['company_kvk'].'<br>';
            $html.= $invoice_lbl['vat'].': '.$meta['company_vat'].'<br>';
            $html.= $invoice_lbl['tax'].': '.$meta['company_tax'].'<br>';
            $html.= '</td>';
            $html.= '<td>';

            $html.= '<label>'.$invoice_lbl['invoice_to'].'</label><br><br>';

            if( $mode == 'edit' && $is_admin == true ) {
                $html.= '<input class="regular-text" type="text" name="invoice_company" value="'.$invoice['invoice_company'].'"><br>';
                $html.= '<input class="regular-text" type="text" name="invoice_attention" value="'.$invoice['invoice_attention'].'"><br>';
                $html.= '<input class="regular-text" type="text" name="invoice_street" value="'.$invoice['invoice_street'].'"><br>';
                $html.= '<input class="regular-text" type="text" name="invoice_street_nr" value="'.$invoice['invoice_street_nr'].'"><br>';
                $html.= '<input class="regular-text" type="text" name="invoice_zip" value="'.$invoice['invoice_zip'].'"><br>';
                $html.= '<input class="regular-text" type="text" name="invoice_city" value="'.$invoice['invoice_city'].'"><br>';
                $html.= '<input class="regular-text" type="text" name="invoice_country" value="'.$invoice['invoice_country'].'"><br><br>';

                $html.= $invoice_lbl['your_ref'].':<br>';
                $html.= '<input class="regular-text" type="text" name="invoice_attribute" value="'.$invoice['invoice_attribute'].'"><br><br>';
            } else {
                $html.= $invoice['invoice_company'].'<br>';
                $html.= $invoice['invoice_attention'].'<br>';
                $html.= $invoice['invoice_street'].' '.$invoice['invoice_street_nr'].'<br>';
                $html.= $invoice['invoice_zip'].', '.$invoice['invoice_city'].'<br>';
                $html.= $invoice['invoice_country'].'<br><br>';
                $html.= $invoice_lbl['your_ref'].':<br>';
                $html.= $invoice['invoice_attribute'].'<br><br>';
            }

            $html.= '</td>';
            $html.= '</tr>';

            $html .= '</table>';

            $html .= '<div>';
            $html .= $meta['company_note'];
            $html .= '</div>';
            $html .= '<br>';
            $html .= '<div>';
            $html .= $meta['company_footer'];
            $html .= '</div>';
            $html .= '<br>';
            $html .= '<br>';

            return $html;
        }

        public function render_invoice_meta($invoice) {

            $invoice = stripslashes_deep( $invoice );

            $html = '';

            $html.= '<table>';

            $html.= '<tr>';
            $html.= '<td width="200">';
            $html.= '<h3>'.__('Invoice details', 'pw-event-manager').'</h3>';
            $html.= '</td>';
            $html.= '<td>';
            $html.= ' ';
            $html.= '</td>';
            $html.= '</tr>';

            $html.= '<tr>';
            $html.= '<td>';
            $html.= '<label>'.__('Action code', 'pw-event-manager').'</label>';
            $html.= '</td>';
            $html.= '<td>';
            $html.= $invoice['invoice_action_code'];
            $html.= '</td>';
            $html.= '</tr>';

            $html.= '<tr>';
            $html.= '<td>';
            $html.= '<br>';
            $html.= '</td>';
            $html.= '<td>';
            $html.= '<br>';
            $html.= '</td>';
            $html.= '</tr>';

            $html.= '<tr>';
            $html.= '<td>';
            $html.= '<label>'.__('Transaction reference', 'pw-event-manager').'</label>';
            $html.= '</td>';
            $html.= '<td>';
            $html.= $invoice['transaction_reference'];
            $html.= '</td>';
            $html.= '</tr>';

            $html.= '<tr>';
            $html.= '<td>';
            $html.= '<label>'.__('Transaction status', 'pw-event-manager').'</label>';
            $html.= '</td>';
            $html.= '<td>';
            $html.= $invoice['transaction_status'];
            $html.= '</td>';
            $html.= '</tr>';

            $html.= '<tr>';
            $html.= '<td>';
            $html.= '<label>'.__('Transaction id', 'pw-event-manager').'</label>';
            $html.= '</td>';
            $html.= '<td>';
            $html.= $invoice['transaction_id'];
            $html.= '</td>';
            $html.= '</tr>';

            $html.= '<tr>';
            $html.= '<td>';
            $html.= '<label>'.__('Transaction date', 'pw-event-manager').'</label>';
            $html.= '</td>';
            $html.= '<td>';
            $html.= $invoice['transaction_date'];
            $html.= '</td>';
            $html.= '</tr>';

            $html.= '</table>';

            return $html;
        }


        public function render_invoice_multi_pdf( $invoice_array, $fileName = '', $to_browser = true ) {

            global $pw_config;

            if( $fileName == '' ) {
                $fileName = 'print_'.time().'.pdf';
            }

            $pdf = new PDF('P','mm','A4');

            if( is_array($invoice_array) ) {
                foreach( $invoice_array as $key=>$invoice ) {



                    $lang = $invoice['invoice_lang'];
                    $invoice = stripslashes_deep( $invoice );

                    $version = (int) $invoice['invoice_version'];

                    if( $version < 1 ) {
                        $version = 1;
                    }

                    $date_format = $pw_config['date_format'][$lang];


                    if( $lang == 'nl' ) {

                        $meta = $pw_config['invoice_meta_nl'][$version];

                        $dec_point = ',';
                        $thousands_sep = '.';

                        $intro = 'Een kopie van deze factuur is gestuurd aan ';
                        $download_lbl = 'Download als PDF';

                        $invoice_lbl = array(
                            'invoice_heading' => 'Factuur',
                            'credit_heading' => 'Creditfactuur',
                            'invoice_nr' => 'Factuurnummer',
                            'order_date' => 'Besteldatum',
                            'payment_method' => 'Betaalmethode',
                            'payment_status' => 'Betaalstatus',
                            'invoice_date' => 'Factuurdatum',
                            'description' => 'Omschrijving',
                            'amount' => 'Aantal',
                            'price' => 'Prijs',
                            'total' => 'Totaal',
                            'discount' => 'Korting',
                            'subscribers' => 'Deelnemer(s)',
                            'duration' => 'Aantal dagen',
                            'start_date' => 'Aanvangsdatum',
                            'other_dates' => 'Overige datums',
                            'invoice_to' => 'Factureren aan',
                            'your_ref' => 'Factuurkenmerk',
                            'phone' => 'Tel',
                            'fax' => 'Fax',
                            'email' => 'E-mail',
                            'bank_account' => 'Bankrekening',
                            'iban' => 'IBAN',
                            'swift_bic' => 'Swiftadres/BIC',
                            'kvk' => 'KvK-nr',
                            'vat' => 'BTW-nr',
                            'tax' => 'Loonheffingsnr',
                        );

                        $payment_options = array(
                            'ideal' => 'iDeal',
                            'creditcard' => 'Creditcard',
                            'invoice' => 'Factuur',
                        );

                        $payment_status = array(
                            'new' => 'Open',
                            'payed' => 'Betaald'
                        );

                    } else if( $lang == 'de' ) {

                        $meta = $pw_config['invoice_meta_de'][$version];

                        $dec_point = ',';
                        $thousands_sep = '.';

                        $intro = 'Eine Kopie der Rechnung wurde gesendet an';
                        $download_lbl = 'Download als PDF';

                        $invoice_lbl = array(
                            'invoice_heading' => 'Rechnung',
                            'credit_heading' => 'Gutschrift',
                            'invoice_nr' => 'Rechnungsnummer',
                            'order_date' => 'Auftragsdatum',
                            'payment_method' => 'Zahlungsmethode',
                            'payment_status' => 'Zahlungsstatus',
                            'invoice_date' => 'Rechnungsdatum',
                            'description' => 'Beschreibung',
                            'amount' => 'Betrag',
                            'price' => 'Preis',
                            'total' => 'Gesamt',
                            'discount' => 'Rabatt',
                            'subscribers' => 'Teilnehmer',
                            'duration' => 'Anzahl der Tage',
                            'start_date' => 'Startdatum',
                            'other_dates' => 'Anderes Datum',
                            'invoice_to' => 'Rechnung zu',
                            'your_ref' => 'Deine Referenzen',
                            'phone' => 'Tel',
                            'fax' => 'Fax',
                            'email' => 'Email',
                            'bank_account' => 'Bank Konto',
                            'iban' => 'IBAN',
                            'swift_bic' => 'Swift/BIC',
                            'kvk' => 'CoC-NR',
                            'vat' => 'VAT-Nr',
                            'tax' => 'Tax-Nr',
                        );

                        $payment_options = array(
                            'ideal' => 'iDeal DE',
                            'creditcard' => 'Creditcard DE',
                            'invoice' => 'Factuur DE',
                        );

                        $payment_status = array(
                            'new' => 'Offen',
                            'payed' => 'Bezahlt'
                        );

                    } else {

                        $meta = $pw_config['invoice_meta_en'][$version];

                        $dec_point = '.';
                        $thousands_sep = ',';

                        $intro = 'A copy of this invoice has been mailed to ';
                        $download_lbl = 'Download as PDF';

                        $invoice_lbl = array(
                            'invoice_heading' => 'Invoice',
                            'credit_heading' => 'Credit invoice',
                            'invoice_nr' => 'Invoice number',
                            'order_date' => 'Order date',
                            'payment_method' => 'Payment',
                            'payment_status' => 'Payment status',
                            'invoice_date' => 'Invoice date',
                            'description' => 'Description',
                            'amount' => 'Quantity',
                            'price' => 'Price',
                            'total' => 'Total',
                            'discount' => 'Discount',
                            'subscribers' => 'Participant(s)',
                            'duration' => 'Number of days',
                            'start_date' => 'Start date',
                            'other_dates' => 'Other dates',
                            'invoice_to' => 'Bill to',
                            'your_ref' => 'Your reference',
                            'phone' => 'Tel',
                            'fax' => 'Fax',
                            'email' => 'Email',
                            'bank_account' => 'Bank account',
                            'iban' => 'IBAN',
                            'swift_bic' => 'Swift/BIC',
                            'kvk' => 'CoC-nr',
                            'vat' => 'VAT-nr',
                            'tax' => 'Tax-nr',
                        );

                        $payment_options = array(
                            'ideal' => 'iDeal',
                            'creditcard' => 'Creditcard',
                            'invoice' => 'Invoice',
                        );

                        $payment_status = array(
                            'new' => 'Open',
                            'payed' => 'Payed'
                        );

                    }

                    $col1 = '50';
                    $col2 = '80';
                    $col3 = '30';

                    $text_color = 80;
                    $text_size = 10;
                    $heading_size = 13;
                    $line_color = 220;
                    $line_height = 6;

                    if( ! defined('EURO_SYMBOL')) {

                        define('EURO_SYMBOL',chr(128));
                    }

                    $pdf->SetTextColor($text_color);
                    $pdf->SetDrawColor($line_color,$line_color,$line_color);

                    $pdf->AddPage();

                    $pdf->SetFont('Arial','B',$heading_size);

                    if( $invoice['invoice_type'] == 'debet') {
                        $pdf->Cell(0, 10,$invoice_lbl['invoice_heading'], 'LTR');
                    } else {
                        $pdf->Cell(0, 10,$invoice_lbl['credit_heading'], 'LTR');
                    }

                    $pdf->Ln();

                    //Invoice number
                    $pdf->SetFont('Arial','B',$text_size);
                    $pdf->Cell($col1, $line_height,$invoice_lbl['invoice_nr'], 'L');

                    $pdf->SetFont('Arial','',$text_size);
                    $pdf->Cell(0, $line_height, $this->replace_special_chars( $invoice['invoice_nr'] ), 'R');
                    $pdf->Ln();

                    //Order date
                    $pdf->SetFont('Arial','B',$text_size);
                    $pdf->Cell($col1, $line_height,$invoice_lbl['order_date'], 'L');

                    $pdf->SetFont('Arial','',$text_size);
                    $pdf->Cell(0, $line_height, date( $date_format ,strtotime( $invoice['invoice_date'] )), 'R');
                    $pdf->Ln();

                    //Payment method
                    $pdf->SetFont('Arial','B',$text_size);
                    $pdf->Cell($col1, $line_height, $invoice_lbl['payment_method'], 'L');

                    if($invoice['payment_method'] == 'creditcard' ) {
                        $txt= $payment_options['creditcard'];
                    } else if($invoice['payment_method'] == 'ideal' ) {
                        $txt= $payment_options['ideal'];
                    } else {
                        $txt= $payment_options['invoice'];
                    }

                    $pdf->SetFont('Arial','',$text_size);
                    $pdf->Cell(0, $line_height, $txt, 'R');
                    $pdf->Ln();

                    //Payment status
                    $pdf->SetFont('Arial','B',$text_size);
                    $pdf->Cell($col1, $line_height, $invoice_lbl['payment_status'], 'L');

                    if( $invoice['status'] == 'payed' ) {
                        $txt = $payment_status['payed'];
                    } else {
                        $txt = $payment_status['new'];
                    }

                    $pdf->SetFont('Arial','',$text_size);
                    $pdf->Cell(0, $line_height, $txt, 'R');
                    $pdf->Ln();

                    //Invoice date
                    $pdf->SetFont('Arial','B',$text_size);
                    $pdf->Cell($col1, $line_height, $invoice_lbl['invoice_date'], 'LB');

                    $pdf->SetFont('Arial','',$text_size);
                    $pdf->Cell(0, $line_height, date( $date_format ,strtotime( $invoice['invoice_date'] )), 'RB');
                    $pdf->Ln();

                    $pdf->Ln();

                    //Table with invoice
                    $pdf->SetFont('Arial','B',$text_size);
                    $pdf->Cell($col2, $line_height,$invoice_lbl['description'], 'TL');
                    $pdf->Cell($col3, $line_height,$invoice_lbl['amount'], 'T');
                    $pdf->Cell($col3, $line_height,$invoice_lbl['price'], 'T');
                    $pdf->Cell(0, $line_height,$invoice_lbl['total'], 'TR');
                    $pdf->Ln();

                    $x = $pdf->GetX();
                    $y = $pdf->GetY();

                    $dArray = explode( ', ', $invoice['description'] );

                    $l = strtolower( $invoice['invoice_lang'] );
                    $df = $pw_config['date_format'][$l];
                    $dArray[1] =  date( $df ,strtotime( $dArray[1] ));
                    $event_date = $dArray[1];

                    $description = implode( ', ', $dArray );

                    $pdf->SetFont('Arial','',$text_size);
                    $pdf->MultiCell($col2, $line_height,$description, 'L', 'L');

                    $H = $pdf->GetY();
                    $height= $H-$y;
                    $nr_lines = $height / $line_height;

                    $x = $x + $col2;
                    $pdf->SetXY($x, $y);
                    $pdf->MultiCell($col3, $line_height,$invoice['invoice_nr_subscribed'].str_repeat("\n", $nr_lines), 0, 'L');

                    $x = $x + $col3;
                    $pdf->SetXY($x, $y);
                    $txt = EURO_SYMBOL.' '.number_format( $invoice['price_single_evt'], 2, $dec_point, $thousands_sep);
                    $pdf->MultiCell($col3, $line_height,$txt.str_repeat("\n", $nr_lines), 0, 'L');

                    $x = $x + $col3;
                    $pdf->SetXY($x, $y);
                    $total_event = $invoice['invoice_nr_subscribed'] * $invoice['price_single_evt'];

                    if( $invoice['invoice_type'] == 'debet') {
                        $txt = EURO_SYMBOL . ' ' . number_format($total_event, 2, $dec_point, $thousands_sep);
                    } else {
                        $txt = '- '.EURO_SYMBOL . ' ' . number_format($total_event, 2, $dec_point, $thousands_sep);
                    }

                    $pdf->MultiCell(0, $line_height,$txt.str_repeat("\n", $nr_lines), 'R', 'L');

                    $total_discount = $invoice['discount_nr'] * $invoice['discount_amount'];

                    if( isset($invoice['pay_online_discount']) ) {
                        $total_discount += $invoice['pay_online_discount'];
                    }

                    if( $total_discount > 0 ) {
                        $pdf->SetFont('Arial','',$text_size);
                        $pdf->Cell($col2, $line_height,'', 'L');
                        $pdf->Cell($col3, $line_height,'', 0);
                        $pdf->Cell($col3, $line_height,$invoice_lbl['discount'], 0);

                        if( $invoice['invoice_type'] == 'debet') {
                            $txt = '- '.EURO_SYMBOL.' '.number_format( $total_discount, 2, $dec_point, $thousands_sep);
                        } else {
                            $txt = EURO_SYMBOL.' '.number_format( $total_discount, 2, $dec_point, $thousands_sep);
                        }

                        $pdf->Cell(0, $line_height, $txt, 'R');
                        $pdf->Ln();
                    }

                    $pdf->SetFont('Arial','',$text_size);
                    $pdf->Cell($col2, $line_height,'', 'BL');
                    $pdf->Cell($col3, $line_height,'', 'B');

                    $pdf->SetFont('Arial','B',$text_size);
                    $pdf->Cell($col3, $line_height,$invoice_lbl['total'], 'B');

                    $total = $total_event - $total_discount;

                    if( $invoice['invoice_type'] == 'debet') {
                        $txt = EURO_SYMBOL.' '.number_format( $total, 2, $dec_point, $thousands_sep);
                    } else {
                        $txt = '- '.EURO_SYMBOL.' '.number_format( $total, 2, $dec_point, $thousands_sep);
                    }

                    $pdf->SetFont('Arial','',$text_size);
                    $pdf->Cell(0, $line_height, $txt, 'BR');
                    $pdf->Ln();

                    $pdf->Ln();

                    //Table with details

                    //Subscribers
                    $pdf->SetFont('Arial','B',$text_size);
                    $pdf->Cell($col1, $line_height,$invoice_lbl['subscribers'], 'TL');

                    $pdf->SetFont('Arial','',$text_size);
                    $pdf->Cell(0, $line_height, $this->replace_special_chars( $invoice['subscribers'] ), 'TR');
                    $pdf->Ln();

                    if( $invoice['other_dates'] <> '' ) {

                        $l = strtolower( $invoice['invoice_lang'] );
                        $df = $pw_config['date_format'][$l];
                        $dArray = explode(', ', $invoice['other_dates'] );

                        foreach( $dArray as $k=>$v ) {
                            $dArray[$k] =  date( $df ,strtotime( $v ));
                        }

                        $other_dates = implode( ', ', $dArray );

                        //Duration
                        $pdf->SetFont('Arial','B',$text_size);
                        $pdf->Cell($col1, $line_height,$invoice_lbl['duration'], 'L');

                        $pdf->SetFont('Arial','',$text_size);
                        $pdf->Cell(0, $line_height,$invoice['duration'], 'R');
                        $pdf->Ln();

                        //Startdate
                        $pdf->SetFont('Arial','B',$text_size);
                        $pdf->Cell($col1, $line_height,$invoice_lbl['start_date'], 'L');

                        $pdf->SetFont('Arial','',$text_size);
                        $pdf->Cell(0, $line_height,$event_date, 'R');
                        $pdf->Ln();

                        //Other dates
                        $pdf->SetFont('Arial','B',$text_size);
                        $pdf->Cell($col1, $line_height,$invoice_lbl['other_dates'], 'BL');

                        $pdf->SetFont('Arial','',$text_size);
                        $pdf->Cell(0, $line_height,$other_dates, 'BR');
                        $pdf->Ln();
                    } else {

                        //Duration
                        $pdf->SetFont('Arial','B',$text_size);
                        $pdf->Cell($col1, $line_height,$invoice_lbl['duration'], 'L');

                        $pdf->SetFont('Arial','',$text_size);
                        $pdf->Cell(0, $line_height,$invoice['duration'], 'R');
                        $pdf->Ln();

                        //Startdate
                        $pdf->SetFont('Arial','B',$text_size);
                        $pdf->Cell($col1, $line_height,$invoice_lbl['start_date'], 'BL');

                        $pdf->SetFont('Arial','',$text_size);
                        $pdf->Cell(0, $line_height,$event_date, 'BR');
                        $pdf->Ln();

                    }

                    $pdf->Ln();

                    //Table with address and other details
                    $pdf->SetFont('Arial','B',$text_size);
                    $pdf->Cell($pdf->w /2, $line_height,$meta['company_name'], 'TL');

                    $pdf->SetFont('Arial','B',$text_size);
                    $pdf->Cell(0, $line_height,$invoice_lbl['invoice_to'], 'TR');
                    $pdf->Ln();

                    $x = $pdf->GetX();
                    $y = $pdf->GetY();

                    $pdf->SetFont('Arial','',$text_size);

                    $txt = $meta['company_address']."\n".$meta['company_zip'].', '.$meta['company_city']."\n".$meta['company_country']."\n\n";
                    $txt.= $invoice_lbl['phone'].': '.$meta['company_phone']."\n";
                    $txt.= $invoice_lbl['fax'].': '.$meta['company_fax']."\n";
                    $txt.= $invoice_lbl['email'].': '.$meta['company_email']."\n";
                    $txt.= $meta['company_website']."\n\n";
                    $txt.= $invoice_lbl['bank_account'].': '.$meta['company_bank']."\n";
                    $txt.= $invoice_lbl['iban'].': '.$meta['company_iban']."\n";
                    $txt.= $invoice_lbl['swift_bic'].': '.$meta['company_swift_bic']."\n";
                    $txt.= $invoice_lbl['kvk'].': '.$meta['company_kvk']."\n";
                    $txt.= $invoice_lbl['vat'].': '.$meta['company_vat']."\n";
                    $txt.= $invoice_lbl['tax'].': '.$meta['company_tax']."\n";

                    $pdf->MultiCell($pdf->w /2, $line_height,$txt, 'BL');

                    $x = $x + ($pdf->w /2);

                    $H = $pdf->GetY();
                    $height= $H-$y;
                    $nr_lines = $height / $line_height;

                    $pdf->SetXY($x, $y);

                    $txt= $this->replace_special_chars( $invoice['invoice_company'] )."\n";
                    $txt.= $this->replace_special_chars( $invoice['invoice_attention'] )."\n";
                    $txt.= $this->replace_special_chars( $invoice['invoice_street'] ).' '.
                        $this->replace_special_chars( $invoice['invoice_street_nr'] )."\n";
                    $txt.= $this->replace_special_chars( $invoice['invoice_zip'] ).', '.
                        $this->replace_special_chars( $invoice['invoice_city'] )."\n";
                    $txt.= $this->replace_special_chars( $invoice['invoice_city'] )."\n\n";
                    $txt.= $this->replace_special_chars( $invoice_lbl['your_ref'] ).':'."\n";

                    $attr = wordwrap($this->replace_special_chars( $invoice['invoice_attribute']), 42, "\n");
                    $nr_attr_lines = count( explode("\n", $attr ) );

                    $txt.= $attr."\n";
                    $txt.= str_repeat("\n", ( $nr_lines - 7 - $nr_attr_lines ));
                    $pdf->SetFont('Arial','',$text_size);
                    $pdf->MultiCell(0, $line_height,$txt, 'BR');
                    $pdf->Ln();

                    $pdf->MultiCell(0, $line_height,$meta['company_note'], 0);
                    $pdf->Ln();

                    $pdf->WriteHTML($meta['company_footer']);

                }
            }

            if( $to_browser === true ) {
                $pdf->Output( $fileName, 'I');
            } else {
                $pdf->Output( $fileName, "F");
            }

        }

        public function render_invoice_pdf( $invoice, $lang, $fileName = '', $to_browser = true ) {

            global $pw_config;

            $invoice = stripslashes_deep( $invoice );

            $version = (int) $invoice['invoice_version'];

            if( $fileName == '' ) {
                $fileName = $invoice['invoice_nr'].'.pdf';
            }

            if( $version < 1 ) {
                $version = 1;
            }

            $date_format = $pw_config['date_format'][$lang];

            if( $lang == 'nl' ) {

                $meta = $pw_config['invoice_meta_nl'][$version];

                $dec_point = ',';
                $thousands_sep = '.';

                $intro = 'Een kopie van deze factuur is gestuurd aan ';
                $download_lbl = 'Download als PDF';

                $invoice_lbl = array(
                    'invoice_heading' => 'Factuur',
                    'credit_heading' => 'Creditfactuur',
                    'invoice_nr' => 'Factuurnummer',
                    'order_date' => 'Besteldatum',
                    'payment_method' => 'Betaalmethode',
                    'payment_status' => 'Betaalstatus',
                    'invoice_date' => 'Factuurdatum',
                    'description' => 'Omschrijving',
                    'amount' => 'Aantal',
                    'price' => 'Prijs',
                    'total' => 'Totaal',
                    'discount' => 'Korting',
                    'subscribers' => 'Deelnemer(s)',
                    'duration' => 'Aantal dagen',
                    'start_date' => 'Aanvangsdatum',
                    'other_dates' => 'Overige datums',
                    'invoice_to' => 'Factureren aan',
                    'your_ref' => 'Factuurkenmerk',
                    'phone' => 'Tel',
                    'fax' => 'Fax',
                    'email' => 'E-mail',
                    'bank_account' => 'Bankrekening',
                    'iban' => 'IBAN',
                    'swift_bic' => 'Swiftadres/BIC',
                    'kvk' => 'KvK-nr',
                    'vat' => 'BTW-nr',
                    'tax' => 'Loonheffingsnr',
                );

                $payment_options = array(
                    'ideal' => 'iDeal',
                    'creditcard' => 'Creditcard',
                    'invoice' => 'Factuur',
                );

                $payment_status = array(
                    'new' => 'Open',
                    'payed' => 'Betaald'
                );

            } else if( $lang == 'de' ) {

                $meta = $pw_config['invoice_meta_de'][$version];

                $dec_point = ',';
                $thousands_sep = '.';

                $intro = 'Eine Kopie der Rechnung wurde gesendet an';
                $download_lbl = 'Download als PDF';

                $invoice_lbl = array(
                    'invoice_heading' => 'Rechnung',
                    'credit_heading' => 'Gutschrift',
                    'invoice_nr' => 'Rechnungsnummer',
                    'order_date' => 'Auftragsdatum',
                    'payment_method' => 'Zahlungsmethode',
                    'payment_status' => 'Zahlungsstatus',
                    'invoice_date' => 'Rechnungsdatum',
                    'description' => 'Beschreibung',
                    'amount' => 'Betrag',
                    'price' => 'Preis',
                    'total' => 'Gesamt',
                    'discount' => 'Rabatt',
                    'subscribers' => 'Teilnehmer',
                    'duration' => 'Anzahl der Tage',
                    'start_date' => 'Startdatum',
                    'other_dates' => 'Anderes Datum',
                    'invoice_to' => 'Rechnung zu',
                    'your_ref' => 'Deine Referenzen',
                    'phone' => 'Tel',
                    'fax' => 'Fax',
                    'email' => 'Email',
                    'bank_account' => 'Bank Konto',
                    'iban' => 'IBAN',
                    'swift_bic' => 'Swift/BIC',
                    'kvk' => 'CoC-NR',
                    'vat' => 'VAT-Nr',
                    'tax' => 'Tax-Nr',
                );

                $payment_options = array(
                    'ideal' => 'iDeal DE',
                    'creditcard' => 'Creditcard DE',
                    'invoice' => 'Factuur DE',
                );

                $payment_status = array(
                    'new' => 'Offen',
                    'payed' => 'Bezahlt'
                );

            } else {

                $meta = $pw_config['invoice_meta_en'][$version];

                $dec_point = '.';
                $thousands_sep = ',';

                $intro = 'A copy of this invoice has been mailed to ';
                $download_lbl = 'Download as PDF';

                $invoice_lbl = array(
                    'invoice_heading' => 'Invoice',
                    'credit_heading' => 'Credit invoice',
                    'invoice_nr' => 'Invoice number',
                    'order_date' => 'Order date',
                    'payment_method' => 'Payment',
                    'payment_status' => 'Payment status',
                    'invoice_date' => 'Invoice date',
                    'description' => 'Description',
                    'amount' => 'Quantity',
                    'price' => 'Price',
                    'total' => 'Total',
                    'discount' => 'Discount',
                    'subscribers' => 'Participant(s)',
                    'duration' => 'Number of days',
                    'start_date' => 'Start date',
                    'other_dates' => 'Other dates',
                    'invoice_to' => 'Bill to',
                    'your_ref' => 'Your reference',
                    'phone' => 'Tel',
                    'fax' => 'Fax',
                    'email' => 'Email',
                    'bank_account' => 'Bank account',
                    'iban' => 'IBAN',
                    'swift_bic' => 'Swift/BIC',
                    'kvk' => 'CoC-nr',
                    'vat' => 'VAT-nr',
                    'tax' => 'Tax-nr',
                );

                $payment_options = array(
                    'ideal' => 'iDeal',
                    'creditcard' => 'Creditcard',
                    'invoice' => 'Invoice',
                );

                $payment_status = array(
                    'new' => 'Open',
                    'payed' => 'Payed'
                );

            }

            $col1 = '50';
            $col2 = '80';
            $col3 = '30';

            $text_color = 80;
            $text_size = 10;
            $heading_size = 13;
            $line_color = 220;
            $line_height = 6;

            if( ! defined('EURO_SYMBOL')) {
                define('EURO_SYMBOL', chr(128));
            }

            $pdf = new PDF('P','mm','A4');
            $pdf->SetTextColor($text_color);
            $pdf->SetDrawColor($line_color,$line_color,$line_color);

            $pdf->AddPage();

            $pdf->SetFont('Arial','B',$heading_size);

            if( $invoice['invoice_type'] == 'debet') {
                $pdf->Cell(0, 10,$invoice_lbl['invoice_heading'], 'LTR');
            } else {
                $pdf->Cell(0, 10,$invoice_lbl['credit_heading'], 'LTR');
            }

            $pdf->Ln();

            //Invoice number
            $pdf->SetFont('Arial','B',$text_size);
            $pdf->Cell($col1, $line_height,$invoice_lbl['invoice_nr'], 'L');

            $pdf->SetFont('Arial','',$text_size);
            $pdf->Cell(0, $line_height, $this->replace_special_chars( $invoice['invoice_nr'] ), 'R');
            $pdf->Ln();

            //Order date
            $pdf->SetFont('Arial','B',$text_size);
            $pdf->Cell($col1, $line_height,$invoice_lbl['order_date'], 'L');

            $pdf->SetFont('Arial','',$text_size);
            $pdf->Cell(0, $line_height, date( $date_format ,strtotime( $invoice['invoice_date'] )), 'R');
            $pdf->Ln();

            //Payment method
            $pdf->SetFont('Arial','B',$text_size);
            $pdf->Cell($col1, $line_height, $invoice_lbl['payment_method'], 'L');

            if($invoice['payment_method'] == 'creditcard' ) {
                $txt= $payment_options['creditcard'];
            } else if($invoice['payment_method'] == 'ideal' ) {
                $txt= $payment_options['ideal'];
            } else {
                $txt= $payment_options['invoice'];
            }

            $pdf->SetFont('Arial','',$text_size);
            $pdf->Cell(0, $line_height, $txt, 'R');
            $pdf->Ln();

            //Payment status
            $pdf->SetFont('Arial','B',$text_size);
            $pdf->Cell($col1, $line_height, $invoice_lbl['payment_status'], 'L');

            if( $invoice['status'] == 'payed' ) {
                $txt = $payment_status['payed'];
            } else {
                $txt = $payment_status['new'];
            }

            $pdf->SetFont('Arial','',$text_size);
            $pdf->Cell(0, $line_height, $txt, 'R');
            $pdf->Ln();

            //Invoice date
            $pdf->SetFont('Arial','B',$text_size);
            $pdf->Cell($col1, $line_height, $invoice_lbl['invoice_date'], 'LB');

            $pdf->SetFont('Arial','',$text_size);
            $pdf->Cell(0, $line_height, date( $date_format ,strtotime( $invoice['invoice_date'] )), 'RB');
            $pdf->Ln();

            $pdf->Ln();

            //Table with invoice
            $pdf->SetFont('Arial','B',$text_size);
            $pdf->Cell($col2, $line_height,$invoice_lbl['description'], 'TL');
            $pdf->Cell($col3, $line_height,$invoice_lbl['amount'], 'T');
            $pdf->Cell($col3, $line_height,$invoice_lbl['price'], 'T');
            $pdf->Cell(0, $line_height,$invoice_lbl['total'], 'TR');
            $pdf->Ln();

            $x = $pdf->GetX();
            $y = $pdf->GetY();

            $dArray = explode( ', ', $invoice['description'] );

            $l = strtolower( $invoice['invoice_lang'] );
            $df = $pw_config['date_format'][$l];
            $dArray[1] =  date( $df ,strtotime( $dArray[1] ));
            $event_date = $dArray[1];

            $description = implode( ', ', $dArray );

            $pdf->SetFont('Arial','',$text_size);
            $pdf->MultiCell($col2, $line_height,$description, 'L', 'L');

            $H = $pdf->GetY();
            $height= $H-$y;
            $nr_lines = $height / $line_height;

            $x = $x + $col2;
            $pdf->SetXY($x, $y);
            $pdf->MultiCell($col3, $line_height,$invoice['invoice_nr_subscribed'].str_repeat("\n", $nr_lines), 0, 'L');

            $x = $x + $col3;
            $pdf->SetXY($x, $y);
            $txt = EURO_SYMBOL.' '.number_format( $invoice['price_single_evt'], 2, $dec_point, $thousands_sep);
            $pdf->MultiCell($col3, $line_height,$txt.str_repeat("\n", $nr_lines), 0, 'L');

            $x = $x + $col3;
            $pdf->SetXY($x, $y);
            $total_event = $invoice['invoice_nr_subscribed'] * $invoice['price_single_evt'];

            if( $invoice['invoice_type'] == 'debet') {
                $txt = EURO_SYMBOL . ' ' . number_format($total_event, 2, $dec_point, $thousands_sep);
            } else {
                $txt = '- '.EURO_SYMBOL . ' ' . number_format($total_event, 2, $dec_point, $thousands_sep);
            }

            $pdf->MultiCell(0, $line_height,$txt.str_repeat("\n", $nr_lines), 'R', 'L');

            $total_discount = $invoice['discount_nr'] * $invoice['discount_amount'];

            if( isset($invoice['pay_online_discount']) ) {
                $total_discount += $invoice['pay_online_discount'];
            }

            if( $total_discount > 0 ) {
                $pdf->SetFont('Arial','',$text_size);
                $pdf->Cell($col2, $line_height,'', 'L');
                $pdf->Cell($col3, $line_height,'', 0);
                $pdf->Cell($col3, $line_height,$invoice_lbl['discount'], 0);

                if( $invoice['invoice_type'] == 'debet') {
                    $txt = '- '.EURO_SYMBOL.' '.number_format( $total_discount, 2, $dec_point, $thousands_sep);
                } else {
                    $txt = EURO_SYMBOL.' '.number_format( $total_discount, 2, $dec_point, $thousands_sep);
                }

                $pdf->Cell(0, $line_height, $txt, 'R');
                $pdf->Ln();
            }

            $pdf->SetFont('Arial','',$text_size);
            $pdf->Cell($col2, $line_height,'', 'BL');
            $pdf->Cell($col3, $line_height,'', 'B');

            $pdf->SetFont('Arial','B',$text_size);
            $pdf->Cell($col3, $line_height,$invoice_lbl['total'], 'B');

            $total = $total_event - $total_discount;

            if( $invoice['invoice_type'] == 'debet') {
                $txt = EURO_SYMBOL.' '.number_format( $total, 2, $dec_point, $thousands_sep);
            } else {
                $txt = '- '.EURO_SYMBOL.' '.number_format( $total, 2, $dec_point, $thousands_sep);
            }

            $pdf->SetFont('Arial','',$text_size);
            $pdf->Cell(0, $line_height, $txt, 'BR');
            $pdf->Ln();

            $pdf->Ln();

            //Table with details

            //Subscribers
            $pdf->SetFont('Arial','B',$text_size);
            $pdf->Cell($col1, $line_height,$invoice_lbl['subscribers'], 'TL');

            $pdf->SetFont('Arial','',$text_size);
            $pdf->Cell(0, $line_height, $this->replace_special_chars( $invoice['subscribers'] ), 'TR');
            $pdf->Ln();

            if( $invoice['other_dates'] <> '' ) {

                $l = strtolower( $invoice['invoice_lang'] );
                $df = $pw_config['date_format'][$l];
                $dArray = explode(', ', $invoice['other_dates'] );

                foreach( $dArray as $k=>$v ) {
                    $dArray[$k] =  date( $df ,strtotime( $v ));
                }

                $other_dates = implode( ', ', $dArray );

                //Duration
                $pdf->SetFont('Arial','B',$text_size);
                $pdf->Cell($col1, $line_height,$invoice_lbl['duration'], 'L');

                $pdf->SetFont('Arial','',$text_size);
                $pdf->Cell(0, $line_height,$invoice['duration'], 'R');
                $pdf->Ln();

                //Startdate
                $pdf->SetFont('Arial','B',$text_size);
                $pdf->Cell($col1, $line_height,$invoice_lbl['start_date'], 'L');

                $pdf->SetFont('Arial','',$text_size);
                $pdf->Cell(0, $line_height,$event_date, 'R');
                $pdf->Ln();

                //Other dates
                $pdf->SetFont('Arial','B',$text_size);
                $pdf->Cell($col1, $line_height,$invoice_lbl['other_dates'], 'BL');

                $pdf->SetFont('Arial','',$text_size);
                $pdf->Cell(0, $line_height,$other_dates, 'BR');
                $pdf->Ln();
            } else {

                //Duration
                $pdf->SetFont('Arial','B',$text_size);
                $pdf->Cell($col1, $line_height,$invoice_lbl['duration'], 'L');

                $pdf->SetFont('Arial','',$text_size);
                $pdf->Cell(0, $line_height,$invoice['duration'], 'R');
                $pdf->Ln();

                //Startdate
                $pdf->SetFont('Arial','B',$text_size);
                $pdf->Cell($col1, $line_height,$invoice_lbl['start_date'], 'BL');

                $pdf->SetFont('Arial','',$text_size);
                $pdf->Cell(0, $line_height,$event_date, 'BR');
                $pdf->Ln();
            }

            $pdf->Ln();

            //Table with address and other details
            $pdf->SetFont('Arial','B',$text_size);
            $pdf->Cell($pdf->w /2, $line_height,$meta['company_name'], 'TL');

            $pdf->SetFont('Arial','B',$text_size);
            $pdf->Cell(0, $line_height,$invoice_lbl['invoice_to'], 'TR');
            $pdf->Ln();

            $x = $pdf->GetX();
            $y = $pdf->GetY();

            $pdf->SetFont('Arial','',$text_size);

            $txt = $meta['company_address']."\n".$meta['company_zip'].', '.$meta['company_city']."\n".$meta['company_country']."\n\n";
            $txt.= $invoice_lbl['phone'].': '.$meta['company_phone']."\n";
            $txt.= $invoice_lbl['fax'].': '.$meta['company_fax']."\n";
            $txt.= $invoice_lbl['email'].': '.$meta['company_email']."\n";
            $txt.= $meta['company_website']."\n\n";
            $txt.= $invoice_lbl['bank_account'].': '.$meta['company_bank']."\n";
            $txt.= $invoice_lbl['iban'].': '.$meta['company_iban']."\n";
            $txt.= $invoice_lbl['swift_bic'].': '.$meta['company_swift_bic']."\n";
            $txt.= $invoice_lbl['kvk'].': '.$meta['company_kvk']."\n";
            $txt.= $invoice_lbl['vat'].': '.$meta['company_vat']."\n";
            $txt.= $invoice_lbl['tax'].': '.$meta['company_tax']."\n";

            $pdf->MultiCell($pdf->w /2, $line_height,$txt, 'BL');

            $x = $x + ($pdf->w /2);

            $H = $pdf->GetY();
            $height= $H-$y;
            $nr_lines = $height / $line_height;

            $pdf->SetXY($x, $y);

            $txt= $this->replace_special_chars( $invoice['invoice_company'] )."\n";
            $txt.= $this->replace_special_chars( $invoice['invoice_attention'] )."\n";
            $txt.= $this->replace_special_chars( $invoice['invoice_street'] ).' '.
                $this->replace_special_chars( $invoice['invoice_street_nr'] )."\n";
            $txt.= $this->replace_special_chars( $invoice['invoice_zip'] ).', '.
                $this->replace_special_chars( $invoice['invoice_city'] )."\n";
            $txt.= $this->replace_special_chars( $invoice['invoice_city'] )."\n\n";
            $txt.= $this->replace_special_chars( $invoice_lbl['your_ref'] ).':'."\n";

            $attr = wordwrap($this->replace_special_chars( $invoice['invoice_attribute']), 42, "\n");
            $nr_attr_lines = count( explode("\n", $attr ) );

            $txt.= $attr."\n";
            $txt.= str_repeat("\n", ( $nr_lines - 7 - $nr_attr_lines ));
            $pdf->SetFont('Arial','',$text_size);
            $pdf->MultiCell(0, $line_height,$txt, 'BR');
            $pdf->Ln();

            $pdf->MultiCell(0, $line_height,$meta['company_note'], 0);
            $pdf->Ln();

            $pdf->WriteHTML($meta['company_footer']);

            if( $to_browser === true ) {
                $pdf->Output( $fileName, 'I');
            } else {
                $pdf->Output( $fileName, "F");
            }

        }

        public function render_invoice_html_admin( $invoice, $lang, $mode ) {
            $bar = '';
            $content = '';
            $meta = '';

            $bar .= '<div class="pw-action-bar">';

            if( $mode == 'edit' ) {
                $bar .= '<input type="submit" class="button button-primary" name="saveInvoice" value="' . __('Save', 'pw-event-manager') . '">';
                $bar .= '<input type="submit" class="button button-primary-2" name="saveCloseInvoice" value="' . __('Save & close', 'pw-event-manager') . '">';
                $bar .= '<input type="submit" class="button button-secondary" name="closeInvoice" value="' . __('Cancel', 'pw-event-manager') . '">';
            } else {
                $bar .= '<a href="?page=pw-events-invoices&action=edit&id='.$invoice['id'].'" class="button button-primary" id="editInvoice">' . __('Edit', 'pw-event-manager') . '</a>';
                $bar .= '<a href="?page=pw-events-invoices" class="button button-primary-2" id="closeInvoice">' . __('Cancel', 'pw-event-manager') . '</a>';
                $bar .= '<a href="?page=pw-events-invoices&action=resend&id='.$invoice['id'].'" class="button button-secondary" id="resendInvoice">' . __('(Re)send', 'pw-event-manager') . '</a>';
                if( $invoice['invoice_type'] == 'debet') {
                    $bar .= '<a href="?page=pw-events-invoices&action=credit&id='.$invoice['id'].'" class="button button-secondary" id="creditInvoice">' . __('Credit', 'pw-event-manager') . '</a>';
                }

                $bar .= '<a href="?page=pw-events-invoices&action=topdf&id='.$invoice['id'].'" class="button button-secondary" id="pdfInvoice">' . __('Download as PDF', 'pw-event-manager') . '</a>';
                $bar .= '<a href="?page=pw-events-invoices&action=copycfo&id='.$invoice['id'].'" class="button button-secondary" id="copyCFO">' . __('Mail to CFO', 'pw-event-manager') . '</a>';
            }

            $bar .= '</div>';

            $content .= $this->render_invoice_html($invoice, $lang, null, true, $mode);

            if( $invoice['invoice_type'] == 'debet') {
                $meta = $this->render_invoice_meta($invoice);

                $content = '<form method="POST" name="formInvoiceEdit" action="'.$_SERVER['REQUEST_URI'].'" enctype="multipart/form-data">'.
                    $bar.'<div id="pw-invoice">'.$content.'</div>'.'<div id="pw-invoice-meta">'.$meta.
                    '</div><form>';
            } else {
                $content = '<form method="POST" name="formInvoiceEdit" action="'.$_SERVER['REQUEST_URI'].'" enctype="multipart/form-data">'.
                    $bar.'<div id="pw-invoice">'.$content.'</div><form>';
            }

            return $content;
        }

        public function process() {

            global $pw;
            global $pw_config;

            //Get the language
            $lang = $this->get_pwml_page_lang();

            //Get the session
            $session = '';

            $this->session_md->expire_sessions();
            $token = '';

            if( isset( $_GET['sess'] ) ) {
                $token = $_GET['sess'];
            }

            if( $token <> '' && strlen($token) == 32 ) {
                $session = $this->session_md->get_by_token( $token );
            }

            $html = '';
            if( $session == '' ) {
                if( $lang == 'nl' ) {
                    $html = '<p>De session is niet (meer) geldig.</p>';
                } else if( $lang == 'de' ) {
                    $html = '<p>Diese Sitzung ist nicht (mehr) verfügbar.</p>';
                } else {
                    $html = '<p>This session is not valid (anymore).</p>';
                }
            } else {
                //Get the invoice
                $invoice = $this->invoice_mdl->get_by_id($session['invoiceID']);

                if( $invoice == '' ) {
                    if( $lang == 'nl' ) {
                        $html = '<p>De session is niet (meer) geldig.</p>';
                    } else if( $lang == 'de' ) {
                        $html = '<p>Diese Sitzung ist nicht (mehr) verfügbar.</p>';
                    } else {
                        $html = '<p>This session is not valid (anymore).</p>';
                    }
                } else {

                    // Email the invoice when it is not allready sent
                    if( $invoice['invoice_sent'] <> '1' ) {
                        $send_to = $invoice['invoice_email'];

                        $this->email_invoice_pdf( $invoice, $send_to, $invoice['invoice_lang']);

                        if( $invoice['payment_method'] == 'invoice' ) {
                            //Sent copy to CFO
                            $this->email_invoice_pdf( $invoice, $pw_config['cfo_email'], $invoice['invoice_lang'] );
                        }

                        // Set the sent flag
                        $this->invoice_mdl->update($invoice['id'], array( 'invoice_sent' => '1'));
                    }

                    // Render the invoice
                    $html = $this->render_invoice_html( $invoice, $invoice['invoice_lang'], $token );
                }
            }

            //Show the invoice or error message
            echo '<div id="pw-invoice">'.$html.'</div>';
            //$pw->print_var($invoice);
        }

        //This function is called by page-pdf.php in the child theme directory
        public function create_pdf() {

            global $pw;

            //Get the language
            $lang = $this->get_pwml_page_lang();

            //Get the session
            $session = '';

            $this->session_md->expire_sessions();
            $token = '';

            if( isset( $_GET['sess'] ) ) {
                $token = $_GET['sess'];
            }

            if( $token <> '' && strlen($token) == 32 ) {
                $session = $this->session_md->get_by_token( $token );
            }

            $html = '';
            if( $session == '' ) {
                if( $lang == 'nl' ) {
                    $html = '<p>De PDF is niet (meer) beschikbaar.</p>';
                } else if( $lang == 'de' ) {
                    $html = '<p>Das PDF-Dokument ist nicht (mehr) verfügbar.</p>';
                } else {
                    $html = '<p>The PDF is not available (anymore).</p>';
                }
            } else {
                //Get the invoice
                $invoice = $this->invoice_mdl->get_by_id($session['invoiceID']);
                //$pw->print_var($invoice);

                if( $invoice == '' ) {
                    if( $lang == 'nl' ) {
                        $html = '<p>De PDF is niet (meer) beschikbaar.</p>';
                    } else if( $lang == 'de' ) {
                        $html = '<p>Das PDF-Dokument ist nicht (mehr) verfügbar.</p>';
                    } else {
                        $html = '<p>The PDF is not available (anymore).</p>';
                    }
                } else {

                    // Email the invoice when it is not allready sent

                    //Render the invoice in PDF
                    $this->render_invoice_pdf( $invoice, $invoice['invoice_lang'] );
                    exit(); //To prevent output to be sent
                }
            }

            //Show the invoice or error message
            echo '<p>'.$html.'<p>';

        }

    }

endif;