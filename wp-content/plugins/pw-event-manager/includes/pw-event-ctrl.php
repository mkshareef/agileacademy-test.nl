<?php

/**
 * Direct access to the script is not allowed
 */
defined('ABSPATH') or die("No script kiddies please!");

require_once( plugin_dir_path( plugin_dir_path(__file__) ).'includes/settings.php');

class pwEventCtrl
{
    public $mysql_table_names = array();
    public $event_db_version = "0.83";

    public function __construct()
    {
        global $wpdb;

        $this->mysql_table_names['events'] = $wpdb->prefix . "ev_events";
        $this->mysql_table_names['groups'] = $wpdb->prefix . "ev_groups";
        $this->mysql_table_names['trainers'] = $wpdb->prefix . "ev_trainers";
        $this->mysql_table_names['locations'] = $wpdb->prefix . "ev_locations";
        $this->mysql_table_names['event-group'] = $wpdb->prefix . "ev_event_group";
        $this->mysql_table_names['event-trainer'] = $wpdb->prefix . "ev_event_trainer";
        $this->mysql_table_names['event-know-how'] = $wpdb->prefix . "ev_event_know_how";
        $this->mysql_table_names['event-date'] = $wpdb->prefix . "ev_event_date";
        $this->mysql_table_names['action-code'] = $wpdb->prefix . "ev_action_code";
        $this->mysql_table_names['action-code-assignment'] = $wpdb->prefix . "ev_action_code_assignment";
        $this->mysql_table_names['sessions'] = $wpdb->prefix . "ev_sessions";
        $this->mysql_table_names['subscriptions'] = $wpdb->prefix . "ev_subscriptions";
        $this->mysql_table_names['invoices'] = $wpdb->prefix . "ev_invoices";
        $this->mysql_table_names['participants'] = $wpdb->prefix . "ev_participants";

        register_activation_hook(__FILE__, array($this, 'create_event_db_tables'));
        add_action('plugins_loaded', array($this, 'create_event_db_tables'));

        // hook add_query_vars function into query_vars
        add_filter('query_vars', array($this, 'add_query_vars') );

        //add_shortcode('pa-trainers', array($this, 'pa_trainers_shortcode'));
        $this->pw_event_register_shortcodes();
        add_action('admin_menu', array($this, 'populate_admin_menu'));

        add_filter( 'tiny_mce_before_init', array($this, 'setup_tinymce_settings') );

        add_action('admin_enqueue_scripts', array($this, 'load_assets_be'));
        add_action( 'wp_enqueue_scripts', array($this, 'load_assets_fe') );

        add_action('plugins_loaded', array($this, 'load_event_plugin_textdomain'));

        add_action( 'wp_ajax_ajax_get_know_how_select_options', array($this, 'ajax_get_know_how_select_options') );
        add_action( 'wp_ajax_ajax_get_selected_know_how_html', array($this, 'ajax_get_selected_know_how_html') );

        add_action( 'wp_ajax_ajax_get_trainers_select_options', array($this, 'ajax_get_trainers_select_options') );
        add_action( 'wp_ajax_ajax_get_selected_trainers_html', array($this, 'ajax_get_selected_trainers_html') );
        add_action( 'wp_ajax_ajax_populate_event_group_checkbox', array($this, 'ajax_populate_event_group_checkbox') );

        add_action( 'wp_ajax_nopriv_fe_ajax_get_event_names', array($this, 'fe_ajax_get_event_names') );
        add_action( 'wp_ajax_fe_ajax_get_event_names', array($this, 'fe_ajax_get_event_names') );

        add_filter('pll_the_language_link',  array($this, 'map_translations'), 10, 2);

        add_filter( 'wp_title', array($this, 'set_event_title' ), 1 );

        add_filter('wpseo_title', array($this, 'set_event_title'));

        add_filter('wpseo_canonical', array($this, 'set_event_canonical'));

        add_action('wp_head', array($this, 'add_wp_head'), 1);
        add_action('wp_footer', array($this, 'add_wp_footer'), 1);
    }

    /*
     *  This function handles the url locale of the events.
     */
    function map_translations($url, $slug) {

        $map = array(
            '/event/',
            '/training/',
            '/workshop/'
        );

        foreach($map as $m) {
            if (stripos($_SERVER['REQUEST_URI'],$m) !== false) {

                $url_el = explode("/", $_SERVER['REQUEST_URI'] );
                $url_el[2] = $slug;
                $url = implode('/', $url_el );

                return $url;
                break;
            }
        }

        return $url;

    }

    function add_query_vars($aVars) {
        $aVars[] = "eventtitle"; // represents the name of the event as shown in the URL
        $aVars[] = "search_event";
        $aVars[] = "category";

        return $aVars;
    }

    //----------------------------------------------------------------
    //     Add description and keywords to event/training/workshop
    //----------------------------------------------------------------
    function set_event_title( $title, $sep = "|" ) {

        global $pw_config;

        require_once('models/model-event.php');
        $event_mdl = new modelEvent();

        $url_slug = get_query_var('eventtitle');

        if( $url_slug <> "" ) {

            $lang = strtolower( $pw_config['lang_primary'] );

            if (has_filter('wpml_post_language_details')) {
                $lang_details = apply_filters('wpml_post_language_details', NULL);
                $lang = explode('_', $lang_details['locale'])[0];
            }

            $event = $event_mdl->get_by_slug($url_slug, $lang);

            if( isset( $event['id'] ) ) {
                if ($event['html_title'] <> "") {
                    $title = $event['html_title'];
                } else if ($event['name'] <> "") {
                    $title = $event['name'];
                }
            }
        }

        return $title;
    }

    function set_event_canonical($canonical) {

        $url_slug = get_query_var('eventtitle');

        if( $url_slug <> '' ) {
            return false;
        } else {
            return $canonical;
        }

    }

    function add_wp_head() {

        global $pw_config;

        require_once('models/model-event.php');
        $event_mdl = new modelEvent();

        $url_slug = get_query_var('eventtitle');

        if( $url_slug <> "" ) {
            $lang = strtolower( $pw_config['lang_primary'] );

            if (has_filter('wpml_post_language_details')) {
                $lang_details = apply_filters( 'wpml_post_language_details', NULL ) ;
                $lang = explode('_', $lang_details['locale'])[0];
            }

            $event = $event_mdl->get_by_slug( $url_slug, $lang );

            if( isset( $event['id'] ) ) {

                if( $event['html_description'] <> "" ) {
                    echo '<meta name="description" content="'.$event['html_description'].'" />';
                }
                if( $event['html_keywords'] <> "" ) {
                    echo '<meta name="keywords" content="'.$event['html_keywords'].'" />';
                }

                if( $event['html_add_header'] <> "" ) {
                    echo stripslashes( $event['html_add_header'] );
                }
            }

        }

    }

    function add_wp_footer() {

        global $pw_config;

        require_once('models/model-event.php');
        $event_mdl = new modelEvent();

        $url_slug = get_query_var('eventtitle');

        if( $url_slug <> "" ) {
            $lang = strtolower( $pw_config['lang_primary'] );

            if (has_filter('wpml_post_language_details')) {
                $lang_details = apply_filters( 'wpml_post_language_details', NULL ) ;
                $lang = explode('_', $lang_details['locale'])[0];
            }

            $event = $event_mdl->get_by_slug( $url_slug, $lang );

            if( isset( $event['id'] ) ) {
                if( $event['html_add_footer'] <> "" ) {
                    echo stripslashes( $event['html_add_footer'] );
                }
            }

        }

    }

    //----------------------------------------------------------------
    //                  Setup Database tables
    //----------------------------------------------------------------
    function create_event_db_tables()
    {
        global $wpdb;
        global $pw_config;

        $locale = strtoupper( $pw_config['lang_primary'] );

        if (get_site_option('event_db_version') != $this->event_db_version) {

            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            $charset_collate = $wpdb->get_charset_collate();

            // This table stores events
            $table_name = $this->mysql_table_names['events'];

            $sql = "CREATE TABLE $table_name (
              id int(11) NOT NULL AUTO_INCREMENT,
              name varchar(128) DEFAULT '' NOT NULL,
              locale varchar(3) DEFAULT '".$locale."' NOT NULL,
              type varchar(12) DEFAULT 'training' NOT NULL,
              publication_status varchar(12) DEFAULT 'concept' NOT NULL,
              attachment_path varchar(255) DEFAULT '' NULL,
              url_slug varchar(128) DEFAULT '' NOT NULL,
              description TEXT DEFAULT '' NULL,
              teaser varchar(255) DEFAULT '' NULL,
              notify_from varchar(256) DEFAULT '' NOT NULL,
              notify_emails varchar(256) DEFAULT '' NOT NULL,
              subject_subscribed_ext varchar(256) DEFAULT '' NOT NULL,
              mail_subscribed_ext TEXT DEFAULT '' NULL,
              subject_confirm_ext varchar(256) DEFAULT '' NOT NULL,
              mail_confirm_ext TEXT DEFAULT '' NULL,
              html_title varchar(128) DEFAULT '' NULL,
              html_description TEXT DEFAULT '' NULL,
              html_keywords varchar(255) DEFAULT '' NULL,
              html_add_header TEXT DEFAULT '' NULL,
              html_add_footer TEXT DEFAULT '' NULL,
              price_normal DECIMAL(9,2) NULL,
              price_last_minute DECIMAL(9,2) NULL,
              price_early_bird DECIMAL(9,2) NULL,
              duration DECIMAL(5,2) NULL,
              updated datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
              updated_by varchar(64) DEFAULT '' NOT NULL,
              removed varchar(1) DEFAULT '0' NOT NULL,
              ip varchar(46) DEFAULT '' NOT NULL,
            UNIQUE KEY id (id)
            ) $charset_collate;";

            dbDelta($sql);

            // This table stores locations
            $table_name = $this->mysql_table_names['locations'];

            $sql = "CREATE TABLE $table_name (
              id int(11) NOT NULL AUTO_INCREMENT,
              locale varchar(3) DEFAULT '".$locale."' NOT NULL,
              location_name varchar(64) DEFAULT '' NOT NULL,
              profile text DEFAULT '' NULL,
              maps_url text DEFAULT '' NULL,
              attachment_path varchar(255) DEFAULT '' NULL,
              updated datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
              updated_by varchar(64) DEFAULT '' NOT NULL,
              removed varchar(1) DEFAULT '0' NOT NULL,
              ip varchar(46) DEFAULT '' NOT NULL,
            UNIQUE KEY id (id)
            ) $charset_collate;";

            dbDelta($sql);


            // This table stores trainers
            $table_name = $this->mysql_table_names['trainers'];

            $sql = "CREATE TABLE $table_name (
              id int(11) NOT NULL AUTO_INCREMENT,
              locale varchar(3) DEFAULT '".$locale."' NOT NULL,
              trainer_name varchar(64) DEFAULT '' NOT NULL,
              company varchar(64) DEFAULT '' NULL,
              function_title varchar(64) DEFAULT '' NULL,
              profile text DEFAULT '' NULL,
              attachment_path varchar(255) DEFAULT '' NULL,
              updated datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
              updated_by varchar(64) DEFAULT '' NOT NULL,
              removed varchar(1) DEFAULT '0' NOT NULL,
              ip varchar(46) DEFAULT '' NOT NULL,
            UNIQUE KEY id (id)
            ) $charset_collate;";

            dbDelta($sql);

            // This table stores event groups
            $table_name = $this->mysql_table_names['groups'];

            $sql = "CREATE TABLE $table_name (
              id int(11) NOT NULL AUTO_INCREMENT,
              locale varchar(3) DEFAULT '".$locale."' NOT NULL,
              group_name varchar(64) DEFAULT '' NOT NULL,
              updated datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
              updated_by varchar(64) DEFAULT '' NOT NULL,
              removed varchar(1) DEFAULT '0' NOT NULL,
              ip varchar(46) DEFAULT '' NOT NULL,
            UNIQUE KEY id (id)
            ) $charset_collate;";

            dbDelta($sql);

            // This table stores event-group mappings
            $table_name = $this->mysql_table_names['event-group'];

            $sql = "CREATE TABLE $table_name (
              id int(11) NOT NULL AUTO_INCREMENT,
              eventID int(11) NOT NULL,
              groupID int(11) NOT NULL,
            UNIQUE KEY id (id)
            ) $charset_collate;";

            dbDelta($sql);

            // This table stores event-trainer mappings
            $table_name = $this->mysql_table_names['event-trainer'];

            $sql = "CREATE TABLE $table_name (
              id int(11) NOT NULL AUTO_INCREMENT,
              eventID int(11) NOT NULL,
              trainerID int(11) NOT NULL,
            UNIQUE KEY id (id)
            ) $charset_collate;";

            dbDelta($sql);

            // This table stores event-date mappings
            $table_name = $this->mysql_table_names['event-date'];

            $sql = "CREATE TABLE $table_name (
              id int(11) NOT NULL AUTO_INCREMENT,
              eventID int(11) NOT NULL,
              locationID int(11) NOT NULL,
              start_date date DEFAULT '0000-00-00' NOT NULL,
              start_time time DEFAULT '00:00:00' NOT NULL,
              stop_time time DEFAULT '00:00:00' NOT NULL,
              other_dates varchar(255) DEFAULT '' NOT NULL,
              alt_price_normal DECIMAL(9,2) NULL,
              alt_price_last_minute DECIMAL(9,2) NULL,
              alt_price_early_bird DECIMAL(9,2) NULL,
              status varchar(16) DEFAULT 'open' NOT NULL,
              updated datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
              updated_by varchar(64) DEFAULT '' NOT NULL,
              removed varchar(1) DEFAULT '0' NOT NULL,
              ip varchar(46) DEFAULT '' NOT NULL,
            UNIQUE KEY id (id)
            ) $charset_collate;";

            dbDelta($sql);

            // This table stores event-date mappings
            $table_name = $this->mysql_table_names['action-code'];

            $sql = "CREATE TABLE $table_name (
              id int(11) NOT NULL AUTO_INCREMENT,
              code varchar(32) DEFAULT '' NOT NULL,
              check_string varchar(64) DEFAULT '' NOT NULL,
              type varchar(16) DEFAULT 'training' NOT NULL,
              is_blocked varchar(1) DEFAULT '0' NOT NULL,
              description varchar(255) DEFAULT '' NOT NULL,
              start_date date DEFAULT '0000-00-00' NOT NULL,
              stop_date date DEFAULT '0000-00-00' NOT NULL,
              amount DECIMAL(9,2) DEFAULT '0' NOT NULL,
              amount_type varchar(16) DEFAULT 'percent' NOT NULL,
              code_scope varchar(16) DEFAULT 'all' NOT NULL,
              start_book_period date NULL,
              stop_book_period date NULL,
              max_use int(11) DEFAULT 0 NULL,
              updated datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
              updated_by varchar(64) DEFAULT '' NOT NULL,
              removed varchar(1) DEFAULT '0' NOT NULL,
              ip varchar(46) DEFAULT '' NOT NULL,
            UNIQUE KEY id (id)
            ) $charset_collate;";

            dbDelta($sql);

            // This table stores event-group mappings
            $table_name = $this->mysql_table_names['action-code-assignment'];

            $sql = "CREATE TABLE $table_name (
              id int(11) NOT NULL AUTO_INCREMENT,
              actionCodeID int(11) NOT NULL,
              eventID int(11) NULL,
            UNIQUE KEY id (id)
            ) $charset_collate;";

            dbDelta($sql);

            // This table stores sessions
            $table_name = $this->mysql_table_names['sessions'];

            $sql = "CREATE TABLE $table_name (
              id int(11) NOT NULL AUTO_INCREMENT,
              nonce varchar(32) NOT NULL,
              subscriptionID int(11) NOT NULL,
              invoiceID int(11) NULL,
              ui_lang varchar(2) DEFAULT '".strtolower( $locale )."' NOT NULL,
              payment_done varchar(1) DEFAULT '0' NOT NULL,
              expire_time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
              ip varchar(46) DEFAULT '' NOT NULL,
            UNIQUE KEY id (id)
            ) $charset_collate;";

            dbDelta($sql);

            // This table stores subscriptions
            $table_name = $this->mysql_table_names['subscriptions'];

            $sql = "CREATE TABLE $table_name (
              id int(11) NOT NULL AUTO_INCREMENT,
              eventID int(11) NOT NULL,
              eventDateId int(11) NOT NULL,
              status varchar(16) DEFAULT 'concept' NOT NULL,
              company varchar(128) NULL,
              invoice_lang varchar(2) DEFAULT '0' NOT NULL,
	          invoice_attention varchar(128) NULL,
              invoice_attribute varchar(255) NULL,
              street varchar(128) NULL,
	          street_nr varchar(32) NULL,
	          zip varchar(16) NULL,
	          city varchar(32) NULL,
	          country varchar(32) NULL,
	          invoice_email varchar(256) NULL,
              invoice_action_code varchar(32) NULL,
              payment_method varchar(32) NULL,
              know_how varchar(128) NULL,
              know_how_else varchar(128) NULL,
              diet TEXT NULL,
              comments TEXT NULL,
              process_comments TEXT NULL,
              agree_terms varchar(1) DEFAULT '0' NOT NULL,
              removed varchar(1) DEFAULT '0' NOT NULL,
              created datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
              updated datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
              updated_by varchar(64) DEFAULT '' NOT NULL,
              ip varchar(46) DEFAULT '' NOT NULL,
            UNIQUE KEY id (id)
            ) $charset_collate;";

            dbDelta($sql);

            // This table stores participants
            $table_name = $this->mysql_table_names['participants'];

            $sql = "CREATE TABLE $table_name (
              id int(11) NOT NULL AUTO_INCREMENT,
              eventID int(11) NOT NULL,
              eventDateID int(11) NOT NULL,
              subscriptionID int(11) NOT NULL,
              nonce varchar(32) NOT NULL,
              salutation varchar(16) NULL,
              first_name varchar(128) NULL,
              middle_name varchar(32) NULL,
              last_name varchar(128) DEFAULT '' NOT NULL,
              email varchar(256) NULL,
              phone varchar(64) NULL,
              newsletter varchar(1) DEFAULT '0' NOT NULL,
              notification_sent varchar(1) DEFAULT '0' NOT NULL,
              confirm_sent varchar(1) DEFAULT '0' NOT NULL,
              status varchar(16) DEFAULT 'concept' NOT NULL,
              process_comments TEXT NULL,
              updated datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
              updated_by varchar(64) DEFAULT '' NOT NULL,
              removed varchar(1) DEFAULT '0' NOT NULL,
              ip varchar(46) DEFAULT '' NOT NULL,
            UNIQUE KEY id (id)
            ) $charset_collate;";

            dbDelta($sql);

            // This table stores invoices
            $table_name = $this->mysql_table_names['invoices'];

            $sql = "CREATE TABLE $table_name (
              id int(11) NOT NULL AUTO_INCREMENT,
              subscriptionID int(11) NOT NULL,
              invoice_company varchar(128) NULL,
	          invoice_attention varchar(128) NULL,
              invoice_attribute varchar(255) NULL,
              invoice_street varchar(128) NULL,
	          invoice_street_nr varchar(32) NULL,
	          invoice_zip varchar(16) NULL,
	          invoice_city varchar(32) NULL,
	          invoice_country varchar(32) NULL,
	          invoice_email varchar(256) NULL,
              invoice_action_code varchar(32) NULL,
              invoice_nr_subscribed int(11) NULL,
              payment_method varchar(32) NULL,
              pay_online_discount DECIMAL(9,2) DEFAULT '0' NOT NULL,
              description TEXT NULL,
              duration int(11) NULL,
              subscribers TEXT NULL,
              location varchar(255) NULL,
              other_dates varchar(255) DEFAULT '' NULL,
              invoice_nr varchar(16) DEFAULT '' NOT NULL,
              invoice_date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
              price_single_evt DECIMAL(9,2) DEFAULT '0' NOT NULL,
              discount_nr int(11) NULL,
              discount_amount DECIMAL(9,2) DEFAULT '0' NOT NULL,
              discount_type varchar(16) DEFAULT 'percent' NOT NULL,
              vat_percent DECIMAL(5,2) DEFAULT '0' NOT NULL,
              status varchar(16) DEFAULT 'open' NOT NULL,
              invoice_sent varchar(1) DEFAULT '0' NOT NULL,
              invoice_version varchar(2) DEFAULT '0' NOT NULL,
              invoice_type varchar(6) DEFAULT 'debet' NOT NULL,
              invoice_lang varchar(2) DEFAULT '0' NOT NULL,
              transaction_reference varchar(32) DEFAULT '' NOT NULL,
              transaction_status varchar(32) DEFAULT '' NOT NULL,
              transaction_id varchar(32) DEFAULT '' NOT NULL,
              transaction_date varchar(32) DEFAULT '' NOT NULL,
              removed varchar(1) DEFAULT '0' NOT NULL,
              ip varchar(46) DEFAULT '' NOT NULL,
            UNIQUE KEY id (id)
            ) $charset_collate;";

            dbDelta($sql);

            update_option('event_db_version', $this->event_db_version);
        }
    }

    //----------------------------------------------------------------
    //                  TinyMCE initialization
    //----------------------------------------------------------------

    function setup_tinymce_settings(  $settings )
    {

        //The parent form should have a global javascript variable
        //named eventChanged, outside the jquery document ready
        $settings['setup'] = "function(ed) {
                                      ed.on('Change', function(ed, evt) {
                                              parent.eventChanged = true;
                                              console.log('set changed');
                                      });
                              }";

        return $settings;
    }

    //----------------------------------------------------------------
    //                  SHORT CODES
    //----------------------------------------------------------------

    private function get_search_form_data( $atts = null ) {

        global $pw_config;

        require_once('models/model-group.php');

        $group_mdl = new modelGroup();

        $data['title'] = "Zoek training";
        $data['search-place-holder'] = "Voer een trefwoord in";
        $data['search-button'] = "Zoek";

        $lang = strtolower( $pw_config['lang_primary'] );

        if (has_filter('wpml_post_language_details')) {
            $lang_details = apply_filters( 'wpml_post_language_details', NULL ) ;
            $lang = explode('_', $lang_details['locale'])[0];
        }

        $result_page_id = strtolower( isset($atts['result_page_id']) ? (int) $atts['result_page_id'] : 0 );

        $result_page = "";

        if( $result_page_id > 0 ) {
            if (has_filter('wpml_object_id')) {
                $result_page = get_permalink( apply_filters( 'wpml_object_id', $result_page_id, 'post', FALSE, $lang ) );
            } else {
                $result_page = get_permalink( $result_page_id );
            }
        }

        $data['result_page'] = $result_page;

        if($lang == "nl") {
            $data['title'] = "Zoek training";
            $data['search-place-holder'] = "Voer een trefwoord in";
            $data['search-button'] = "Zoek";

            $data['categories'] = $group_mdl->get_all_by_locale('nl');
            array_unshift($data['categories'], array('id'=>0, 'group_name'=>'Alle categoriën') );

        } elseif($lang == "de") {
            $data['title'] = "Suche Training";
            $data['search-place-holder'] = "Geben Sie ein Passwort ein";
            $data['search-button'] = "Suche";

            $data['categories'] = $group_mdl->get_all_by_locale('de');
            array_unshift($data['categories'], array('id'=>0, 'group_name'=>'Alle Kategorien') );

        } else {
            $data['title'] = "Search training";
            $data['search-place-holder'] = "Enter a keyword";
            $data['search-button'] = "Search";

            $data['categories'] = $group_mdl->get_all_by_locale('en');
            array_unshift($data['categories'], array('id'=>0, 'group_name'=>'All catgories') );
        }

        if( isset( $_GET['search_event'] ) && $_GET['search_event'] <> "" ) {
            $data['search_event'] = $this->make_safe_text($_GET['search_event']);
        }

        if( isset( $_GET['category'] ) && $_GET['category'] <> "" ) {
            $data['category_selected'] = (int) $_GET['category'];
        }

        return $data;

    }

    public function pw_event_keyword_search_shortcode( $atts )
    {

        require_once('views/view-fe-keyword-search.php');

        $html = new feKeywordSearchHtmlView();

        $data = $this->get_search_form_data( $atts );

        $html->render($data);

    }

    public function pw_event_search_results_shortcode() {

        global $pw_config;

        require_once('views/view-fe-keyword-search-results.php');

        require_once('models/model-event.php');
        require_once('models/model-event-date.php');
        require_once('models/model-event-group.php');

        require_once('classes/class-util.php');

        $key = "";
        $cat= 0;
        $event_mdl = new modelEvent();
        $planning_mdl = new modelEventDate();
        $group_mdl = new modelEventGroup();

        //TODO Move this setting to the plugin config
        $nr_per_page = 12;
        $qs = array();

        // Get variables from URL
        if( isset( $_GET['search_event'] ) && $_GET['search_event'] <> "" ) {
            $key = $this->make_safe_text($_GET['search_event']);
            $qs['search_event'] = $key;
        }

        if( isset( $_GET['category'] ) && $_GET['category'] <> "" ) {
            $cat = (int) $_GET['category'];
            $qs['category'] = $cat;
        }

        $page = 1;
        if( isset( $_GET['pag'] ) && $_GET['pag'] <> "" ) {
            $page = (int) $_GET['pag'];
        }
        if( $page < 1 ) {
            $page = 1;
        }

        $show_all = false;
        if( isset( $_GET['all'] ) && $_GET['all'] <> "" ) {
            $show_all = true;
            $qs['all'] = $this->make_safe_text($_GET['all']);
        }

        // Get the language
        $lang = strtolower( $pw_config['lang_primary'] );

        if (has_filter('wpml_post_language_details')) {
            $lang_details = apply_filters( 'wpml_post_language_details', NULL ) ;
            $lang = explode('_', $lang_details['locale'])[0];
        }

        //Get the training(s) matching the keywords (AND)
        $events = $event_mdl->search_by_locale( $key, $lang );

        //Filter by category
        if( is_array( $events ) && $cat > 0 ) {
            $filtered_list = array();

            foreach( $events as $event ) {
                $found = false;

                $ids = $group_mdl->get_all_by_id($event['id']);

                if( $ids <> "" ) {
                    foreach( $ids as $rec ) {
                        if( (int) $cat == (int) $rec['groupID'] ) {
                            $found = true;
                        }
                    }
                }

                if( $found == true ) {
                    $filtered_list[] = $event;
                }

            }

            $events = $filtered_list;
        }

        //Show the paginated results

        $data['title'] = "";

        if( $key <> "" ) {
            if($lang == "nl") {
                $data['title'] = "Zoekresultaten voor <b>".$key."</b>";
            } elseif($lang == "de") {
                $data['title'] = "Suche Ergebnisse für <b>".$key."</b>";
            } else {
                $data['title'] = "Search results for <b>".$key."</b>";
            }
        } else{
            if($lang == "nl") {
                $data['title'] = "Zoekresultaten";
            } elseif($lang == "de") {
                $data['title'] = "Suche Ergebnisse";
            } else {
                $data['title'] = "Search results";
            }
        }

        if($lang == "nl") {
            $data['no_results_msg'] = "Geen resultaten gevonden";
        } elseif($lang == "de") {
            $data['no_results_msg'] = "Keine Ergebnisse gefunden";
        } else {
            $data['no_results_msg'] = "No results found";
        }

        $data['lang'] = $lang;

        $results = array();

        if( is_array($events) && isset( $events[0] )) {
            foreach( $events as $key=>$event ) {

                $summary = $event['html_description'];

                if( $summary=="" ) {
                    $summary = implode(' ', array_slice(explode(' ', strip_tags( $event['description']) ), 0, 30));
                }

                if( isset($event['type']) ) {

                    if( $event['type']=="event" ) {
                        $more_link = "/event/?eventtitle=".$event['url_slug'];
                    }
                    elseif( $event['type']=="training" ) {
                        $more_link = "/training/?eventtitle=".$event['url_slug'];
                    }
                    elseif( $event['type']=="workshop" ) {
                        $more_link = "/workshop/?eventtitle=".$event['url_slug'];
                    }

                    if( $lang <> strtolower( $pw_config['lang_primary'] ) ) {
                        $more_link = "/".$lang.$more_link;
                    }

                }

                //$planning = $planning_mdl->get_planning_by_id( $event['id'] );

                $details = array();

                if( isset($event['type']) ) {
                    if( $event['type']=="training" ) {
                        if($lang == "nl") {
                            $details[] = array(
                                'plan_id' => '0',
                                'date' => 'In company',
                                'other_dates' => 'In overleg',
                                'location' => 'In overleg',
                                'status' => 'request',
                                'url_subscribe' =>  '/subscribe?evt='.$event['id'].'&dt=0',
                                'price' => 'In overleg'
                            );
                        } elseif($lang == "de") {
                            $details[] = array(
                                'plan_id' => '0',
                                'date' => 'In betrieb',
                                'other_dates' => 'In Absprache',
                                'location' => 'In Absprache',
                                'status' => 'request',
                                'url_subscribe' =>  '/de/subscribe?evt='.$event['id'].'&dt=0',
                                'price' => 'In Absprache'
                            );
                        } else {
                            $details[] = array(
                                'plan_id' => '0',
                                'date' => 'In company',
                                'other_dates' => 'To be discussed',
                                'location' => 'To be discussed',
                                'status' => 'request',
                                'url_subscribe' =>  '/en/subscribe?evt='.$event['id'].'&dt=0',
                                'price' => 'To be discussed'
                            );
                        }
                    }
                }

                $results[] = array(
                    'id'=>$event['id'],
                    'title'=>$event['name'],
                    'img_path'=>$event['attachment_path'],
                    'summary'=>$summary,
                    'type'=>$event['type'],
                    'url'=>$more_link,
                    'lang'=>$lang,
                    'details'=>$details
                );

            }

        }

        if( $show_all == true ) {
            $data['nr_per_page'] =  count( $results );
        } else {
            $data['nr_per_page'] = $nr_per_page;
        }

        $data['nr_results'] = count( $results );
        $nr_pages = ceil( count( $results ) / $data['nr_per_page'] );
        if( $nr_pages < $page ) {
            $page= $nr_pages;
        }

        $tmp = array();
        foreach( $qs as $key=>$value ) {
            $tmp[] = $key.'='.$value;
        }
        $data['qrs'] = implode( '&', $tmp );

        $data['nr_pages'] = $nr_pages;
        $data['show_page'] = $page;

        //Leave only records for the selected page
        $offset = ($page -1) * $nr_per_page;

        if( $show_all == true ) {
            $data['results'] = $results;
        } else {
            $data['results'] = array_slice( $results, $offset, $nr_per_page );
        }

        $resultsView = new feKeywordSearchResultsHtmlView();

        $resultsView->render($data);

    }

    public function pw_event_title_shortcode() {

        require_once('modelview/modelview-event.php');
        require_once('views/view-fe-event-title.php');

        $event_mv = new eventModelView();
        $resultsView = new feEventTitleHtmlView();

        $url_slug = get_query_var('eventtitle');
        $lang = $event_mv->get_pwml_page_lang();

        $event = $event_mv->get_event_by_slug( $url_slug, $lang );

        if( $event <> "" && isset( $event['publication_status'] ) && $event['publication_status'] == "publish" ) {
            $data['title'] = $event['name'];
        } else {

            if ($lang == "nl") {
                $data['title'] = "Fout: niet gevonden";
            } elseif ($lang == "de") {
                $data['title'] = "Error: nicht gefunden";
            } else {
                $data['title'] = "Error: not found";
            }

        }

        $resultsView->render($data);

    }

    public function pw_event_subscribe_btn_shortcode() {

        require_once('modelview/modelview-event.php');
        require_once('views/view-fe-event-subscribe-btn.php');

        $event_mv = new eventModelView();
        $resultsView = new feEventSubscribeBtnHtmlView();

        $url_slug = get_query_var('eventtitle');
        $lang = $event_mv->get_pwml_page_lang();

        $event = $event_mv->get_event_by_slug( $url_slug, $lang );

        $data = array();

        if( $event <> "" && isset( $event['publication_status'] ) && $event['publication_status'] == "publish" ) {

            if ($lang == "nl") {
                $data['label'] = 'Nu inschrijven';
                $data['url_subscribe'] = '/subscribe?evt=' . $event['id'] . '&dt=';
            } elseif ($lang == "de") {
                $data['label'] = 'Jetzt bewerben';
                $data['url_subscribe'] = $lang . '/subscribe?evt=' . $event['id'] . '&dt=';
            } else {
                $data['label'] = 'Apply now';
                $data['url_subscribe'] = $lang . '/subscribe?evt=' . $event['id'] . '&dt=';
            }

        }

        $resultsView->render($data);

    }

    public function pw_event_details_shortcode() {

        require_once('modelview/modelview-event.php');
        require_once('views/view-fe-event-details.php');

        $event_mv = new eventModelView();
        $resultsView = new feEventDetailsHtmlView();

        $url_slug = get_query_var('eventtitle');
        $lang = $event_mv->get_pwml_page_lang();

        $event = $event_mv->get_event_by_slug( $url_slug, $lang );

        if( $event <> "" && isset( $event['publication_status'] ) && $event['publication_status'] == "publish" ) {
            $data['description'] = $event['description'];
        } else {
            $data['description'] = "";
        }

        $resultsView->render($data);

    }

    public function pw_event_teaser_shortcode() {

        require_once('modelview/modelview-event.php');
        require_once('views/view-fe-event-teaser.php');

        $event_mv = new eventModelView();
        $resultsView = new feEventTeaserHtmlView();

        $url_slug = get_query_var('eventtitle');
        $lang = $event_mv->get_pwml_page_lang();

        $event = $event_mv->get_event_by_slug( $url_slug, $lang );

        if( $event <> "" && isset( $event['teaser'] ) && $event['teaser'] <> "" ) {
            $data['teaser'] = $event['teaser'];
        } else {
            $data['teaser'] = "";
        }

        $resultsView->render($data);

    }

    public function pw_event_planning_shortcode() {

        global $pw_config;

        require_once('modelview/modelview-event.php');
        require_once('views/view-fe-event-planning.php');

        $event_mv = new eventModelView();

        $url_slug = get_query_var('eventtitle');
        $lang = $event_mv->get_pwml_page_lang();

        $event = $event_mv->get_event_by_slug( $url_slug, $lang );

        $details = array();

        if( $event <> "" && isset( $event['publication_status'] ) && $event['publication_status'] == "publish" ) {

            if( is_array( $event['incompany']) ) {
                $details[] = array(
                    'plan_id' => $event['incompany']['id'],
                    'date' => $event['incompany']['start_date'],
                    'time' => $event['incompany']['time'],
                    'other_dates' => '',
                    'duration' => $event['incompany']['duration'],
                    'location' => $event['incompany']['location'],
                    'status' => $event['incompany']['status'],
                    'url_subscribe' => '/subscribe?evt='.$event['id'].'&dt=0',
                    'price' => $event['incompany']['real_price']
                );
            }

            $planning = $event['planning'];

            if( $planning <> "" ) {

                foreach( $planning as $plan ) {

                    //Set the right status
                    $status = $plan['status'];

                    $location = $plan['location']['location_name'];

                    $phpdate = strtotime( $plan['start_time'] );
                    $start = date( 'H:i', $phpdate );

                    $phpdate = strtotime( $plan['stop_time'] );
                    $stop = date( 'H:i', $phpdate );

                    $duration = $start.' - '.$stop;

                    $price = $plan['real_price'];

                    if( $lang == strtolower( $pw_config['lang_primary'] ) ) {
                        $details[] = array(
                            'plan_id' => $plan['id'],
                            'date' => $plan['start_date'],
                            'time' => $duration,
                            'other_dates' => $plan['other_dates'],
                            'duration' => $event['duration'],
                            'location' => $location,
                            'status' => $status,
                            'url_subscribe' => '/subscribe?evt='.$event['id'].'&dt='.$plan['id'],
                            'price' => $price
                        );
                    } else {
                        $details[] = array(
                            'plan_id' => $plan['id'],
                            'date' => $plan['start_date'],
                            'time' => $duration,
                            'other_dates' => $plan['other_dates'],
                            'duration' => $event['duration'],
                            'location' => $location,
                            'status' => $status,
                            'url_subscribe' => $lang.'/subscribe?evt='.$event['id'].'&dt='.$plan['id'],
                            'price' => $price
                        );
                    }

                }

            }

        }

        $data['lang'] = $lang;
        $data['planning'] = $details;

        $resultsView = new feEventPlanningHtmlView();

        $resultsView->render($data);

    }

    public function pw_event_trainers_shortcode() {

        require_once('modelview/modelview-event.php');
        require_once('views/view-fe-event-trainers.php');

        $event_mv = new eventModelView();

        $url_slug = get_query_var('eventtitle');
        $lang = $event_mv->get_pwml_page_lang();

        $event = $event_mv->get_event_by_slug( $url_slug, $lang );

        $data['trainers'] = $event['trainers'];
        $data['lang'] = $lang;
        $data['type'] = $event['type'];
        $resultsView = new feEventTrainerHtmlView();

        $resultsView->render($data);
    }

    public function pw_event_locations_shortcode() {

        require_once('modelview/modelview-event.php');
        require_once('views/view-fe-event-locations.php');

        $event_mv = new eventModelView();

        $url_slug = get_query_var('eventtitle');
        $lang = $event_mv->get_pwml_page_lang();

        $event = $event_mv->get_event_by_slug( $url_slug, $lang );

        $locations = array();

        if( is_array($event['planning']) ) {
           foreach( $event['planning'] as $value ) {
               $found = false;

               foreach( $locations as $loc ) {

                   if( $loc['id'] == $value['location']['id'] ) {
                       $found = true;
                   }
               }

               if( $found == false ) {
                   $locations[] = $value['location'];
               }
           }


        }

        $data['locations'] = $locations;
        $data['lang'] = $lang;
        $resultsView = new feEventLocationHtmlView();

        $resultsView->render($data);

    }

    public function pw_event_subscribe_shortcode() {

        require_once('modelview/modelview-subscribe.php');

        $subscribe_mv = new subscribeModelView();
        $subscribe_mv->process();

    }

    public function pw_event_invoice_shortcode() {

        require_once('modelview/modelview-invoice.php');

        $invoice_mv = new invoiceModelView();
        $invoice_mv->process();

    }

    /**
     * Register the shortcodes in Wordpress
     */
    function pw_event_register_shortcodes(){
        add_shortcode('pw_event_keyword_search', array($this, 'pw_event_keyword_search_shortcode'));
        add_shortcode('pw_event_search_results', array($this, 'pw_event_search_results_shortcode'));
        add_shortcode('pw_event_title', array($this, 'pw_event_title_shortcode'));
        add_shortcode('pw_event_details', array($this, 'pw_event_details_shortcode'));
        add_shortcode('pw_event_teaser', array($this, 'pw_event_teaser_shortcode'));
        add_shortcode('pw_event_planning', array($this, 'pw_event_planning_shortcode'));
        add_shortcode('pw_event_subscribe_btn', array($this, 'pw_event_subscribe_btn_shortcode'));
        add_shortcode('pw_event_trainers', array($this, 'pw_event_trainers_shortcode'));
        add_shortcode('pw_event_locations', array($this, 'pw_event_locations_shortcode'));
        add_shortcode('pw_event_subscribe', array($this, 'pw_event_subscribe_shortcode'));
        add_shortcode('pw_event_invoice', array($this, 'pw_event_invoice_shortcode'));
    }

    //----------------------------------------------------------------
    //                  TRANSLATIONS
    //----------------------------------------------------------------

    function load_event_plugin_textdomain()
    {
        load_plugin_textdomain('pw-event-manager', false, dirname( dirname(plugin_basename(__FILE__))) . '/languages');
    }

    //----------------------------------------------------------------
    //                  LOAD ASSETS
    //----------------------------------------------------------------
    public function load_assets_fe() {
        wp_enqueue_script( 'jquery-ui-autocomplete' );

        wp_register_script( 'pw-events-fe', plugins_url( 'pw-event-manager/assets/js/pw-events-fe.js' ) );
        wp_localize_script( 'pw-events-fe', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
        wp_enqueue_script( 'pw-events-fe' );

        wp_register_script( 'pw-more-fe', plugins_url( 'pw-event-manager/assets/js/readmore.min.js' ) );
        wp_enqueue_script( 'pw-more-fe' );

        wp_register_style('pw-event-jquery-ui-css', plugins_url('assets/css/jquery-ui.min.css', dirname(__FILE__)));
        wp_enqueue_style('pw-event-jquery-ui-css');

    }

    public function load_assets_be()
    {

        wp_register_script( 'pw-jquery-qubit', plugins_url( 'pw-event-manager/assets/js/jquery.qubit.js' ) );
        wp_enqueue_script( 'pw-jquery-qubit' );

        wp_register_script( 'pw-jquery-tree', plugins_url( 'pw-event-manager/assets/js/jquery.bonsai.js' ) );
        wp_enqueue_script( 'pw-jquery-tree' );

        wp_register_script( 'pw-events', plugins_url( 'pw-event-manager/assets/js/pw-event.js' ) );
        wp_localize_script( 'pw-events', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
        wp_enqueue_script( 'pw-events' );

        wp_register_style('pw-jquery-ui-css', plugins_url('assets/css/jquery-ui.min.css', dirname(__FILE__)));
        wp_enqueue_style('pw-jquery-ui-css');

        wp_register_style('pw-jquery-tree-css', plugins_url('assets/css/jquery.bonsai.css', dirname(__FILE__)));
        wp_enqueue_style('pw-jquery-tree-css');

        wp_register_style('pw-event-main-css', plugins_url('assets/css/event-be.css', dirname(__FILE__)));
        wp_enqueue_style('pw-event-main-css');

    }

    //----------------------------------------------------------------
    //                  Admin Menu
    //----------------------------------------------------------------

    public function populate_admin_menu()
    {
        global $submenu;

        add_menu_page("PW Events",
            "PW Events",
            "prowareness_events",
            "pw-events",
            array($this, 'render_event_list'),
            "dashicons-calendar",
            27
        );

        add_submenu_page("pw-events",
            __('Subscriptions', 'pw-event-manager'),
            __('Subscriptions', 'pw-event-manager'),
            "prowareness_events",
            "pw-events-subscriptions",
            array($this, 'render_subscriptions_list')
        );

        add_submenu_page("pw-events",
            __('Subscribers', 'pw-event-manager'),
            __('Subscribers', 'pw-event-manager'),
            "prowareness_events",
            "pw-events-subscribers",
            array($this, 'render_subscriber_list')
        );

        add_submenu_page("pw-events",
            __('Invoices', 'pw-event-manager'),
            __('Invoices', 'pw-event-manager'),
            "prowareness_invoices",
            "pw-events-invoices",
            array($this, 'render_invoices_list')
        );

        add_submenu_page("pw-events",
            __('Trainers', 'pw-event-manager'),
            __('Trainers', 'pw-event-manager'),
            "prowareness_events",
            "pw-events-trainers",
            array($this, 'render_trainer_list')
        );

        add_submenu_page("pw-events",
            __('Locations', 'pw-event-manager'),
            __('Locations', 'pw-event-manager'),
            "prowareness_events",
            "pw-events-locations",
            array($this, 'render_locations_list')
        );

        add_submenu_page("pw-events",
            __('Action codes', 'pw-event-manager'),
            __('Action codes', 'pw-event-manager'),
            "prowareness_events",
            "pw-event-action-codes",
            array($this, 'render_action_codes')
        );

        add_submenu_page("pw-events",
            __('Settings', 'pw-event-manager'),
            __('Settings', 'pw-event-manager'),
            "prowareness_events",
            "pw-event-settings",
            array($this, 'render_settings')
        );
/*
        add_submenu_page("pw-events",
            __('Trash', 'pw-event-manager'),
            __('Trash', 'pw-event-manager'),
            "prowareness_events",
            "pw-event-trash",
            array($this, 'render_trash')
        );
*/
        add_submenu_page(null,
            'hidden_event_date',
            'hidden_event_date',
            "prowareness_events",
            "pw-event-date",
            array($this, 'render_event_date')
        );

        $submenu['pw-events'][0][0] = __('All events', 'pw-event-manager');

    }

    //----------------------------------------------------------------
    //                  AJAX
    //----------------------------------------------------------------

    function know_how_options() {
        $data = array(
            'en' => array(
                ''                  => '- Make a choice -',
                'google'            => 'Via Google',
                'website'           => 'Via the website',
                'pro-contributor'   => 'Via a Prowareness employee',
                'friend'            => 'Via friends',
                'colleagues'        => "Via colleagues",
                'news'              => 'Via the newsletter',
                'twitter'           => 'Via Twitter',
                'linkedin'          => 'Via Linkedin',
                'bnr'               => 'Via BNR radio campaign',
                'sdn-nl'            => 'Via sdn.nl',
                'improve'           => 'Via Improve',
                'agile-market'      => 'Via Agile+ Market',
                'others'            => 'Other...'
            ),
            'nl' => array(
                ''                  => '- Maak een keuze -',
                'google'            => 'Via Google',
                'website'           => 'Via de website',
                'bnr'               => 'Via BNR',
                'news'              => 'Via de nieuwsbrief',
                'twitter'           => 'Via Twitter',
                'linkedin'          => 'Via Linkedin',
                'pro-contributor'   => 'Via een Prowareness medewerker',
                'colleagues'        => "Via collega's",
                'pro-event'         => 'Via een Prowareness event',
                'pro-training'      => 'Via een Prowareness training',
                'conference'        => 'Via een conferentie',
                'scrum-org'         => 'Via Scrum.org',
                'others'            => 'Anders...'
            ),
            'de' => array(
                ''                  => '- Treffen Sie eine Wahl -',
                'google'            => 'Via Google',
                'website'           => 'Via Webseite',
                'pro-contributor'   => 'Via einem Prowareness Mitarbeiter',
                'friend'            => 'Via Freunde',
                'colleagues'        => "Via Kollegen",
                'news'              => 'Via dem Newsletter',
                'twitter'           => 'Via Twitter',
                'linkedin'          => 'Via Linkedin',
                'bnr'               => 'Via BNR Raio Kampagne',
                'sdn-nl'            => 'Via sdn.nl',
                'improve'           => 'Via Improve',
                'agile-market'      => 'Via Agile+ Market',
                'others'            => 'Andere…'
            )
        );

        return $data;
    }

    function ajax_get_know_how_select_options() {
        $data = $this->know_how_options();

        //return the resulting html
        echo( json_encode( $data['nl'] ) );

        // Always die in functions echoing ajax content
        die();
    }

    function ajax_get_selected_know_how_html() {
        $ids = "";

        if ( isset($_REQUEST['ids']) && $_REQUEST['ids'] <> "" ) {
            $ids = $_REQUEST['ids'];
        }

        $ids_array = explode( ',', $ids );
        $knowhow = $this->know_how_options();
        $knowhow = $knowhow['nl'];

        $html = "";
        foreach( $ids_array as $key => $id ) {
            if( isset( $knowhow[$id] ) && $id != '') {
                $html .= '<div id="remove-know-how-'.$id.'"><a href="javascript:void(0)">'. $knowhow[$id] .'</a> <input type="button" class="button-delete btn-delete-know-how" know-how-id="'. $id .'" name="removeKnowHow-'. $id .'" value="X"></div>';
            } elseif ($id != '') {
                $html .= '<div id="remove-know-how-'.$id.'"><a href="javascript:void(0)">'. str_replace('-', ' ', $id) .'</a> <input type="button" class="button-delete btn-delete-know-how" know-how-id="'. $id .'" name="removeKnowHow-'. $id .'" value="X"></div>';
            }
        }
        echo $html;

        // Always die in functions echoing ajax content
        die();
    }

    function ajax_get_trainers_select_options() {

        global $pw_config;

        require_once('models/model-trainer.php');

        // Get the language the event is writen for
        if ( isset($_REQUEST['locale']) && $_REQUEST['locale'] <> "" ) {
            $locale = $this->make_safe_text( $_REQUEST['locale'], 3 );
        } else {
            $locale = strtoupper( $pw_config['lang_primary'] );
        }

        $trainers = new modelTrainer();
        $trainers_list = $trainers->get_by_locale_sorted( $locale );

        $data[] = array('id'=>'', 'name'=>__('- Make a choice -', 'pw-event-manager'));

        foreach( $trainers_list as $key => $trainer ) {
            $data[] = array('id'=>$trainer['id'], 'name'=> stripslashes( $trainer['trainer_name']) );
        }

        //return the resulting html
        echo( json_encode( $data ) );

        // Always die in functions echoing ajax content
        die();

    }

    function ajax_get_selected_trainers_html() {

        require_once('models/model-trainer.php');

        $ids = "";

        if ( isset($_REQUEST['ids']) && $_REQUEST['ids'] <> "" ) {
            $ids = $_REQUEST['ids'];
        }

        $ids_array = explode( ',', $ids );

        $trainers = new modelTrainer();

        $html = "";
        foreach( $ids_array as $key => $id ) {

            $rec = $trainers->get_by_id( (int) $id );

            if( isset( $rec['id'] ) && $rec['id'] == $id ) {

                $html .= '<a href="?page=pw-events-trainers&action=edit&id='.$rec['id'].'" target="_new">'.stripslashes( $rec['trainer_name'] ).'</a> '.
                         '<input type="button" class="button-delete btn-delete-trainer" trainer-id='.$rec['id'].' name="removeTrainer-'.$rec['id'].'" value="X"><br>';

            }
        }

        echo $html;

        // Always die in functions echoing ajax content
        die();

    }

    function ajax_populate_event_group_checkbox() {

        global $pw_config;

        require_once('models/model-group.php');
        require_once('models/model-event-group.php');

        if ( isset($_REQUEST['locale']) && $_REQUEST['locale'] <> "" ) {
            $locale = substr( $_REQUEST['locale'], 0, 3 );
        } else {
            $locale = strtoupper( $pw_config['lang_primary'] );
        }

        $groups_set = array();
        if ( isset($_REQUEST['ids']) && $_REQUEST['ids'] <> "" ) {
            $groups_set = explode(',', $_REQUEST['ids']);
        }

        $groups_model = new modelGroup();
        $groups = $groups_model->get_all_by_locale( $locale );

        $html = "";

        foreach( $groups as $key => $group ) {
            $catIsSelected = false;

            foreach( $groups_set as $cId ) {
                if( $group['id'] == $cId ) {
                    $catIsSelected = true;
                }
            }

            if( $catIsSelected == true ) {
                $html .= "<input type='checkbox' name='groups[]' value='".$group['id']."' checked />".stripslashes( $group['group_name'] )."<br>";
            } else {
                $html .= "<input type='checkbox' name='groups[]' value='".$group['id']."' />".stripslashes( $group['group_name'] )."<br>";
            }
        }

        echo $html;

        // Always die in functions echoing ajax content
        die();

    }


    function fe_ajax_get_event_names() {

        global $pw_config;

        require_once('models/model-event.php');

        if ( isset($_REQUEST['page-id']) && $_REQUEST['page-id'] <> "" ) {
            $post_id = (int) $_REQUEST['page-id'];
        }

        $data = array();
        $lang = strtolower( $pw_config['lang_primary'] );

        if( $post_id > 0 ) {
            if (has_filter('wpml_post_language_details')) {
                $lang_details = apply_filters('wpml_post_language_details', NULL, $post_id );
                $lang = explode('_', $lang_details['locale'])[0];
            }

            $events = new modelEvent();

            $events_list = $events->get_event_list_by_locale( $lang );

            $data = array();
            foreach( $events_list as $key => $event ) {
                $data[] = array('id'=>$event['id'], 'name'=>$event['name']);
            }
        }

        //return the resulting html
        echo( json_encode( $data ) );

        // Always die in functions echoing ajax content
        die();

    }

    //----------------------------------------------------------------
    //                  Render pages
    //----------------------------------------------------------------


    function render_event_list()
    {

        global $pw_config;

        require_once('views/view-event-list.php');
        require_once('views/view-event-edit.php');

        require_once('models/model-event.php');
        require_once('models/model-group.php');
        require_once('models/model-location.php');
        require_once('models/model-trainer.php');
        require_once('models/model-event-group.php');
        require_once('models/model-event-trainer.php');
        require_once('models/model-event-know-how.php');
        require_once('models/model-event-date.php');

        require_once('classes/class-event-table.php');
        require_once('classes/class-event-date-table.php');

        require_once('classes/class-user.php');
        require_once('classes/class-eventdatetime.php');
        require_once('classes/class-util.php');
        require_once('classes/class-dialog.php');

        $title = "Events";
        $errors = array();
        $mode = "";
        $id = 0;
        $originalImagePath = "";

        $evtTrainderMdl = new modelEventTrainer();
        $trainerMdl = new modelTrainer();
        $evtGroupMdl = new modelEventGroup();
        $evtKnowHowMdl = new modelEventKnowHow();
        $evtModel = new modelEvent();
        $evtDateMdl = new modelEventDate();
        $evtLocationMdl = new modelLocation();

        $locale = substr(get_locale(), 0, 2 );

        if( $locale == 'nl' ) {
            $dec_point = ',';
            $thousands_sep = '.';
        } else if( $locale == 'de' ) {
            $dec_point = ',';
            $thousands_sep = '.';
        } else {
            $dec_point = '.';
            $thousands_sep = ',';
        }

        /* General
         *      Name, type, status, etc
         *      SEO / HTML
         * Description + Assign trainers + Tags
         * Pricing
         *      Free / Payed
         *      Price   ->  Opgeven in de planning (dus per datum) of generiek op event niveau?
         *          Early bird
         *          Regular
         *          Last minute
         * Action codes (Discount)
         * Planning
         *      In company + Date / time + Other date-time for same training / apply status / special price / action code
         *      Location
         * Kortingcodes
         * Financieel
         *
         */

        //Determine the requested action
        if (isset($_GET['action']) && $_GET['action'] == 'new') {
            $mode = "new";

        } elseif (isset($_GET['action']) && $_GET['action'] == 'edit') {
            $mode = "edit";

            if (isset($_GET['id'])) {
                $id = (int)$_GET['id'];
            }
        } else {
             $mode = "list";
        }

        /*
        if ($mode == "list") {
            if (isset($_POST['action']) || isset($_POST['action2'])) {

                if ($_POST['action'] == "list_delete" || $_POST['action'] == "list_delete") {
                    foreach ($_POST['list'] as $key => $val) {
                        $evtModel->update((int)$val, array('removed' => '1'));
                    }
                }
            }
        }
        */

        if (isset($_GET['tab'] )) {
            $active_tab = substr( $this->make_safe_text($_GET['tab']), 0, 24 );
        } else {
            $active_tab = "description";
        }

        if( $active_tab == "planning" ) {
            if (isset($_POST['action']) || isset($_POST['action2'])) {

                if ($_POST['action'] == "list_delete_date" || $_POST['action'] == "list_delete_date") {
                    foreach ($_POST['list_dates'] as $key => $val) {
                        $evtDateMdl->update((int)$val, array('removed' => '1'));
                    }
                }
            }
        }

        //Initialize data
        $data['id'] = 0;
        $data['locale'] = strtoupper( $pw_config['lang_primary'] );
        $data['event_title'] = "";
        $data['event_type'] = "training";
        $data['attachment_path'] = "";
        $data['description'] = "";
        $data['teaser'] = "";
        $data['selected_trainer_ids'] = '';
        $data['selected_know_how_ids'] = '';
        $data['selected_group_ids'] = '';

        $data['price_normal'] = "";
        $data['price_last_minute'] = "";
        $data['price_early_bird'] = "";

        global $pw_config;

        // Set defaults from the config
        $data['notify_from'] = $pw_config['default_notify_from'];
        $data['notify_emails'] = $pw_config['default_internal_notify'];

        $data['subject_subscribed_ext'] = "";
        $data['mail_subscribed_ext'] = "";
        $data['subject_confirm_ext'] = "";
        $data['mail_confirm_ext'] = "";

        $data['html_title'] = "";
        $data['html_description'] = "";
        $data['html_keywords'] = "";
        $data['html_add_head'] = "";
        $data['html_add_footer'] = "";
        $data['url_slug'] = "";

        $data['duration'] = 1;
        $data['event_status'] = "concept";
        $data['updated'] = eventDateTime::current_time_mysql();
        $data['updated'] = eventDateTime::mysql_to_display($data['updated']); //Format for display
        $data['updated_by'] = substr(user::current_user_name(), 0, 64);

        //Get data from database
        if( $id > 0 ) {

            $event = $evtModel->get_by_id( (int) $id);

            $data['id'] = $id;
            $data['locale'] = $event['locale'];
            $data['event_title'] = $event['name'];
            $data['event_type'] = $event['type'];
            $data['attachment_path'] = $event['attachment_path'];
            $originalImagePath = $data['attachment_path'];

            $data['description'] = $event['description'];
            $data['teaser'] = $event['teaser'];

            //Get assigned trainers
            $trainers_array = $evtTrainderMdl->get_all_by_id( $id );

            $trainers_set = array();
            foreach( $trainers_array as $key=>$value ) {
                $trainers_set[] = $value['trainerID'];
            }
            $data['selected_trainer_ids'] =  implode( ',', $trainers_set);

            //Get assigned knowhows
            $know_how_array = $evtKnowHowMdl->get_all_by_id( $id );
            $know_how_set = array();
            foreach( $know_how_array as $key=>$value ) {
                $know_how_set[] = $value['know_how'];
            }
            $data['selected_know_how_ids'] =  implode( ',', $know_how_set);

            //Get assigned groups
            $groups = $evtGroupMdl->get_all_by_id( $id );

            $groups_set = "";
            if( empty($groups)) {
                $groups_set = "";
            } else {
                foreach( $groups as $key => $group ) {
                    $groups_set .= $groups_set == "" ? $group['groupID'] : ','.$group['groupID'];
                }
            }

            $data['selected_group_ids'] =  $groups_set;

            $data['price_normal'] = $event['price_normal'];
            $data['price_last_minute'] = $event['price_last_minute'];
            $data['price_early_bird'] = $event['price_early_bird'];

            $data['notify_from'] = $event['notify_from'];
            $data['notify_emails'] = $event['notify_emails'];

            $data['subject_subscribed_ext'] = $event['subject_subscribed_ext'];
            $data['mail_subscribed_ext'] = $event['mail_subscribed_ext'];
            $data['subject_confirm_ext'] = $event['subject_confirm_ext'];
            $data['mail_confirm_ext'] = $event['mail_confirm_ext'];

            $data['html_title'] = $event['html_title'];
            $data['html_description'] = $event['html_description'];
            $data['html_keywords'] = $event['html_keywords'];
            $data['html_add_head'] = $event['html_add_header'];
            $data['html_add_footer'] = $event['html_add_footer'];
            $data['url_slug'] = $event['url_slug'];

            $data['duration'] = $event['duration'] == '' ? 1 : $event['duration'];
            $data['event_status'] = $event['publication_status'];
            $data['updated'] = $event['updated'];
            $data['updated'] = eventDateTime::mysql_to_display($data['updated']); //Format for display
            $data['updated_by'] = $event['updated_by'];

        }

        //Update with data from $_POST
        if( isset( $_POST )) {

            $data['locale'] = isset( $_POST['locale'] ) ?  $this->make_safe_text( $_POST['locale'], 3 ) : $data['locale'];
            $data['event_title'] = isset( $_POST['event_title'] ) ?  $this->make_safe_text( $_POST['event_title'], 128 ) : $data['event_title'];

            $data['event_type'] = isset( $_POST['event_type'] ) ?  $this->make_safe_text( $_POST['event_type'], 12) : $data['event_type'];

            if( isset( $_POST['duration'] ) ) {
                $data['duration'] = str_replace( $thousands_sep, '', $this->make_safe_text( $_POST['duration'] ));
                $data['duration'] = str_replace( $dec_point, '.',  $data['duration'] );
            }

            $data['attachment_path'] = isset( $_POST['attachment_path'] ) ?  $this->make_safe_text( $_POST['attachment_path'], 255 ) : $data['attachment_path'];

            $description = isset( $_POST['description'] ) ?  $_POST['description'] : $data['description'];
            $description = str_replace("\n", "", $description);
            $description = str_replace("\r", "", $description);
            $description = str_replace("\r\n", "", $description);
            $data['description'] = $description;

            $data['teaser'] = isset( $_POST['teaser'] ) ?  $this->make_safe_text( $_POST['teaser'], 255) : $data['teaser'];

            $data['selected_trainer_ids'] = isset( $_POST['selected_trainer_ids'] ) ?  $_POST['selected_trainer_ids'] : $data['selected_trainer_ids'];

            if( ! empty($data['selected_trainer_ids'] ) && is_array($data['selected_trainer_ids']) ) {
                foreach($data['selected_trainer_ids'] as $key => $val ) {
                    $data['selected_trainer_ids'][$key] = (int) $val;
                }
            }

            $data['selected_know_how_ids'] = isset( $_POST['selected_know_how_ids'] ) ?  $_POST['selected_know_how_ids'] : $data['selected_know_how_ids'];

            if( ! empty($data['selected_know_how_ids'] ) && is_array($data['selected_know_how_ids']) ) {
                foreach($data['selected_know_how_ids'] as $key => $val ) {
                    $data['selected_know_how_ids'][$key] = (int) $val;
                }
            }

            if( !empty($data['selected_group_ids']) && is_array($data['selected_group_ids']) ) {
                foreach($data['selected_group_ids'] as $key => $val ) {
                    $data['selected_group_ids'][$key] = (int) $val;
                }
            }

            $data['selected_group_ids']= isset( $_POST['groups'] ) ?  implode(',', $_POST['groups'] ) : $data['selected_group_ids'];

            if( isset( $_POST['price_normal'] ) ) {
                $price = str_replace( $thousands_sep, '', $_POST['price_normal']);
                $price = str_replace( $dec_point, '.',  $price );
                if( is_numeric( $price ) ) {
                    $data['price_normal'] = $price;
                }
            }

            if( isset( $_POST['price_last_minute'] ) ) {
                $price = str_replace( $thousands_sep, '', $_POST['price_last_minute']);
                $price = str_replace( $dec_point, '.',  $price );
                if( is_numeric( $price ) ) {
                    $data['price_last_minute'] = $price;
                }
            }

            if( isset( $_POST['price_early_bird'] ) ) {
                $price = str_replace( $thousands_sep, '', $_POST['price_early_bird']);
                $price = str_replace( $dec_point, '.',  $price );
                if( is_numeric( $price ) ) {
                    $data['price_early_bird'] = $price;
                }
            }

            $data['notify_from'] = isset( $_POST['notify_from'] ) ?  $_POST['notify_from'] : $data['notify_from'];
            $data['notify_emails'] = isset( $_POST['notify_emails'] ) ? $_POST['notify_emails'] : $data['notify_emails'];

            $data['subject_subscribed_ext'] =  isset( $_POST['subject_subscribed_ext'] ) ?  $this->make_safe_text( $_POST['subject_subscribed_ext'] ) : $data['subject_subscribed_ext'];

            $data['mail_subscribed_ext'] =  isset( $_POST['mail_subscribed_ext'] ) ?  $_POST['mail_subscribed_ext'] : $data['mail_subscribed_ext'];
            $data['mail_subscribed_ext'] = str_replace("\n", "", $data['mail_subscribed_ext']);
            $data['mail_subscribed_ext'] = str_replace("\r", "", $data['mail_subscribed_ext']);
            $data['mail_subscribed_ext'] = str_replace("\r\n", "", $data['mail_subscribed_ext']);

            $data['subject_confirm_ext'] =  isset( $_POST['subject_confirm_ext'] ) ?  $this->make_safe_text( $_POST['subject_confirm_ext'] ) : $data['subject_confirm_ext'];

            $data['mail_confirm_ext'] =  isset( $_POST['mail_confirm_ext'] ) ?  $_POST['mail_confirm_ext'] : $data['mail_confirm_ext'];
            $data['mail_confirm_ext'] = str_replace("\n", "", $data['mail_confirm_ext']);
            $data['mail_confirm_ext'] = str_replace("\r", "", $data['mail_confirm_ext']);
            $data['mail_confirm_ext'] = str_replace("\r\n", "", $data['mail_confirm_ext']);

            $data['html_title'] = isset( $_POST['html_title'] ) ?  $this->make_safe_text( $_POST['html_title'] ) : $data['html_title'];
            $data['html_description'] = isset( $_POST['html_description'] ) ?  $this->make_safe_text( $_POST['html_description'] ) : $data['html_description'];
            $data['html_keywords'] = isset( $_POST['html_keywords'] ) ?  $this->make_safe_text( $_POST['html_keywords'] ) : $data['html_keywords'];
            $data['html_add_head'] = isset( $_POST['html_add_head'] ) ?  $_POST['html_add_head'] : $data['html_add_head'];
            $data['html_add_footer'] = isset( $_POST['html_add_footer'] ) ?  $_POST['html_add_footer'] : $data['html_add_footer'];
            $data['url_slug'] = isset( $_POST['url_slug'] ) ?  $this->make_safe_text( $_POST['url_slug'] ) : $data['url_slug'];

            $data['event_status'] = $data['event_status'] = isset( $_POST['event_status'] ) ?  $this->make_safe_text( $_POST['event_status'] ) : $data['event_status'];

        }

        //Handle the removal of the profile picture
        if (isset($_POST['removePictureTraining'])) {

            //If ID is tampered with, the image path will be different
            if ($originalImagePath == $_POST['attachment_path']) {

                util::delete_img($_POST['attachment_path']);

                if ($mode == "edit") {
                    $evtModel->update($id, array('attachment_path' => ''));
                }

                $data['attachment_path'] = "";
            }

        }

        $hasError = false;

        if( isset($_POST) ) {

            //Close without save requested
            if( isset($_POST['closeEvent'])) {
                $returnurl = "?page=pw-events";
                echo('<meta http-equiv="refresh" content="0; url=' . $returnurl . '">');
                exit();
            }

            if( isset($_POST['submitDescription']) || isset($_POST['submitAndCloseDescription']) ) {
                if( $data['event_title'] == "" ) {
                    $errors[] = __('Event title is required', 'pw-event-manager');
                    $hasError = true;
                }

                if( $data['duration'] == "" ) {
                    $errors[] = __("Duration is required", 'pw-event-manager');
                    $hasError = true;
                }
            }

            //Upload attached image
            if (isset($_FILES['training_image']['name']) && $_FILES['training_image']['name'] <> "") {

                $file = $_FILES['training_image'];
                $path = util::upload_img( $file, "events" );

                if ($path == "") {

                    // There was an error uploading the image.
                    $error['image'][] = __('Upload of the image failed', 'pw-event-manager');
                    $hasError = true;

                } else {
                    $data['attachment_path'] = $path;
                }

            }

            if( isset($_POST['submitSetup']) || isset($_POST['submitAndCloseSetup']) ) {
                //TODO Valide the emails for correct formatting
                if( $data['notify_from'] == "" ) {
                    $errors[] = "__(, 'pw-event-manager');From: Sender of the mail is required";
                    $hasError = true;
                }
                if( $data['notify_emails'] == "" ) {
                    $errors[] = __("Notify internal: At least one email to notify is required", 'pw-event-manager');
                    $hasError = true;
                }
                if( $data['subject_subscribed_ext'] == "" ) {
                    $errors[] = __("Mail from the website: Subject of the mail is required", 'pw-event-manager');
                    $hasError = true;
                }
                if( $data['mail_subscribed_ext'] == "" ) {
                    $errors[] = __("Mail from the website: Content of the mail is required", 'pw-event-manager');
                    $hasError = true;
                }
                if( $data['subject_confirm_ext'] == "" ) {
                    $errors[] = __("Confirmation mail: Subject of the mail is required", 'pw-event-manager');
                    $hasError = true;
                }
                if( $data['mail_confirm_ext'] == "" ) {
                    $errors[] = __("Confirmation mail: Content of the mail is required", 'pw-event-manager');
                    $hasError = true;
                }
            }

            $validPost = false;
            if( isset($_POST['submitDescription']) || isset($_POST['submitAndCloseDescription']) ||
                isset($_POST['submitHTML']) || isset($_POST['submitAndCloseHTML']) ||
                isset($_POST['submitPricing']) || isset($_POST['submitAndClosePricing']) ||
                isset($_POST['submitSetup']) || isset($_POST['submitAndCloseSetup'])) {
                $validPost = true;
            }

            $returnToIdx = false;
            if( isset($_POST['submitAndCloseDescription']) || isset($_POST['submitAndCloseHTML']) ||
                isset($_POST['submitAndClosePricing']) || isset($_POST['submitAndCloseSetup']) ) {
                $returnToIdx = true;
            }

            if( $hasError === false && $validPost ) {

                //Make sure the slug is unique for the selected locale
                if( $data['event_title'] <> "" ) {

                    if( $data['url_slug'] == "" ) {
                        $data['url_slug'] = util::generate_slug( $data['event_title'] );
                    }

                    //Enforce uniqueness of the slug
                    $n = 2;

                    $new_slug = $data['url_slug'];
                    while( $evtModel->slug_exists( $new_slug, $data['locale'], $data['id']  ) ) {
                        $new_slug = $data['url_slug'].'-'.$n;
                        $n++;
                        if( $n > 100) {
                            break;
                        }
                    }

                    $data['url_slug'] = $new_slug;
                }

                $item['name'] = sanitize_text_field( $data['event_title'] );
                $item['locale'] = sanitize_text_field( $data['locale'] );
                $item['duration'] = $data['duration'];
                $item['type'] = sanitize_text_field( $data['event_type'] );
                $item['publication_status'] = sanitize_text_field( $data['event_status'] );
                $item['attachment_path'] = sanitize_text_field( $data['attachment_path'] );

                $item['url_slug'] = sanitize_text_field( $data['url_slug'] );
                $item['description'] = $data['description'];
                $item['teaser'] = $data['teaser'];

                //TODO make sure formatting is ok
                $item['price_normal'] = $data['price_normal'];
                $item['price_last_minute'] = $data['price_last_minute'];
                $item['price_early_bird'] = $data['price_early_bird'];

                $item['notify_from'] = $data['notify_from'];
                $item['notify_emails'] = $data['notify_emails'];

                $item['subject_subscribed_ext'] = $data['subject_subscribed_ext'];
                $item['mail_subscribed_ext'] = $data['mail_subscribed_ext'];
                $item['subject_confirm_ext'] = $data['subject_confirm_ext'];
                $item['mail_confirm_ext'] = $data['mail_confirm_ext'];

                $item['html_title'] = sanitize_text_field( $data['html_title'] );
                $item['html_description'] = sanitize_text_field( $data['html_description'] );
                $item['html_keywords'] = sanitize_text_field( $data['html_keywords'] );
                $item['html_add_header'] = $data['html_add_head'];
                $item['html_add_footer'] = $data['html_add_footer'];
                $item['updated'] = eventDateTime::current_time_mysql();
                $item['updated_by'] = substr(user::current_user_name(), 0, 64);
                $item['removed'] = 0;
                $item['ip'] = util::get_user_ip();

                if ( $mode == 'new' ) {

                    $insert_id = $evtModel->insert( $item );

                    if( $insert_id <= 0 ) {
                        $errors[] = "Failed to save event";
                        $hasError = false;
                    } else {

                        //Store trainers for this event
                        $trainer_array = explode(',', $data['selected_trainer_ids']);
                        $evtTrainderMdl->set_from_array( $insert_id, $trainer_array );

                        //Store groups for this event
                        $group_array = explode(',', $data['selected_group_ids']);
                        $evtGroupMdl->set_from_array( $insert_id, $group_array );

                        //Store know hows for this event
                        $know_how_array = explode(',', $data['selected_know_how_ids']);
                        $evtKnowHowMdl->set_from_array( $insert_id, $know_how_array );

                        //Switch to edit mode or index
                        if( $returnToIdx ) {
                            $returnurl = "?page=pw-events";
                        } else {
                            $returnurl = "?page=pw-events&action=edit&id=".$insert_id."&tab=".$active_tab;
                        }

                        echo('<meta http-equiv="refresh" content="0; url=' . $returnurl . '">');
                        exit();
                    }

                } else if ( $mode == 'edit' ) {

                    $evtModel->update($id, $item);

                    //Store trainers for this event
                    $trainer_array = explode(',', $data['selected_trainer_ids']);
                    $evtTrainderMdl->set_from_array( $id, $trainer_array );

                    //Store groups for this event
                    $group_array = explode(',', $data['selected_group_ids']);
                    $evtGroupMdl->set_from_array( $id, $group_array );

                    //Store know hows for this event
                    $know_how_array = explode(',', $data['selected_know_how_ids']);
                    $evtKnowHowMdl->set_from_array( $id, $know_how_array );


                    //Switch to edit mode or index
                    if( $returnToIdx ) {
                        $returnurl = "?page=pw-events";
                    } else {
                        $returnurl = "?page=pw-events&action=edit&id=".$id."&tab=".$active_tab;
                    }

                    echo('<meta http-equiv="refresh" content="0; url=' . $returnurl . '">');
                    exit();
                }
            }

        }

        $data['errors'] = $errors;

        if ( $mode == 'new' || $mode == 'edit' ) {

            $data['dialog_html'] = dialog::yes_no_dialog_html('pw-confirm-save',
                __('Unsaved changes', 'pw-event-manager'),
                __('You have unsave changes.<br><br>Do you want to save these changes?', 'pw-event-manager'),
                __('Yes', 'pw-event-manager'),
                __('No', 'pw-event-manager'));
            /*
            $know_options[1] = 'Via Google';
            $know_options[2] = 'Via de website';
            $know_options[3] = 'Via BNR';
            $know_options[4] = 'Via de nieuwsbrief';
            $know_options[5] = 'Via Twitter';
            $know_options[6] = 'Via Linkedin';
            $know_options[7] = 'Via een Prowareness medewerker';
            $know_options[8] = "Via collega's";
            $know_options[9] = 'Via een Prowareness event';
            $know_options[10] = 'Via een Prowareness training';
            $know_options[11] = 'Via een conferentie';
            $know_options[12] = 'Via Scrum.org';
            $know_options[13] = 'Anders...';
            */

            if( $mode == 'new' ) {
                $title = "New event";
            } else {

                $title = "Edit " . $data['event_title'];
            }

            $event_dates_table = null;

            if( $active_tab == 'planning' ) {

                $event_dates_table = new eventDateTable();
                $date_list = $evtDateMdl->get_all_by_event_id( $id );

                //Add name of the location to each record
                foreach( $date_list as $key => $value ) {
                    $loc = $evtLocationMdl->get_by_id($value['locationID']);
                    $date_list[$key]['location_name'] = $loc['location_name'];
                }

                $event_dates_table->set_table_data($date_list);
                $event_dates_table->prepare_items();

            }

            $content = eventEditHtmlView::render( __($title, 'pw-event-manager'), $data, $mode, $active_tab, $event_dates_table);

        } else {
            global $pw;

            $events = new modelEvent();
            $event_table = new eventTable();

            $data = $events->get_all();

            //Enrich with assoicated trainers
            foreach( $data as $key => $value ) {
                $t = $evtTrainderMdl->get_all_by_id( $value['id']);

                $trainer_names = array();
                foreach( $t as $k=>$v ) {
                    $trainer = $trainerMdl->get_by_id($v['trainerID']);
                    if( isset($trainer['trainer_name']) && $trainer['trainer_name'] <> '' ) {
                        $trainer_names[] = $trainer['trainer_name'];
                    }
                }

                $data[$key]['trainers'] = $trainer_names;
				
				// enrich with nr of particpants
				// $data[$key]['parts'] = 5;
            }

            $event_table->set_table_data($data);
            $event_table->prepare_items();

            $title = __('Events', 'pw-event-manager');

            $content = eventListHtmlView::render( $title, $event_table );
        }

        echo $content;
    }

    function render_event_date() {

        require_once('views/view-event-date-edit.php');

        require_once('models/model-event.php');
        require_once('models/model-event-date.php');

        require_once('classes/class-user.php');
        require_once('classes/class-eventdatetime.php');
        require_once('classes/class-util.php');

        $content = "";
        $error = array();
        $hasError = false;
        $returnurl = "";
        $id = 0;
        $mdlEvent = new modelEvent();
        $mdlEventDate = new modelEventDate();

        global $pw_config;

        $locale = substr(get_locale(), 0, 2 );
        $date_format = $pw_config['date_format'][$locale];

        if( $locale == 'nl' ) {
            $dec_point = ',';
            $thousands_sep = '.';
        } else if( $locale == 'de' ) {
            $dec_point = ',';
            $thousands_sep = '.';
        } else {
            $dec_point = '.';
            $thousands_sep = ',';
        }

        //Determine the requested action
        if (isset($_GET['action']) && $_GET['action'] == 'new') {
            $mode = "new";

        } elseif (isset($_GET['action']) && $_GET['action'] == 'edit') {
            $mode = "edit";

            if (isset($_GET['id'])) {
                $id = (int)$_GET['id'];
            }
        }

        $event_id = 0;
        if (isset($_GET['eventId'])) {
            $event_id = (int)$_GET['eventId'];
        }

        $event_date_id = 0;
        if (isset($_GET['id'])) {
            $event_date_id = (int)$_GET['id'];
        }

        //Fill the data array with the appropriate data
        $data = array();

        //Initialize Data

        if ($mode == "new") {

            $data['eventID'] = $event_id;

            $event = $mdlEvent->get_by_id($event_id);

            if( ! is_null($event) && isset( $event['name'] )) {
                $data['event_name'] = $event['name'];
                $data['locale'] = $event['locale'];
            } else {
                $data['event_name'] = "";
                $data['locale'] = strtoupper( $pw_config['lang_primary'] );
            }

            $data['locationID'] = 0;
            $data['start_date'] = "";
            $data['start_time'] = "";
            $data['stop_time'] = "";
            $data['event_threshold'] = "";
            $data['other_dates'] = "";
            $data['alt_price_normal'] = "";
            $data['alt_price_last_minute'] = "";
            $data['alt_price_early_bird'] = "";
            $data['status'] = 'open';
            $data['removed'] = 0;
            $data['updated'] = eventDateTime::current_time_mysql();
            $data['updated'] = eventDateTime::mysql_to_display($data['updated']); //Format for display
            $data['updated_by'] = substr(user::current_user_name(), 0, 64);

        } elseif ($mode == "edit") {

            $item = $mdlEventDate->get_by_key_id($event_date_id); //Get record by event date ID

            if( ! is_null($item) && isset( $item['start_date'] )) {
                $event_id = $item['eventID'];
                $data['eventID'] = $item['eventID'];

                $event = $mdlEvent->get_by_id($item['eventID']);

                if( ! is_null($event) && isset( $event['name'] )) {
                    $data['event_name'] = $event['name'];
                    $data['locale'] = $event['locale'];
                } else {
                    $data['event_name'] = "";
                    $data['locale'] = strtoupper( $pw_config['lang_primary'] );
                }

                $data['locationID'] = $item['locationID'];
                $data['start_date'] = $item['start_date'];
                $data['start_time'] = $item['start_time'];
                $data['stop_time'] = $item['stop_time'];
                $data['other_dates'] = $item['other_dates'];
                $data['event_threshold'] = $item['threshold'];
                $data['alt_price_normal'] = $item['alt_price_normal'];
                $data['alt_price_last_minute'] = $item['alt_price_last_minute'];
                $data['alt_price_early_bird'] = $item['alt_price_early_bird'];
                $data['status'] = $item['status'];
                $data['updated'] = eventDateTime::current_time_mysql();
                $data['updated'] = eventDateTime::mysql_to_display($data['updated']); //Format for display
                $data['updated_by'] = substr(user::current_user_name(), 0, 64);
            }
        }

                    if( isset($_POST['submitEventDate']) || isset($_POST['submitAndCloseEventDate']) ) {

                        $data['locationID'] = (int) $_POST['date_location'];
                        $data['start_date'] = eventDateTime::to_mysql_time( $date_format, substr( $_POST['start_date'], 0, 10) );
                        $data['start_time'] = substr( $_POST['start_time'], 0, 5 );
                        $data['stop_time'] = substr( $_POST['stop_time'], 0, 5);

                        $other_dates = "";

                        $data['event_threshold'] = $_POST['event_threshold'];
                        if( $_POST['other_dates'] <> '' ) {

                            $dtArray = explode( ',', $this->make_safe_text( $_POST['other_dates'] ));

                            foreach( $dtArray as $key => $value ) {
                                $dt = eventDateTime::to_mysql_date( $date_format, trim( $value ) );
                                $dtArray[$key] = $dt;
                            }
                            $other_dates = implode(', ', $dtArray);
                        }

                        $data['other_dates'] = $other_dates;

                        if( isset( $_POST['alt_price_normal'] ) ) {
                            $price = str_replace( $thousands_sep, '', $_POST['alt_price_normal']);
                            $price = str_replace( $dec_point, '.',  $price );
                            if( is_numeric( $price ) ) {
                                $data['alt_price_normal'] = $price;
                            }
                        }

                        if( isset( $_POST['alt_price_last_minute'] ) ) {
                            $price = str_replace( $thousands_sep, '', $_POST['alt_price_last_minute']);
                            $price = str_replace( $dec_point, '.',  $price );
                            if( is_numeric( $price ) ) {
                                $data['alt_price_last_minute'] = $price;
                            }
                        }

                        if( isset( $_POST['alt_price_early_bird'] ) ) {
                            $price = str_replace( $thousands_sep, '', $_POST['alt_price_early_bird']);
                            $price = str_replace( $dec_point, '.',  $price );
                            if( is_numeric( $price ) ) {
                                $data['alt_price_early_bird'] = $price;
                            }
                        }

                        $data['status'] = $this->make_safe_text( $_POST['status'] );
                        $data['updated'] = eventDateTime::current_time_mysql();
                        $data['updated'] = eventDateTime::mysql_to_display($data['updated']); //Format for display
                        $data['updated_by'] = substr(user::current_user_name(), 0, 64);

                    }

        //Do the magic
        if (isset($_POST['closeEventDate'])) {

            //Go back to the index
            $returnurl = "?page=pw-events&action=edit&id=".$event_id."&tab=planning";
            echo('<meta http-equiv="refresh" content="0; url=' . $returnurl . '">');
            exit();

        } elseif (isset($_POST['submitEventDate']) || isset($_POST['submitAndCloseEventDate'])) {

            //TODO add additional validations

            //check for errors
            if ($data['start_date'] == "") {
                $error['start_date'][] = __('Start date is required', 'pw-event-manager');
                $hasError = true;
            }

            if ($data['start_time'] == "") {
                $error['start_time'][] = __('Start time is required', 'pw-event-manager');
                $hasError = true;
            }

            if ($data['stop_time'] == "") {
                $error['stop_time'][] = __('Stop time is required', 'pw-event-manager');
                $hasError = true;
            }

            if ( (int) $data['locationID'] == 0) {
                $error['date_location'][] = __('Location is required', 'pw-event-manager');
                $hasError = true;
            }

            if( $data['event_threshold'] == "" ) {
                $error['event_threshold'][] = __('No Of Participants is required', 'pw-event-manager');
                $hasError = true;
            }

            $data['errors'] = $error;

            if( $hasError == false ) {

                //Prepare record for insert or update
                $rec['eventID'] = $data['eventID'];
                $rec['locationID'] = $data['locationID'];
                $rec['start_date'] = $data['start_date'];
                $rec['start_time'] = $data['start_time'];
                $rec['stop_time'] = $data['stop_time'];
                $rec['threshold'] = $data['event_threshold'];
                $rec['other_dates'] = $data['other_dates'];
                $rec['alt_price_normal'] = $data['alt_price_normal'];
                $rec['alt_price_last_minute'] = $data['alt_price_last_minute'];
                $rec['alt_price_early_bird'] = $data['alt_price_early_bird'];
                $rec['status'] = $data['status'];
                $rec['updated'] = eventDateTime::current_time_mysql();
                $rec['updated_by'] = substr(user::current_user_name(), 0, 64);
                $rec['removed'] = 0;
                $rec['ip'] = util::get_user_ip();

                if ($mode == "new") {
                    //Insert record
                    $id = $mdlEventDate->insert($rec);
                } elseif ($mode == "edit") {
                    //update record
                    $mdlEventDate->update($id, $rec);
                }

                if (isset($_POST['submitAndCloseEventDate'])) {
                    //Go back to the event
                    $returnurl = "?page=pw-events&action=edit&id=".$event_id."&tab=planning";
                    echo('<meta http-equiv="refresh" content="0; url=' . $returnurl . '">');
                    exit();
                } else {
                    //Change into edit mode
                    $returnurl = "?page=pw-event-date&action=edit&eventId=".$event_id."&id=" . $id;
                    echo('<meta http-equiv="refresh" content="0; url=' . $returnurl . '">');
                    exit();
                }
            }

        }

        if( $data['event_name'] <> "" ) {
            if ($mode == 'new') {

                $content = eventDateEditHtmlView::render( __('New date', 'pw-event-manager'), $data);
            } elseif ($mode == 'edit') {

                $content = eventDateEditHtmlView::render( __('Edit date', 'pw-event-manager'), $data);
            }
        } else {
            $content = '<P>Error: Event not found</P>';
        }

        echo $content;
    }

    function render_trainer_list()
    {
        global $pw_config;

        require_once('views/view-trainer-list.php');
        require_once('views/view-trainer-edit.php');
        require_once('models/model-trainer.php');
        require_once('classes/class-trainer-table.php');
        require_once('classes/class-user.php');
        require_once('classes/class-eventdatetime.php');
        require_once('classes/class-util.php');

        $content = "";
        $error = array();
        $hasError = false;
        $returnurl = "?page=pw-events-trainers";
        $id = 0;
        $originalImagePath = "";
        $mode = "";
        $model = new modelTrainer();

        //Determine the requested action
        if (isset($_GET['action']) && $_GET['action'] == 'new') {
            $mode = "new";

        } elseif (isset($_GET['action']) && $_GET['action'] == 'edit') {
            $mode = "edit";

            if (isset($_GET['id'])) {
                $id = (int)$_GET['id'];
            }

        } else {
            $mode = "list";
        }

        // Mark selected items as removed. This will move them to the admin trash
        // from where they can be restored when required.
        if ($mode == "list") {
            if (isset($_POST['action']) || isset($_POST['action2'])) {

                if ($_POST['action'] == "list_delete" || $_POST['action'] == "list_delete") {
                    foreach ($_POST['list'] as $key => $val) {
                        $model->update((int)$val, array('removed' => '1'));
                    }
                }
            }
        }

        //Fill the data array with the appropriate data
        $data = array();

        //Initialize Data
        if ($mode == "new") {

            $data['locale'] = strtoupper( $pw_config['lang_primary'] );
            $data['name'] = "";
            $data['title'] = "";
            $data['company'] = "Prowareness";
            $data['profile'] = "";
            $data['attachment_path'] = "";
            $data['updated'] = eventDateTime::current_time_mysql();
            $data['updated'] = eventDateTime::mysql_to_display($data['updated']); //Format for display
            $data['updated_by'] = substr(user::current_user_name(), 0, 64);

        } elseif ($mode == "edit") {

            $item = $model->get_by_id($id); //Get record by ID

            if( $item <> '' ) {
                $data['locale'] = $item['locale'];
                $data['name'] = $item['trainer_name'];
                $data['company'] = $item['company'];
                $data['title'] = $item['function_title'];
                $data['profile'] = $item['profile'];
                $data['attachment_path'] = isset( $item['attachment_path'] ) ? $item['attachment_path'] : '' ;
                $originalImagePath = $data['attachment_path'];
                $data['updated'] = $item['updated'];
                $data['updated_by'] = $item['updated_by'];
                $data['ip'] = $item['ip'];
            } else {
                die('<p>Invalid record</p>');
            }

        }

        //Override default data with $_POST when applicable
        if (isset($_POST['submitTrainer']) || isset($_POST['submitAndCloseTrainer']) || isset($_POST['removePictureTrainer'])) {

            $data['name'] = $this->make_safe_text( $_POST['name'], 64 );
            $data['locale'] = $this->make_safe_text( $_POST['locale'], 3 );
            $data['title'] = $this->make_safe_text( $_POST['title'], 64 );
            $data['company'] = $this->make_safe_text( $_POST['company'], 64 );
            $data['profile'] = $_POST['profile']; // Can contain HTML
            $data['attachment_path'] = $this->make_safe_text( $_POST['attachment_path'], 255 );
            $data['updated'] = eventDateTime::current_time_mysql();
            $data['updated'] = eventDateTime::mysql_to_display($data['updated']); //Format for display
            $data['updated_by'] = substr(user::current_user_name(), 0, 64);

        }

        //Handle the removal of the profile picture
        if (isset($_POST['removePictureTrainer'])) {

            //If path is tampered with, the posted path will be different from the original
            $path = $this->make_safe_text( $_POST['attachment_path'], 255);
            if ($originalImagePath == $path) {

                util::delete_img($_POST['attachment_path']);

                if ($mode == "edit") {
                    $model->update($id, array('attachment_path' => ''));
                }

                $data['attachment_path'] = "";
            }

        }

        //Do the magic
        if (isset($_POST['closeTrainer'])) {

            //Go back to the index
            echo('<meta http-equiv="refresh" content="0; url=' . $returnurl . '">');
            exit();

        } elseif (isset($_POST['submitTrainer']) || isset($_POST['submitAndCloseTrainer'])) {
            //TODO add nonce to form and check it before accepting the submit

            if ($data['name'] == "") {
                $error['name'][] = __('Name is required', 'pw-event-manager');
                $hasError = true;
            }
            if ($data['profile'] == "") {
                $error['profile'][] = __('Profile is required', 'pw-event-manager');
                $hasError = true;
            }

            //Check for unique trainer name
            if ($mode == "new") {
                $results = $model->get_by_name_locale($data['name'], $data['locale']);

                if (isset($results[0]['id'])) {
                    $error['name'][] = __('Name must be unique for the selected display language', 'pw-event-manager');
                    $hasError = true;
                }
            }

            //Upload attached image
            if (isset($_FILES['trainer_image']['name']) && $_FILES['trainer_image']['name'] <> "") {

                $file = $_FILES['trainer_image'];
                $path = util::upload_img( $file, "trainers" );

                if ($path == "") {

                    // There was an error uploading the image.
                    $error['image'][] = __('Upload of the image failed', 'pw-event-manager');
                    $hasError = true;

                } else {
                    $data['attachment_path'] = $path;
                }

            }

            $data['errors'] = $error;

            if ($hasError === false) {

                //Prepare record for insert or update
                $rec['trainer_name'] = substr($data['name'], 0, 64 );
                $rec['locale'] = substr($data['locale'], 0, 3);
                $rec['company'] = substr($data['company'], 0, 64);
                $rec['function_title'] = substr($data['title'], 0, 64);
                $rec['profile'] = $data['profile'];
                $rec['attachment_path'] = $data['attachment_path'];
                $rec['updated'] = eventDateTime::current_time_mysql();
                $rec['updated_by'] = substr(user::current_user_name(), 0, 64);
                $rec['ip'] = util::get_user_ip();

                //Insert record
                if ($mode == "new") {
                    $id = $model->insert($rec);
                } elseif ($mode == "edit") {
                    //update record
                    $model->update($id, $rec);
                }

                if (isset($_POST['submitAndCloseTrainer'])) {
                    //Go back to the index
                    echo('<meta http-equiv="refresh" content="0; url=' . $returnurl . '">');
                    exit();
                } else {
                    //Change into edit mode
                    $returnurl .= '&action=edit&id=' . $id;

                    echo('<meta http-equiv="refresh" content="0; url=' . $returnurl . '">');
                    exit();
                }

            } else {
                $content = trainerEditHtmlView::render( __('New trainer', 'pw-event-manager'), $data);
            }

        } elseif ($mode == 'new') {

            $content = trainerEditHtmlView::render( __('New trainer', 'pw-event-manager'), $data);

        } elseif ($mode == 'edit') {

            $content = trainerEditHtmlView::render( __('Edit trainer', 'pw-event-manager'), $data);
        } else {

            $trainers = new modelTrainer();
            $trainer_table = new trainerTable();

            $data = $trainers->get_all();
            $trainer_table->set_table_data($data);
            $trainer_table->prepare_items();

            $title = __('Trainers', 'pw-event-manager');

            $content = trainerListHtmlView::render($title, $trainer_table);

        }

        return $content;
    }

    function render_subscriber_list() {

        global $pw_config;

        require_once('classes/class-subscriber-table.php');

        require_once('models/model-subscription.php');
        require_once('models/model-event.php');
        require_once('models/model-event-date.php');
        require_once('models/model-location.php');
        require_once('models/model-participant.php');

        require_once('classes/class-eventdatetime.php');
        require_once('classes/class-user.php');
        require_once('classes/class-mail.php');
        require_once('classes/class-export.php');

        require_once('views/view-subscriber-edit.php');
        require_once('views/view-subscriber-move.php');
        require_once('views/view-subscribers-list.php');
        require_once('views/view-subscribers-download.php');
        require_once('views/view-subscriber-select-period.php');

        require_once('models/model-invoice.php');

        $subscription_mdl = new modelSubscription();
        $event_mdl = new modelEvent();
        $event_date_mdl = new modelEventDate();
        $location_mdl = new modelLocation();
        $participant_mdl = new modelParticipant();
        $invoice_mdl = new modelInvoice();

        $mode = "list";
        $content = '';

        $id = 0;
        $filter_search = "";
        $filter_event_id = 0;
        $filter_event_date_id = 0;
        $filter_location_id = 0;

        if (isset($_GET['s'])) {
            $filter_search = $_GET['s'];
            $mode = 'list-filtered';
        }

        if (isset($_GET['fe'])) {
            $filter_event_id = (int)$_GET['fe'];
            $mode = 'list-filtered';
        }

        // Only show dates when an event is selected
        if( $filter_event_id > 0 ) {
            if (isset($_GET['fed'])) {
                $filter_event_date_id = (int)$_GET['fed'];
                $mode = 'list-filtered';
            }
        }

        if (isset($_GET['fl'])) {
            $filter_location_id = (int)$_GET['fl'];
            $mode = 'list-filtered';
        }

        // No filtering active, switch to default list mode
        if( $filter_search == "" && $filter_event_id == 0 && $filter_event_date_id == 0 && $filter_location_id == 0 ) {
            $mode = 'list';
        }

        if (isset($_GET['id'])) {
            $id = (int)$_GET['id'];
            $mode = 'edit';
        }

        //Export of nr subscribers per event report
        if( isset( $_POST['closeSubscriberSelectPeriod'])) {
            $mode = "list";
        } else if( isset( $_POST['submitSubscriberSelectPeriod'])) {
            if( $_POST['requested_action'] == "total_report" ) {
                $mode = "total_report";
            } else {
                $mode = 'list';
            }
        }

        if ($_POST['action'] == "list_export" || $_POST['action2'] == "list_export") {
            $mode = "list_export";
        }
        // Get all participants above the concept state

        if( $mode == 'list') {
            $results = $participant_mdl->get_all_not_concept();
            $subscribers = $results['list'];
        } else if( $mode == 'list_export') {
            $subscribers = $participant_mdl->get_all_export();
        } else if( $mode == 'list-filtered') {
            // Get Filtered
            if( $filter_event_id == 0 && $filter_event_date_id == 0 && $filter_location_id == 0 ) {
                $results = $participant_mdl->get_all_not_concept();
                $subscribers = $results['list'];

            } else {
                $results = $participant_mdl->get_all_filtered( $filter_event_id, $filter_event_date_id, $filter_location_id);
                $subscribers = $results['list'];

            }

        } else if( $mode == 'total_report') {
            $subscribers = array();
        } else {
            $subscribers[] = $participant_mdl->get_by_id( $id );
        }

        // Do some cleanup of old exports.
        $dir = WP_CONTENT_DIR . '/uploads/export/';

        foreach (glob($dir."*.csv") as $file) {

            // If file is 1 hour (3600 seconds) old then delete it
            if (filemtime($file) < ( time() - 3600 )) {
                unlink($file);
            }
        }

        foreach( $subscribers as $key => $rec) {

            $subscription = $subscription_mdl->get_by_id( $rec['subscriptionID'] );
            $subscribers[$key]['company'] = $subscription['company'];

            // Get the event details for each subscription
            $event = $event_mdl->get_by_id( $rec['eventID'] );
            $event_date = $event_date_mdl->get_by_key_id( $rec['eventDateID'] );

            $subscribers[$key]['event_details']['name'] = $event['name'];
            $subscribers[$key]['event_details']['date'] = $event_date['start_date'];
            $subscribers[$key]['event_details']['start_time'] = $event_date['start_time'];
            $subscribers[$key]['event_details']['stop_time'] = $event_date['stop_time'];

            $location = $location_mdl->get_by_id( $event_date['locationID'] );

            $subscribers[$key]['event_details']['location_name'] = $location['location_name'];

            // Count the nr of confirmed subscribers for each subscription
            $confirmed_participants = $participant_mdl->get_all_confirmed( $rec['eventID'], $rec['eventDateID'], $event_date['locationID'] );
            $subscribers[$key]['nr_confirmed_participants'] = count( $confirmed_participants );

        }

        if ($mode == "list" || $mode == "list-filtered" || $mode == "list_export") {
            if (isset($_POST['action']) || isset($_POST['action2'])) {

                if ($_POST['action'] == "list_export" || $_POST['action2'] == "list_export") {

                    // Write subscribers to file
                    // Important: In php-export-data.class.php the default CSV separator ( a comma ) is replaced with
                    // a ; . Now Excel opens the file with the data in columns.
                    $file = export::export_subscribers($subscribers);

                    // Make the file downloadable
                    chmod ( $file['dir'].'/'.$file['file'], 0644 );

                    // Offer file for download
                    $title = __('Download subscribers', 'pw-event-manager');

                    $upload_dir = wp_upload_dir();
                    $data['file_url'] = $upload_dir['baseurl'].'/export/'.$file['file'];

                    subscribersDownloadHtmlView::render( $title, $data );
                    exit();
                } else if ($_POST['action'] == "total_report" || $_POST['action2'] == "total_report") {
                    $data['selected_action'] = "total_report";
                    $title = __('Nr subscribers per event', 'pw-event-manager');

                    subscriberSelectPeriodHtmlView::render($title, $data);
                    exit();
                }
            }
        }

        //Loop the result set and apply search string
        if(  $mode == 'list-filtered' && $filter_search <> "" ) {

            $tmp_sub = array();
            foreach( $subscribers as $k => $v ) {

                $txt = strtolower( str_replace("  ", " ",
                    stripslashes( $v['first_name'] ).' '.
                    stripslashes($v['middle_name']).' '.
                    stripslashes($v['last_name']).' '.
                    $v['email'].' '.
                    $v['phone'].' '.
                    stripslashes($v['company']).' '
                ));

                if( strpos($txt, strtolower( $filter_search )) !== false ) {

                    $tmp_sub[] = $v;
                }
            }

            //$subscribers = $tmp_sub;
        }

        if( $mode == 'list') {
            $subscriber_table = new subscriberTable();



            $subscriber_table->set_table_data($subscribers, $results['count']);
            $subscriber_table->prepare_items(); // Paginated

            $title = __('Subscribers', 'pw-event-manager');

            $content = subscribersListHtmlView::render($title, $subscriber_table);

        } else if( $mode == 'list-filtered') {


            if( $filter_event_id == 0 && $filter_event_date_id == 0 && $filter_location_id == 0 ) {
                //$results['count'] = count($subscribers);
            }


            $subscriber_table = new subscriberTable();
            $subscriber_table->set_table_data($subscribers, $results['count']);
            $subscriber_table->prepare_items( true ); // No pagination

            $title = __('Subscribers', 'pw-event-manager');

            $filter['s'] = $filter_search;
            $filter['fe'] = $filter_event_id;
            $filter['fed'] = $filter_event_date_id;
            $filter['fl'] = $filter_location_id;

            $content = subscribersListHtmlView::render($title, $subscriber_table, $filter);

        } else if( $mode == 'total_report' ) {

            global $pw_config;
            $locale = substr(get_locale(), 0, 2 );
            $date_format = $pw_config['date_format'][$locale];

            $date_from = eventDateTime::to_mysql_date( $date_format, $_POST['sel_period_from'] );
            $date_to = eventDateTime::to_mysql_date( $date_format, $_POST['sel_period_to'] );

            //Switch dates if from > to
            if( $date_from <> '' && $date_to <> '' ) {
                if( strtotime( $date_from ) > strtotime( $date_to ) ) {
                    $d = $date_from;
                    $date_from = $date_to;
                    $date_to = $d;
                }
            }

            //Get all confirmed participants within the period. Hide turnover columns.
            $report = $invoice_mdl->get_turnover_report( $date_from, $date_to);

            // Write subscribers to file. Hide turnover and revenue columns.
            // Important: In php-export-data.class.php the default CSV separator ( a comma ) is replaced with
            // a ; . Now Excel opens the file with the data in columns.
            $file = export::export_report_turnover($report, true);

            // Make the file downloadable
            chmod ( $file['dir'].'/'.$file['file'], 0644 );

            // Offer file for download
            $title = __('Download nr subscribers per event', 'pw-event-manager');

            $upload_dir = wp_upload_dir();
            $data['file_url'] = $upload_dir['baseurl'].'/export/'.$file['file'];

            subscribersDownloadHtmlView::render( $title, $data );
            exit();
        } else {

            $errors = "";
            $has_error = false;

            if( isset($_POST['submitSubscriber']) || isset($_POST['submitAndCloseSubscriber']) ||
                isset($_POST['moveSubscriber']) || isset($_POST['confirmSubscriber']) ) {

                $status = $this->make_safe_text( $_POST['status'], 16 );
                $process_comments = $_POST['process_comments'];
                $salutation = $this->make_safe_text( $_POST['salutation'], 16 );
                $first_name = $this->make_safe_text( $_POST['first_name'], 128 );
                $middle_name = $this->make_safe_text( $_POST['middle_name'], 32 );
                $last_name = $this->make_safe_text( $_POST['last_name'], 128 );
                $phone = $this->make_safe_text( $_POST['phone'], 64 );
                $email = substr( $_POST['email'], 0, 255 );

                if( $_POST['last_name'] == '' ) {
                    $errors['last_name'] = __('Last name is required', 'pw-event-manager');
                    $has_error = true;
                }

                if( $_POST['email'] == '' ) {
                    $errors['email'] = __('Email is required', 'pw-event-manager');
                    $has_error = true;
                }

                $subscribers[0]['errors'] = $errors;

                $subscribers[0]['status'] = $status;
                $subscribers[0]['process_comments'] = $process_comments;
                $subscribers[0]['salutation'] = $salutation;
                $subscribers[0]['first_name'] = $first_name;
                $subscribers[0]['middle_name'] = $middle_name;
                $subscribers[0]['last_name'] = $last_name;
                $subscribers[0]['phone'] = $phone;
                $subscribers[0]['email'] = $email;

                if( $has_error === false ) {
                    // Update participant details

                    $updated = eventDateTime::current_time_mysql();
                    $updated_by = substr(user::current_user_name(), 0, 64);

                    $participant_mdl->update( $subscribers[0]['id'], array(
                        'status'=>$status,
                        'process_comments'=>$process_comments,
                        'salutation'=>$salutation,
                        'first_name'=>$first_name,
                        'middle_name'=>$middle_name,
                        'last_name'=>$last_name,
                        'phone'=>$phone,
                        'email'=>$email,
                        'updated' => $updated,
                        'updated_by' => $updated_by
                    ));

                    $subscribers[0]['updated'] = $updated;
                    $subscribers[0]['updated_by'] = $updated_by;

                }

            }

            if( isset($_POST['submitSubscriberMove']) ) {

                //If selected date is different from current date:
                //Change the date, reset confirm_sent (has to be sent again)
                $new_date_id = (int) $_POST['new_date'];

                if( $new_date_id <> $subscribers[0]['eventDateID'] ) {
                    $participant_mdl->update( $subscribers[0]['id'], array(
                        'status' => 'new',
                        'eventDateID' => $new_date_id,
                        'confirm_sent' => '0',
                    ) );

                }

            }

            if( isset($_POST['submitConfirm']) ) {

                $email = $subscribers[0]['email'];
                $from = $event['notify_from'];

                $subject = $_POST['subject_confirm_ext'];
                $msg = $_POST['mail_confirm_ext'];

                mail::send_mail( $email, $from, $subject, $msg, null);

                // Update confirmation flag
                $participant_mdl->update( $subscribers[0]['id'], array( 'confirm_sent' => '1') );

            }

            if( isset($_POST['moveSubscriber']) && $has_error === false ) {

                //Subscriber can only be moved within the same event

                $date_list = $event_date_mdl->get_planning_by_id($subscribers[0]['eventID']);

                foreach( $date_list as $d ) {
                    $dates[] = array(
                        'id'=>$d['id'],
                        'start_date'=>$d['start_date'],
                        'start_time'=>$d['start_time']
                    );
                }

                // Obtain a list of columns
                foreach ($dates as $key => $row) {
                    $s_dates[$key]  = $row['start_date'];
                    $s_times[$key] = $row['start_time'];
                }

                // Do the sorting
                array_multisort($s_dates, SORT_ASC, $s_times, SORT_ASC, $dates);

                $subscribers[0]['other_event_dates'] = $dates;

                $title = __('Move to other date', 'pw-event-manager');
                $content = subscriberMoveHtmlView::render($title, $subscribers[0] );
            }
            elseif( isset($_POST['confirmSubscriber']) && $has_error === false ) {

                $m_subject = $event['subject_confirm_ext'];
                $m_msg = $event['mail_confirm_ext'];

                $m_event_title = '';
                $m_event_date = '';
                $m_event_time = '';
                $m_other_dates = '';
                $m_location = '';

                global $pw_config;

                if( strtolower( $event['locale'] ) == 'nl') {
                    $date_format = $pw_config['date_format']['nl'];
                } elseif( strtolower( $event['locale'] ) == 'de') {
                    $date_format = $pw_config['date_format']['de'];
                } else {
                    $date_format = $pw_config['date_format']['en'];
                }

                $salute = $subscribers[0]['salutation'];

                if( strtolower($event['locale']) == 'nl') {

                    $start_formal = "Geachte ";
                    $start_informal = "Beste ";

                    if( $salute == 'male' ) {
                        $salute = 'heer';
                    } elseif( $salute == 'female' ) {
                        $salute = 'mevrouw';
                    } else {
                        $salute = 'heer/mevrouw';
                    }
                } elseif( strtolower($event['locale']) == 'de') {

                    $start_formal = "Sehr geerhte ";
                    $start_informal = "Beste ";

                    if( $salute == 'male' ) {
                        $salute = 'heer DE';
                    } elseif( $salute == 'female' ) {
                        $salute = 'mevrouw DE';
                    } else {
                        $salute = 'heer/mevrouw DE';
                    }
                } else {

                    $start_formal = "Dear ";
                    $start_informal = "Dear ";

                    if( $salute == 'male' ) {
                        $salute = 'Mr.';
                    } elseif( $salute == 'female' ) {
                        $salute = 'Ms.';
                    } else {
                        $salute = 'Mr./Ms.';
                    }
                }

                $formal = $start_formal.str_replace( '  ', ' ', $salute.' '.$subscribers[0]['middle_name'].' '.$subscribers[0]['last_name'] );
                $informal = $start_informal.str_replace( '  ', ' ', $subscribers[0]['first_name'] );

                if( $event_date <> '' ) {
                    $m_event_title = stripslashes( $event['name']);
                    $m_event_date = date( $date_format ,strtotime( $event_date['start_date'] ));
                    $m_event_time = date( 'H:i', strtotime( $event_date['start_time'] )).' - '. date( 'H:i', strtotime( $event_date['stop_time'] ));

                    $m_other_dates = '';

                    if( $event_date['other_dates'] <> '' ) {
                        $tmp = explode(',', $event_date['other_dates']);

                        foreach( $tmp as $k => $t ) {
                            $tmp[$k] = date( $date_format ,strtotime( $t ));
                        }

                        $m_other_dates = implode( ',', $tmp );

                    }



                    if( $location <> '' ) {
                        $m_location = stripslashes( $location['location_name'] );
                    }

                }

                global $current_user;
                get_currentuserinfo();

                $m_sendname = $current_user->user_firstname.' '.$current_user->user_lastname;

                $m_subject = str_replace( '%EVENTTITLE%', $m_event_title, $m_subject);
                $m_subject = str_replace( '%EVENTDATE%', $m_event_date, $m_subject);
                $m_subject = str_replace( '%EVENTTIME%', $m_event_time, $m_subject);
                $m_subject = str_replace( '%OTHERDATES%', $m_other_dates, $m_subject);
                $m_subject = str_replace( '%LOCATION%', $m_location, $m_subject);
                $m_subject = str_replace( '%SENDERNAME%', $m_sendname, $m_subject);
                $m_subject = str_replace( '%FORMALHEAD%', $formal, $m_subject);
                $m_subject = str_replace( '%INFORMALHEAD%', $informal, $m_subject);

                $m_msg = str_replace( '%EVENTTITLE%', $m_event_title, $m_msg);
                $m_msg = str_replace( '%EVENTDATE%', $m_event_date, $m_msg);
                $m_msg = str_replace( '%EVENTTIME%', $m_event_time, $m_msg);
                $m_msg = str_replace( '%OTHERDATES%', $m_other_dates, $m_msg);
                $m_msg = str_replace( '%LOCATION%', $m_location, $m_msg);
                $m_msg = str_replace( '%SENDERNAME%', $m_sendname, $m_msg);
                $m_msg = str_replace( '%FORMALHEAD%', $formal, $m_msg);
                $m_msg = str_replace( '%INFORMALHEAD%', $informal, $m_msg);

                $subscribers[0]['subject_confirm_ext'] = $m_subject;
                $subscribers[0]['mail_confirm_ext'] = $m_msg;

                $title = __('(Re)sent confirmation', 'pw-event-manager');
                $content = subscriberEditHtmlView::render_confirmation( $title, $subscribers[0] );
            }
            elseif( ( isset($_POST['submitSubscriberMove']) || isset($_POST['submitConfirm']) ) && $has_error === false ) {
                $returnurl = "?page=pw-events-subscribers&action=edit&id=".$subscribers[0]['id'];
                echo('<meta http-equiv="refresh" content="0; url=' . $returnurl . '">');
                exit();
            }
            elseif( ( isset($_POST['closeSubscriber']) || isset($_POST['submitAndCloseSubscriber'])) && $has_error === false ) {
                $returnurl = "?page=pw-events-subscribers";
                echo('<meta http-equiv="refresh" content="0; url=' . $returnurl . '">');
                exit();
            } else {
                $title = __('Subscriber', 'pw-event-manager');
                $content = subscriberEditHtmlView::render( $title, $subscribers[0] );
            }

        }


        echo $content;
    }

    function render_subscriptions_list()
    {
        global $pw_config;

        require_once('classes/class-subscription-table.php');

        require_once('models/model-subscription.php');
        require_once('models/model-event.php');
        require_once('models/model-event-date.php');
        require_once('models/model-location.php');
        require_once('models/model-participant.php');
        require_once('models/model-invoice.php');

        require_once('classes/class-user.php');
        require_once('classes/class-eventdatetime.php');
        require_once('classes/class-util.php');
        require_once('classes/class-mail.php');
        require_once('classes/class-export.php');

        require_once('views/view-subscription-list.php');
        require_once('views/view-subscription-edit.php');
        require_once('views/view-subscriptions-download.php');

        $subscription_mdl = new modelSubscription();
        $event_mdl = new modelEvent();
        $event_date_mdl = new modelEventDate();
        $location_mdl = new modelLocation();
        $participant_mdl = new modelParticipant();
        $invoice_mdl = new modelInvoice();

        $mode = "list";
        $status = "";
        $content = '';

        $id = 0;
        $filter_search = "";
        $filter_event_id = 0;
        $filter_event_date_id = 0;
        $filter_location_id = 0;

        if (isset($_GET['s'])) {
            $filter_search = $_GET['s'];
            $mode = 'list-filtered';
        }

        if (isset($_GET['fe'])) {
            $filter_event_id = (int)$_GET['fe'];
            $mode = 'list-filtered';
        }

        // Only show dates when an event is selected
        if( $filter_event_id > 0 ) {
            if (isset($_GET['fed'])) {
                $filter_event_date_id = (int)$_GET['fed'];
                $mode = 'list-filtered';
            }
        }

        if (isset($_GET['fl'])) {
            $filter_location_id = (int)$_GET['fl'];
            $mode = 'list-filtered';
        }

        // No filtering active, switch to default list mode
        if( $filter_search == "" && $filter_event_id == 0 && $filter_event_date_id == 0 && $filter_location_id == 0 ) {
            $mode = 'list';
        }

        if (isset($_GET['id'])) {
            $id = (int)$_GET['id'];
            $mode = 'edit';
        }

        // Get all subscriptions above the concept state

        if ($_POST['action'] == "list_export" || $_POST['action'] == "list_export") {
            $mode = "list_export";
        }

        if( $mode == 'list') {
            $results = $subscription_mdl->get_all_not_concept();
            $subscriptions = $results['list'];
        } else if( $mode == 'list_export') {
            $subscriptions = $subscription_mdl->get_all_export();
        }  else if( $mode == 'list-filtered') {
            // Get Filtered
            if( $filter_event_id == 0 && $filter_event_date_id == 0 && $filter_location_id == 0 ) {
                $results = $subscription_mdl->get_all_not_concept();
                $subscriptions = $results['list'];
            } else {
                $results = $subscription_mdl->get_all_filtered( $filter_event_id, $filter_event_date_id, $filter_location_id);
                $subscriptions = $results['list'];

            }

        } else {
            $subscriptions[] = $subscription_mdl->get_by_id( $id );
        }

        foreach( $subscriptions as $key => $rec) {

            // Get the event details for each subscription
            $event = $event_mdl->get_by_id( $rec['eventID'] );

            $event_date = $event_date_mdl->get_by_key_id( $rec['eventDateId'] );

            $subscriptions[$key]['event_details']['name'] = stripslashes( $event['name'] );
            $subscriptions[$key]['event_details']['date'] = $event_date['start_date'];
            $subscriptions[$key]['event_details']['start_time'] = $event_date['start_time'];
            $subscriptions[$key]['event_details']['stop_time'] = $event_date['stop_time'];
            $subscriptions[$key]['event_details']['other_dates'] = $event_date['other_dates'];

            $location = $location_mdl->get_by_id( $event_date['locationID'] );

            $subscriptions[$key]['event_details']['location_name'] = stripslashes( $location['location_name'] );

            // Count the nr of confirmed subscribers for each subscription
            $confirmed_participants = $participant_mdl->get_all_confirmed( $rec['eventID'], $rec['eventDateId'], $event_date['locationID'] );
            $subscriptions[$key]['nr_confirmed_participants'] = count( $confirmed_participants );

            // Get the subscribers for each subscription
            $subscribers = $participant_mdl->get_all_by_subscription_id( $rec['id'] );
            $subscriptions[$key]['subscribers'] = $subscribers;

            //Get the invoice for each subscription
            $invoice = $invoice_mdl->get_by_subscription_id( $rec['id'] );

            if( isset( $invoice['invoice_nr'] ) ) {
                $subscriptions[$key]['invoice']['invoice_nr'] = $invoice['invoice_nr'];
            } else {
                $subscriptions[$key]['invoice']['invoice_nr'] = '';
            }

            if( isset( $invoice['payment_method'] ) ) {
                $subscriptions[$key]['invoice']['payment_method'] = $invoice['payment_method'];
            } else {
                $subscriptions[$key]['invoice']['payment_method'] = '';
            }

            if( isset( $invoice['status'] ) ) {
                $subscriptions[$key]['invoice']['status'] = $invoice['status'];
            } else {
                $subscriptions[$key]['invoice']['status'] = '';
            }

            if( isset( $invoice['transaction_status'] ) ) {
                $subscriptions[$key]['invoice']['transaction_status'] = $invoice['transaction_status'];
            } else {
                $subscriptions[$key]['invoice']['transaction_status'] = '';
            }

        }

        if ($mode == "list" || $mode == "list-filtered" || $mode == "list_export") {
            if (isset($_POST['action']) || isset($_POST['action2'])) {

                if ($_POST['action'] == "list_export" || $_POST['action'] == "list_export") {

                    // Write subscribtions to file
                    // Important: In php-export-data.class.php the default CSV separator ( a comma ) is replaced with
                    // a ; . Now Excel opens the file with the data in columns.
                    $file = export::export_subscriptions($subscriptions);

                    // Make the file downloadable
                    chmod ( $file['dir'].'/'.$file['file'], 0644 );

                    // Offer file for download
                    $title = __('Download subscriptions', 'pw-event-manager');

                    $upload_dir = wp_upload_dir();
                    $data['file_url'] = $upload_dir['baseurl'].'/export/'.$file['file'];

                    subscriptionsDownloadHtmlView::render( $title, $data );
                    exit();
                }
            }
        }

        //Loop the result set and apply search string
        if(  $mode == 'list-filtered' && $filter_search <> "" ) {

            $tmp_sub = array();
            foreach( $subscriptions as $k => $v ) {

                global $pw;


                $txt = "";

                if( is_array($v['subscribers']) ) {
                    foreach( $v['subscribers'] as $key=>$val ) {
                        //$pw->print_var($val);
                        $txt .= strtolower( str_replace("  ", " ",
                            stripslashes( $val['first_name'] ).' '.
                            stripslashes($val['middle_name']).' '.
                            stripslashes($val['last_name']).' '.
                            $val['email'].' '.
                            $val['phone'].' '));
                    }
                }

                $txt .= strtolower( str_replace("  ", " ",
                    stripslashes($v['company']).' '
                ));

                if( strpos($txt, strtolower( $filter_search )) !== false ) {

                    $tmp_sub[] = $v;
                }


            }

            //$subscriptions = $tmp_sub;
        }

        if( $mode == 'list') {

            $subscriber_table = new subscriptionTable();
            $subscriber_table->set_table_data($subscriptions, $results['count']);
            $subscriber_table->prepare_items();

            $title = __('Subscriptions', 'pw-event-manager');

            $content = subscriptionListHtmlView::render($title, $subscriber_table);

        } else if( $mode == 'list-filtered') {

            if( $filter_event_id == 0 && $filter_event_date_id == 0 && $filter_location_id == 0 ) {
                //$results['count'] = count($subscriptions);
            }

            $subscriber_table = new subscriptionTable();
            $subscriber_table->set_table_data($subscriptions, $results['count']);
            $subscriber_table->prepare_items(true);

            $title = __('Subscriptions', 'pw-event-manager');

            $filter['s'] = $filter_search;
            $filter['fe'] = $filter_event_id;
            $filter['fed'] = $filter_event_date_id;
            $filter['fl'] = $filter_location_id;

            $content = subscriptionListHtmlView::render($title, $subscriber_table, $filter);

        } else {

            $errors = "";
            $has_error = false;

            if( isset($_POST['submitSubscription']) || isset($_POST['submitAndCloseSubscription']) ) {

                $status = $this->make_safe_text( $_POST['status'], 16);

                $subscribers_confirmed = "";

                //Cancel is only allowed when there are no confirmed subscribers
                if( $status == 'cancelled') {

                    $subscribers_confirmed = $participant_mdl->get_confirmed_by_subscription( $subscriptions[0]['id'] );

                }

                $process_comments = $this->make_safe_text( $_POST['process_comments'] );
                $subscriptions[0]['process_comments'] = $process_comments;

                if( $subscribers_confirmed == "" ) {

                    $updated = eventDateTime::current_time_mysql();
                    $updated_by = substr(user::current_user_name(), 0, 64);

                    $subscription_mdl->update($subscriptions[0]['id'],
                        array(
                            'status'=> $status,
                            'process_comments' => $process_comments,
                            'updated' => $updated,
                            'updated_by' => $updated_by,
                        )
                    );

                    $subscriptions[0]['status'] = $status;
                    $subscriptions[0]['updated'] = $updated;
                    $subscriptions[0]['updated_by'] = $updated_by;

                    // Update participants status
                    // new = update to concept
                    // inprogress = update to concept
                    // confirmed = will be updated when confirmation mail is sent
                    // canceled = update to cancelled

                    $subscribers = $subscriptions[0]['subscribers'];

                    if( $status <> 'confirmed' ) {
                        foreach( $subscribers as $key => $subscriber ) {
                            $participant_mdl->update( $subscriber['id'], array( 'status' => $status) );
                        }
                    } else {
                        foreach( $subscribers as $key => $subscriber ) {
                            if( $subscriber['status'] == 'concept' || $subscriber['status'] == 'new' ) {
                                $participant_mdl->update( $subscriber['id'], array( 'status' => 'confirmed' ) );
                            }
                        }

                        // Get the total participant count
                        $participantCount = $participant_mdl->get_event_confirmed_participcant_count($subscriptions[0]['eventID'], $subscriptions[0]['eventDateId']);
                        // Get event data
                        $event = $event_date_mdl->get_by_event_id_and_id( $subscriptions[0]['eventID'], $subscriptions[0]['eventDateId'] );
                        // Threshold for the event
                        $participantThreshold = $event['threshold'];

                        // Count greater or equal close the participation
                        if($participantCount[0]['COUNT'] >= $participantThreshold){
                           //$eventDate = $event_date_mdl->get_by_event_id($subscriptions[0]['eventDateID']);
                           $event_date_mdl->update($event['id'],array( 'status' => 'full' ));
                        }
                    }

                } else {

                    $errors['confirmed_subcribers'] = __('One or more participants have the status confirmed. Therefore you cannot cancel this subscription.', 'pw-event-manager');
                    $has_error = true;
                }

            }

            if( isset($_POST['submitConfirm']) ) {

                //Get the event
                $event = $event_mdl->get_by_id( $subscriptions[0]['eventID']);

                // Validate input
                $has_error = false;

                if( $_POST['subject_confirm_ext'] == '' ) {
                    $errors['subject_confirm_ext'] = __('Subject of the email is required', 'pw-event-manager');
                    $has_error = true;
                }

                if( $_POST['mail_confirm_ext'] == '' ) {
                    $errors['mail_confirm_ext'] = __('Message for the email is required', 'pw-event-manager');
                    $has_error = true;
                }

                if( $has_error === false ) {

                    // Send confirmation emails
                    $subscribers = $subscriptions[0]['subscribers'];

                    if( is_array( $_POST['send_to']) ) {

                        $from = $event['notify_from'];

                        // For each selected participant
                        foreach( $_POST['send_to'] as $id ) {
                            $email = '';
                            $subscriber = '';

                            // Get the email address
                            foreach( $subscribers as $sub ) {
                                if( $sub['id'] == $id ) {
                                    $subscriber = $sub;
                                }
                            }

                            if( is_array($subscriber) ) {
                                $subject = $_POST['subject_confirm_ext'];
                                $msg = $_POST['mail_confirm_ext'];
                                $email = $subscriber['email'];

                                $participant_mdl->update( (int) $subscriber['id'], array( 'status' => 'confirmed' ) );

                                // If valid email address: sent notification & Update confirm email flag
                                if( $email <> '' && is_email( $email )) {

                                    $salute = $subscriber['salutation'];

                                    if( strtolower($event['locale']) == 'nl') {

                                        $start_formal = "Geachte ";
                                        $start_informal = "Beste ";

                                        if( $salute == 'male' ) {
                                            $salute = 'heer';
                                        } elseif( $salute == 'female' ) {
                                            $salute = 'mevrouw';
                                        } else {
                                            $salute = 'heer/mevrouw';
                                        }
                                    } elseif( strtolower($event['locale']) == 'de') {

                                        $start_formal = "Sehr geerhte ";
                                        $start_informal = "Beste ";

                                        if( $salute == 'male' ) {
                                            $salute = 'heer DE';
                                        } elseif( $salute == 'female' ) {
                                            $salute = 'mevrouw DE';
                                        } else {
                                            $salute = 'heer/mevrouw DE';
                                        }
                                    } else {

                                        $start_formal = "Dear ";
                                        $start_informal = "Dear ";

                                        if( $salute == 'male' ) {
                                            $salute = 'Mr.';
                                        } elseif( $salute == 'female' ) {
                                            $salute = 'Ms.';
                                        } else {
                                            $salute = 'Mr./Ms.';
                                        }
                                    }

                                    $formal = $start_formal.str_replace( '  ', ' ', $salute.' '.$subscriber['middle_name'].' '.$subscriber['last_name'] );
                                    $informal = $start_informal.str_replace( '  ', ' ', $subscriber['first_name'] );

                                    $subject = str_replace( '%FORMALHEAD%', $formal, $subject);
                                    $subject = str_replace( '%INFORMALHEAD%', $informal, $subject);

                                    $msg = str_replace( '%FORMALHEAD%', $formal, $msg);
                                    $msg = str_replace( '%INFORMALHEAD%', $informal, $msg);

                                    mail::send_mail( $email, $from, $subject, $msg, null);

                                    // Update confirmation flag
                                    $participant_mdl->update( $subscriber['id'], array( 'confirm_sent' => '1') );
                                }
                            }

                        }
                    }

                }

            }

            if( (isset($_POST['submitSubscription']) || isset($_POST['submitAndCloseSubscription'])) && $has_error === true ) {

                $title = __('Subscription', 'pw-event-manager');

                $subscriptions[0]['errors'] = $errors;

                $content = subscriptionEditHtmlView::render( $title, $subscriptions[0] );

            }
            else if( ( isset($_POST['submitSubscription']) || isset($_POST['submitAndCloseSubscription']) ) && $status == 'confirmed') {
                // Go to screen to sent confirmation

                global $pw_config;

                $title = __('Confirm subscription', 'pw-event-manager');

                //Get the event
                $event = $event_mdl->get_by_id( $subscriptions[0]['eventID']);

                //Get the event date
                $event_date = $event_date_mdl->get_by_key_id( $subscriptions[0]['eventDateId']);

                $event_location = '';
                if( $event_date <> '' ) {
                    $event_location = $location_mdl->get_by_id($event_date['locationID']);
                }

                $m_subject = '';
                $m_msg = '';

                if( $event <> "" ) {

                    if( strtolower( $event['locale'] ) == 'nl') {
                        $date_format = $pw_config['date_format']['nl'];
                    } elseif( strtolower( $event['locale'] ) == 'de') {
                        $date_format = $pw_config['date_format']['de'];
                    } else {
                        $date_format = $pw_config['date_format']['en'];
                    }

                    $m_event_title = '';
                    $m_event_date = '';
                    $m_event_time = '';
                    $m_other_dates = '';
                    $m_location = '';

                    if( $event_date <> '' ) {
                        $m_event_title = stripslashes( $event['name']);
                        $m_event_date = date( $date_format ,strtotime( $event_date['start_date'] ));
                        $m_event_time = date( 'H:i', strtotime( $event_date['start_time'] )).' - '. date( 'H:i', strtotime( $event_date['stop_time'] ));

                        $m_other_dates = '';

                        if( $event_date['other_dates'] <> '' ) {

                            $tmp = explode(',', $event_date['other_dates']);

                            foreach( $tmp as $k => $t ) {
                                $tmp[$k] = date( $date_format ,strtotime( $t ));
                            }

                            $m_other_dates = implode( ',', $tmp );
                        }




                        if( $event_location <> '' ) {
                            $m_location = stripslashes( $event_location['location_name'] );
                        }

                    }

                    global $current_user;
                    get_currentuserinfo();

                    $m_sendname = $current_user->user_firstname.' '.$current_user->user_lastname;

                    //Create subject
                    $m_subject = stripslashes( $event['subject_confirm_ext'] );

                    $m_subject = str_replace( '%EVENTTITLE%', $m_event_title, $m_subject);
                    $m_subject = str_replace( '%EVENTDATE%', $m_event_date, $m_subject);
                    $m_subject = str_replace( '%EVENTTIME%', $m_event_time, $m_subject);
                    $m_subject = str_replace( '%OTHERDATES%', $m_other_dates, $m_subject);
                    $m_subject = str_replace( '%LOCATION%', $m_location, $m_subject);
                    $m_subject = str_replace( '%SENDERNAME%', $m_sendname, $m_subject);

                    //Create message
                    $m_msg = stripslashes( $event['mail_confirm_ext'] );
                    $m_msg = str_replace( '%EVENTTITLE%', $m_event_title, $m_msg);
                    $m_msg = str_replace( '%EVENTDATE%', $m_event_date, $m_msg);
                    $m_msg = str_replace( '%EVENTTIME%', $m_event_time, $m_msg);
                    $m_msg = str_replace( '%OTHERDATES%', $m_other_dates, $m_msg);
                    $m_msg = str_replace( '%LOCATION%', $m_location, $m_msg);
                    $m_msg = str_replace( '%SENDERNAME%', $m_sendname, $m_msg);
                }

                $subscriptions[0]['subject_confirm_ext'] = $m_subject;
                $subscriptions[0]['mail_confirm_ext'] = $m_msg;

                $content = subscriptionEditHtmlView::render_confirmation( $title, $subscriptions[0] );
            }
            elseif( isset($_POST['submitConfirm']) && $has_error === true ) {
                $subscriptions[0]['errors'] = $errors;
                $title = __('Confirm subscription', 'pw-event-manager');
                $content = subscriptionEditHtmlView::render_confirmation( $title, $subscriptions[0] );
            }
            elseif( isset($_POST['submitConfirm']) && $has_error === false ) {
                $returnurl = "?page=pw-events-subscriptions";
                echo('<meta http-equiv="refresh" content="0; url=' . $returnurl . '">');
                exit();
            }
            elseif( isset($_POST['closeSubscription']) || isset($_POST['submitAndCloseSubscription']) ||
                    isset($_POST['closeConfirm'] )) {

                $returnurl = "?page=pw-events-subscriptions";
                echo('<meta http-equiv="refresh" content="0; url=' . $returnurl . '">');
                exit();

            } else {
                $title = __('Subscription', 'pw-event-manager');
                $content = subscriptionEditHtmlView::render( $title, $subscriptions[0] );
            }

        }

        echo $content;

    }

    function render_invoices_list()
    {

        global $pw_config;

        require_once('modelview/modelview-invoice.php');
        require_once('views/view-invoice-list.php');
        require_once('views/view-invoice-resend.php');
        require_once('views/view-invoice-select-period.php');
        require_once('views/view-invoice-download.php');

        require_once('models/model-invoice.php');
        require_once('models/model-participant.php');

        require_once('classes/class-export.php');
        require_once('classes/class-eventdatetime.php');
        require_once('classes/class-invoice-table.php');

        $mode = 'list';
        $content = '';
        $error = array();
        $hasError = false;

        $filter_search = "";
        $filter_date = "";

        $invoice_mdl = new modelInvoice();
        $participant_mdl = new modelParticipant();
        $invoice_mv = new invoiceModelView();

        // Do some cleanup of old PDF's.
        $dir = WP_CONTENT_DIR . '/uploads/invoices/';

        foreach (glob($dir."*.pdf") as $file) {

            // If file is 1 hour (3600 seconds) old then delete it
            if (filemtime($file) < ( time() - 3600 )) {
                unlink($file);
            }
        }

        //Check for id
        $id = 0;
        if (isset($_GET['id'])) {
            $id = (int)$_GET['id'];
        }

        $invoice = "";
        if( $id > 0 ) {
            $invoice = $invoice_mdl->get_by_id($id);
        }

        //Determine the requested action
        if (isset($_GET['action']) && $_GET['action'] == 'edit') {
            $mode = "edit";
        } elseif (isset($_GET['action']) && $_GET['action'] == 'credit') {
            $mode = "credit";
        } elseif (isset($_GET['action']) && $_GET['action'] == 'resend') {
            $mode = "resend";
        } elseif (isset($_GET['action']) && $_GET['action'] == 'topdf') {
            $mode = "topdf";
        } elseif (isset($_GET['action']) && $_GET['action'] == 'copycfo') {
            $mode = "copycfo";
        } else {
            $mode = "list";
        }

        if (isset($_GET['s'])) {
            $filter_search = $_GET['s'];
            $mode = 'list-filtered';
        }

        if (isset($_GET['fd'])) {
            $filter_date = $_GET['fd'];
            $mode = 'list-filtered';
        }

        // No filtering active, switch to default list mode
        if( $mode == 'list-filtered' && $filter_search == "" && $filter_date == "" ) {
            $mode = 'list';
        }

        if( isset($_GET['action']) === false && $id > 0 ) {
            $mode = "read";
        }

        //Export of invoice reports
        if( isset( $_POST['closeInvoiceSelectPeriod'])) {
            $mode = "list";
        } else if( isset( $_POST['submitInvoiceSelectPeriod'])) {
            if( $_POST['requested_action'] == "invoicesummary" ) {
                $mode = "invoicesummary";
            } else if( $_POST['requested_action'] == "subpermonth" ) {
                $mode = "subpermonth";
            } else {
                $mode = 'list';
            }
        }

        if( $mode == 'read' ) {

            if( is_array($invoice) ) {
                require_once('modelview/modelview-invoice.php');

                $invoice_mv = new invoiceModelView();
                $lang = $invoice['invoice_lang'];

                $content = $invoice_mv->render_invoice_html_admin( $invoice, $lang, $mode );

            } else {
                $content = '<br><br>'.__('Invalid invoice', 'pw-event-manager');
            }

        }
        else if( $mode == 'edit' ) {

            if( is_array($invoice) ) {

                $lang = $invoice['invoice_lang'];

                if( isset($_POST['saveInvoice']) || isset($_POST['saveCloseInvoice']) ) {

                    // Save the changes
                    $update['invoice_company'] = $this->make_safe_text( $_POST['invoice_company'], 128 );
                    $update['invoice_attention'] = $this->make_safe_text( $_POST['invoice_attention'], 128);
                    $update['invoice_street'] = $this->make_safe_text( $_POST['invoice_street'], 128);
                    $update['invoice_street_nr'] = $this->make_safe_text( $_POST['invoice_street_nr'], 32);
                    $update['invoice_zip'] = $this->make_safe_text( $_POST['invoice_zip'], 16);
                    $update['invoice_city'] = $this->make_safe_text( $_POST['invoice_city'], 32);
                    $update['invoice_country'] = $this->make_safe_text( $_POST['invoice_country'], 32);
                    $update['invoice_attribute'] = $this->make_safe_text( $_POST['invoice_attribute'], 255);

                    $invoice_mdl->update($id, $update);

                    // Update in memory invoice with the new changes
                    $invoice['invoice_company'] = $update['invoice_company'];
                    $invoice['invoice_attention'] = $update['invoice_attention'];
                    $invoice['invoice_street'] = $update['invoice_street'];
                    $invoice['invoice_street_nr'] = $update['invoice_street_nr'];
                    $invoice['invoice_zip'] = $update['invoice_zip'];
                    $invoice['invoice_city'] = $update['invoice_city'];
                    $invoice['invoice_country'] = $update['invoice_country'];
                    $invoice['invoice_attribute'] = $update['invoice_attribute'];

                }

                if( isset($_POST['closeInvoice']) || isset($_POST['saveCloseInvoice']) ) {
                    // Back to read mode
                    $returnurl = "?page=pw-events-invoices&mode=read&id=".$invoice['id'];
                    echo('<meta http-equiv="refresh" content="0; url=' . $returnurl . '">');
                    exit();
                }

                $content = $invoice_mv->render_invoice_html_admin( $invoice, $lang, $mode );

            } else {
                $content = '<br><br>'.__('Invalid invoice', 'pw-event-manager');
            }

        }
        else if( $mode == 'credit' ) {
            // Create a copy of the invoice with type=credit. Open the result in read mode.
            if( is_array($invoice) ) {

                require_once('modelview/modelview-invoice.php');

                $invoice_mv = new invoiceModelView();
                //$lang = $invoice['invoice_lang'];

                if( $invoice['invoice_type'] == 'debet') {

                    // Check for existing credit invoice
                    $invoice_nr_c = $invoice['invoice_nr']. 'C';

                    $credit_invoice = $invoice_mdl->get_by_invoice_nr( $invoice_nr_c );

                    if( $credit_invoice == '' ) {
                        $credit_invoice = $invoice_mv->create_credit_copy( $invoice );

                        // Save the new credit_invoice
                        $credit_id = $invoice_mdl->insert($credit_invoice);
                    } else {
                        $credit_id = $credit_invoice['id'];
                    }

                    // Open the credit in read mode
                    $returnurl = "?page=pw-events-invoices&mode=read&id=".$credit_id;
                    echo('<meta http-equiv="refresh" content="0; url=' . $returnurl . '">');
                    exit();

                } else {
                    $content = '<br><br>'.__('This invoice is already a credit', 'pw-event-manager');
                }

            }

        }
        else if( $mode == 'resend' ) {
            // (Re)send the invoice
            if( isset( $_POST['submitInvoiceResend']) ) {

                $send_to = substr( $_POST['invoice_email'], 0, 255 );

                if( $send_to == "" ) {
                    $errors['invoice_email'] = __("Email is required", 'pw-event-manager');
                    $hasError = true;
                }

                if( $hasError == false ) {
                    //Do the sending
                    $lang = $invoice['invoice_lang'];

                    // Change the message and subject in case of credit invoice
                    $invoice_mv->email_invoice_pdf( $invoice, $send_to, $lang );

                    //Return to the invoice
                    $returnurl = "?page=pw-events-invoices&mode=read&id=".$invoice['id'];
                    echo('<meta http-equiv="refresh" content="0; url=' . $returnurl . '">');
                    exit();
                } else {
                    $invoice['errors'] = $errors;

                    $invoice['invoice_email'] = $send_to;
                    $title = __('(Re)send invoice', 'pw-event-manager');
                    $content = invoiceResendHtmlView::render($title, $invoice);
                }

            } else if( isset( $_POST['submitInvoiceResend']) ) {
                //Return to the invoice
                $returnurl = "?page=pw-events-invoices&mode=read&id=".$invoice['id'];
                echo('<meta http-equiv="refresh" content="0; url=' . $returnurl . '">');
                exit();
            } else {
                $title = __('(Re)send invoice', 'pw-event-manager');
                $content = invoiceResendHtmlView::render($title, $invoice);
            }

        }
        else if( $mode == 'topdf' ) {
            // Show a link to the PDF of the (credit) invoice
            if( is_array($invoice) ) {
                $lang = $invoice['invoice_lang'];
                $fileName = WP_CONTENT_DIR . '/uploads/invoices/INV_'.$invoice['invoice_nr'].'.pdf';
                $fileUrl = content_url(). '/uploads/invoices/INV_'.$invoice['invoice_nr'].'.pdf';
                $invoice_mv->render_invoice_pdf( $invoice, $lang, $fileName, false );

                $content = '<br><br><p>';
                $content .= '<a href="'.$fileUrl.'" target="_new">'.__('Click to open and download the PDF', 'pw-event-manager').'</a></p><br>';
                $content .= '<a href="?page=pw-events-invoices&id='.$invoice['id'].'" class="button button-primary">&laquo; '.__('Back', 'pw-event-manager').'</a>';
            }

        }
        else if( $mode == 'copycfo' ) {

            global $pw_config;
            $lang = $invoice['invoice_lang'];

            $invoice_mv->email_invoice_pdf( $invoice, $pw_config['cfo_email'], $lang );

            //Return to the invoice
            $returnurl = "?page=pw-events-invoices&id=".$invoice['id'];
            echo('<meta http-equiv="refresh" content="0; url=' . $returnurl . '">');
            exit();

        }
        else if( $mode == 'subpermonth' ) {
            global $pw_config;
            $locale = substr(get_locale(), 0, 2 );
            $date_format = $pw_config['date_format'][$locale];

            $date_from = eventDateTime::to_mysql_date( $date_format, $_POST['sel_period_from'] );
            $date_to = eventDateTime::to_mysql_date( $date_format, $_POST['sel_period_to'] );

            //Switch dates if from > to
            if( $date_from <> '' && $date_to <> '' ) {
                if( strtotime( $date_from ) > strtotime( $date_to ) ) {
                    $d = $date_from;
                    $date_from = $date_to;
                    $date_to = $d;
                }
            }

            //Get all confirmed participants within the period
            $subscribers = $participant_mdl->get_report_period_confirmed( $date_from, $date_to);

            // Write subscribers to file
            // Important: In php-export-data.class.php the default CSV separator ( a comma ) is replaced with
            // a ; . Now Excel opens the file with the data in columns.
            $file = export::export_report_subpermonth($subscribers);

            // Make the file downloadable
            chmod ( $file['dir'].'/'.$file['file'], 0644 );

            // Offer file for download
            $title = __('Download subscribers', 'pw-event-manager');

            $upload_dir = wp_upload_dir();
            $data['file_url'] = $upload_dir['baseurl'].'/export/'.$file['file'];

            invoiceDownloadHtmlView::render( $title, $data );
            exit();

        }
        else if( $mode == 'invoicesummary' ) {

            global $pw_config;
            $locale = substr(get_locale(), 0, 2 );
            $date_format = $pw_config['date_format'][$locale];

            $date_from = eventDateTime::to_mysql_date( $date_format, $_POST['sel_period_from'] );
            $date_to = eventDateTime::to_mysql_date( $date_format, $_POST['sel_period_to'] );

            //Switch dates if from > to
            if( $date_from <> '' && $date_to <> '' ) {
                if( strtotime( $date_from ) > strtotime( $date_to ) ) {
                    $d = $date_from;
                    $date_from = $date_to;
                    $date_to = $d;
                }
            }

            //Get all confirmed participants within the period
            $report = $invoice_mdl->get_turnover_report( $date_from, $date_to);

            // Write subscribers to file
            // Important: In php-export-data.class.php the default CSV separator ( a comma ) is replaced with
            // a ; . Now Excel opens the file with the data in columns.
            $file = export::export_report_turnover($report);

            // Make the file downloadable
            chmod ( $file['dir'].'/'.$file['file'], 0644 );

            // Offer file for download
            $title = __('Download revenue per event', 'pw-event-manager');

            $upload_dir = wp_upload_dir();
            $data['file_url'] = $upload_dir['baseurl'].'/export/'.$file['file'];

            invoiceDownloadHtmlView::render( $title, $data );
            exit();
        }
        else {

            $selected_invoices = "";

            if (isset($_POST['action']) || isset($_POST['action2'])) {

                // Print selected invoices
                if ($_POST['action'] == "print" || $_POST['action2'] == "print") {
                    foreach ($_POST['list'] as $key => $val) {
                        $selected_invoices[] = $invoice_mdl->get_by_id($val);
                    }

                    if ($selected_invoices <> '') {
                        $fileName = WP_CONTENT_DIR . '/uploads/invoices/print_' . time() . '.pdf';
                        $fileUrl = content_url() . '/uploads/invoices/print_' . time() . '.pdf';
                        $invoice_mv->render_invoice_multi_pdf($selected_invoices, $fileName, false);

                        $content = '<br><br><p>';
                        $content .= '<a href="' . $fileUrl . '" target="_new">' . __('Click to open and download/print the PDF', 'pw-event-manager') . '</a></p><br>';
                        $content .= '<a href="?page=pw-events-invoices" class="button button-primary">&laquo; ' . __('Back', 'pw-event-manager') . '</a>';

                    } else {
                        echo __('No invoices selected', 'pw-event-manager');
                        exit();
                    }
                } else if($_POST['action'] == "mailtocfo" || $_POST['action2'] == "mailtocfo") {

                    global $pw_config;

                    foreach ($_POST['list'] as $key => $val) {
                        $invoice = $invoice_mdl->get_by_id($val);

                        if( $invoice <> '' ) {

                            //Do the sending
                            $lang = $invoice['invoice_lang'];

                            $invoice_mv->email_invoice_pdf( $invoice, $pw_config['cfo_email'], $lang );
                        }
                    }

                    // List table of invoices
                    $invoice_table = new invoiceTable();

                    $data = $invoice_mdl->get_all();
                    $invoice_table->set_table_data($data);
                    $invoice_table->prepare_items();

                    $title = __('Invoices', 'pw-event-manager');

                    $content = invoiceListHtmlView::render($title, $invoice_table);

                } else if ($_POST['action'] == "invoicesummary" || $_POST['action2'] == "invoicesummary") {
                    $data['selected_action'] = "invoicesummary";
                    $title = __('Revenue per event', 'pw-event-manager');

                    invoiceSelectPeriodHtmlView::render($title, $data);
                    exit();
                } else if ($_POST['action'] == "subpermonth" || $_POST['action2'] == "subpermonth") {
                    $data['selected_action'] = "subpermonth";
                    $title = __('Subscribers per month', 'pw-event-manager');

                    invoiceSelectPeriodHtmlView::render($title, $data);
                    exit();
                } else {
                    echo  '<p>'.__('Unknown action', 'pw-event-manager').'</p>';
                    exit();
                }
            }
            else if( $mode == 'list-filtered') {

                // List table of invoices
                $invoice_table = new invoiceTable();

                $data = $invoice_mdl->get_all_filtered( $filter_date );

                //Loop the result set and apply search string
                if(  $filter_search <> "" ) {

                    $tmp_inv = array();
                    foreach( $data as $k => $v ) {

                        $txt = strtolower( str_replace("  ", " ",
                            stripslashes( $v['invoice_company'] ).' '.
                            stripslashes($v['description']).' '.
                            stripslashes($v['subscribers']).' '.
                            stripslashes($v['invoice_nr']).' '
                        ));

                        if( strpos($txt, strtolower( $filter_search )) !== false ) {
                            $tmp_inv[] = $v;
                        }
                    }

                    $data = $tmp_inv;
                }

                $filter['s'] = $filter_search;
                $filter['fd'] = $filter_date;

                $invoice_table->set_table_data($data);
                $invoice_table->prepare_items();

                $title = __('Invoices', 'pw-event-manager');

                $content = invoiceListHtmlView::render($title, $invoice_table, $filter);

            } else {
                // List table of invoices
                $invoice_table = new invoiceTable();

                $data = $invoice_mdl->get_all();
                $invoice_table->set_table_data($data);
                $invoice_table->prepare_items();

                $title = __('Invoices', 'pw-event-manager');

                $content = invoiceListHtmlView::render($title, $invoice_table);
            }
        }

        echo $content;

    }

    function render_locations_list()
    {
        global $pw_config;

        require_once('views/view-location-list.php');
        require_once('views/view-location-edit.php');
        require_once('models/model-location.php');
        require_once('classes/class-location-table.php');

        require_once('classes/class-user.php');
        require_once('classes/class-eventdatetime.php');
        require_once('classes/class-util.php');

        $content = "";
        $error = array();
        $hasError = false;
        $returnurl = "?page=pw-events-locations";
        $id = 0;
        $originalFilePath = "";
        $mode = "";
        $model = new modelLocation();

        //Determine the requested action
        if (isset($_GET['action']) && $_GET['action'] == 'new') {
            $mode = "new";

        } elseif (isset($_GET['action']) && $_GET['action'] == 'edit') {
            $mode = "edit";

            if (isset($_GET['id'])) {
                $id = (int)$_GET['id'];
            }

        } else {
            $mode = "list";
        }

        // Mark selected items as removed. This will move them to the admin trash
        // from where they can be restored when required.

        /*
        if ($mode == "list") {
            if (isset($_POST['action']) || isset($_POST['action2'])) {

                if ($_POST['action'] == "list_delete" || $_POST['action'] == "list_delete") {
                    foreach ($_POST['list'] as $key => $val) {
                        $model->update((int)$val, array('removed' => '1'));
                    }
                }
            }
        }
        */

        //Fill the data array with the appropriate data

        $data = array();

        //Initialize Data
        if ($mode == "new") {

            $data['locale'] = strtoupper( $pw_config['lang_primary'] );
            $data['location_name'] = "";
            $data['profile'] = "";
            $data['maps_url'] = "";
            $data['attachment_path'] = "";
            $data['updated'] = eventDateTime::current_time_mysql();
            $data['updated'] = eventDateTime::mysql_to_display($data['updated']); //Format for display
            $data['updated_by'] = substr(user::current_user_name(), 0, 64);

        } elseif ($mode == "edit") {

            $item = $model->get_by_id($id); //Get record by ID

            $data['locale'] = $item['locale'];
            $data['location_name'] = $item['location_name'];
            $data['profile'] = $item['profile'];
            $data['maps_url'] = $item['maps_url'];
            $data['attachment_path'] = $item['attachment_path'];
            $originalFilePath = $data['attachment_path'];
            $data['updated'] = $item['updated'];
            $data['updated_by'] = $item['updated_by'];
            $data['ip'] = $item['ip'];

        }

        //Override default data with $_POST when applicable
        if (isset($_POST['submitLocation']) || isset($_POST['submitAndCloseLocation']) || isset($_POST['removeAttachmentLocation'])) {

            $data['location_name'] = $this->make_safe_text( $_POST['location_name'], 64 );
            $data['locale'] = $this->make_safe_text( $_POST['locale'], 2 );

            $data['profile'] = $_POST['profile']; //Can contain HTML
            $data['maps_url'] = $_POST['maps_url']; //Can contain HTML

            $data['attachment_path'] = $this->make_safe_text( $_POST['attachment_path'], 255);
            $data['updated'] = eventDateTime::current_time_mysql();
            $data['updated'] = eventDateTime::mysql_to_display($data['updated']); //Format for display
            $data['updated_by'] = substr(user::current_user_name(), 0, 64);

        }

        //Handle the removal of the profile picture
        if (isset($_POST['removeAttachmentLocation'])) {

            //If path is tampered with, the original image path will be different
            $path = $this->make_safe_text( $_POST['attachment_path'], 255);

            if ($originalFilePath == $path) {

                util::delete_file($_POST['attachment_path']);

                if ($mode == "edit") {
                    $model->update($id, array('attachment_path' => ''));
                }

                $data['attachment_path'] = "";
            }

        }

        //Do the magic

        if (isset($_POST['closeLocation'])) {

            //Go back to the index
            echo('<meta http-equiv="refresh" content="0; url=' . $returnurl . '">');
            exit();

        } elseif (isset($_POST['submitLocation']) || isset($_POST['submitAndCloseLocation'])) {
            //TODO add nonce to form and check it before accepting the submit

            if ($data['location_name'] == "") {
                $error['location_name'][] = __('Location is required', 'pw-event-manager');
                $hasError = true;
            }
            if ($data['profile'] == "") {
                $error['profile'][] = __('Location details are required', 'pw-event-manager');
                $hasError = true;
            }

            //Check for unique trainer name
            if ($mode == "new") {
                $results = $model->get_by_name_locale($data['location_name'], $data['locale']);

                if (isset($results[0]['id'])) {
                    $error['location_name'][] = __('Location must be unique for the selected display language', 'pw-event-manager');
                    $hasError = true;
                }
            }

            //Upload attached file
            if (isset($_FILES['location_file']['name']) && $_FILES['location_file']['name'] <> "") {
                $file = $_FILES['location_file'];

                $path = util::upload_file( $file, "route");

                if ($path == "") {

                    // There was an error uploading the image.
                    $error['location_file'][] = __('Upload of the file failed', 'pw-event-manager');
                    $hasError = true;

                } else {
                    $data['attachment_path'] = $path;
                }

            }

            $data['errors'] = $error;

            if ($hasError === false) {

                //Prepare record for insert or update
                $rec['location_name'] = substr($data['location_name'], 0, 64);
                $rec['locale'] = substr($data['locale'], 0, 3);
                $rec['profile'] = $data['profile'];
                $rec['maps_url'] = $data['maps_url'];
                $rec['attachment_path'] = substr( $data['attachment_path'], 0, 255 );
                $rec['updated'] = eventDateTime::current_time_mysql();
                $rec['updated_by'] = substr(user::current_user_name(), 0, 64);
                $rec['ip'] = util::get_user_ip();

                //Insert record
                if ($mode == "new") {
                    $id = $model->insert($rec);
                } elseif ($mode == "edit") {
                    //update record
                    $model->update($id, $rec);
                }

                if (isset($_POST['submitAndCloseLocation'])) {
                    //Go back to the index
                    echo('<meta http-equiv="refresh" content="0; url=' . $returnurl . '">');
                    exit();
                } else {
                    //Change into edit mode
                    $returnurl .= '&action=edit&id=' . $id;

                    echo('<meta http-equiv="refresh" content="0; url=' . $returnurl . '">');
                    exit();
                }

            } else {
                $content = locationEditHtmlView::render( __('New location', 'pw-event-manager'), $data);
            }

        } elseif ($mode == 'new') {

            $content = locationEditHtmlView::render( __('New location', 'pw-event-manager'), $data);

        } elseif ($mode == 'edit') {

            $content = locationEditHtmlView::render( __('Edit location', 'pw-event-manager'), $data);
        } else {

            $locations = new modelLocation();
            $location_table = new locationTable();

            $data = $locations->get_all();
            $location_table->set_table_data($data);
            $location_table->prepare_items();

            $title = __('Locations', 'pw-event-manager');

            $content = locationListHtmlView::render($title, $location_table);

        }

        return $content;
    }

    function render_action_codes()
    {
        require_once('views/view-action-code-list.php');
        require_once('views/view-action-code-edit.php');

        require_once('models/model-action-code.php');
        require_once('models/model-action-code-assignment.php');
        require_once('models/model-event.php');
        require_once('classes/class-action-code-table.php');

        require_once('classes/class-user.php');
        require_once('classes/class-eventdatetime.php');
        require_once('classes/class-util.php');

        $content = "";
        $mode = "";
        $id = 0;
        $data= array();
        $error = array();
        $hasError = false;

        $mdlActionCode = new modelActionCode();
        $mdlActionCodeAssign = new modelActionCodeAssignment();
        $mdlEvent = new modelEvent();
        $event_list = $mdlEvent->get_action_code_list();

        //Determine the requested action
        if (isset($_GET['action']) && $_GET['action'] == 'new') {
            $mode = "new";

        } elseif (isset($_GET['action']) && $_GET['action'] == 'edit') {
            $mode = "edit";

            if (isset($_GET['id'])) {
                $id = (int)$_GET['id'];
            }

        } else {
            $mode = "list";
        }

        if (isset($_GET['tab'] )) {
            $active_tab = $this->make_safe_text( $_GET['tab'], 24 );
        } else {
            $active_tab = "settings";
        }

        /*
        if ($mode == "list") {
            if (isset($_POST['action']) || isset($_POST['action2'])) {

                if ($_POST['action'] == "list_delete" || $_POST['action'] == "list_delete") {
                    foreach ($_POST['list'] as $key => $val) {

                        // Remove assignments
                        $assignments = $mdlActionCodeAssign->get_all_by_id( (int) $val);

                        foreach( $assignments as $key=>$val) {
                            $mdlActionCodeAssign->delete_by_id( (int) $val['id'] );
                        }

                        // Remove the action code
                        $mdlActionCode->update((int)$val, array('removed' => '1'));
                    }
                }
            }
        }
        */

        //Initialize Data
        if ($mode == "new") {

            $data['id'] = 0;
            $data['code'] = "";
            $data['check_string'] = "";
            $data['type'] = "training";
            $data['is_blocked'] = "0";
            $data['description'] = "";
            $data['start_date'] = "";
            $data['stop_date'] = "";
            $data['start_book_period'] = "";
            $data['stop_book_period'] = "";
            $data['amount'] = "0";
            $data['amount_type'] = "percent";
            $data['code_scope'] = "all";
            $data['max_use'] = 0;
            $data['event_ids'] = array();
            $data['updated'] = eventDateTime::current_time_mysql();
            $data['updated'] = eventDateTime::mysql_to_display($data['updated']); //Format for display
            $data['updated_by'] = substr(user::current_user_name(), 0, 64);

        } elseif ($mode == "edit") {

            $item = $mdlActionCode->get_by_id($id); //Get record by ID

            if( $item <> '' ) {
                $data['id'] = $item['id'];
                $data['code'] = $item['code'];
                $data['check_string'] = $item['check_string'];
                $data['type'] = $item['type'];
                $data['is_blocked'] = $item['is_blocked'];
                $data['description'] = $item['description'];
                $data['start_date'] = $item['start_date'];
                $data['stop_date'] = $item['stop_date'];
                $data['start_book_period'] = $item['start_book_period'];
                $data['stop_book_period'] = $item['stop_book_period'];
                $data['amount'] = $item['amount'];
                $data['amount_type'] = $item['amount_type'];
                $data['code_scope'] = $item['code_scope'];
                $data['max_use'] = $item['max_use'];
                $data['event_ids'] = $mdlActionCodeAssign->get_all_by_id($id);
                $data['updated'] = $item['updated'];
                $data['updated_by'] = $item['updated_by'];
                $data['ip'] = $item['ip'];
            } else {
                die('<p>Invalid record</p>');
            }
        }

        $data['all_events'] = $event_list;

        if ( isset($_POST['submitActionCode']) || isset($_POST['submitAndCloseActionCode']) ) {

            global $pw_config;

            $locale = substr(get_locale(), 0, 2 );
            $date_format = $pw_config['date_format'][$locale];

            if( $locale == 'nl' ) {
                $dec_point = ',';
                $thousands_sep = '.';
            } else if( $locale == 'de' ) {
                $dec_point = ',';
                $thousands_sep = '.';
            } else {
                $dec_point = '.';
                $thousands_sep = ',';
            }

            if( $active_tab == "settings") {

                $data['code'] = $this->make_safe_text( $_POST['code'], 32 );
                $data['check_string'] = $this->make_safe_text( $_POST['check_string'], 64 );
                $data['type'] = $this->make_safe_text( $_POST['type'], 16 );

                if( isset( $_POST['is_blocked'] ) ) {
                    $data['is_blocked'] = "1";
                } else {
                    $data['is_blocked'] = "";
                }

                $data['description'] = $this->make_safe_text( $_POST['description'], 255 );
                $data['start_date'] = eventDateTime::to_mysql_time( $date_format, $this->make_safe_text( $_POST['start_date'] ) );
                $data['stop_date'] = eventDateTime::to_mysql_time( $date_format, $this->make_safe_text( $_POST['stop_date'] ) );
                $data['amount'] = str_replace( $thousands_sep, '', $this->make_safe_text( $_POST['amount'] ));
                $data['amount'] = str_replace( $dec_point, '.',  $data['amount']);

                $data['amount_type'] = $this->make_safe_text( $_POST['amount_type'], 16 );

            } elseif( $active_tab == "assignment") {

                $data['start_book_period'] = eventDateTime::to_mysql_time( $date_format, $this->make_safe_text( $_POST['start_book_period'] ));
                $data['stop_book_period'] = eventDateTime::to_mysql_time( $date_format, $this->make_safe_text( $_POST['stop_book_period'] ));

                $data['code_scope'] = $this->make_safe_text( $_POST['code_scope'], 16 );
                $data['max_use'] = (int) $_POST['max_use'];

                $_POST['event_ids'] = isset( $_POST['event_ids'] ) ? $_POST['event_ids'] : array();
                $data['event_ids'] = $data['code_scope'] == "all" ? array() : $_POST['event_ids'];

            }

            $data['updated'] = eventDateTime::current_time_mysql();
            $data['updated'] = eventDateTime::mysql_to_display($data['updated']); //Format for display
            $data['updated_by'] = substr(user::current_user_name(), 0, 64);
        }

        if (isset($_POST['closeActionCode'])) {

            //Go back to the index
            $returnurl = "?page=pw-event-action-codes";
            echo('<meta http-equiv="refresh" content="0; url=' . $returnurl . '">');
            exit();
        } elseif (isset($_POST['submitActionCode']) || isset($_POST['submitAndCloseActionCode'])) {

            //TODO add nonce to form and check it before accepting the submit

            if( $active_tab == "settings") {

                if ($data['code'] == "") {
                    $error['code'][] = __('Action code is required', 'pw-event-manager');
                    $hasError = true;
                }

                if ( $data['check_string'] == "" && $data['type'] == 'company' ) {
                    $error['check_string'][] = __('Check string is required', 'pw-event-manager');
                    $hasError = true;
                }

                if ( (int) $data['amount'] == 0) {
                    $error['amount'][] = __('Discount is required', 'pw-event-manager');
                    $hasError = true;
                }

                if ($data['start_date'] == "") {
                    $error['start_date'][] = __('Valid from date is required', 'pw-event-manager');
                    $hasError = true;
                }

                if ($data['stop_date'] == "") {
                    $error['stop_date'][] = __('Valid until date is required', 'pw-event-manager');
                    $hasError = true;
                }

                //Check for unique code
                if ($mode == "new" && $data['code'] <> "" ) {
                    $results = $mdlActionCode->get_by_code($data['code']);

                    if (isset($results[0]['id'])) {
                        $error['code'][] = __('Code must be unique', 'pw-event-manager');
                        $hasError = true;
                    }
                }

            }

            $data['errors'] = $error;

            if ($hasError === false) {

                //Prepare record for insert or update
                $rec['code'] = $data['code'];
                $rec['check_string'] = $data['check_string'];
                $rec['type'] = $data['type'];
                $rec['is_blocked'] = $data['is_blocked'];
                $rec['description'] = $data['description'];
                $rec['start_date'] = $data['start_date'];
                $rec['stop_date'] = $data['stop_date'];
                $rec['start_book_period'] = $data['start_book_period'];
                $rec['stop_book_period'] = $data['stop_book_period'];
                $rec['amount'] = $data['amount'];
                $rec['amount_type'] = $data['amount_type'];
                $rec['code_scope'] = $data['code_scope'];
                $rec['max_use'] = $data['max_use'];
                $rec['removed'] = 0;
                $rec['updated'] = eventDateTime::current_time_mysql();
                $rec['updated_by'] = substr(user::current_user_name(), 0, 64);
                $rec['ip'] = util::get_user_ip();

                if ($mode == "new") {
                    $id = $mdlActionCode->insert($rec);

                    if( $id > 0 ) {
                        $mdlActionCodeAssign->set_from_array( $id, $data['event_ids'] );
                    }

                } elseif ($mode == "edit") {

                    $mdlActionCode->update($id, $rec);

                    if( $active_tab == "assignment") {
                        $mdlActionCodeAssign->set_from_array( $id, $data['event_ids'] );
                    }

                }

                if (isset($_POST['submitAndCloseActionCode'])) {
                    //Go back to the index
                    $returnurl = '?page=pw-event-action-codes';
                    echo('<meta http-equiv="refresh" content="0; url=' . $returnurl . '">');
                    exit();
                } else {
                    //Reload in edit mode
                    $returnurl = '?page=pw-event-action-codes&action=edit&id=' . $id . '&tab='.$active_tab;

                    echo('<meta http-equiv="refresh" content="0; url=' . $returnurl . '">');
                    exit();
                }
            } else {
                if ($mode == 'new') {
                    $content = actionCodeEditHtmlView::render( __('New action code', 'pw-event-manager'), $data, $mode, $active_tab);

                } elseif ($mode == 'edit') {
                    $content = actionCodeEditHtmlView::render( __('Edit action code', 'pw-event-manager'), $data, $mode, $active_tab);
                }
            }

        } elseif ($mode == 'new') {

            $content = actionCodeEditHtmlView::render( __('New action code', 'pw-event-manager'), $data, $mode, $active_tab);

        } elseif ($mode == 'edit') {
            $content = actionCodeEditHtmlView::render( __('Edit action code', 'pw-event-manager'), $data, $mode, $active_tab);
        } else {
            $codes = new modelActionCode();
            $code_table = new actionCodeTable();

            $data = $codes->get_all();

            $code_table->set_table_data($data);
            $code_table->prepare_items();

            $title = __('Action codes', 'pw-event-manager');

            $content = acionCodeListHtmlView::render($title, $code_table);
        }

        return $content;

    }

    function render_trash()
    {
        echo "Trash";
    }

    function render_settings()
    {
        global $pw;
        global $pw_config;

        require_once('views/view-setup-list.php');
        require_once('views/view-event-group-edit.php');
        require_once('models/model-group.php');
        require_once('classes/class-group-table.php');

        require_once('classes/class-user.php');
        require_once('classes/class-eventdatetime.php');
        require_once('classes/class-util.php');

        $content = "";
        $error = array();
        $hasError = false;
        $returnurl = "?page=pw-event-settings";
        $id = 0;
        $mode = "";
        $model_event_group = new modelGroup();

        //Determine the requested action
       if (isset($_GET['action']) && $_GET['action'] == 'new-group') {
            $mode = "new-group";

        } elseif (isset($_GET['action']) && $_GET['action'] == 'edit-group') {
            $mode = "edit-group";

            if (isset($_GET['id'])) {
                $id = (int)$_GET['id'];
            }

           // Invalid ID has been given. Switching to new-group mode.
            if( $id == 0 ) {
               $mode = "new-group";
            }

        } else {
            $mode = "list";
        }

        if (isset( $_GET['tab'] )) {
            $active_tab = $this->make_safe_text( $_GET['tab'], 24 );
        } else {
            $active_tab = "event_groups";
        }

        // Mark selected items as removed. This will move them to the admin trash
        // from where they can be restored when required.
        if ($mode == "list") {

            if (isset($_POST['action']) || isset($_POST['action2'])) {

                if ($_POST['action'] == "list_delete_event_group" || $_POST['action2'] == "list_delete_event_group") {
                    foreach ($_POST['list'] as $key => $val) {
                        $model_event_group->update((int)$val, array('removed' => '1'));
                    }
                }
            }
        }

        //Fill the data array with the appropriate data
        $data = array();

        if( $mode == "new-group") {

            $data['locale'] = strtoupper( $pw_config['lang_primary'] );
            $data['group_name'] = "";
            $data['updated'] = eventDateTime::current_time_mysql();
            $data['updated'] = eventDateTime::mysql_to_display($data['updated']); //Format for display
            $data['updated_by'] = substr(user::current_user_name(), 0, 64);

        } elseif ($mode == "edit-group") {

            $item = $model_event_group->get_by_id($id); //Get record by ID

            if( $item <> '' ) {

                $data['locale'] = $item['locale'];
                $data['group_name'] = $item['group_name'];
                $data['updated'] = $item['updated'];
                $data['updated_by'] = $item['updated_by'];
                $data['ip'] = $item['ip'];

            } else {
                die( '<p>'.__('Invalid record', 'pw-event-manager').'</p>' );
            }

        }

        //Override default data with $_POST when applicable
        if (isset($_POST['submitEventGroup']) || isset($_POST['submitAndCloseEventGroup']) ) {

            $data['locale'] = $this->make_safe_text( $_POST['locale'], 3);
            $data['group_name'] = $this->make_safe_text( $_POST['group_name'], 64 );
            $data['updated'] = eventDateTime::current_time_mysql();
            $data['updated'] = eventDateTime::mysql_to_display( $data['updated'] ); //Format for display
            $data['updated_by'] = substr(user::current_user_name(), 0, 64);

        }

        //Do the magic
        if (isset($_POST['closeEventGroup'])) {

            //Go back to the index
            echo('<meta http-equiv="refresh" content="0; url=' . $returnurl . '">');
            exit();

        } elseif (isset($_POST['submitEventGroup']) || isset($_POST['submitAndCloseEventGroup'])) {
            //TODO add nonce to form and check it before accepting the submit
            if ($data['group_name'] == "") {
                $error['group_name'][] = __('Name is required', 'pw-event-manager');
                $hasError = true;
            }

            //Check for unique group name
            if ($mode == "new-group") {
                $results = $model_event_group->get_by_name_locale($data['group_name'], $data['locale']);

                if (isset($results[0]['id'])) {
                    $error['group_name'][] = __('Name must be unique for the selected display language', 'pw-event-manager');
                    $hasError = true;
                }
            }

            $data['errors'] = $error;

            if ($hasError === false) {

                //Prepare record for insert or update
                $rec['group_name'] = substr($data['group_name'], 0, 64);
                $rec['locale'] = substr($data['locale'], 0, 3);
                $rec['updated'] = eventDateTime::current_time_mysql();
                $rec['updated_by'] = substr(user::current_user_name(), 0, 64);
                $rec['ip'] = util::get_user_ip();

                //Insert record
                if ($mode == "new-group") {
                    $id = $model_event_group->insert($rec);
                } elseif ($mode == "edit-group") {
                    //update record
                    $model_event_group->update($id, $rec);
                }

                if (isset($_POST['submitAndCloseEventGroup'])) {
                    //Go back to the index
                    echo('<meta http-equiv="refresh" content="0; url=' . $returnurl . '">');
                    exit();
                } else {
                    //Change into edit mode
                    $returnurl .= '&action=edit-group&id=' . $id;

                    echo('<meta http-equiv="refresh" content="0; url=' . $returnurl . '">');
                    exit();
                }

            } else {
                $content = eventGroupEditHtmlView::render( __('New event group', 'pw-event-manager'), $data);
            }

        } elseif ($mode == 'new-group') {

            $content = eventGroupEditHtmlView::render( __('New event group', 'pw-event-manager'), $data);

        } elseif ($mode == 'edit-group') {

            $content = eventGroupEditHtmlView::render( __('Edit group', 'pw-event-manager'), $data);
        } else {

            $groups = new modelGroup();
            $event_group_table = new groupTable();

            $data_groups = $groups->get_all();
            $event_group_table->set_table_data($data_groups);
            $event_group_table->prepare_items();

            $title = __('Settings', 'pw-event-manager');
            $content = setupListHtmlView::render( $title, $active_tab, $event_group_table );

        }

        return $content;
    }

    //----------------------------------------------------------------
    //                  DEBUG
    //----------------------------------------------------------------
    public function print_var($v)
    {
        print('<pre>');
        print_r($v);
        print('</pre>');
    }

    //----------------------------------------------------------------
    //                  SECURITY
    //----------------------------------------------------------------

    public function make_safe_text( $var, $max_length = 0 ) {

        $v = filter_var( $var, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES );

        if( $max_length > 0 ) {
            $v = substr( $v, 0, $max_length );
        }

        return $v;
    }
}

$pw = new pwEventCtrl();
?>