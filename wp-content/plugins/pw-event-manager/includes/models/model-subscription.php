<?php

/**
 * Direct access to the script is not allowed
 */
defined('ABSPATH') or die("No script kiddies please!");

if( !class_exists( 'modelSubscription' ) ):

    class modelSubscription
    {

        public $table = "";

        public function __construct() {

            global $pw;
            $this->table = $pw->mysql_table_names['subscriptions'];

            $this->table_event = $pw->mysql_table_names['events'];
            $this->table_event_date = $pw->mysql_table_names['event-date'];
            $this->table_location = $pw->mysql_table_names['locations'];

        }

        public function insert( $data ){

            global $wpdb;

            $wpdb->insert(
                $this->table,
                $data
            );

            return( $wpdb->insert_id );

        }

        public function update( $id, $data ){

            global $wpdb;

            $wpdb->update( $this->table, $data, array( 'id' => $id ), null, null );

        }

        public function get_all(){

            global $wpdb;

            $list = $wpdb->get_results("SELECT * FROM ".$this->table." WHERE removed='0' LIMIT 10000", ARRAY_A);

            return $list;
        }

        public function get_all_export() {
            global $wpdb;

            $ids = 0;
            if(isset($_POST['list'])){
                $ids = implode(",", $_POST['list']);
            }

            $qry = "SELECT * FROM ".$this->table." WHERE removed='0' AND status <> 'concept'";

            if($ids){
                $qry .= " and id in($ids)";
            }
            $qry .= " LIMIT 10000";

            $list = $wpdb->get_results( $qry, ARRAY_A);


            return $list;

        }


        public function get_all_not_concept(){

            global $wpdb;


            $pagenum = isset( $_REQUEST['paged'] ) ? absint( $_REQUEST['paged'] ) : 1;
            $orderby = isset( $_REQUEST['orderby'] ) ? $_REQUEST['orderby'] : " id ";
            $order = isset( $_REQUEST['order'] ) ? $_REQUEST['order'] : " desc ";
            $startRecord = ($pagenum-1) * 20;
            $pageRecords = 20;

            $qry = "SELECT e.* FROM ".$this->table." as e WHERE e.removed='0' AND e.status <> 'concept'";
            $search = false;

            if(isset($_REQUEST['s']) && $_REQUEST['s'] != '') {

                $qry = "SELECT e.*, p.first_name,p.middle_name,p.last_name,p.email,p.phone FROM ".$this->table." as e, wp_ev_participants as p  WHERE e.removed='0' AND e.status <> 'concept'";

                $searchWords = explode(' ', $_REQUEST['s']);

                $qry .= " and (";
                foreach ($searchWords as $k => $word) {
                    if($k)
                    {
                        $qry .= " or ";
                    }
                    $qry .= " first_name like '%" . $word . "%' ";
                    $qry .= " or middle_name like '%" . $word . "%' ";
                    $qry .= " or last_name like '%" . $word . "%' ";
                    $qry .= " or email like '%" . $word . "%' ";
                    $qry .= " or phone like '%" . $word . "%' ";
                    $qry .= " or company like '%" . $word . "%' ";
                }
                $qry .= ") ";
                $qry .= " and  p.subscriptionID = e.id  ";

                $startRecord = 0;
                $pageRecords = 10000;
                $search = true;

            }

            $list = $wpdb->get_results($qry, ARRAY_A);
            $totalCount = count($list);

            if($search && $totalCount> 20){
                $startRecord = ($pagenum-1) * 20;
                $pageRecords = 20;
            }

            $qry .= "  order by e.$orderby $order LIMIT $startRecord, $pageRecords ";

            $list = $wpdb->get_results($qry, ARRAY_A);
            $pageCount = count($list);

            return array("count" => $totalCount, "pageCount" => $pageCount, "list" => $list);
        }

        public function get_all_filtered( $event_id = 0, $event_date_id = 0, $location_id = 0 ) {

            //global $wpdb;

            //$list = $wpdb->get_results("SELECT * FROM ".$this->table." WHERE removed='0' AND status <> 'concept' LIMIT 10000", ARRAY_A);

            //return $list;

            global $wpdb;

            $pagenum = isset( $_REQUEST['paged'] ) ? absint( $_REQUEST['paged'] ) : 1;
            $orderby = isset( $_REQUEST['orderby'] ) ? $_REQUEST['orderby'] : " id ";
            $order = isset( $_REQUEST['order'] ) ? $_REQUEST['order'] : " desc ";
            $startRecord = ($pagenum-1) * 20;

            $qry = "SELECT TS.*, TL.id AS location_id ";
            $qry .= "FROM ".$this->table." AS TS ";
            $qry .= "INNER JOIN ".$this->table_event_date." AS TED ON TS.eventDateId = TED.id ";
            $qry .= "INNER JOIN ".$this->table_location." AS TL ON TED.locationID = TL.id ";
            $qry .= "INNER JOIN ".$this->table_event." AS TE ON TED.eventID = TE.id ";
            $qry.= "WHERE TS.removed='0' ";
            $qry.= "AND TS.status <> 'concept' ";

            if( $event_id > 0 ) {
              $qry.= "AND TED.eventID=".$event_id." ";
            }

            if( $event_date_id > 0 ) {
              $qry.= "AND TED.id=".$event_date_id." ";
            }

            if( $location_id > 0 ) {
              $qry.= "AND TED.locationID=".$location_id." ";
            }
            $list = $wpdb->get_results( $qry, ARRAY_A);
            $totalCount = count($list);

            $qry.= "order by $orderby $order LIMIT 0, 10000";

            $list = $wpdb->get_results( $qry, ARRAY_A);
            $pageCount = count($list);

            return array("count" => $totalCount, "pageCount" => $pageCount, "list" => $list);
        }

        public function get_by_id( $id ){
            global $wpdb;

            $qry = $wpdb->prepare(
                "SELECT * FROM ".$this->table." WHERE id='%d' LIMIT 1",
                $id
            );

            $list = $wpdb->get_results($qry, ARRAY_A);

            $result = "";
            if( $wpdb->num_rows > 0 ) {
                $result= $list[0];
            }

            return $result;
        }

    }

endif;

?>