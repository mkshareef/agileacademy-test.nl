<?php

/**
 * Direct access to the script is not allowed
 */
defined('ABSPATH') or die("No script kiddies please!");

if( !class_exists( 'modelInvoice' ) ):

    class modelInvoice
    {

        public $table = "";
        private $table_event = "";
        private $table_event_date = "";
        private $table_location = "";
        private $table_subscription = "";
        private $table_participant = "";
        private $table_invoice = "";

        public function __construct() {

            global $pw;
            $this->table = $pw->mysql_table_names['invoices'];

            $this->table_event = $pw->mysql_table_names['events'];
            $this->table_event_date = $pw->mysql_table_names['event-date'];
            $this->table_location = $pw->mysql_table_names['locations'];
            $this->table_subscription = $pw->mysql_table_names['subscriptions'];
            $this->table_participant = $pw->mysql_table_names['participants'];
            $this->table_invoice = $pw->mysql_table_names['invoices'];
        }

        public function insert( $data ){

            global $wpdb;

            $wpdb->insert(
                $this->table,
                $data
            );

            return( $wpdb->insert_id );

        }

        public function update( $id, $data ){

            global $wpdb;

            $wpdb->update( $this->table, $data, array( 'id' => $id ), null, null );

        }

        public function get_all(){

            global $wpdb;

            $list = $wpdb->get_results("SELECT * FROM ".$this->table." WHERE removed='0' LIMIT 10000", ARRAY_A);

            return $list;
        }

        public function get_all_filtered( $date ){

            global $wpdb;

            $qry = $wpdb->prepare(
                "SELECT * FROM ".$this->table.
                " WHERE invoice_date LIKE '%%%s%%' AND removed='0' ORDER BY invoice_nr DESC LIMIT 10000",
                $date
            );

            $list = $wpdb->get_results( $qry, ARRAY_A);

            return $list;
        }

        public function get_by_id( $id ){
            global $wpdb;

            $qry = $wpdb->prepare(
                "SELECT * FROM ".$this->table." WHERE id='%d' LIMIT 1",
                $id
            );

            $list = $wpdb->get_results( $qry, ARRAY_A);

            $result = "";
            if( $wpdb->num_rows > 0 ) {
                $result= $list[0];
            }

            return $result;
        }

        public function get_by_subscription_id( $subscription_id ){
            global $wpdb;

            $qry = $wpdb->prepare(
                "SELECT * FROM ".$this->table." WHERE subscriptionID='%d' LIMIT 1",
                $subscription_id
            );

            $list = $wpdb->get_results( $qry, ARRAY_A);

            $result = "";
            if( $wpdb->num_rows > 0 ) {
                $result= $list[0];
            }

            return $result;
        }

        public function get_by_invoice_nr( $invoice_nr ) {
            global $wpdb;

            $qry = $wpdb->prepare(
                "SELECT * FROM ".$this->table." WHERE invoice_nr='%s' LIMIT 1",
                $invoice_nr
            );

            $list = $wpdb->get_results( $qry, ARRAY_A);

            $result = "";
            if( $wpdb->num_rows > 0 ) {
                $result= $list[0];
            }

            return $result;
        }

        public function count_action_code_used( $action_code, $exclude_invoice_id ) {
            global $wpdb;

            $exclude_invoice_id = (int) $exclude_invoice_id;

            $qry = $wpdb->prepare(
                "SELECT id, invoice_nr_subscribed FROM ".$this->table.
                " WHERE invoice_action_code='%s' AND removed='0'",
                $action_code
            );

            $list = $wpdb->get_results( $qry, ARRAY_A);

            $count = 0;
            if( $wpdb->num_rows > 0 ) {

                foreach( $list as $row ) {
                    if( is_null( $row['invoice_nr_subscribed'] ) == false && empty( $row['invoice_nr_subscribed'] ) == false ) {

                        if( $exclude_invoice_id > 0 ) {
                            if( $exclude_invoice_id <> $row['id'] ) {
                                $count += (int) $row['invoice_nr_subscribed'];
                            }
                        } else {
                            $count += (int) $row['invoice_nr_subscribed'];
                        }
                    }
                }

            }

            return $count;

        }

        public function get_last_used_invoice_nr( $prefix ) {
            global $wpdb;

            $qry = $wpdb->prepare(
                "SELECT * FROM ".$this->table.
                " WHERE invoice_nr LIKE '%%%s%%' AND invoice_type='debet' AND removed='0' ORDER BY invoice_nr DESC LIMIT 1",
                $prefix
            );

            $list = $wpdb->get_results( $qry, ARRAY_A);

            $result = "";
            if( $wpdb->num_rows > 0 ) {
                $result = $list[0]['invoice_nr'];
            }

            return $result;
        }

        public function get_by_transaction_reference( $transaction_reference ){
            global $wpdb;

            $qry = $wpdb->prepare(
                "SELECT * FROM ".$this->table." WHERE transaction_reference='%s' AND removed='0' LIMIT 1",
                $transaction_reference
            );

            $list = $wpdb->get_results( $qry, ARRAY_A);

            $result = "";
            if( $wpdb->num_rows > 0 ) {
                $result= $list[0];
            }

            return $result;
        }

        public function get_turnover_report( $date_from, $date_to ) {

            global $wpdb;

            $qry = "SELECT TE.id AS event_id, TE.name AS event_name, ";
            $qry .= "TED.id AS event_date_id, TED.start_date as event_start_date, TED.start_time as event_start_time,  TED.stop_time as event_stop_time, ";
            $qry .= "TL.id as location_id, TL.location_name as event_location, TS.id as subscription_id, TS.status as subscription_status, ";
            $qry .= "TI.invoice_nr, TI.invoice_type, ";
            $qry .= "TS.company as subscription_company, TP.salutation as part_salutation, TP.first_name as part_first_name, TP.middle_name as part_middle_name, ";
            $qry .= "TP.last_name as part_last_name, TP.eventDateID as part_event_date_id, TP.status as part_status, ";
            $qry .= "CASE WHEN TED.id <> TP.eventDateID THEN 2 ELSE 1 END as discount_prio ";
            $qry .= "FROM ".$this->table_event." AS TE ";
            $qry .= "INNER JOIN ".$this->table_event_date." AS TED ON TED.eventID = TE.id ";
            $qry .= "INNER JOIN ".$this->table_location." AS TL ON TED.locationID = TL.id ";
            $qry .= "INNER JOIN ".$this->table_subscription." AS TS ON TS.eventDateId = TED.id ";
            $qry .= "INNER JOIN ".$this->table_invoice." AS TI ON TI.subscriptionID = TS.id ";
            $qry .= "INNER JOIN ".$this->table_participant." AS TP ON TP.subscriptionID = TS.id ";
            $qry .= "WHERE TE.removed='0' AND TS.removed='0' AND TS.status='confirmed' AND TP.status = 'confirmed' AND TI.invoice_type <> 'credit' ";
            $qry .= "ORDER BY TE.name ASC, TED.start_date DESC, TED.start_time ASC, TI.invoice_nr DESC, discount_prio ASC ";
            $qry .= "LIMIT 25000";

            $list = $wpdb->get_results( $qry, ARRAY_A);

            $result = "";
            $invoices = "";

            global $pw;

            if( $wpdb->num_rows > 0 ) {

                $result = $list;

                //Extract the subscription ids from the result set
                $sub_ids = array_unique( array_column($result, 'subscription_id') );
                $sub_ids_select = join( ',', $sub_ids );

                //Get the credit and debet invoices
                $qry = "SELECT TI.id, subscriptionID, invoice_nr, invoice_type, invoice_nr_subscribed, discount_nr, pay_online_discount, price_single_evt, discount_amount, discount_type, ";
                $qry .= "TS.eventID AS sub_event_id, TS.eventDateId as sub_event_date_id ";
                $qry .= "FROM ".$this->table_invoice." AS TI ";
                $qry .= "INNER JOIN ".$this->table_subscription." AS TS ON TS.id = subscriptionID ";
                $qry .= "WHERE subscriptionID IN ($sub_ids_select) ";
                $qry .= "AND TI.removed='0' ";
                $qry .= "ORDER BY subscriptionID ASC ";
                $invoices = $wpdb->get_results( $qry, ARRAY_A);

                //Get the revenue for each subscription, taking credit invoices into account
                $revenue_per_sub = "";

                foreach( $sub_ids as $sub_key => $sub_row ) {

                    $rev_rec['invoice_nr'] = "";
                    $rev_rec['subscription_id'] = $sub_row;
                    $rev_rec['invoice_nr_subscribed'] = 0;
                    $rev_rec['discount_nr'] = 0;
                    $rev_rec['discount_distributed'] = 0;
                    $rev_rec['price_single_evt'] = 0;
                    $rev_rec['discount_amount'] = 0;
                    $rev_rec['pay_online_discount_amount'] = 0;

                    foreach( $invoices as $inv_key => $inv_row ) {

                        if( $inv_row['subscriptionID'] == $sub_row ) {

                            if( $inv_row['invoice_type'] == 'debet') {

                                $rev_rec['invoice_nr'] = $inv_row['invoice_nr'];
                                $rev_rec['sub_event_id'] = $inv_row['sub_event_id'];
                                $rev_rec['sub_event_date_id'] = $inv_row['sub_event_date_id'];
                                $rev_rec['invoice_nr_subscribed'] = $inv_row['invoice_nr_subscribed'];
                                $rev_rec['discount_nr'] += $inv_row['discount_nr'];
                                $rev_rec['price_single_evt'] += $inv_row['price_single_evt'];
                                $rev_rec['discount_amount'] += $inv_row['discount_amount']; //Discount for a single subscriber.

                                // Online discount is applied on invoice level.
                                // For the revenue calculation we distribute it over the subscribers
                                if( isset( $inv_row['pay_online_discount'] )) {
                                    $rev_rec['pay_online_discount_amount'] += ( $inv_row['pay_online_discount'] / $inv_row['invoice_nr_subscribed'] );
                                }

                            } else if( $inv_row['invoice_type'] == 'credit') {

                                $rev_rec['invoice_nr_subscribed'] = $inv_row['invoice_nr_subscribed'];
                                $rev_rec['discount_nr'] -= $inv_row['discount_nr'];
                                $rev_rec['price_single_evt'] -= $inv_row['price_single_evt'];
                                $rev_rec['discount_amount'] -= $inv_row['discount_amount']; //Discount for a single subscriber.

                                // Online discount is applied on invoice level.
                                // For the revenue calculation we distribute it over the subscribers
                                if( isset( $inv_row['pay_online_discount'] )) {
                                    $rev_rec['pay_online_discount_amount'] -= ( $inv_row['pay_online_discount'] / $inv_row['invoice_nr_subscribed'] );
                                }

                            }

                        }

                    }

                    $revenue_per_sub[] = $rev_rec;
                }

                //Distribute the revenue over the selected subscribers.
                //Discount is first distributed to the subscribers that are not moved to a different date.
                //If discount is left after this, it is distributed to subscribers that are moved to a different date.

                //First pass = level 1 discount distribution
                foreach( $result as $res_key => $res_row ) {

                    $result[ $res_key ]['price_single_evt'] = 0.00;
                    $result[ $res_key ]['discount_amount'] = 0.00;

                    foreach( $revenue_per_sub as $rev_key => $rev_row ) {

                        if( ($res_row['invoice_nr'] == $rev_row['invoice_nr']) && ($res_row['event_date_id'] == $rev_row['sub_event_date_id'])  ) {

                            $result[ $res_key ]['price_single_evt'] = $rev_row['price_single_evt'];

                            if( $res_row['discount_prio'] == 1 ) {

                                if( $rev_row['discount_nr'] > 0 ) {
                                    //echo $rev_row['discount_nr'].' / '.$rev_row['invoice_nr'].' / '.$rev_row['discount_amount'].'<br>';
                                    $result[ $res_key ]['discount_amount'] = $rev_row['discount_amount'];
                                    $revenue_per_sub[$rev_key]['discount_nr'] -= 1;
                                }

                                $result[ $res_key ]['discount_amount'] += $rev_row['pay_online_discount_amount'];
                            }
                        }
                    }
                }

                //Second pass = level 2 discount distribution
                foreach( $result as $res_key => $res_row ) {

                    foreach( $revenue_per_sub as $rev_key => $rev_row ) {

                        if( ($res_row['invoice_nr'] == $rev_row['invoice_nr']) && ($res_row['event_date_id'] == $rev_row['sub_event_date_id'])  ) {
                            $result[ $res_key ]['price_single_evt'] = $rev_row['price_single_evt'];

                            if( $res_row['discount_prio'] == 2 ) {

                                if( $rev_row['discount_nr'] > 0 ) {
                                    $result[ $res_key ]['discount_amount'] = $rev_row['discount_amount'];
                                    $revenue_per_sub[$rev_key]['discount_nr'] -= 1;
                                }

                                $result[ $res_key ]['discount_amount'] += $rev_row['pay_online_discount_amount'];
                            }
                        }
                    }
                }

                //Get the unique events from the results
                $summary = "";

                foreach( $result as $res_key => $res_row ) {

                    $sum_rec["event_id"] = $res_row["event_id"];
                    $sum_rec["event_date_id"] = $res_row["event_date_id"];
                    $sum_rec["event_name"] = $res_row["event_name"];
                    $sum_rec["event_start_date"] = $res_row["event_start_date"];
                    $sum_rec["event_start_time"] = $res_row["event_start_time"];
                    $sum_rec["event_stop_time"] = $res_row["event_stop_time"];
                    $sum_rec["location_id"] = $res_row["location_id"];
                    $sum_rec["event_location"] = $res_row["event_location"];

                    $sum_rec['nr_participants'] = 0;
                    $sum_rec['gross_revenue'] = 0.00;
                    $sum_rec['discount'] = 0.00;
                    $sum_rec['net_revenue'] = 0.00;

                    $summary[] = $sum_rec;
                }

                $summary = array_map("unserialize", array_unique(array_map("serialize", $summary)));

                //Foreach event, calculate the totals

                foreach( $result as $res_key => $res_row ) {

                    foreach( $summary as $sum_key => $sum_row ) {

                        //A participant can be moved to a different date for the event.
                        // Therefore look at the actual date of the participant, instead of the event date of the subscription
                        if( $res_row['event_id'] == $sum_row['event_id'] && $res_row['part_event_date_id'] == $sum_row['event_date_id'] ) {

                            $summary[$sum_key]['nr_participants'] += 1;
                            $summary[$sum_key]['gross_revenue'] += $res_row['price_single_evt'];
                            $summary[$sum_key]['discount'] += $res_row['discount_amount'];
                            $summary[$sum_key]['net_revenue'] += ( $res_row['price_single_evt'] - $res_row['discount_amount'] );
                        }

                    }

                }


                //Prepare for output
                foreach( $summary as $sum_key => $sum_row ) {
                    $summary[$sum_key]['gross_revenue'] = number_format($summary[$sum_key]['gross_revenue'], 2, ',', '');
                    $summary[$sum_key]['discount'] = number_format($summary[$sum_key]['discount'], 2, ',', '');
                    $summary[$sum_key]['net_revenue'] = number_format($summary[$sum_key]['net_revenue'], 2, ',', '');
                }

                //Filter by date
                $result = "";

                foreach( $summary as $sum_key=>$sum_row ) {

                    //Filter by date
                    $evt_date = $sum_row['event_start_date'];

                    $include = true;
                    if( $date_from <> '' ) {
                        if( strtotime( $evt_date ) < strtotime( $date_from ) ) {
                            $include = false;
                        }
                    }

                    if( $date_to <> '' ) {
                        if( strtotime( $evt_date ) > strtotime( $date_to ) ) {
                            $include = false;
                        }
                    }

                    if( $include == true ) {
                        $result[] = $sum_row;
                    }

                }

            }

            return $result;
        }


    }

endif;

?>