<?php

/**
 * Direct access to the script is not allowed
 */
defined('ABSPATH') or die("No script kiddies please!");

if( !class_exists( 'modelSession' ) ):

    class modelSession
    {

        public $table = "";

        public function __construct() {

            global $pw;
            $this->table = $pw->mysql_table_names['sessions'];

        }

        private function crypto_rand_secure($min, $max) {
            $range = $max - $min;
            if ($range < 0) return $min; // not so random...
            $log = log($range, 2);
            $bytes = (int) ($log / 8) + 1; // length in bytes
            $bits = (int) $log + 1; // length in bits
            $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
            do {
                $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
                $rnd = $rnd & $filter; // discard irrelevant bits
            } while ($rnd >= $range);
            return $min + $rnd;
        }

        public function get_new_token(){
            $length = 32;
            $token = "";
            $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
            $codeAlphabet.= "0123456789";
            for($i=0;$i<$length;$i++){
                $token .= $codeAlphabet[$this->crypto_rand_secure(0,strlen($codeAlphabet))];
            }

            return $token;
        }

        public function start_session( $session_data ) {
            $id = $this->insert($session_data);
            return $id;
        }

        public function get_by_token( $token ) {
            global $wpdb;

            $token = substr( $token, 0, 32 );

            $qry = $wpdb->prepare(
                "SELECT * FROM ".$this->table." WHERE nonce='%s' LIMIT 1",
                $token
            );

            $list = $wpdb->get_results( $qry, ARRAY_A);

            $result = "";
            if( $wpdb->num_rows > 0 ) {
                $result= $list[0];
            }

            return $result;
        }

        public function get_by_invoice_id( $invoice_id ) {
            global $wpdb;

            $qry = $wpdb->prepare(
                "SELECT * FROM ".$this->table." WHERE invoiceID='%d' LIMIT 1",
                $invoice_id
            );

            $list = $wpdb->get_results( $qry, ARRAY_A);

            $result = "";
            if( $wpdb->num_rows > 0 ) {
                $result= $list[0];
            }

            return $result;
        }

        public function expire_sessions() {

            //Get all records
            $sessions = $this->get_all();

            //Loop all records and check if they are expired. Delete expired.
            if( is_array($sessions) ) {
                $now_time = strtotime( current_time("Y-m-d H:i:s") );

                foreach( $sessions as $session ) {
                    $expire_time = strtotime($session['expire_time']);

                    if ($expire_time <= $now_time) {
                        $this->delete($session['id']);
                    }
                }
            }

        }

        public function insert( $data ){

            global $wpdb;

            $wpdb->insert(
                $this->table,
                $data
            );

            return( $wpdb->insert_id );

        }

        public function update( $id, $data ){

            global $wpdb;

            $wpdb->update( $this->table, $data, array( 'id' => $id ), null, null );

        }

        public function get_all(){

            global $wpdb;

            $list = $wpdb->get_results("SELECT * FROM ".$this->table." LIMIT 10000", ARRAY_A);

            return $list;
        }

        public function get_by_id( $id ){
            global $wpdb;

            $qry = $wpdb->prepare(
                "SELECT * FROM ".$this->table." WHERE id='%d' LIMIT 1",
                $id
            );

            $list = $wpdb->get_results( $qry, ARRAY_A);

            $result = "";
            if( $wpdb->num_rows > 0 ) {
                $result= $list[0];
            }

            return $result;
        }

        public function delete( $id ){
            global $wpdb;

            $id = (int) $id;

            $wpdb->delete( $this->table, array( 'id' => $id ) );
        }

    }

endif;

?>