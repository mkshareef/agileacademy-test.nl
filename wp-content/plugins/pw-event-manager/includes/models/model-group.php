<?php

/**
 * Direct access to the script is not allowed
 */
defined('ABSPATH') or die("No script kiddies please!");

if( !class_exists( 'modelGroup' ) ):

    class modelGroup
    {

        public $table = "";

        public function __construct() {

            global $pw;
            $this->table = $pw->mysql_table_names['groups'];

        }

        public function insert( $data ){

            global $wpdb;

            $wpdb->insert(
                $this->table,
                $data
            );

            return( $wpdb->insert_id );

        }

        public function update( $id, $data ){

            global $wpdb;

            $wpdb->update( $this->table, $data, array( 'id' => $id ), null, null );

        }

        public function get_all(){

            global $wpdb;

            $list = $wpdb->get_results("SELECT * FROM ".$this->table." WHERE removed='0' ORDER BY group_name ASC LIMIT 10000", ARRAY_A);

            return $list;
        }

        public function get_all_by_locale( $locale ){

            global $wpdb;

            $qry = $wpdb->prepare(
                "SELECT * FROM ".$this->table." WHERE removed='0' AND locale='%s' ORDER BY group_name ASC LIMIT 10000",
                $locale
            );

            $list = $wpdb->get_results( $qry, ARRAY_A);

            return $list;

        }

        public function get_by_id( $id ){
            global $wpdb;

            $qry = $wpdb->prepare(
                "SELECT * FROM ".$this->table." WHERE id='%d' LIMIT 1",
                $id
            );

            $list = $wpdb->get_results( $qry , ARRAY_A);

            $result = "";
            if(  $wpdb->num_rows > 0 ) {
                $result = $list[0];
            }

            return $result;
        }

        public function get_by_name_locale( $name, $locale ){
            global $wpdb;

            $qry = $wpdb->prepare(
                "SELECT * FROM ".$this->table." WHERE group_name='%s' AND removed='0' AND locale='%s'",
                $name,
                $locale
            );

            $list = $wpdb->get_results( $qry , ARRAY_A);

            return $list;
        }

    }

endif;

?>