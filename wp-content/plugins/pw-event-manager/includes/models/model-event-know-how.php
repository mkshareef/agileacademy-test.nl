<?php

/**
 * Direct access to the script is not allowed
 */
defined('ABSPATH') or die("No script kiddies please!");


if( !class_exists( 'modelEventKnowHow' ) ):

class modelEventKnowHow {

    public $table = "";

    public function __construct() {

        global $pw;
        $this->table = $pw->mysql_table_names['event-know-how'];

    }

    public function insert( $event_id, $trainer_id ){

        global $wpdb;

        $wpdb->insert(
            $this->table,
            array( 'eventID'=>$event_id, 'know_how'=>$trainer_id )
        );

        return( $wpdb->insert_id );

    }

    public function update( $id, $data ){

        global $wpdb;

        $wpdb->update( $this->table, $data, array( 'id' => $id ), null, null );

    }

    public function set_from_array( $event_id, $trainer_ids ) {

        $event_id = (int) $event_id;
        $del_list = array();
        $add_list = array();

        if( ! empty( $trainer_ids ) && $event_id > 0 ) {

            //Get current list (Select * with $event_id)
            $db_list = $this->get_all_by_id( $event_id );

            if( ! empty($db_list) ) {

                //Ids in the db that are not in the trainer_ids should be removed
                foreach( $db_list as $key=>$in_db ) {

                    $found = false;
                    foreach( $trainer_ids as $k=>$id ) {
                        if( $in_db['know_how'] == $id ) {
                            $found = true;
                        }
                    }
                    if( $found === false ) {
                        $del_list[] = $in_db['know_how'];
                    }

                }

                //Ids in the trainer_ids that are not in the  db should be added
                foreach( $trainer_ids as $k=>$id ) {
                    $found = false;
                    foreach( $db_list as $key=>$in_db ) {

                        if( $in_db['know_how'] == $id ) {
                            $found = true;
                        }

                    }
                    if( $found === false ) {
                        $add_list[] = $id;
                    }
                }

            } else {
                foreach( $trainer_ids as $k=>$id ) {
                    $add_list[] = $id;
                }
            }

            //Process the ids to be deleted
            foreach($del_list as $k=>$id ) {
                $this->delete($event_id, $id);
            }

            //Process the ids to be added
            foreach($add_list as $k=>$id ) {
                $this->insert($event_id, $id);
            }

        }

    }

    public function get_all_by_id( $eventId ){

        global $wpdb;

        $qry = $wpdb->prepare(
            "SELECT * FROM ".$this->table." WHERE eventID='%d'",
            $eventId
        );

        $list = $wpdb->get_results( $qry, ARRAY_A);

        return $list;
    }

    public function delete( $event_id, $trainer_id ) {

        global $wpdb;
        $wpdb->delete( $this->table, array( 'eventID'=>$event_id, 'know_how'=>$trainer_id ), array( '%d', '%s') );

    }


}

endif;

?>