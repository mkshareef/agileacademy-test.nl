<?php

/**
 * Direct access to the script is not allowed
 */
defined('ABSPATH') or die("No script kiddies please!");

if( !class_exists( 'modelEventDate' ) ):

    class modelEventDate
    {

        public $table = "";
        private $table_event = "";
        private $table_location = "";

        public function __construct() {

            global $pw;
            $this->table = $pw->mysql_table_names['event-date'];
            $this->table_event = $pw->mysql_table_names['events'];
            $this->table_location = $pw->mysql_table_names['locations'];

        }

        public function insert( $data ){

            global $wpdb;

            $wpdb->insert(
                $this->table,
                $data
            );

            return( $wpdb->insert_id );

        }

        public function update( $id, $data ){

            global $wpdb;

            $wpdb->update( $this->table, $data, array( 'id' => $id ), null, null );

        }

        /**
         * Read all groups from database and return and array of group objects
         * @return array of group objects (max. 10.000)
         */
        public function get_all(){

            global $wpdb;

            $list = $wpdb->get_results("SELECT * FROM ".$this->table." WHERE removed='0' ORDER BY start_date ASC LIMIT 10000", ARRAY_A);

            return $list;
        }

        public function get_all_filtered( $event_id = 0, $event_date_id = 0 , $location_id = 0 ) {

            global $wpdb;

            $qry = "SELECT TED.eventID, TE.name, TE.locale, ";
            $qry .= "TED.id AS eventDateID, TED.start_date, TED.start_time,  TED.stop_time, ";
            $qry .= "TED.locationID, TL.location_name ";
            $qry .= "FROM ".$this->table." AS TED ";
            $qry .= "INNER JOIN ".$this->table_event." AS TE ON TED.eventID = TE.id ";
            $qry .= "INNER JOIN ".$this->table_location." AS TL ON TED.locationID = TL.id ";
            $qry.= "WHERE TE.removed='0' ";

            if( $event_id > 0 ) {
                $qry.= "AND TED.eventID=".$event_id." ";
            }

            if( $event_date_id > 0 ) {
                $qry.= "AND TED.id=".$event_date_id." ";
            }

            if( $location_id > 0 ) {
                $qry.= "AND TED.locationID=".$location_id." ";
            }

            $qry.= "ORDER BY TE.name ASC, TED.start_date DESC LIMIT 10000";
            $list = $wpdb->get_results( $qry, ARRAY_A);

            return $list;

        }

        public function get_all_by_locale( $locale ){

            global $wpdb;

            $qry = $wpdb->prepare(
                "SELECT * FROM ".$this->table." WHERE removed='0' AND locale='%s' ORDER BY group_name ASC LIMIT 10000",
                $locale
            );

            $list = $wpdb->get_results( $qry, ARRAY_A );

            return $list;

        }

        public function get_all_by_event_id( $eventId ){

            global $wpdb;

            $qry = $wpdb->prepare(
                "SELECT * FROM ".$this->table." WHERE removed='0' AND eventID='%d' LIMIT 10000",
                $eventId
            );

            $list = $wpdb->get_results($qry, ARRAY_A);

            return $list;

        }

        public function get_by_event_id( $eventId ){
            global $wpdb;

            $qry = $wpdb->prepare(
                "SELECT * FROM ".$this->table." WHERE removed='0' AND eventID='%d' LIMIT 1",
                $eventId
            );

            $list = $wpdb->get_results( $qry, ARRAY_A );

            $result = "";
            if( $wpdb->num_rows > 0 ) {
                $result= $list[0];
            }

            return $result;
        }

        public function get_by_event_id_and_id( $eventId, $id ){
            global $wpdb;

            $qry = $wpdb->prepare(
                "SELECT * FROM ".$this->table." WHERE removed='0' AND eventID='%d' AND id='%d' LIMIT 1",
                array($eventId, $id)
            );

            $list = $wpdb->get_results( $qry, ARRAY_A );

            $result = "";
            if( $wpdb->num_rows > 0 ) {
                $result= $list[0];
            }

            return $result;
        }

        public function get_by_key_id( $id ){
            global $wpdb;

            $qry = $wpdb->prepare(
                "SELECT * FROM ".$this->table." WHERE id='%d' LIMIT 1",
                $id
            );

            $list = $wpdb->get_results($qry, ARRAY_A);

            $result = "";
            if( $wpdb->num_rows > 0 ) {
                $result= $list[0];
            }

            return $result;
        }

        public function get_planning_by_id( $eventId ){
            global $wpdb;

            $now = current_time('Y-m-d');

            $qry = $wpdb->prepare(
                "SELECT * FROM ".$this->table.
                " WHERE eventID='%d' AND removed='0' AND start_date > '".$now."'".
                " ORDER BY start_date ASC",
                $eventId
            );

            $list = $wpdb->get_results( $qry, ARRAY_A);

            $result = "";
            if( $wpdb->num_rows > 0 ) {
                $result= $list;
            }

            return $result;
        }

    }

endif;

?>