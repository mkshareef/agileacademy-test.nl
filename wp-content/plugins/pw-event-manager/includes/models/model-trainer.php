<?php

/**
 * Direct access to the script is not allowed
 */
defined('ABSPATH') or die("No script kiddies please!");

if( !class_exists( 'modelTrainer' ) ):

    class modelTrainer
    {

        public $table = "";

        public function __construct() {

            global $pw;
            $this->table = $pw->mysql_table_names['trainers'];

        }

        public function insert( $data ){

            global $wpdb;

            $wpdb->insert(
                $this->table,
                $data
            );

            return( $wpdb->insert_id );

        }

        public function update( $id, $data ){

            global $wpdb;

            $wpdb->update( $this->table, $data, array( 'id' => $id ), null, null );

        }

        public function get_all(){

            global $wpdb;

            $list = $wpdb->get_results("SELECT * FROM ".$this->table." WHERE removed='0' LIMIT 10000", ARRAY_A);

            return $list;
        }

        public function get_by_id( $trainerId ){
            global $wpdb;

            $qry = $wpdb->prepare(
                "SELECT * FROM ".$this->table." WHERE id='%d' LIMIT 1",
                $trainerId
            );

            $list = $wpdb->get_results( $qry, ARRAY_A);

            $result = "";
            if(  $wpdb->num_rows > 0 ) {
                $result = $list[0];
            }

            return $result;
        }

        public function get_by_locale_sorted( $locale ){
            global $wpdb;

            $qry = $wpdb->prepare(
                "SELECT * FROM ".$this->table.
                " WHERE removed='0' AND locale='%s'".
                " ORDER BY trainer_name ASC",
                $locale
            );

            $list = $wpdb->get_results( $qry, ARRAY_A);

            return $list;
        }

        public function get_by_name_locale( $trainerName, $locale ){
            global $wpdb;

            $qry = $wpdb->prepare(
                "SELECT * FROM ".$this->table.
                " WHERE trainer_name='%s' AND removed='0' AND locale='%s'",
                $trainerName,
                $locale
            );

            $list = $wpdb->get_results( $qry, ARRAY_A);

            return $list;
        }

    }

endif;

?>