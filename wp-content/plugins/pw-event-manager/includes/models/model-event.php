<?php

/**
 * Direct access to the script is not allowed
 */
defined('ABSPATH') or die("No script kiddies please!");


if( !class_exists( 'modelEvent' ) ):

class modelEvent {

    public $table = "";

    public function __construct() {

        global $pw;
        $this->table = $pw->mysql_table_names['events'];

    }

    public function insert( $data ){

        global $wpdb;

        $wpdb->insert(
            $this->table,
            $data
        );

        return( $wpdb->insert_id );

    }

    public function update( $id, $data ){

        global $wpdb;

        $wpdb->update( $this->table, $data, array( 'id' => $id ), null, null );

    }

    public function get_all(){

        global $wpdb;

        $list = $wpdb->get_results("SELECT * FROM ".$this->table." WHERE removed='0' ORDER BY name ASC LIMIT 10000", ARRAY_A);

        return $list;
    }

    public function get_action_code_list(){

        global $wpdb;

        $list = $wpdb->get_results("SELECT id, name, locale FROM ".$this->table." WHERE removed='0' AND publication_status='publish' ORDER BY locale ASC, name ASC LIMIT 10000", ARRAY_A);

        return $list;
    }

    public function get_event_list_by_locale( $locale = '' ){

        global $wpdb;
        global $pw_config;

        if( $locale == '' ) {
            $locale = strtolower( $pw_config['lang_primary'] );
        }

        $qry = $wpdb->prepare(
            "SELECT id, name FROM ".$this->table.
            " WHERE removed='0' AND publication_status='publish' AND locale='%s' ORDER BY name ASC LIMIT 10000",
            $locale
        );

        $list = $wpdb->get_results( $qry, ARRAY_A);

        return $list;
    }

    public function get_by_id( $id ){
        global $wpdb;

        $qry = $wpdb->prepare(
            "SELECT * FROM ".$this->table." WHERE id='%d' LIMIT 1",
            $id
        );

        $list = $wpdb->get_results( $qry, ARRAY_A);

        $result = "";
        if(  $wpdb->num_rows > 0 ) {
            $result = $list[0];
        }

        return $result;
    }


    public function search_by_locale( $search, $locale = "" ) {
        global $wpdb;
        global $pw_config;

        if( $locale == "" ) {
            $locale = strtolower( $pw_config['lang_primary'] );
        }

        $keywords = explode( " ", $search );
        $totalKeywords = count($keywords);

        $qry_title = "";
        $qry_title_keys = array();
        $qry_description = "";
        $qry_description_keys = array();
        $qry_html_description = "";
        $qry_html_description_keys = array();

        for($i=0 ; $i < $totalKeywords; $i++){
            $qry_title .= $qry_title == '' ? " name LIKE '%%%s%%' " : " AND name LIKE '%%%s%%' ";
            $qry_title_keys[] = $keywords[$i];

            $qry_description .= $qry_description =='' ? " description LIKE '%%%s%%' " : " AND description LIKE '%%%s%%' ";
            $qry_description_keys[] = $keywords[$i];

            $qry_html_description .= $qry_html_description == '' ? " html_description LIKE '%%%s%%' " : " AND html_description LIKE '%%%s%%' ";
            $qry_html_description_keys[] = $keywords[$i];
        }

        if( $search == "" ) {

            $qry = $wpdb->prepare(
                "SELECT * FROM ".$this->table.
                " WHERE removed='0' AND publication_status='publish' AND locale='%s' ORDER BY name ASC LIMIT 10000",
                $locale
            );

            $list = $wpdb->get_results( $qry, ARRAY_A);

        } else {

            $qry_locale[] = $locale;

            $prepare_keys = array_merge( $qry_locale, $qry_title_keys,$qry_title_keys,$qry_title_keys );

            $qry = $wpdb->prepare(
                "SELECT * FROM ".$this->table.
                " WHERE removed='0' AND publication_status='publish' AND locale='%s' AND ".
                "( (".$qry_title.") OR (".$qry_description.") OR (".$qry_html_description."))".
                " ORDER BY name ASC LIMIT 10000",
                $prepare_keys
            );

            $list = $wpdb->get_results( $qry, ARRAY_A);
        }

        $result = "";
        if(  $wpdb->num_rows > 0 ) {
            $result = $list;
        }

        return $result;
    }

    public function slug_exists( $slug, $locale, $current_event_id ) {
        global $wpdb;

        $qry = $wpdb->prepare(
            "SELECT * FROM ".$this->table.
            " WHERE url_slug='%s' AND id <> '%d' AND locale='%s' LIMIT 10000",
            $slug,
            $current_event_id,
            $locale
        );

        $list = $wpdb->get_results( $qry, ARRAY_A);

        $result = false;
        if(  $wpdb->num_rows > 0 ) {
            $result = true;
        }

        return $result;
    }

    public function get_by_slug( $slug, $locale ) {
        global $wpdb;

        $qry = $wpdb->prepare(
            "SELECT * FROM ".$this->table.
            " WHERE url_slug='%s' AND locale='%s' LIMIT 1",
            $slug,
            $locale
        );

        $list = $wpdb->get_results( $qry, ARRAY_A);

        $result = "";
        if(  $wpdb->num_rows > 0 ) {
            $result = $list[0];
        }

        return $result;
    }


}

endif;

?>