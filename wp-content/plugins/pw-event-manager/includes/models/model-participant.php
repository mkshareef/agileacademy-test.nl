<?php

/**
 * Direct access to the script is not allowed
 */
defined('ABSPATH') or die("No script kiddies please!");

if( !class_exists( 'modelParticipant' ) ):

    class modelParticipant
    {

        public $table = "";
        private $table_event = "";
        private $table_event_date = "";
        private $table_location = "";
        private $table_subscription = "";

        public function __construct() {

            global $pw;
            $this->table = $pw->mysql_table_names['participants'];

            $this->table_event = $pw->mysql_table_names['events'];
            $this->table_event_date = $pw->mysql_table_names['event-date'];
            $this->table_location = $pw->mysql_table_names['locations'];
            $this->table_subscription = $pw->mysql_table_names['subscriptions'];

        }

        public function insert( $data ){

            global $wpdb;

            $wpdb->insert(
                $this->table,
                $data
            );

            return( $wpdb->insert_id );

        }

        public function update( $id, $data ){

            global $wpdb;

            $wpdb->update( $this->table, $data, array( 'id' => $id ), null, null );

        }

        public function update_with_nonce( $id, $nonce, $data ){

            global $wpdb;

            //TODO: Check if nonce is not expired

            $wpdb->update( $this->table, $data, array( 'id' => $id, 'nonce' => $nonce ), null, null );

        }

        public function get_all(){

            global $wpdb;

            $list = $wpdb->get_results("SELECT * FROM ".$this->table." WHERE removed='0' LIMIT 25000", ARRAY_A);

            return $list;
        }

        public function get_all_filtered( $event_id = 0, $event_date_id = 0, $location_id = 0 ) {

            global $wpdb;

            $pagenum = isset( $_REQUEST['paged'] ) ? absint( $_REQUEST['paged'] ) : 1;
            $orderby = isset( $_REQUEST['orderby'] ) ? $_REQUEST['orderby'] : " id ";
            $order = isset( $_REQUEST['order'] ) ? $_REQUEST['order'] : " desc ";
            $startRecord = ($pagenum-1) * 20;


            $qry = "SELECT TP.*, TL.id AS location_id ";
            //$qry .= "TED.id AS eventDateID, TED.start_date, TED.start_time,  TED.stop_time, ";
            //$qry .= "TED.locationID, TL.location_name ";
            $qry .= "FROM ".$this->table." AS TP ";
            $qry .= "INNER JOIN ".$this->table_event_date." AS TED ON TP.eventDateID = TED.id ";
            $qry .= "INNER JOIN ".$this->table_location." AS TL ON TED.locationID = TL.id ";
            $qry .= "INNER JOIN ".$this->table_event." AS TE ON TED.eventID = TE.id ";
            $qry.= "WHERE TP.removed='0' ";
            $qry.= "AND TP.status <> 'concept' ";

            if( $event_id > 0 ) {
                $qry.= "AND TED.eventID=".$event_id." ";
            }

            if( $event_date_id > 0 ) {
                $qry.= "AND TED.id=".$event_date_id." ";
            }

            if( $location_id > 0 ) {
                $qry.= "AND TED.locationID=".$location_id." ";
            }

            $list = $wpdb->get_results( $qry, ARRAY_A);
            $totalCount = count($list);

            $qry.= "order by $orderby $order LIMIT 0, 10000";

            $list = $wpdb->get_results( $qry, ARRAY_A);
            $pageCount = count($list);

            return array("count" => $totalCount, "pageCount" => $pageCount, "list" => $list);


        }

        public function get_all_confirmed( $event_id = 0, $event_date_id = 0, $location_id = 0 ) {

            global $wpdb;

            $qry = "SELECT TP.*, TL.id AS location_id ";
            $qry .= "FROM ".$this->table." AS TP ";
            $qry .= "INNER JOIN ".$this->table_event_date." AS TED ON TP.eventDateID = TED.id ";
            $qry .= "INNER JOIN ".$this->table_location." AS TL ON TED.locationID = TL.id ";
            $qry .= "INNER JOIN ".$this->table_event." AS TE ON TED.eventID = TE.id ";
            $qry.= "WHERE TP.removed='0' ";
            $qry.= "AND TP.status = 'confirmed' ";

            if( $event_id > 0 ) {
                $qry.= "AND TED.eventID=".$event_id." ";
            }

            if( $event_date_id > 0 ) {
                $qry.= "AND TED.id=".$event_date_id." ";
            }

            if( $location_id > 0 ) {
                $qry.= "AND TED.locationID=".$location_id." ";
            }

            $qry.= "LIMIT 25000";
            $list = $wpdb->get_results( $qry, ARRAY_A);

            return $list;

        }


        public function get_all_by_subscription_id( $subscriptionId ){
            global $wpdb;

            $qry = $wpdb->prepare(
                "SELECT * FROM ".$this->table." WHERE subscriptionID='%s' AND removed='0'",
                $subscriptionId
            );

            $list = $wpdb->get_results( $qry, ARRAY_A);

            $result = "";
            if( $wpdb->num_rows > 0 ) {
                $result= $list;
            }

            return $result;
        }

        public function get_by_id( $id ){
            global $wpdb;

            $qry = $wpdb->prepare(
                "SELECT * FROM ".$this->table." WHERE id='%d' LIMIT 1",
                $id
            );

            $list = $wpdb->get_results( $qry, ARRAY_A);

            $result = "";
            if( $wpdb->num_rows > 0 ) {
                $result= $list[0];
            }

            return $result;
        }

        public function get_all_export()
        {
            global $wpdb;


            $ids = 0;
            if(isset($_POST['list'])){
                $ids = implode(",", $_POST['list']);
            }

            $qry = "SELECT * FROM ".$this->table." WHERE removed='0' AND status <> 'concept'";

            if($ids){
                $qry .= " and id in($ids)";
            }
            $qry .= " LIMIT 10000";

            $list = $wpdb->get_results($qry, ARRAY_A);
            return $list;

        }

        public function get_all_not_concept(){

            global $wpdb;

            $pagenum = isset( $_REQUEST['paged'] ) ? absint( $_REQUEST['paged'] ) : 1;
            $orderby = isset( $_REQUEST['orderby'] ) ? $_REQUEST['orderby'] : " id ";
            $order = isset( $_REQUEST['order'] ) ? $_REQUEST['order'] : " desc ";
            $startRecord = ($pagenum-1) * 20;
            $pageRecords = 20;

            $qry = "SELECT e.* FROM ".$this->table." as e WHERE e.removed='0' AND e.status <> 'concept'";
            $search = false;

            if(isset($_REQUEST['s']) && $_REQUEST['s'] != '') {

                $qry = "SELECT e.*, p.company FROM ".$this->table." as e, wp_ev_subscriptions as p  WHERE e.removed='0' AND e.status <> 'concept'";

                $searchWords = explode(' ', $_REQUEST['s']);

                $qry .= " and (";
                foreach ($searchWords as $k => $word) {
                    if($k)
                    {
                        $qry .= " or ";
                    }
                    $qry .= " first_name like '%" . $word . "%' ";
                    $qry .= " or middle_name like '%" . $word . "%' ";
                    $qry .= " or last_name like '%" . $word . "%' ";
                    $qry .= " or email like '%" . $word . "%' ";
                    $qry .= " or phone like '%" . $word . "%' ";
                    $qry .= " or company like '%" . $word . "%' ";
                }
                $qry .= ") ";
                $qry .= " and  e.subscriptionID = p.id  ";

                $startRecord = 0;
                $pageRecords = 10000;
                $search = true;

            }

            $list = $wpdb->get_results($qry, ARRAY_A);
            $totalCount = count($list);

            if($search && $totalCount> 20){
                $startRecord = ($pagenum-1) * 20;
                $pageRecords = 20;
            }

            $qry .= "  order by e.$orderby $order LIMIT $startRecord, $pageRecords ";
            $list = $wpdb->get_results($qry, ARRAY_A);
            $pageCount = count($list);

            return array("count" => $totalCount, "pageCount" => $pageCount, "list" => $list);

        }

        public function get_confirmed_by_subscription( $subscription_id){

            global $wpdb;

            $subscription_id = (int) $subscription_id;

            $list = $wpdb->get_results("SELECT * FROM ".$this->table.
                " WHERE removed='0' AND status = 'confirmed' AND subscriptionID='".$subscription_id."' LIMIT 25000", ARRAY_A);

            $result = "";

            if( $wpdb->num_rows > 0 ) {
                $result= $list;
            }

            return $result;
        }

        //Date should be in mysql format
        public function get_report_period_confirmed($date_from, $date_to){

            global $wpdb;

            //$list = $wpdb->get_results("SELECT * FROM ".$this->table." WHERE removed='0' AND status = 'confirmed' LIMIT 25000", ARRAY_A);

            $qry = "SELECT TP.*, TL.id AS location_id, ";
            $qry .= "TED.id AS eventDateID, TS.company, TE.name, TED.start_date, TED.start_time,  TED.stop_time, ";
            $qry .= "TED.locationID, TL.location_name ";
            $qry .= "FROM ".$this->table." AS TP ";
            $qry .= "INNER JOIN ".$this->table_event_date." AS TED ON TP.eventDateID = TED.id ";
            $qry .= "INNER JOIN ".$this->table_location." AS TL ON TED.locationID = TL.id ";
            $qry .= "INNER JOIN ".$this->table_event." AS TE ON TED.eventID = TE.id ";
            $qry .= "INNER JOIN ".$this->table_subscription." AS TS ON TP.subscriptionID = TS.id ";
            $qry .= "WHERE TP.removed='0' ";
            $qry .= "AND TP.status = 'confirmed' ";
            $qry .= "ORDER BY TED.start_date DESC, TE.name ASC, TED.start_time ASC, TL.location_name ASC, TP.last_name ASC ";
            $qry .= "LIMIT 25000";

            $list = $wpdb->get_results( $qry, ARRAY_A);

            $result = "";
            if( $wpdb->num_rows > 0 ) {

                foreach( $list as $key=>$row ) {
                    $evt_date = $row['start_date'];

                    $include = true;
                    if( $date_from <> '' ) {
                        if( strtotime( $evt_date ) < strtotime( $date_from ) ) {
                            $include = false;
                        }
                    }

                    if( $date_to <> '' ) {
                        if( strtotime( $evt_date ) > strtotime( $date_to ) ) {
                            $include = false;
                        }
                    }

                    if( $include == true ) {
                        $result[] = $row;
                    }

                }

            }

            return $result;
        }

        public function remove_from_subscription( $participantId, $subscriptionId, $nonce ){

            global $wpdb;

            //TODO: Check if nonce is not expired

            $participantId = (int) $participantId;
            $subscriptionId = (int) $subscriptionId;

            $data = array('removed'=> '1');

            $wpdb->update( $this->table, $data, array( 'id' => $participantId, 'subscriptionID' => $subscriptionId,
                'nonce' => $nonce ), null, null );

        }

        public function get_event_confirmed_participcant_count($eventId, $eventDateID){
            global $wpdb;

            $result = $wpdb->get_results("SELECT count(*) as COUNT FROM ".$this->table." WHERE eventID='$eventId' AND eventDateID ='$eventDateID' AND status='confirmed'", ARRAY_A);

            return $result;
        }

    }

endif;

?>