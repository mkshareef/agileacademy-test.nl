<?php

/**
 * Direct access to the script is not allowed
 */
defined('ABSPATH') or die("No script kiddies please!");

if( !class_exists( 'modelActionCodeAssignment' ) ):

    class modelActionCodeAssignment
    {

        public $table = "";

        public function __construct() {

            global $pw;
            $this->table = $pw->mysql_table_names['action-code-assignment'];

        }

        public function insert( $action_code_id, $event_id ){

            global $wpdb;

            $wpdb->insert(
                $this->table,
                array( 'actionCodeID'=>$action_code_id, 'eventID'=>$event_id )
            );

            return( $wpdb->insert_id );

        }

        public function update( $id, $data ){

            global $wpdb;

            $wpdb->update( $this->table, $data, array( 'id' => $id ), null, null );

        }

        public function set_from_array( $action_code_id, $event_ids ) {

            $action_code_id = (int) $action_code_id;
            $del_list = array();
            $add_list = array();

            $db_list = $this->get_all_by_id( $action_code_id );

            if( ! empty( $event_ids ) && $action_code_id > 0 ) {

                if( ! empty($db_list) ) {

                    //Ids in the db that are not in the $event_ids should be removed
                    foreach( $db_list as $key=>$in_db ) {

                        $found = false;
                        foreach( $event_ids as $k=>$id ) {
                            if( (int) $in_db['eventID'] == (int) $id ) {
                                $found = true;
                            }
                        }
                        if( $found === false ) {
                            $del_list[] = $in_db['eventID'];
                        }

                    }

                    //Ids in the $event_ids that are not in the  db should be added
                    foreach( $event_ids as $k=>$id ) {
                        $found = false;
                        foreach( $db_list as $key=>$in_db ) {

                            if( (int) $in_db['eventID'] == (int) $id ) {
                                $found = true;
                            }

                        }
                        if( $found === false ) {
                            $add_list[] = $id;
                        }
                    }

                } else {
                    foreach( $event_ids as $k=>$id ) {
                        $add_list[] = $id;
                    }
                }

                //Process the ids to be deleted
                foreach($del_list as $k=>$id ) {
                    $this->delete($action_code_id, $id);
                }

                //Process the ids to be added
                foreach($add_list as $k=>$id ) {
                    $this->insert($action_code_id, $id);
                }

            } else {
                foreach( $db_list as $k=>$rec ) {
                    $this->delete_by_id($rec['id']);
                }
            }

        }

        public function get_all_by_id( $actionCodeID ){

            global $wpdb;

            $qry = $wpdb->prepare(
                "SELECT * FROM ".$this->table." WHERE actionCodeID='%d'",
                $actionCodeID
            );

            $list = $wpdb->get_results($qry, ARRAY_A);

            return $list;
        }

        public function delete( $action_code_id, $event_id ) {

            global $wpdb;
            $wpdb->delete( $this->table, array( 'actionCodeID'=>$action_code_id, 'eventID'=>$event_id, array( '%d', '%d' ) ) );

        }

        public function delete_by_id( $id ) {

            global $wpdb;
            $wpdb->delete( $this->table, array( 'id'=>$id ), array( '%d' ) );

        }

    }

endif;

?>