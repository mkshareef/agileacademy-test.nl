jQuery(document).ready(function( $ ) {

    function getUrlVars()
    {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');

        if( hashes.length ) {
            for(var i = 0; i < hashes.length; i++)
            {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
        }

        return vars;
    }

    var availableTrainings = [];

    function ajax_populate_event_titles() {

        var lang = getUrlVars()["lang"];

        if( typeof lang == "undefined" || lang == "" ) {
            lang='nl';
        }

        $id = $('#search-page-id').val();

        if( $('#search-page-id').length ) {
            $.ajax({
                url: ajax_object.ajax_url,
                data: {
                    'action':'fe_ajax_get_event_names',
                    'page-id' : $id
                },
                success:function(results) {

                    var data = $.parseJSON( results );

                    availableTrainings = []; // remove old options

                    if( data.length > 0 ) {
                        $.each(data, function(id, value) {
                            availableTrainings.push( value['name'] );
                        });
                    }

                    $( "#search_keyword-input" ).autocomplete({
                        source: availableTrainings,
                        appendTo: '#event-cat-autocomplete'
                    });

                },
                error: function(errorThrown){
                    console.log('Error loading events');
                }
            });
        }

    }

    //---------------------------------------------
    //                  SCROLL TO ANCHOR
    //---------------------------------------------
    function scroll_to_anchor(){
        var jumpTo = getUrlVars()["st"];
        var compensateX = 150;

        if( typeof jumpTo !== "undefined" ) {
            var aTag = $("a[name='"+ jumpTo +"']");
            $('html,body').animate({scrollTop: aTag.offset().top - compensateX},'slow');
        }
    }

    //---------------------------------------------
    //                  Initialize
    //---------------------------------------------

    ajax_populate_event_titles();

    scroll_to_anchor();

    $('.show-event-planning').readmore({
        speed: 500,
        collapsedHeight: 250,
        moreLink: '<div class="show-event-read-more"><a href="#">&#x25BC;</a></div>',
        lessLink: '<div class="show-event-read-more"><a href="#">&#x25B2;</a></div>',
        embedCSS: false
    });
});
