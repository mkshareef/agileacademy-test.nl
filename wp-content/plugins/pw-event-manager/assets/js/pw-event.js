
var eventChanged = false;

jQuery(document).ready(function( $ ) {

    //---------------------------------------------
    //          Track Changes
    //---------------------------------------------

    $(document).on('change','input',function(){
        eventChanged = true;
        console.log('set changed');
    });

    $(document).on('change','textarea',function(){
        eventChanged = true;
        console.log('set changed');
    });

    $(document).on('change','select',function(){
        eventChanged = true;
        console.log('set changed');
    });

    //---------------------------------------------
    //             Handle tab switching
    //---------------------------------------------

    $('.pw-ui-tab-enabled').click(function( evt ){

        if( eventChanged == true ) {

            var go_url = $(this).attr('href');

            if( go_url != "" ) {
                go_on_dialog('open', go_url);

                evt.preventDefault();
                evt.stopPropagation();
            }
        }

    });

    //---------------------------------------------
    //              Go on Dialog
    //---------------------------------------------

    function go_on_dialog( action, to_url ) {

        if( $( '#pw-confirm-save' ).length > 0 ) {

            if( action == 'open' ) {

                //Show modal dialog
                $( '#pw-confirm-save' ).show();

                //Attach event handlers
                $( '#pw-confirm-save #ok-btn' ).on( 'click', function(evt) {

                    go_on_dialog( 'close' );

                    //Trigger a submit
                    $("input.button-primary[type=submit]").click();
                });

                $( '#pw-confirm-save #cancel-btn' ).on( 'click', function(evt) {
                    go_on_dialog( 'close' );
                    window.location = to_url;
                });

            } else {
                $( '#pw-confirm-save' ).hide();
                $( '#pw-confirm-save #ok-btn' ).off('click');
                $( '#pw-confirm-save #cancel-btn' ).off('click');
            }

        } else {
            window.location = to_url;
        }
    }

    $('[name=formEventEdit]').submit(function () {
        if($("#selected_know_how_ids").val() == '') {
            alert("Please select atleast one know how");
            return false;
        }
    });

    //---------------------------------------------
    //                  Event, tab Description
    //---------------------------------------------

    $(document).on('change','input[name="locale"]',function(){
        $('#selected_trainer_ids').val("");
        ajax_populate_trainer_select();
        ajax_populate_selected_trainers();
        ajax_populate_event_group_checkbox();

        ajax_populate_know_how_select();
        ajax_populate_selected_know_how();
    });

    init_custom_know_how();

    function init_custom_know_how() {
        $("#custom-know-how").keydown(function (e) {
                //space, backspace, delete, escape, enter
            if ($.inArray(e.keyCode, [32, 46, 8, 27, 13]) !== -1 ||
                    //Ctrl+A, Ctrl+C, Ctrl+X
                (e.ctrlKey === true && $.inArray(e.keyCode, [65, 67, 88])) ||
                    //home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39) ||
                    //alphabets
                (e.keyCode >= 65 && e.keyCode <= 90) ||
                    //numbers only
                (e.shiftKey === false && (e.keyCode >= 48 && e.keyCode <= 57)) ||
                    //numpad numbers
                (e.keyCode >= 96 && e.keyCode <= 105))
            {
                //allow the above
                return true;
            }
            return false;
        });
    }

    function ajax_populate_know_how_select() {

        var locale = "NL";

        var selected = $("input[type='radio'][name='locale']:checked");

        if (selected.length > 0) {
            locale = selected.val();
        }

        $.ajax({
            url: ajax_object.ajax_url,
            data: {
                'action':'ajax_get_know_how_select_options',
                'locale': locale
            },
            success:function(results) {

                var list = $('#select-add-know-how');
                var data = $.parseJSON( results );

                list.empty(); // remove old options

                $.each(data, function(value,key) {
                    list.append($("<option></option>").attr("value", value).text(key));
                });

            },
            error: function(errorThrown){
                $('#select-add-know-how').html('<option value="">- Failed to load -<option>');
            }
        });
    }

    function ajax_populate_trainer_select() {

        var locale = "NL";

        var selected = $("input[type='radio'][name='locale']:checked");

        if (selected.length > 0) {
            locale = selected.val();
        }

        $.ajax({
            url: ajax_object.ajax_url,
            data: {
                'action':'ajax_get_trainers_select_options',
                'locale': locale
            },
            success:function(results) {

                var list = $('#select-add-trainer');
                var data = $.parseJSON( results );

                list.empty(); // remove old options

                $.each(data, function(value,key) {
                    list.append($("<option></option>").attr("value", key['id']).text(key['name']));

                });

            },
            error: function(errorThrown){
                $('#select-add-trainer').html('<option value="">- Failed to load -<option>');
            }
        });
    }

    //Remove double entries from arrays
    function unique(array){
        return $.grep(array,function(el,index){
            return index == $.inArray(el,array);
        });
    }

    function ajax_populate_selected_know_how() {

        var ids = $('#selected_know_how_ids').val();

        $.ajax({
            url: ajax_object.ajax_url,
            data: {
                'action':'ajax_get_selected_know_how_html',
                'ids': ids
            },
            success:function(results) {
                var data = results;
                $('#selected_know_how_list').html( data );
            },
            error: function(errorThrown){
                $('#selected_know_how_list').html('- Laden mislukt -');
            }
        });

    }

    function ajax_populate_selected_trainers() {

        var ids = $('#selected_trainer_ids').val();

        $.ajax({
            url: ajax_object.ajax_url,
            data: {
                'action':'ajax_get_selected_trainers_html',
                'ids': ids
            },
            success:function(results) {
                var data = results;
                $('#selected_trainers_list').html( data );
            },
            error: function(errorThrown){
                $('#selected_trainers_list').html('- Laden mislukt -');
            }
        });

    }

    function ajax_populate_event_group_checkbox() {

        var locale = "NL";

        var selected = $("input[type='radio'][name='locale']:checked");

        if (selected.length > 0) {
            locale = selected.val();
        }

        var ids = $('#selected_group_ids').val();

        $.ajax({
            url: ajax_object.ajax_url,
            data: {
                'action':'ajax_populate_event_group_checkbox',
                'locale': locale,
                'ids': ids
            },
            success:function(results) {
                var data = results;
                $('#event_group_checkbox').html( data );
            },
            error: function(errorThrown){
                $('#event_group_checkbox').html('- Laden mislukt -');
            }
        });

    }

    //Add trainer to the list on + button
    $('#btn-add-trainer').on('click', function(evt) {

        var id   = $( "#select-add-trainer" ).val();
        var name = $( "#select-add-trainer option:selected" ).text();

        //Add id to the list with selected trainers
        if (id > 0) {

            ids = $('#selected_trainer_ids').val();

            if( ids == "" ) {
                ids = id;
            } else {
                ids = ids + ',' + id;
            }

            idsArray = ids.split(',');
            idsArray = unique(idsArray);    //Unique values
            $('#selected_trainer_ids').val( idsArray.join(',') );

        }

        $("#select-add-trainer").val($("#select-add-trainer option:first").val());

        //Ajax call to get html for selected trainers
        ajax_populate_selected_trainers();

        evt.preventDefault();
        evt.stopPropagation();
    });

    $('#btn-add-know-how').on('click', function(evt) {

        var id   = $( "#select-add-know-how" ).val();
        var name = $( "#select-add-know-how option:selected" ).text();

        //Add id to the list with selected trainers
        if (id != '') {
            ids = $('#selected_know_how_ids').val();
            ids = ( ids == "" ) ? id : ids + ',' + id;
            idsArray = ids.split(',');
            idsArray = unique(idsArray);    //Unique values
            $('#selected_know_how_ids').val( idsArray.join(',') );
        }

        $("#select-add-know-how").val($("#select-add-know-how option:first").val());

        //Ajax call to get html for selected trainers
        ajax_populate_selected_know_how();

        evt.preventDefault();
        evt.stopPropagation();
    });

    $('#btn-add-custom-know-how').on('click', function(evt) {

        var name = $( "#custom-know-how" ).val();
        var id = name.replace(/\ /g, '-');

        //Add id to the list with selected trainers
        if (id != '') {
            ids = $('#selected_know_how_ids').val();
            ids = ( ids == "" ) ? id : ids + ',' + id;
            idsArray = ids.split(',');
            idsArray = unique(idsArray);    //Unique values
            $('#selected_know_how_ids').val( idsArray.join(',') );
        }

        $("#custom-know-how").val('');

        //Ajax call to get html for selected trainers
        ajax_populate_selected_know_how();

        evt.preventDefault();
        evt.stopPropagation();
    });

    $(document).on('click', '.btn-delete-trainer', function() {
        var remove_id = $(this).attr('trainer-id');

        if( remove_id != "" ) {
            ids = $('#selected_trainer_ids').val();
            idsArray = ids.split(',');

            idsArray = jQuery.grep(idsArray, function(value) {
                return value != remove_id;
            });

            $('#selected_trainer_ids').val( idsArray.join(',') );
        }

        ajax_populate_selected_trainers();

    });

    $(document).on('click', '.btn-delete-know-how', function() {
        var remove_id = $(this).attr('know-how-id');

        if( remove_id != "" ) {
            ids = $('#selected_know_how_ids').val();
            idsArray = ids.split(',');

            idsArray = jQuery.grep(idsArray, function(value) {
                return value != remove_id;
            });

            $('#selected_know_how_ids').val( idsArray.join(',') );
        }

        ajax_populate_selected_know_how();

/*        $('#remove-know-how-'+ remove_id).remove();

        ids = $('#selected_know_how_ids').val();
        idsArray = ids.split(',');
        aId = idsArray.indexOf(remove_id);
        idsArray.splice(aId, 1);
        idsArray = unique(idsArray);
        $('#selected_know_how_ids').val( idsArray.join(',') );*/

    });

    //---------------------------------------------
    //      Handle the action code event tree
    //---------------------------------------------

    $('#checkboxes-action-code-nl').bonsai({
        expandAll: true,
        checkboxes: true, // depends on jquery.qubit plugin
        handleDuplicateCheckboxes: false // optional
    });

    $('#checkboxes-action-code-de').bonsai({
        expandAll: true,
        checkboxes: true, // depends on jquery.qubit plugin
        handleDuplicateCheckboxes: false // optional
    });

    $('#checkboxes-action-code-en').bonsai({
        expandAll: true,
        checkboxes: true, // depends on jquery.qubit plugin
        handleDuplicateCheckboxes: false // optional
    });

    function disable_action_code_event_list() {

        if(  $("input:radio[name=code_scope]:checked").val() == 'all') {

            if( $( '#checkboxes-action-code-nl').length > 0 ) {
                $('#checkboxes-action-code-nl input[type=checkbox]').attr('disabled','true');
                $('#checkboxes-action-code-nl').addClass('disabled');
                var bonsai_nl = $('#checkboxes-action-code-nl').data('bonsai');
                bonsai_nl.collapseAll();
            }
            if( $( '#checkboxes-action-code-de').length > 0 ) {
                $('#checkboxes-action-code-de input[type=checkbox]').attr('disabled','true');
                $('#checkboxes-action-code-de').addClass('disabled');
                var bonsai_de = $('#checkboxes-action-code-de').data('bonsai');
                bonsai_de.collapseAll();
            }
            if( $( '#checkboxes-action-code-en').length > 0 ) {
                $('#checkboxes-action-code-en input[type=checkbox]').attr('disabled','true');
                $('#checkboxes-action-code-en').addClass('disabled');
                var bonsai_en = $('#checkboxes-action-code-en').data('bonsai');
                bonsai_en.collapseAll();
            }

            $('#label-action-code-events').addClass('disabled');

        }

    }

    $('input[name=code_scope]:radio').on('click', function() {

        if( $(this).val() == "all" ) {
            console.log( 'disabled' );
            disable_action_code_event_list();

        } else {
            console.log( 'enabled' );
            if( $( '#checkboxes-action-code-nl').length > 0 ) {
                $('#checkboxes-action-code-nl input[type=checkbox]').removeAttr("disabled");
                $('#checkboxes-action-code-nl').removeClass('disabled');
                var bonsai_nl = $('#checkboxes-action-code-nl').data('bonsai');
                bonsai_nl.expandAll();
            }
            if( $( '#checkboxes-action-code-de').length > 0 ) {
                $('#checkboxes-action-code-de input[type=checkbox]').removeAttr("disabled");
                $('#checkboxes-action-code-de').removeClass('disabled');
                var bonsai_de = $('#checkboxes-action-code-de').data('bonsai');
                bonsai_de.expandAll();
            }
            if( $( '#checkboxes-action-code-en').length > 0 ) {
                $('#checkboxes-action-code-en input[type=checkbox]').removeAttr("disabled");
                $('#checkboxes-action-code-en').removeClass('disabled');
                var bonsai_en = $('#checkboxes-action-code-en').data('bonsai');
                bonsai_en.expandAll();
            }

            $('#label-action-code-events').removeClass('disabled');

        }
    })

    //---------------------------------------------
    //             Filter subscriptions table
    //---------------------------------------------

    function do_filter_subscriptions() {
        var searchVal = $('#search_sub_val');
        var eventSelected = $('#filter_sub_event').find("option:selected");
        var dateSelected = $('#filter_sub_date').find("option:selected");
        var locationSelected = $('#filter_sub_location').find("option:selected");

        var qry = '?page=pw-events-subscriptions&s='+ searchVal.val() +'&fe='+ eventSelected.val() +'&fed=' + dateSelected.val() +'&fl=' + locationSelected.val() +'';

        window.location = qry;
    }

    $('#filter_sub_event').add( '#filter_sub_date' ).add( '#filter_sub_location').on('change', function() {
        do_filter_subscriptions();
    });

    $('#search_sub_val').on('keyup', function() {
        do_filter_subscriptions();
    });

    //---------------------------------------------
    //             Filter subscriber table
    //---------------------------------------------

    function do_filter_participants() {
        var searchVal = $('#search_val');
        var eventSelected = $('#filter_event').find("option:selected");
        var dateSelected = $('#filter_date').find("option:selected");
        var locationSelected = $('#filter_location').find("option:selected");

        var qry = '?page=pw-events-subscribers&s='+ searchVal.val() +'&fe='+ eventSelected.val() +'&fed=' + dateSelected.val() +'&fl=' + locationSelected.val() +'';

        window.location = qry;
    }

    $('#filter_event').add( '#filter_date' ).add( '#filter_location').on('change', function() {
        do_filter_participants();
    });

    $('#search_val').on('keyup', function() {
        do_filter_participants();
    });

    //---------------------------------------------
    //             Filter invoice table
    //---------------------------------------------

    function do_filter_invoices() {
        var searchVal = $('#search_inv_val');
        var dateSelected = $('#filter_inv_date').find("option:selected");

        var qry = '?page=pw-events-invoices&s='+ searchVal.val() +'&fd=' + dateSelected.val() +'';

        window.location = qry;
    }

    $('#filter_inv_date').on('change', function() {
        do_filter_invoices();
    });

    $('#search_inv_val').on('keyup', function() {
        do_filter_invoices();
    });

    //---------------------------------------------
    //             Get from Query String
    //---------------------------------------------
    function get_from_query_string(name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.search);
        if (results == null)
            return "";
        else
            return decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    //---------------------------------------------
    //                  Initialize
    //---------------------------------------------
    function initialize_event_form() {
        ajax_populate_trainer_select();
        ajax_populate_selected_trainers();
        ajax_populate_event_group_checkbox();
        disable_action_code_event_list();

        ajax_populate_know_how_select();
        ajax_populate_selected_know_how();

    }
    initialize_event_form();

});